$(document).ready(function(){
   validaciontabla(); 
});

function bloquear(obj, tabla){
    if(obj.src.indexOf("cerrado") > -1){
        new PNotify({
                    title: 'Error',
                    text:'No esta autorizado para desbloquearlo',
                    type: 'error'
                    });
                    return;
    }
    var id = $(obj).attr('rel');
    $.ajax({
            url:'index.php?r=/control/bloquear',
            type:'GET',
            data:{'recepcion':id,'tabla':tabla},
            success:function (resp){
                obj.src= resp;
            },
        });
}

function recepcion(obj){
    var id = $(obj).html();
    var rel = $(obj).attr('rel');
    $.ajax({
            url:'index.php?r=/control/idrecepcion',
            type:'GET',
            data: {'id':id},
            success:function (resp){
                if(resp=='.'){
                    new PNotify({
                    title: 'Error',
                    text:'Id revisión no existe',
                    type: 'error'
                    });
                    resp = ' _ ';
                }
                var r = resp.split('_');
                $('#peso'+rel).html(r[0]);
                $('#especie'+rel).html (r[1]);
            },
        });
}
function horaActualTolva(obj){
    var id = $(obj).attr('rel');
    $.ajax({
            url:'index.php?r=/control/horaActual',
            type:'GET',
            success:function (resp){
                var mat = resp.split(':');
                $('#horatolva'+id).val(mat[0]);$('#minutostolva'+id).val(mat[1]);
            },
        });
    }
function horaActual(obj,pest){
    var id = $(obj).attr('rel');
    $.ajax({
            url:'index.php?r=/control/horaActual',
            type:'GET',
            success:function (resp){
                var mat = resp.split(':');
                switch(pest){
                    case 1:
                        $('#hora'+id).val(mat[0]);$('#minutos'+id).val(mat[1]);
                        break;
                    case 2:
                        $('#horacocc'+id).val(mat[0]);$('#minutoscocc'+id).val(mat[1]);
                        break;
                    case 3:
                        $('#horadeca'+id).val(mat[0]);$('#minutosdeca'+id).val(mat[1]);
                        break;
                    case 4:
                        $('#horaprensa'+id).val(mat[0]);$('#minutosprensa'+id).val(mat[1]);
                        break;
                    case 5:
                        $('#horarefi'+id).val(mat[0]);$('#minutosrefi'+id).val(mat[1]);
                        break;
                    case 6:
                        $('#horatrata'+id).val(mat[0]);$('#minutostrata'+id).val(mat[1]);
                        break;
                    case 7:
                        $('#horaprese'+id).val(mat[0]);$('#minutosprese'+id).val(mat[1]);
                        break;
                    case 8:
                        $('#horasecado'+id).val(mat[0]);$('#minutossecado'+id).val(mat[1]);
                        break;
                    case 9:
                        $('#horahari'+id).val(mat[0]);$('#minutoshari'+id).val(mat[1]);
                        break;
                }
               $(obj).parent().find('input').each(function(index){$(this).attr('camb','si');});
            },
        });
};
function agregar(ind,fecha){
    var t = '';
     $.ajax({
        url:'index.php?r=/control/addFila&fecha='+fecha+'&indice='+ind,
        type:'GET',
        success:function (resp){
            switch(ind){
                case 1:
                    $("#tabla_inicio").find("#totales_recepcion").before(resp);
                    $('.fechaproduccion').datepicker();
                    $('#tabla_inicio').editableTableWidget();
                    break;
                case 2:
                    $(resp).appendTo("#tabla_cocina tbody"); 
                    $('#tabla_cocina').editableTableWidget().numericInputExample();
                    break;
                case 3:
                    $(resp).appendTo("#tabla_prensas tbody"); 
                    $('#tabla_prensas').editableTableWidget().numericInputExample();
                    break;
                case 4:
                    $(resp).appendTo("#tabla_decantasion tbody"); 
                    $('#tabla_decantasion').editableTableWidget().numericInputExample();
                    break;
                case 5:
                    $(resp).appendTo("#tabla_refinacion tbody"); 
                    $('#tabla_refinacion').editableTableWidget().numericInputExample();
                    break;
                case 6:
                    $(resp).appendTo("#tabla_tratamiento tbody"); 
                    $('#tabla_tratamiento').editableTableWidget().numericInputExample();
                    break;
                case 7:
                    $(resp).appendTo("#tabla_presecado tbody"); 
                    $('#tabla_presecado').editableTableWidget().numericInputExample();
                    break;
                case 8:
                    $(resp).appendTo("#tabla_secado tbody"); 
                    $('#tabla_secado').editableTableWidget().numericInputExample();
                    break;
                case 9:
                    $(resp).appendTo("#tabla_fin tbody"); 
                    $('#tabla_fin').editableTableWidget();
                    sumaSaco();
                    break;
            }
            validaciontabla()
        },
        error:function(e){
            alert("Se ha Producido un error al obtener los datos");
        },
    });
}

function getEspecies(id,val){
    if(val!='0'){
    var hora = $('#hora'+id).val();
     $.ajax({
        url:'index.php?r=/control/getEspecie&hora='+hora+'&fecha='+Fecha+'&sec='+val,
        type:'GET',
        success:function (resp){    
            var r = resp.split('_');
            r[2]= Number(r[2])/1000;
            var desc= $('#desc'+id).html();
            desc = desc.replace(DECIMAL,'.');
            $('#idrecep'+id).html(r[0]);
            $('#especie'+id).html(r[1]);
            $('#peso'+id).html(formato_numero(Number(r[2]),3,DECIMAL,''));
            var pesoneto=Number(r[2])-Number(desc);
            $('#pesoneto'+id).html(formato_numero(pesoneto,3,DECIMAL,''));
            if(r[3]!='no'){
                new PNotify({
                    title: 'Advertencia',
                    text:'La secuencia '+r[3]+', Ya ha sido registrada',
                    });
            }
            sumPeso();
            sumNeto();
    },
    error:function(e){
            alert("Se ha Producido un error al obtener los datos");
        },
    });
    }
}
function descuento(obj){
    var id = $(obj).attr('rel');
    var peso = $('#peso'+id).html();
    var tarra = $('#tarra'+id).html();
    var desc = $('#desc'+id).html() ? $('#desc'+id).html() : '0';
    peso =peso.replace(DECIMAL,'.');
    tarra =tarra.replace(DECIMAL,'.');
    desc =Number(desc.replace(DECIMAL,'.'));
    var pesoneto = Number(peso)-Number(tarra);
    pesoneto = pesoneto - ((pesoneto*desc)/100);
    $('#pesoneto'+id).html(formato_numero(pesoneto,3,DECIMAL,''));
    if(pesoneto<0){
        $('#pesoneto'+id).css('background-color','#FE2E2E');
    }else{
        $('#pesoneto'+id).css('background-color','');
    }
    $('#pesonetoton'+id).html(formato_numero(pesoneto/1000,3,DECIMAL,''));
    sumTarra();
    sumPeso();
    sumNeto();
}
/* Guardar cambios de recepción*/
function guardar(obj){
    var id = $(obj).attr('rel');
    var tolva = $('#tolva'+id).val() ? $('#tolva'+id).val() :'nulo';
    var especie = $('#especie'+id).val() ? $('#especie'+id).val() : 'nulo';
    var tbvn = $('#tbvn'+id).html()? $('#tbvn'+id).html() : 'nulo';
    var proveedor = $('#prov'+id).val() ? $('#prov'+id).val() : 'nulo';
    var placa = $('#placa'+id).html() ? $('#placa'+id).html() : 'nulo';
    var chofer = $('#chof'+id).html() ? $('#chof'+id).html() : 'nulo';
    var secuencia = $('#secu'+id).html() ? $('#secu'+id).html() : 'nulo';
    var h = $('#hora'+id).val() ? $('#hora'+id).val() : '00';
    var m = $('#minutos'+id).val() ? $('#minutos'+id).val() : '00';
    var hora = h+':'+m;
    var ht = $('#horatolva'+id).val() ? $('#horatolva'+id).val() : '00';
    var mt = $('#minutostolva'+id).val() ? $('#minutostolva'+id).val() : '00';
    var horatolva = ht+':'+mt;
    var tarra = $('#tarra'+id).html() ? $('#tarra'+id).html() : 'nulo';
    var peso = $('#peso'+id).html() ? $('#peso'+id).html() : 'nulo';
    var desc = $('#desc'+id).html() ? $('#desc'+id).html() : 'nulo';
    var pesoneto = $('#pesonetoton'+id).html() ? $('#pesonetoton'+id).html() : 'nulo';
    var fechaproduccion = $('#fechaproduccion'+id).val() ? $('#fechaproduccion'+id).val() :'nulo';
    if(tolva=='nulo'){
        new PNotify({
        title: 'Datos incompletos',
        text:'<strong>Tolva</strong> es obligatoria'
        });
        return;
    }else if(especie=="nulo"){
        new PNotify({
        title: 'Datos incompletos',
        text:'<strong>Especie</strong> es obligatoria'
        });
        return;
    }else if(proveedor=="nulo"){
        new PNotify({
        title: 'Datos incompletos',
        text:'<strong>Proveedor</strong> es obligatorio'
        });
        return;
    }
    $.ajax({
            url:'index.php?r=/control/actualizarvalores',
            type:'GET',
            data:{'id':id,'especie':especie,'tolva':tolva,'tbvn':tbvn,'prov':proveedor,'placa':placa,'chofer':chofer,'hora':hora,'peso':peso,'desc':desc,'pesoneto':pesoneto,'horatolva':horatolva,secuencia:secuencia,tarra:tarra,fechaproduccion:fechaproduccion},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input, select').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
}
/* Guardar cambios de Coccion*/
function guardarcocinas(obj){
    var rpms = Array();var ejes = Array();var chaqs = Array();var temperaturas = Array();
    var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        rpms.push($('#rpm'+i+id).html() ? $('#rpm'+i+id).html():'nulo');
        ejes.push($('#presioneje'+i+id).html() ? $('#presioneje'+i+id).html():'nulo');
        chaqs.push($('#presionchaqueta'+i+id).html() ? $('#presionchaqueta'+i+id).html():'nulo');
        temperaturas.push($('#temperatura'+i+id).html() ? $('#temperatura'+i+id).html():'nulo');
    }
    var h = $('#horacocc'+id).val() ? $('#horacocc'+id).val() : '00';
    var m = $('#minutoscocc'+id).val() ? $('#minutoscocc'+id).val() : '00';
    var hora = h+':'+m;
    $.ajax({
            url:'index.php?r=/control/actualizarvalorescocina',
            type:'GET',
            data:{'rpms':rpms,'ejes':ejes,'chaqs':chaqs,'temperaturas':temperaturas,'total':total,'revision':id,'fecha':Fecha,'hora':hora},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
}
/* Guardar cambios de Prensado*/
function guardarprensado(obj){
    var Amps = Array();var Hums = Array();
    var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        Amps.push($('#preAmp'+i+id).html() ? $('#preAmp'+i+id).html():'nulo');
        Hums.push($('#preHum'+i+id).html() ? $('#preHum'+i+id).html():'nulo');
    }
    var LicH = $('#preLicH'+id).html() ? $('#preLicH'+id).html(): 'nulo';
    var LicG = $('#preLicG'+id).html() ? $('#preLicG'+id).html(): 'nulo';
    var h = $('#horaprensa'+id).val() ? $('#horaprensa'+id).val() : '00';
    var m = $('#minutosprensa'+id).val() ? $('#minutosprensa'+id).val() : '00';
    var licCon = $('#licCon'+id).html() ? $('#licCon'+id).html(): 0;
    
    var hora = h+':'+m;
    $.ajax({
            url:'index.php?r=/control/actualizarprensado',
            type:'GET',
            data:{'amps':Amps,'hums':Hums,'lich':LicH,'licg':LicG,'total':total,'revision':id,'fecha':Fecha,'hora':hora,concentrado:licCon},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
}

function guardardecantacion(obj){
    var Detems= Array();
    var Dehums = Array();
    var Desols = Array();
    var Degras = Array();
    var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        Detems.push($('#Dtem'+i+id).html() ? $('#Dtem'+i+id).html():'nulo');
        Dehums.push($('#Dhum'+i+id).html() ? $('#Dhum'+i+id).html():'nulo');
        Desols.push($('#Dsol'+i+id).html() ? $('#Dsol'+i+id).html():'nulo');
        Degras.push($('#Dgra'+i+id).html() ? $('#Dgra'+i+id).html():'nulo');
    }
    var h = $('#horadeca'+id).val() ? $('#horadeca'+id).val() : '00';
    var m = $('#minutosdeca'+id).val() ? $('#minutosdeca'+id).val() : '00';
    var hora = h+':'+m;
     $.ajax({
            url:'index.php?r=/control/actualizarvaloresdecantacion',
            type:'GET',
            data:{'Detems':Detems,'Dehums':Dehums,'Desols':Desols,'Degras':Degras,'total':total,'revision':id,'fecha':Fecha,'hora':hora},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            },
        });


}

function guardarrefinacion(obj){
    var Reftemp = Array();
    var Refhum = Array();
    var Refsol = Array();
    var Refaci = Array();
    var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        Reftemp.push($('#retem'+i+id).html() ? $('#retem'+i+id).html():'nulo');
        Refhum.push($('#rehum'+i+id).html() ? $('#rehum'+i+id).html():'nulo');
        Refsol.push($('#resol'+i+id).html() ? $('#resol'+i+id).html():'nulo');
        Refaci.push($('#reAcid'+i+id).html() ? $('#reAcid'+i+id).html():'nulo');
    }
    var h = $('#horarefi'+id).val() ? $('#horarefi'+id).val() : '00';
    var m = $('#minutosrefi'+id).val() ? $('#minutosrefi'+id).val() : '00';
    var hora = h+':'+m;
     $.ajax({
            url:'index.php?r=/control/actualizarvaloresrefinacion',
            type:'GET',
            data:{'Reftemp':Reftemp,'Refhum':Refhum,'Refsol':Refsol,'Refaci':Refaci,'total':total,'revision':id,'fecha':Fecha,'hora':hora},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });

}
function guardartratamiento(obj){
        var Tratemp = Array();
        var Trasoli = Array();
        var Tratemp2 = Array();
        var Trapre = Array();
        var Tratemp3 =  Array();
        var Tratemp4 =  Array();
        var Tratemp5 =  Array();
        var Trasoli2  =  Array();
        var AguaArr  =  Array();
        var ConcentracionArr  =  Array();
        var id = $(obj).attr('rel');
        var total = parseInt($(obj).attr('total'));
        for(var i = 1; i<=total;i++){
        Tratemp.push($('#tratemp'+i+id).html() ? $('#tratemp'+i+id).html():'nulo');
        Trasoli.push($('#trasoli'+i+id).html() ? $('#trasoli'+i+id).html():'nulo');
        Tratemp2.push($('#tratemp2'+i+id).html() ? $('#tratemp2'+i+id).html():'nulo');
        Trapre.push($('#trapre'+i+id).html() ? $('#trapre'+i+id).html():'nulo');
        Tratemp3.push($('#tratemp3'+i+id).html() ? $('#tratemp3'+i+id).html():'nulo');
        Tratemp4.push($('#tratemp4'+i+id).html() ? $('#tratemp4'+i+id).html():'nulo');
        Tratemp5.push($('#tratemp5'+i+id).html() ? $('#tratemp5'+i+id).html():'nulo');
        Trasoli2.push($('#trasoli2'+i+id).html() ? $('#trasoli2'+i+id).html():'nulo');
        AguaArr.push($('#aguacola'+i+id).html() ? $('#aguacola'+i+id).html():0);
        ConcentracionArr.push($('#concentrado'+i+id).html() ? $('#concentrado'+i+id).html():0);
    }
    var h = $('#horatrata'+id).val() ? $('#horatrata'+id).val() : '00';
    var m = $('#minutostrata'+id).val() ? $('#minutostrata'+id).val() : '00';
    var hora = h+':'+m;

     $.ajax({
            url:'index.php?r=/control/actualizarvalorestratamiento',
            type:'GET',
            data:{
                'Tratemp':Tratemp,
                'Trasoli':Trasoli,
                'Tratemp2':Tratemp2,
                'Trapre':Trapre,
                'Tratemp3':Tratemp3,
                'Tratemp4':Tratemp4,
                'Tratemp5':Tratemp5,
                'Trasoli2':Trasoli2,
                'total':total,
                'revision':id,
                'fecha':Fecha,
                'hora':hora,
                aguaarr:AguaArr,
                concentracionarr:ConcentracionArr
            },
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            },
        });

}

function guardarpresecado(obj){
var PreAmps = Array();
var PrePreEjes= Array();
var PreChaqs = Array();
var PreTemps = Array();
var PreHums = Array();
var PreAlimMins = Array();
var PreAlimMaxs = Array();
var PreTempCha1 = Array();
var PreTempCha2 = Array();

     var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        PreAlimMins.push($('#preseAlimMin'+i+id).html() ? $('#preseAlimMin'+i+id).html():'nulo');
        PreAlimMaxs.push($('#preseAlimMax'+i+id).html() ? $('#preseAlimMax'+i+id).html():'nulo');
        PreAmps.push($('#preseAmp'+i+id).html() ? $('#preseAmp'+i+id).html():'nulo');
        PrePreEjes.push($('#presePreEje'+i+id).html() ? $('#presePreEje'+i+id).html():'nulo');
        PreChaqs.push($('#presePreChaq'+i+id).html() ? $('#presePreChaq'+i+id).html():'nulo');
        PreTemps.push($('#preseTemp'+i+id).html() ? $('#preseTemp'+i+id).html():'nulo');
        PreHums.push($('#preseHum'+i+id).html() ? $('#preseHum'+i+id).html():'nulo');
        
        PreTempCha1.push($('#preseTempCha1'+i+id).html() ? $('#preseTempCha1'+i+id).html():'0');
        PreTempCha2.push($('#preseTempCha2'+i+id).html() ? $('#preseTempCha2'+i+id).html():'0');
    }
    var h = $('#horaprese'+id).val() ? $('#horaprese'+id).val() : '00';
    var m = $('#minutosprese'+id).val() ? $('#minutosprese'+id).val() : '00';
    var hora = h+':'+m;

     $.ajax({
            url:'index.php?r=/control/actualizarvalorespresecado',
            type:'GET',
            data:{'PreAlimMins':PreAlimMins,
                'PreAlimMaxs':PreAlimMaxs,
                'PreAmps':PreAmps,
                'PrePreEjes':PrePreEjes,
                'PreChaqs':PreChaqs,
                'PreTemps':PreTemps,
                'PreHums':PreHums,
                'total':total,
                'revision':id,
                'fecha':Fecha,
                'hora':hora,
                tempcha1:PreTempCha1,
                tempcha2:PreTempCha2
            },
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            },
        });

}

function guardarsecado(obj){
var SeAmps = Array();
var SePreEjes= Array();
var SeTemps = Array();
var SeHums = Array();
    var id = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    for(var i = 1; i<=total;i++){
        SeAmps.push($('#seAmp'+i+id).html() ? $('#seAmp'+i+id).html():'nulo');
        SePreEjes.push($('#sePreEje'+i+id).html() ? $('#sePreEje'+i+id).html():'nulo');

        SeTemps.push($('#seTemp'+i+id).html() ? $('#seTemp'+i+id).html():'nulo');
        SeHums.push($('#seHum'+i+id).html() ? $('#seHum'+i+id).html():'nulo');
    }
    var h = $('#horasecado'+id).val() ? $('#horasecado'+id).val() : '00';
    var m = $('#minutossecado'+id).val() ? $('#minutossecado'+id).val() : '00';
    var hora = h+':'+m;

     $.ajax({
            url:'index.php?r=/control/actualizarvaloressecado',
            type:'GET',
            data:{'SeAmps':SeAmps,'SePreEjes':SePreEjes,'SeTemps':SeTemps,'SeHums':SeHums,'total':total,'revision':id,'fecha':Fecha,'hora':hora},
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
}

function guardarharina(obj){
    var id = $(obj).attr('rel');
    var temp = $('#hariTem'+id).html() ? $('#hariTem'+id).html() : 'nulo';
    var hum = $('#hariPorH'+id).html() ? $('#hariPorH'+id).html(): 'nulo';
    var ao = $('#hariAO'+id).html() ? $('#hariAO'+id).html(): 'nulo';
    var ini = $('#codini'+id).html() ? $('#codini'+id).html() : 'nulo';
    var fin = $('#codfin'+id).html() ? $('#codfin'+id).html() : 'nulo';
    var obser = $('#hariObser'+id).val() ? $('#hariObser'+id).val(): 'nulo';
    var sacos = $('#hariSacos'+id).html() ? $('#hariSacos'+id).html(): 'nulo';
    var h = $('#horahari'+id).val() ? $('#horahari'+id).val() : '00';
    var m = $('#minutoshari'+id).val() ? $('#minutoshari'+id).val() : '00';
    var hora = h+':'+m;
    
     $.ajax({
            url:'index.php?r=/control/actualizarvaloresharina',
            type:'GET',
            data:{'ini':ini,
                'fin':fin,
                'temp':temp,
                'hum':hum,
                'ao':ao,
                'obser':obser,
                'sacos':sacos,
                'revision':id,
                'fecha':Fecha,
                'hora':hora
            },
            success:function (resp){
                verificar(resp);
                $(obj).parent().parent().find('td, .ui-spinner-input,select').each(function(index){$(this).removeAttr('camb');});
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            },
        });

}
/* Funcion para verificar si actualiza corrrectamente (notificacion)*/
function verificar(resp){
    if(resp=='1')
        new PNotify({
        title: 'Actualizado',
        type: 'success'
        });
    else if(resp=='0')
        new PNotify({
        title: 'Sin cambios en actualización',
        });
    else if(resp=='2'){
        new PNotify({
        title: 'Sin permiso al guardar',
        type: 'error'
        });
    }
}
function validaciontabla(){
$('table td').on('change', function(evt, newValue) {
    
    newValue = newValue.toUpperCase(); 
    var campo = $(this);
    campo.html(newValue);
    newValue= newValue.replace(DECIMAL,'.');
    if($.isNumeric(newValue) && Number(newValue)<0){
        newValue = -1*Number(newValue);
    }
    
    if(pestania=='btnrecepcion'){
        var clasesrecep = campo.attr('class');
        if( campo.hasClass('text')) return;
        if(campo.hasClass('secu')){
            campo.html(formato_numero(newValue,0,DECIMAL,''));
            return;
        }
        if(campo.hasClass('desc')){
            if(newValue<0){
                newValue = 0;
            }
            if(newValue>100){
                newValue=100;
            }
            if(newValue>40){
                new PNotify({
                    title: '¡Atención!',
                    text:'Ha ingresado un descuento superior al 40%',
                });
            }
            campo.html(formato_numero(newValue,0,DECIMAL,''));
            return;
        }
        campo.html(formato_numero(newValue,3,DECIMAL,''));
    }
   if(pestania=='btnfin'){
       if(campo.hasClass('pasa'))
           campo.html(formato_numero(newValue,0,DECIMAL,''));
   }
   $(this).attr('camb','si');
   var val = Number(newValue);
   if($(this).attr('min')===undefined){
       if(campo.hasClass('new')){
           campo.html(formato_numero(newValue,2,DECIMAL,''));
       }
       return;
   }
   campo.html(formato_numero(newValue,2,DECIMAL,''));
   var min = Number(campo.attr('min'));
   var max = Number(campo.attr('max'));
   var nara = Number(campo.attr('nara'));
   var p_naramax = (max*nara)/100;
   if((val>(min+p_naramax) && val<(max-p_naramax)) || val==0){
       campo.attr('class','pnormal');
   }else if(val>=min && val<=max) {
       campo.attr('class','pnaranja');
   }else{
       campo.attr('class','projo');
   }
});
    $(".hora").spinner( {min:0,max:23,step:1, });    
    $(".minutos").spinner( {min:0,max:59,step:1, }); 
    $(".hora").blur(function(){
        if(this.value>23){
            this.value=23;
        }  
        else if(this.value<0){
            this.value=0;
        }
        $(this).attr('camb','si');
    });
    $(".minutos").blur(function(){
      if(this.value>59){
          this.value=59;
      }  
      else if(this.value<0){
          this.value=0;
      }
      $(this).attr('camb','si');
    });
}

function verificarcambios(){
    var result = true;
    var tc = $('table').find("[camb='si']").length;
    if(tc>0){
        result = confirm('¿ Desea salir sin guardar ?');
        if(result){
            $('table').find("[camb='si']").each(function(index){$(this).attr('camb','no');});
        }
    }
    return !result;
}

function eliminar(obj, tb){
    if(confirm('¿ Desea eliminar este registro ?')){
        $.ajax({
            url:'index.php?r=/control/eliminarregistro',
            type:'GET',
            data:{'id':$(obj).attr('rel'),'pest':tb},
            success:function (resp){
                if(Number(resp)){
                    $(obj).parent().parent().remove();
                    if(tb===1){
                            var idg = $(obj).attr('grupo');
                            $('#gruphora'+idg).attr('rowspan',Number($('#gruphora'+idg).attr('rowspan'))-1);
                            sumTarra();
                            sumPeso();
                            sumNeto();
                    }
                }
                else
                    new PNotify({
                    title: 'Error',
                    text:'No esta autorizado para borrar este registro',
                    type: 'error'
                    });
            },
            error:function(e){
                alert('Se produjo un error al intentar eliminar el registro');
            },
        });
    }
}
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
function sumPeso(){
    var v =0;
    var t =0;
    $('.ton').each(function(){ 
        v = $(this).html();
        v = v.replace(DECIMAL,'.');
        t+=Number(v);
     });
     $('#btnpeso').html(formato_numero(t,3,DECIMAL,''));
}
function sumNeto(){
    var v =0;
    var t =0;
    $('.neto').each(function(){ 
        v = $(this).html();
        v = v.replace(DECIMAL,'.');
        t+=Number(v);
    });
    $('#btnneto').html(formato_numero(t,3,DECIMAL,''));
}
function sumTarra(){
    var v =0;
    var t =0;
    $('.tarra').each(function(){ 
        v = $(this).html();
        v = v.replace(DECIMAL,'.');
        t+=Number(v);
    });
    $('#btntarra').html(formato_numero(t,3,DECIMAL,''));
}