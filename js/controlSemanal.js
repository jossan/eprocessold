function load(){  
    $(".hora").blur(function(){
        if(this.value>23){
            this.value=23;
        }  
        else if(this.value<0){
            this.value=0;
        }
        $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
    $(".minutos").blur(function(){
      if(this.value>59){
          this.value=59;
      }  
      else if(this.value<0){
          this.value=0;
      }
      $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
        $( ".fechafin" ).datepicker();
        $('table td').on('change', function(evt, newValue) {
          var campo = $(this);
          newValue = newValue.replace('.','');
          newValue = newValue.replace(',','.');
          $(this).parent().css( "background-color",'#CDF473');
          $('.RSemanal').css( "background-color",'#ffffff');
          var id =$(this).parent().attr('rel');
          var clase = $(this).attr('class');
          var t =0;
          var valor;
          if(clase=='sp'){
              campo.html(formato_numero(newValue, 0, ',', '.'));
              $(this).parent().find('.sp').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace('.','');
                  valor = valor.replace(',','.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 0, ',', '.');
              $('#SP'+id).html(t);
          }else if(clase=='mp'){
              campo.html(formato_numero(newValue, 3, ',', '.')); 
              $(this).parent().find('.mp').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace('.','');
                  valor = valor.replace(',','.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 3, ',', '.');
              $('#MP'+id).html(t);
          }
          else if(clase=='gl'){
              newValue = Math.ceil(newValue);
              campo.html(formato_numero(newValue, 0, ',', '.')); 
              valor = $(this).html();
              valor = valor.replace('.','');
              valor = valor.replace(',','.');
              t = Number(valor) / (Number($('#SP'+id).html())/20);
              t= isFinite(t) ? t : 0 ;
              $('#RDGL'+id).html(formato_numero(t, 2, ',', '.'));
          }
          else if(clase=='sac'){
              campo.html(formato_numero(newValue, 0, ',', '.')); 
          }
          rendimientoDiario(id);
        });
    }
        function actualiza(mes){
            if(mes)
                $('#semanas').val(-1);
            var a = $('#anios').val();
            var m = $('#meses').val();
            var s = $('#semanas').val();
            var htmlBef = '<img src="images/loader.gif">';
            $.ajax({
                type:"GET",
                url:"index.php?r=/controlSemanal/tabla&a="+a+"&m="+m+"&s="+s,
                 beforeSend: function( ){
                    $('#yw0').html(htmlBef);
                 },
                 success:function (resp){
                $('#yw0').html(resp);
                gridviewScroll();
                $('#gridedi').editableTableWidget().numericInputExample().find('td:first').focus();
                $(".hora").spinner( {min:0,max:23,step:1, });    
                $(".minutos").spinner( {min:0,max:59,step:1, });
                load();
                visible('.mp',visiblemp=true);
                visible('.tr',visibletr=true);
                visible('.sp',visiblesp=true);
                
            },
                error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
            });
            if(mes)
            $.getJSON('index.php?r=/controlSemanal/semanas&mes='+m+'&anio='+a,function(list){
                 $('#semanas').find('option').each(function(){ $(this).remove(); });
                 $('#semanas').append("<option value=-1> Ninguno </option>");
                 $.each(list, function(key, valor) {
                    $('#semanas').append("<option value='"+valor+"'>Semana "+valor+"</option>");
                });
            });
        }
        
      var mpprocesada;  
function guardar(obj){
    var idper = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    var h1= $('#inicioprodh'+idper).val() ? $('#inicioprodh'+idper).val() : '00';
    var m1 = $('#inicioprodm'+idper).val() ? $('#inicioprodm'+idper).val() : '00';
    var fechafin = $('#fechafin'+idper).val() ? $('#fechafin'+idper).val() : 'nulo';
    var inicioprod = h1+':'+m1;
    var h2= $('#finprodh'+idper).val() ? $('#finprodh'+idper).val() : '00';
    var m2 = $('#finprodm'+idper).val() ? $('#finprodm'+idper).val() : '00';
    var finprod = h2+':'+m2; 
    var h3= $('#noprogh'+idper).val() ? $('#noprogh'+idper).val() : '00';
    var m3 = $('#noprogm'+idper).val() ? $('#noprogm'+idper).val() : '00';
    var noprog = h3+':'+m3;    
     var h4= $('#progh'+idper).val() ? $('#progh'+idper).val() : '00';
    var m4 = $('#progm'+idper).val() ? $('#progm'+idper).val() : '00';
    var prog = h4+':'+m4;    
     var h5= $('#tercerh'+idper).val() ? $('#tercerh'+idper).val() : '00';
    var m5 = $('#tercerm'+idper).val() ? $('#tercerm'+idper).val() : '00';
    var tercer = h5+':'+m5;    
    var sachumed = $('#sachumed'+idper).html() ? $('#sachumed'+idper).html() : 'nulo';
    var sacbajacali = $('#sacbajacali'+idper).html() ? $('#sacbajacali'+idper).html() : 'nulo';
    var gl = $('#GL'+idper).html() ? $('#GL'+idper).html(): 'nulo';
    var valores = Array();
    mpprocesada = Array();
    $(grupos).each(function(){
       mpprocesada.push($('#g'+this+''+idper).html());
    });
    for(var i = 0; i<total;i++){
        valores.push($('#valorc'+i+idper).html() ? $('#valorc'+i+idper).html():'nulo');
    }
    $.ajax({
            url:'index.php?r=/controlSemanal/actualizartabla',
            type:'GET',
            dataType: "json",
            data:{'idper':idper,'total':total,'inicioprod':inicioprod,'fechafin':fechafin,'finprod':finprod,'noprog':noprog,'prog':prog,'tercer':tercer,'sachumed':sachumed,'sacbajacali':sacbajacali,'valores':valores,'gl':gl,'mp':mpprocesada},
            success:function (resp){
                verificar(resp.status);
                var sem= $('#Semana'+idper).html();
                //resp.Mp = parseFloat(resp.Mp);
                $('#MP'+idper).html(resp.Mp);
                $('#TR'+idper).html(resp.Tr);
                $('#SP'+idper).html(resp.Sp);
                $('#CA'+idper).html(resp.Ca);
                $('#RD'+idper).html(resp.Rd);
                $('#RS'+sem).html(resp.Rs);
                $('#RDGL'+sem).html(resp.Gd);
                $('#GS'+sem).html(resp.Gs);
                $(obj).parent().parent().css( "background-color",'');
                $('#MP'+idper).parent().css( "background-color",'');
                $('#fechafin'+idper).css( "background-color",'');
                $.getJSON('index.php?r=/controlSemanal/obtenertotalesmp',function(data){
                    $('#mpcocido').val(formato_numero(data.COCIDO, 3, ',', '.'));
                    $('#mpcocido').css('background-color',data.COCIDO>=0 ? 'transparent':'LightCoral');
                    $('#mpmerluza').val(formato_numero(data.MERLUZA, 3, ',', '.'));
                    $('#mpmerluza').css('background-color',data.MERLUZA>=0 ? 'transparent':'LightCoral');
                    $('#mppelagico').val(formato_numero(data.PELÁGICO, 3, ',', '.'));
                    $('#mppelagico').css('background-color',data.PELÁGICO>=0 ? 'transparent':'LightCoral');
                    $('#mpentero').val(formato_numero(data.ENTERO, 3, ',', '.'));
                    $('#mpentero').css('background-color',data.ENTERO>=0 ? 'transparent':'LightCoral');
                });
            },
            error:function(e){
                alert('Se produjo un error al intentar guardar los datos');
            }
        });
}

/* Funcion para verificar si actualiza corrrectamente (notificacion)*/
function verificar(resp){
    if(resp=='1')
        new PNotify({
        title: 'Actualizado',
        type: 'success'
        });
    else if(resp=='0')
        new PNotify({
        title: 'Sin cambios en actualización',
        });
    else if(resp=='2'){
        new PNotify({
        title: 'Sin permiso al guardar',
        type: 'error'
        });
    }
}
function visible(opc,validar){
    if (validar){
        $(opc).hide();
    }else{
        $(opc).show();
    }
    gridviewScroll();
    switch(opc){
        case '.mp':
            visiblemp = !visiblemp;
            break;
        case '.tr':
            visibletr = !visibletr;
            break;
        case '.sp':
            visiblesp = !visiblesp;
            break;
    }
}
visible('.mp',visiblemp);
visible('.tr',visibletr);
visible('.sp',visiblesp);
$( ".fechafin" ).change(function(){
    $(this).parent().parent().css("background-color",'#CDF473');
    $(this).css("background-color",'#CDF473');
});

function rendimientoDiario(ident){
    var SP = $('#SP'+ident).html();
    var MP = $('#MP'+ident).html();
    var RD = $('#RD'+ident);
    SP = SP.replace('.','');
    SP = SP.replace(',','.');
    MP = MP.replace('.','');
    MP = MP.replace(',','.');
    var rd = (Number(SP)/Number(MP));
    rd = isFinite(rd) ? rd : 0;
    RD.html(formato_numero(rd,2,',','.'));
}
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
$('.panel-heading').append('<div class="mp" style="font-size:11px;padding-top:10px;" ><strong>Cocido:</strong> <input id="mpcocido" size=8 value='+formato_numero(gesp.COCIDO,3,',','.')+' type="text" id="g1" '+cl1+'><strong> Merluza:</strong><input id="mpmerluza" value='+formato_numero(gesp.MERLUZA,3,',','.')+' size=8 type="text" id="g2" '+cl2+'> <strong>Pelágico:</strong><input id="mppelagico" value='+formato_numero(gesp.PELÁGICO,3,',','.')+' size=8 type="text" id="g3" '+cl3+'><strong>Entero:</strong><input id="mpentero" value='+formato_numero(gesp.ENTERO,3,',','.')+' type="text" size=8 id="g4" '+cl4+'></div>');


