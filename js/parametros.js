function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}


$('input').change(function(){
    var valor = this.value;
    if(DECIMAL=="," && valor.indexOf('.')<0){
        valor = valor.replace(DECIMAL,'.');
    }else{
        valor = "x";
    }
    if(!$.isNumeric(valor)){
        this.value = 0
    }
    $(this).parent().parent().attr('class','form-group')
});

$('input').blur(function(){
    var valor = this.value;
    if(DECIMAL=="," && valor.indexOf('.')<0){
        valor = valor.replace(DECIMAL,'.');
    }else{
        valor = "x";
    }
    if(!$.isNumeric(valor)){
        this.value = 0
    }
    $(this).parent().parent().attr('class','form-group')
});

$('input').keyup(function(){
    var valor = this.value;
    if(DECIMAL=="," && valor.indexOf('.')<0){
        valor = valor.replace(DECIMAL,'.');
    }else{
        valor = "x";
    }
    if($.isNumeric(valor)){
        $(this).parent().parent().attr('class','form-group has-success')
    }
    else{
        $(this).parent().parent().attr('class','form-group has-error')
    }
});

$('input').each(function(){
    this.value=formato_numero(this.value,2,DECIMAL,'');
});

function verificar(valor){
    /* resultado 0 -> es falso entonces significa q ocurrio un error
         * resultado 1 -> es verdadero entonces significa que si guardo */
    if(valor){
        js:$.notify("Actualizado Correctamente", "success");//el valor es 1
        
    }else{
        js:$.notify("Error al Actualizar", "error")//el valor es 0
    }
}
    