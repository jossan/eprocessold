<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
      public $_id;
      public $_rol;
      
	public function authenticate()
	{
		         
              $username=$this->username;
        $var=Yii::app()->db->createCommand("Select * from usuarios where Estado =1 and activo =1")->query();
        
                $users=array();
                foreach ($var as $result){
                        $usu=$result["Usuario"];
                        $users[$usu]=$result["Contrasenia"];
                  }
  		if(!isset($users[$this->username]))
  			$this->errorCode=self::ERROR_USERNAME_INVALID;
  		elseif($users[$this->username]!==MD5($this->password))
  			$this->errorCode=self::ERROR_PASSWORD_INVALID;
  		else{
                  $this->errorCode=self::ERROR_NONE;
                  $user=Usuarios::model()->findByAttributes(array('Usuario'=>$this->username));       
                  $this->_id=$user->ID;
                  $this->username=$user->Usuario;
                    $this->setState('idrol',$user->TipoRolID);
                    //$this->setState('nombre',$user->NombreCompleto);
                   
                  }
  		return !$this->errorCode;
                  
               
	}
        
        public function getId(){
            return $this->_id;
        }
        public function getRol(){
              return $this->_rol;
        }
                
        
    

}