<?php

include 'pdfControlDiaria.php';
include 'excelControlDiaria.php';

class ControlController extends Controller
{    
        /**
    * @return array action filters
    */
    public function filters()
    {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
    }
        public function accessRules()
    {
    return array(
    array('allow', // allow admin user to perform 'admin' and 'delete' actions
    'actions'=>array(
        'index',
        'produccionRango',
        'recepcion',
        'cocinas',
        'decantacion',
        'prensado',
        'refinacion',
        'tratamiento',
        'presecado',
        'secado',
        'empaque',
        'crearnuevo',
        'recepciones',
        'actualizarvalores',
        'actualizarvalorescocina',
        'Actualizarprensado',
        'actualizarvaloresrefinacion',
        'actualizarvalorestratamiento',
        'actualizarvaloresdecantacion',
        'actualizarvalorespresecado',
        'actualizarvaloressecado',
        'actualizarvaloresharina',
        'agregaregistro',
        'bloquear',
        'idrecepcion',
        'verpdfRango',
        'generarExcelprod',
        'horaActual',
        'addFila',
        'secuencia',
        'getEspecie',
        'eliminarregistro',
        'detalles',
        'produccionRecepcion'
        ),
    'users'=>array('@'),
    ),
    array('deny',  // deny all users
        'users'=>array('*'),
    ),
    );
    }
    public function actionIndex()
    {
        $decimal = $this->decimal();
        $param = null;
        if(isset($_GET['fecha'])){
            $param = array('fecha'=>$_GET['fecha'],'decimal'=>$decimal);
        }else{
            $param = array('decimal'=>$decimal);
        }
        $this->render('index', $param);
    }
    public function actionProduccionRango()
    {//.' 23:59'
        if(isset($_GET['fechaini']) && isset($_GET['fechafin'])&& isset($_GET['horaini'])&& isset($_GET['horafin'])){
            $fechaini=$_GET['fechaini'];
            $fechafin=$_GET['fechafin'];
            $horaini=$_GET['horaini'];
            $horafin=$_GET['horafin'];
            $hora = 0;
        }else{
            $fechaini=date('Y-m-d');
            $fechafin=date('Y-m-d');
            $horafin = '23:59';
            $horaini = '00:00';
            $hora = 0;
        }
        $model = new Recepcion('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Recepcion']))
            $model->attributes=$_GET['Recepcion'];
        
        
        $this->render('Produccion_rango',array(
                'model'=>$model,
                'f1'=>$fechaini,
                'f2'=>$fechafin,
                'horafin'=>$horafin,
                'horaini'=>$horaini,
                'hora'=>$hora,
                'proceso'=> isset($_GET['proceso']) ? $_GET['proceso'] : 0,
            ));
    }
    
    
    function idrol(){
        return Yii::app()->user->idrol;
    }

    public function recepcion($fecha){
        $decimal = $this->decimal();
        $countPeso=0;
        $countPesoNeto=0;
        $countTarra = 0;
        $falsa = -1;
        if($fecha=='') {$fecha= date('Y-m-d');}
        $model = Recepcion::model()->findAll("Fecha='".$fecha."' order by Hora, ID");
        $rt = '<table id="tabla_inicio" class="items table table-striped table-bordered table-condensed">';
        $rt = $rt.'<tr>';
        $rt =$rt.'<th>Grupo Horario</th>'
                . '<th><center>Hora</center></th>'
                . '<th><center>Id Recepción</center></th>'
                . '<th><center>Proveedor</center></th>'
                . '<th><center>Placa</center></th>'
                . '<th><center>NOM. Chofer</center></th>'
                . '<th><center>Secuencia</center></th>'
                . '<th><center>Especie</center></th>'
                . '<th>Peso(Kg)</th>'
                . '<th>Tara carro(Kg)</th>'
                . '<th>Desc (%)</th>'
                . '<th>Peso Neto(Kg)</th>'
                . '<th>Peso Neto(Tn)</th>'
                /*. '<th>TBVN</th>'*/
                . '<th>Entrada a tolva</th>'
                . '<th>Tolva</th>'
                . '<th>Fecha producción</th>';
        $rt = $rt.'</tr>';
        foreach($model as $row){
            $status =$row->IdUsuario;
            if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
            $img = 'images/candado.png';
        }else {$img = 'images/candadoabierto.jpeg';}
        $horag = strftime ('%H:00',strtotime($row->Hora));
        $horanum = strftime ('%H',strtotime($row->Hora));

        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:7px;">';
        if($horanum != $falsa){

        $modelRecep = Recepcion::model()->findAll("Fecha = '".$row->Fecha."' and HOUR(Hora) =".$horanum);
        $c=count($modelRecep);
        $gp = $row->ID;
        $rt =$rt.'<th style="vertical-align:middle;" id="gruphora'.$gp.'" rowspan="'.$c.'" >'.$horag.'</th>';
        $falsa = $horanum;
        }
        $hora=  explode(':', $row->Hora);
        $rt =$rt.'<th ><div style="display:flex;"><input onblur="cambiohora('.$row->ID.',this.value);" id="hora'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutos'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,1)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></div></th>';
        $rt =$rt.'<th >'.$row->ID.'</th>';
        $rt =$rt.'<th class="prov text" ><select id="prov'.$row->ID.'" class="form-control" style="height:18px; font-size:9px;padding:1px;margin:1px;">'.$this->Proveedores($row->ProveedorId).'</select></div></th>';
        $rt =$rt.'<td class="placa text" id="placa'.$row->ID.'">'.$row->Placa.'</td>';
        $rt =$rt.'<td class="chof text" id="chof'.$row->ID.'">'.$row->Chofer.'</td>';
        $rt =$rt.'<td class="secu" id="secu'.$row->ID.'">'.$row->IdSecuencia.'</td>';
        $rt =$rt.'<th ><select id= "especie'.$row->ID.'" class="form-control" style="height:18px; font-size:9px;padding:1px;margin:1px;">'.$this->Especie($row->EspecieID).'</select></th>';
        $rt =$rt.'<td class="ton" id="peso'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.number_format((float)$row->PesoTonelada*1000,3,$decimal,'').'</td>';
        $rt =$rt.'<td class="tarra" id="tarra'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.number_format((float)$row->tarracarro*1000,3,$decimal,'').'</td>';
        $rt =$rt.'<td class="desc" id="desc'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.number_format($row->Descuento,0,$decimal,'').'</td>';
        $rt =$rt.'<th class="neto" id="pesoneto'.$row->ID.'">'.number_format(((float)$row->Pesoneto)*1000,3,$decimal,'').'</th>';
        $rt =$rt.'<th class="netoton" id="pesonetoton'.$row->ID.'">'.number_format($row->Pesoneto,3,$decimal,'').'</th>';
        /*$rt =$rt.'<td id="tbvn'.$row->ID.'">'.$row->TBVN.'</td>';*/
        $hora=  explode(':', $row->Horatolva);
        $rt = $rt.'<th ><div style="display:flex;"><input id="horatolva'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutostolva'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActualTolva(this)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></div></th>';
        $rt  =$rt.'<th>'.CHtml::dropDownList('Lista',$row->TolvaID,CHtml::listData(Tolva::model()->findAll('Estado=1 order by Nombre'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"height:18px; font-size:9px;padding:1px;margin:1px;",'id'=>'tolva'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';
        $rt = $rt.'<th style="padding:0px;"><input type="text" id="fechaproduccion'.$row->ID.'" class="fechaproduccion" style="width:80px;text-align:center;box-shadow: none;margin:0px;" value="'.$row->fechaproduccion.'"></th>';
        if($this->idrol()!=4 && $this->idrol()!=6):
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            if($this->idrol()==2):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            endif;
            if($this->idrol()==1 || $this->idrol()==7):
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,1);','rel'=>$row->ID, 'grupo'=>$gp,'style'=>'margin:0px;')).'</th>';
            endif;
        endif;
        $rt =$rt.'</tr>';
        $countPeso +=(float)$row->PesoTonelada; 
        $countPesoNeto +=(float)$row->Pesoneto;
        $countTarra +=(float)$row->tarracarro;
            }
        $rt=$rt."<tr id='totales_recepcion' ><th colspan=8 style='background-color:DimGray;color:white;' >TOTALES:</th>"
                . "<th id='btnpeso' style='background-color:DimGray;color:white;' >".number_format(($countPeso*1000),3,$decimal,'')."</th>"
                . "<th id='btntarra' style='background-color:DimGray;color:white;' >".number_format(($countTarra*1000),3,$decimal,'')."</th>"
                . "<th id='btndesc' style='background-color:DimGray;color:white;'></th><th id='btnneto' style='background-color:DimGray;color:white;' >".number_format(($countPesoNeto*1000),3,$decimal,'')."</th>"
                . "<th colspan=6 style='background-color:DimGray;color:white;' ><a id='detallestotales' href='javascript:getDetalles();' style='color:white;'>+ Detalles<div id='contedetalles' style='display:none;'>"
                . "</div></a></th></tr>";
        $rt =$rt.'</table>';
        if($this->idrol()<=3 || $this->idrol()==7)
            $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(1,"'.$fecha.'")',array('class'=>'btn btn-success'));
        return $rt;
    }
    public function actionCocinas($fecha){
                    $decimal = $this->decimal();
                    $count= 0;
                    $model = Cocina::model()->findAll('Estado=1 and EstadoCocina=1 order by ID');
                    $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_cocina"><tr style="font-size:11px;">'
                            . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
                    foreach($model as $row){
                        $count++;$rt =$rt.'<th colspan="4" ><center>'.$row->Nombre.'</center></th>';
                    }
                    $rt = $rt.'</tr><tr style="font-size:11px;">';
                    for($y = 0 ;$y<$count;$y++){
                        $rt =$rt.'<th rowspan="2"><center>RPM</center></th><th colspan="2" ><center>Presion PSI</center></th><th rowspan="2"><center>T °C</center></th>';
                    }
                    $rt = $rt.'</tr><tr style="font-size:11px;">';
                    for($y = 0 ;$y<$count;$y++){
                        $rt =$rt.'<th><center>Eje</center></th><th><center>Chaq</center></th>';
                    }
                    $rt = $rt.'</tr>';
                    /*Consultas*/
                    $cri=new CDbCriteria();
                    $cri->select ='t.*';
                    $cri->with = array ('cocina');
                    $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and cocina.EstadoCocina =1";
                    $cri->order ='Hora, codigofila, CocinaID';
                    $result =Coccion::model()->findAll($cri);
                    $contadortabla=1;
                    /*Contenido*/
                    $rt = $rt.'</tr><tr style="font-size:11px;">';
                    foreach($model as $p){
                        $rt =$rt.'<th><center>'.(float)$p->CoccionMinRmp.' _ '.(float)$p->CoccionMaxRmp.'</center></th><th><center>'.(float)$p->CoccionMinPresionEje.' _ '.(float)$p->CoccionMaxPresionEje.'</center></th><th><center>'.(float)$p->CoccionMinPresionChaqueta.' _ '.(float)$p->CoccionMaxPresionChaqueta.'</center></th><th><center>'.(float)$p->CoccionMinTemperatura.' _ '.(float)$p->CoccionMaxTemperatura.'</center></th>';
                    }
                    $rt = $rt.'</tr>';
                    foreach($result as $row){
                        $p=$model[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
                        if ($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);
                            $rt =$rt.'<th ><input  id="horacocc'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoscocc'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,2)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
                        }
                        $rt =$rt.'<td camb="no" id="rpm'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->RPM,$p->CoccionMinRmp,$p->CoccionMaxRmp,$p->CoccionRpmAlertaNaranja).' >'.number_format($row->RPM,2,$decimal,'').'</td>';
                        $rt =$rt.'<td camb="no" id="presioneje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->CoccionMinPresionEje,$p->CoccionMaxPresionEje,$p->CoccionPresionEjeAlertaNaranja).'>'.number_format($row->PresionEje,2,$decimal,'').'</td>';
                        $rt =$rt.'<td camb="no" id="presionchaqueta'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->CoccionMinPresionChaqueta,$p->CoccionMaxPresionChaqueta,$p->CoccionPresionChaquetaAlertaNaranja).'>'.number_format($row->PresionChaqueta,2,$decimal,'').'</td>';
                        $rt =$rt.'<td camb="no" id="temperatura'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura,$p->CoccionMinTemperatura,$p->CoccionMaxTemperatura,$p->CoccionPresionTemperaturaAlertaNaranja).'>'.number_format($row->Temperatura,2,$decimal,'').'</td>';
                        $contadortabla++;
                        if($contadortabla>$count){
                            $contadortabla=1;
                            if($this->idrol()!=4 && $this->idrol()!=6):
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarcocinas(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                                if($this->idrol()==2):
                                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,2);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                                endif;
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,2);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                            endif;
                            $rt =$rt.'</tr>';
                        }
                    }
                    $rt =$rt.'</table>';
                    if($this->idrol()<3 || $this->idrol()==7)
                        $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(2, "'.$fecha.'")',array('class'=>'btn btn-success'));
                    print $rt;
            }
    public function actionPrensado($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model = Prensa::model()->findAll('Estado=1 and EstadoPrensa=1 order by ID');
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_prensas"><tr style="font-size:10px;">'
                    . '<th style="font-size:11px;" rowspan="3"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                $rt =$rt.'<th colspan="2" ><center>'.$row->Nombre.'</center></th>';
            }
            $rt =$rt.'<th colspan="1" ><center>LICOR</center></th>'
                    . '<th rowspan=3 ><center> Dosificación de<br> concentrado (l) <center></th>'
                    . '</tr>'
                    . '<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                $rt =$rt.'<th><center>Amp</center></th><th><center>%H</center></th>';
            }
            $rt = $rt.'<th><center>%S</center></th>';
                    /*. '<th><center>%G</center></th></tr>';*/
            $cri=new CDbCriteria();
            $cri->select ='t.*';
            $cri->with = array ('prensa');
            $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and prensa.EstadoPrensa =1";
            $cri->order ='Hora, codigofila, PrensaID';
            $result = Prensado::model()->findAll($cri);
            $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja from parametros LIMIT 1";
            $p = Yii::app()->db->createCommand($sql)->queryRow();
            $contadortabla=1;
            $rt = $rt.'<tr style="font-size:11px;">';
            foreach($model as $pp){
                $rt =$rt.'<th><center>'.(float)$pp->PrensadoMinAmperaje.' _ '.(float)$pp->PrensadoMaxAmperaje.'</center></th><th><center>'.(float)$pp->PrensadoMinPorcentajeHumedad.' _ '.(float)$pp->PrensadoMaxPorcentajeHumedad.'</center></th>';
            }
            $rt =$rt.'<th><center>'.(float)$p['LicorMinPorcentajeSolidos'].' _ '.(float)$p['LicorMaxPorcentajeSolidos'].'</center></th>';
                    /*. '<th><center>'.(float)$p['LicorMinPorcentajeGrasas'].' _'.(float)$p['LicorMaxPorcentajeGrasas'].'</center></th>'; */
            $rt = $rt.'</tr>';
            foreach($result as $row){
            $pp=$model[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);
                            $rt =$rt.'<th ><input  id="horaprensa'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprensa'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,4)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                    }
            $rt =$rt.'<td id="preAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje, $pp->PrensadoMinAmperaje, $pp->PrensadoMaxAmperaje, $pp->PrensadoAmperajeAlertaNaranja).' >'.number_format($row->Amperaje,2,$decimal,'').'</td>';
            $rt =$rt.'<td id="preHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$pp->PrensadoMinPorcentajeHumedad, $pp->PrensadoMaxPorcentajeHumedad, $pp->PrensadoPorcentajeHumedadAlertaNaranja).' >'.number_format($row->PorcentajeHumedad,2,$decimal,'').'</td>';
            $contadortabla++;
            if($contadortabla>$count){
                    $contadortabla=1;
                    $licor = Licor::model()->find('codigofila ='.$row->codigofila);
                    $rt =$rt.'<td id="preLicH'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeSolidos, $p['LicorMinPorcentajeSolidos'], $p['LicorMaxPorcentajeSolidos'], $p['LicorPorcentajeSolidosAlertaNaranja']).' >'.number_format($licor->LicorPorcentajeSolidos,2,$decimal,'').'</td>';
                    /*$rt =$rt.'<td id="preLicG'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeGrasas, $p['LicorMinPorcentajeGrasas'], $p['LicorMaxPorcentajeGrasas'], $p['LicorPorcentajeGrasasAlertaNaranja']).' >'.number_format($licor->LicorPorcentajeGrasas,2,$decimal,'').'</td>';*/
                    $rt = $rt.'<td id="licCon'.$row->codigofila.'" class="new">'.number_format($licor->concentrado,2,$decimal,'').'</td>';
                    if($this->idrol()!=4 && $this->idrol()!=6):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Bloquear', array ('onclick'=>'guardarprensado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Actualizar', array ('onclick'=>'bloquear(this,3);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,3);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    endif;
                    $rt =$rt.'</tr>';
            }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(3, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }

    public function actionDecantacion($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model =  Tricanter::model()->findAll('Estado=1 and EstadoTricanter=1 order by ID');
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_decantasion"><tr style="font-size:11px;">'
                   .'<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                $rt =$rt.'<th colspan="2" ><center>'.$row->Nombre.'</center></th>';
            }
            $rt = $rt.'</tr><tr>';
            for($y = 0 ;$y<$count;$y++){
                $rt =$rt.'<th colspan="1" ><center>ALIM</center></th>'
                        . '<th colspan="1" ><center>Agua de cola</center></th>';
            }
            $rt = $rt.'</tr>';
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                $rt =$rt.'<th><center>T °C</center></th>'
                        /*. '<th ><center>%H</center></th>'*/
                        . '<th><center>%S</center></th>';
                        /*. '<th><center>%G</center></th>';*/
            }
            $cri=new CDbCriteria();
            $cri->select ='t.*';
            $cri->with = array ('tricanter');
            $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and tricanter.EstadoTricanter =1";
            $cri->order ='Hora, codigofila, TricanterID';
            $result = Decantacion::model()->findAll($cri);
            $contadortabla=1;
            $rt = $rt.'</tr><tr style="font-size:11px;">';
            foreach($model as $p){
                $rt =$rt.'<th><center>'.(float)$p->DecantacionMinAlimentacionTemperatura.' _ '.(float)$p->DecantacionMaxAlimentacionTemperatura.'</center></th>';
                /*$rt =$rt . '<th><center>'.(float)$p->DecantacionMinAlimentacionPorcentajeHumedad.' _ '.(float)$p->DecantacionMaxAlimentacionPorcentajeHumedad.'</center></th>';*/
                $rt =$rt . '<th><center>'.(float)$p->DecantacionMinAguaColaPorcentajeSolidos.' _ '.(float)$p->DecantacionMaxAguaColaPorcentajeSolidos.'</center></th>';
                /*$rt =$rt . '<th><center>'.(float)$p->DecantacionMinAguaColaPorcentajeGrasas.' _ '.(float)$p->DecantacionMaxAguaColaPorcentajeGrasas.'</center></th>';*/
            }
            $rt = $rt.'</tr>';
            foreach($result as $row){
                $p = $model[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){ $ban = 1; }  else {$ban =0; }
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                if($contadortabla==1){
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                    $hora=  explode(':', $row->Hora);
                    $rt =$rt.'<th ><input  id="horadeca'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosdeca'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,3)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                }
                $rt =$rt.'<td id="Dtem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura,$p->DecantacionMinAlimentacionTemperatura,$p->DecantacionMaxAlimentacionTemperatura,$p->DecantacionAlimentacionTemperaturaAlertaNaranja).' >'.number_format($row->AlimentacionTemperatura,2,$decimal,'').'</td>';
                /*$rt =$rt.'<td id="Dhum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionPorcentajeHumedad, $p->DecantacionMinAlimentacionPorcentajeHumedad, $p->DecantacionMaxAlimentacionPorcentajeHumedad, $p->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja).' >'.number_format($row->AlimentacionPorcentajeHumedad,2,$decimal,'').'</td>';*/
                $rt =$rt.'<td id="Dsol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeSolidos,$p->DecantacionMinAguaColaPorcentajeSolidos, $p->DecantacionMaxAguaColaPorcentajeSolidos, $p->DecantacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.number_format($row->AguaColaPorcentajeSolidos,2,$decimal,'').'</td>';
                /*$rt =$rt.'<td id="Dgra'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeGrasas, $p->DecantacionMinAguaColaPorcentajeGrasas, $p->DecantacionMaxAguaColaPorcentajeGrasas, $p->DecantacionAguaColaPorcentajeGrasasAlertaNaranja).' >'.number_format($row->AguaColaPorcentajeGrasas,2,$decimal,'').'</td>';*/
                $contadortabla++;
                if($contadortabla>$count){
                    $contadortabla=1;
                    if($this->idrol()!=4 && $this->idrol()!=6):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardardecantacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,4);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,4);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    endif;
                    $rt =$rt.'</tr>';
                }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(4, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }

    public function actionRefinacion($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model = Pulidora::model()->findAll('Estado = 1 and EstadoPulidora=1 order by ID');
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_refinacion">';
            $rt = $rt.'<tr style="font-size:11px;">'
                    . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                $rt =$rt.'<th colspan="4" ><center>'.$row->Nombre.'</center></th>';
            }
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th colspan="1" ><center>ALIM</center></th><th colspan="3" ><center>ACEITE</center></th>';
            }
            $rt = $rt.'</tr>';
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th><center>T °C</center></th><th ><center>%H</center></th><th><center>%S</center></th><th><center>%A</center></th>';
            }
            $rt = $rt.'</tr>';
            $cri=new CDbCriteria();
            $cri->select ='t.*';
            $cri->with = array ('pkpulidora');
            $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and pkpulidora.EstadoPulidora =1";
            $cri->order ='Hora, codigofila, PulidoraID';
            $result = Refinacion::model()->findAll($cri);
            $contadortabla=1;
            $rt = $rt.'<tr style="font-size:11px;">';
            foreach($model as $p){
                $rt =$rt.'<th><center>'.(float)$p->RefinacionMinAlimentacionTemperatura.' _ '.(float)$p->RefinacionMaxAlimentacionTemperatura.'</center></th><th ><center>'.(float)$p->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$p->RefinacionMaxAceitePorcentajeHumedad.'</center></th><th><center>'.(float)$p->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$p->RefinacionMaxAceitePorcentajeSolidos.'</center></th><th><center>'.(float) $p->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$p->RefinacionMaxAceitePorcentajeAcidez.'</center></th>';
            }
            $rt = $rt.'</tr>';
            foreach($result as $row){
                $status = $row->IdUsuario;
                $p= $model[$contadortabla-1];
                if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                if($contadortabla==1){
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                    $hora=  explode(':', $row->Hora);                   
                    $rt =$rt.'<th ><input  id="horarefi'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosrefi'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,5)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
                }
                $rt =$rt.'<td id="retem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura, $p->RefinacionMinAlimentacionTemperatura, $p->RefinacionMaxAlimentacionTemperatura, $p->RefinacionTemperaturaAlertaNaranja).' >'.number_format($row->AlimentacionTemperatura,2,$decimal,'').'</td>';
                $rt =$rt.'<td id="rehum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeHumedad, $p->RefinacionMinAceitePorcentajeHumedad, $p->RefinacionMaxAceitePorcentajeHumedad, $p->RefinacionAceitePorcentajeHumedadAlertaNaranja).' >'.number_format($row->AceitePorcentajeHumedad,2,$decimal,'').'</td>';
                $rt =$rt.'<td id="resol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeSolidos, $p->RefinacionMinAceitePorcentajeSolidos, $p->RefinacionMaxAceitePorcentajeSolidos, $p->RefinacionAceitePorcentajeSolidosAlertaNaranja).' >'.number_format($row->AceitePorcentajeSolidos,2,$decimal,'').'</td>';
                $rt =$rt.'<td id="reAcid'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeAcidez, $p->RefinacionMinAceitePorcentajeAcidez, $p->RefinacionMaxAceitePorcentajeAcidez, $p->RefinacionAceitePorcentajeAcidezAlertaNaranja).' >'.number_format($row->AceitePorcentajeAcidez,2,$decimal,'').'</td>';
                $contadortabla++;
                if($contadortabla>$count){
                    $contadortabla=1;
                    if($this->idrol()!=4 && $this->idrol()!=6):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarrefinacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,5);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,5);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    endif;
                    $rt =$rt.'</tr>';
                }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(5, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }

    public function actionTratamiento($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model = Planta::model()->findAll('Estado=1 and EstadoPlanta=1 order by ID');
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_tratamiento"><tr style="font-size:11px;">'
                    . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                $rt =$rt.'<th colspan="8" ><center>'.$row->Nombre.'</center></th>'
                    . '<th colspan="2" ></th>';
            }
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                    $rt =$rt.'<th colspan="2" ><center>ALIM A.Cola</center></th>'
                            . '<th colspan="1" ><center>VAH</center></th>'
                            . '<th colspan="1" ><center>Vacio</center></th>'
                            . '<th colspan="1" ><center>1°E</center></th>'
                            . '<th colspan="1" ><center>2°E</center></th>'
                            . '<th colspan="1" ><center>3°E</center></th>'
                            . '<th colspan="1" ><center>CON</center></th>'
                            . '<th rowspan="3" ><center>AGUA DE COLA<br>PROCESADA (m<sup>3</sup>)</center></th>'
                            . '<th rowspan="3" ><center> CONCENTRADO<br>PRODUCIDO (m<sup>3</sup>) </center></th>';
            }
            $rt = $rt.'</tr>';
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                    $rt =$rt.'<th><center>T °C</center></th><th ><center>%S</center></th><th><center>T°C</center></th><th><center>p</center></th><th><center>T °C</center></th><th ><center>__</center></th><th><center>T°C</center></th>'
                            . '<th><center>%S</center></th>';
            }
            
        $rt = $rt.'</tr>';
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('planta');
        $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and planta.EstadoPlanta =1";
        $cri->order ='Hora, codigofila, PlantaID';
        $result = Tratamiento::model()->findAll($cri);
        $contadortabla=1;
        $rt = $rt.'<tr style="font-size:11px;">';
        foreach($model as $p){
            $rt =$rt.'<th><center>'.(float)$p->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaTemperatura.'</center></th>'
                    . '<th ><center>'.(float)$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos.'</center></th>'
                    . '<th><center>'.(float)$p->TratamientoMinVahTemperatura.' _ '.(float)$p->TratamientoMaxVahTemperatura.'</center></th><th><center>'.(float)$p->TratamientoMinVacPeso.' _ '.(float)$p->TratamientoMaxVacPeso.'</center></th>'
                    . '<th><center>'.(float)$p->TratamientoMinTemperatura1.' _ '.(float)$p->TratamientoMaxTemperatura1.'</center></th><th ><center>'.(float)$p->TratamientoMinTemperatura2.' _ '.(float)$p->TratamientoMaxTemperatura2.'</center></th>'
                    . '<th><center>'.(float)$p->TratamientoMinTemperatura3.' _ '.(float)$p->TratamientoMaxTemperatura3.'</center></th><th><center>'.(float)$p->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxSalidaPorcentajeSolidos.'</center></th>';
        }
        
        $rt = $rt.'</tr>';
            foreach($result as $row){
                $p= $model[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                if($contadortabla==1){
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                    $hora=  explode(':', $row->Hora);                        
                    $rt =$rt.'<th ><input  id="horatrata'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutostrata'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,6)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
                }
                    $rt =$rt.'<td id="tratemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaTemperatura,$p->TratamientoMinAlimentacionAguaColaTemperatura,$p->TratamientoMaxAlimentacionAguaColaTemperatura,$p->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja).' >'.number_format($row->AlimentacionAguaColaTemperatura,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="trasoli'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.number_format($row->AlimentacionAguaColaPorcentajeSolidos,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="tratemp2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VahTemperatura,$p->TratamientoMinVahTemperatura,$p->TratamientoMaxVahTemperatura,$p->TratamientoVahTemperaturaAlertaNaranja).' >'.number_format($row->VahTemperatura,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="trapre'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VacPeso,$p->TratamientoMinVacPeso,$p->TratamientoMaxVacPeso,$p->TratamientoVacPesoAlertaNaranja).' >'.number_format($row->VacPeso,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="tratemp3'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura1,$p->TratamientoMinTemperatura1,$p->TratamientoMaxTemperatura1,$p->TratamientoTemperatura1AlertaNaranja).' >'.number_format($row->Temperatura1,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="tratemp4'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura2,$p->TratamientoMinTemperatura2,$p->TratamientoMaxTemperatura2,$p->TratamientoTemperatura2AlertaNaranja).' >'.number_format($row->Temperatura2,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="tratemp5'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura3,$p->TratamientoMinTemperatura3,$p->TratamientoMaxTemperatura3,$p->TratamientoTemperatura3AlertaNaranja).' >'.number_format($row->Temperatura3,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="trasoli2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SalidaPorcentajeSolidos,$p->TratamientoMinSalidaPorcentajeSolidos,$p->TratamientoMaxSalidaPorcentajeSolidos,$p->TratamientoSalidaPorcentajeSolidosAlertaNaranja).' >'.number_format($row->SalidaPorcentajeSolidos,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="aguacola'.$contadortabla.''.$row->codigofila.'"  class="new" >'.number_format($row->aguacola,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="concentrado'.$contadortabla.''.$row->codigofila.'"  class="new" >'.number_format($row->concentrado,2,$decimal,'').'</td>';
                    
                    $contadortabla++;
                    if($contadortabla>$count){
                        $contadortabla=1;
                        if($this->idrol()!=4 && $this->idrol()!=6):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardartratamiento(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                            if($this->idrol()==2):
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,6);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                            endif;
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,6);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        endif;
                        $rt =$rt.'</tr>';
                    }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(6, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }
    public function actionPresecado($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model = Rotadisk::model()->findAll('Estado=1 and EstadoRotadisk=1 order by ID');
            /*Comienzo de tabla*/
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_presecado">';
            /*FILA 1*/
            $rt = $rt.'<tr style="font-size:11px;">'
                    . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                /*$rt =$rt.'<th ><center>ALIM MIN</center></th>';/*Columna Alimentacion*/
                /*$rt =$rt.'<th ><center>ALIM MAX</center></th>';/*Columna Alimentacion*/
                $rt =$rt.'<th colspan="6" ><center>'.$row->Nombre.'</center></th>';
            }
            $rt = $rt.'</tr>';
            /*FILA 2*/
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                /*$rt =$rt.'<th rowspan="2" ><center>% H</center></th>';/*Columna Alimentacion*/
                /*$rt =$rt.'<th rowspan="2"><center>Presión</center></th>';/*Columna Alimentacion*/
                $rt =$rt.'<th rowspan="2"><center>Amp</center></th>'
                        . '<th colspan="1"><center>Presión PSI</center></th>'
                        . '<th colspan="3" ><center>Temperaturas</center></th>'
                        /*. '<th rowspan="2" ><center>Temp</center></th>'*/
                        . '<th rowspan="2" ><center>%H</center></th>';
            }
            $rt = $rt.'</tr>';
            /*FILA 3*/
            $rt = $rt.'<tr style="font-size:11px;">';
    /*Columnas Alimentación*/
            for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th><center>EJE</center>'
                    . '<th ><center>Chaq 1</center></th>'
                    . '<th ><center>chaq 2</center></th>'
                    . '<th><center>Temp</center></th>';
            }
            $rt = $rt.'</tr>';
            /*Contenido de la tabla*/
            $cri=new CDbCriteria();
            $cri->select ='t.*';
            $cri->with = array ('rotadisk');
            $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and rotadisk.EstadoRotadisk =1";
            $cri->order ='Hora, codigofila, RotadiskID';
            $result = Presecado::model()->findAll($cri);
            /*Consulta de parametros de validación*/
            $contadortabla=1;
            $rt = $rt.'<tr style="font-size:11px;">';
            foreach($model as $p){
                /*$rt = $rt.'<th><center>'.(float)$p->PresecadoMinMinimoPorcentajeHumedad.' _ '.(float)$p->PresecadoMaxMinimoPorcentajeHumedad.'</center></th>'
                        . '<th ><center>'.(float)$p->PresecadoMinMaximoPresion.' _ '.(float)$p->PresecadoMaxMaximoPresion.'</center></th>';/*Columnas Alimentación*/
                $rt =$rt.'<th><center>'.(float)$p->PresecadoMinAmperaje.' _ '.(float)$p->PresecadoMaxAmperaje.'</center></th>'
                        . '<th><center>'.(float)$p->PresecadoMinPresionEje.' _ '.(float)$p->PresecadoMaxPresionEje.'</center></th>'
                        
                        . '<th> <center>'.(float)$p->PresecadoMinTempCha1.' _ '.(float)$p->PresecadoMaxTempCha1.'</center> </th>'
                        . '<th> <center>'.(float)$p->PresecadoMinTempCha2.' _ '.(float)$p->PresecadoMaxTempCha2.'</center> </th>'
                        /*. '<th><center>'.(float)$p->PresecadoMinPresionChaqueta.' _ '.(float)$p->PresecadoMaxPresionChaqueta.'</center></th>'*/
                        . '<th ><center>'.(float)$p->PresecadoMinSCR.' _ '.(float)$p->PresecadoMaxSCR.'</center></th>'
                        . '<th><center>'.(float)$p->PresecadoMinPorcentajeHumedad.' _ '.(float)$p->PresecadoMaxPorcentajeHumedad.'</center></th>';
            }
            $rt = $rt.'</tr>';
            foreach($result as $row){
                $p = $model[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
            $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);
                        $rt =$rt.'<th ><input  id="horaprese'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprese'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,7)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                    }
                    /*$rt =$rt.'<td id="preseAlimMin'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MinimoPorcentajeHumedad,$p->PresecadoMinMinimoPorcentajeHumedad,$p->PresecadoMaxMinimoPorcentajeHumedad,$p->PresecadoMinimoPorcentajeHumedadAlertaNaranja).' >'.number_format($row->MinimoPorcentajeHumedad,2,$decimal,'').'</td>';/*Columna Alimentacion*/
                    /*$rt =$rt.'<td id="preseAlimMax'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MaximoPresion,$p->PresecadoMinMaximoPresion,$p->PresecadoMaxMaximoPresion,$p->PresecadoMaximoPresionAlertaNaranja).' >'.number_format($row->MaximoPresion,2,$decimal,'').'</td>';/*Columna Alimentacion*/
                    $rt =$rt.'<td id="preseAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->PresecadoMinAmperaje,$p->PresecadoMaxAmperaje,$p->PresecadoAmperajeAlertaNaranja).' >'.number_format($row->Amperaje,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="presePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->PresecadoMinPresionEje,$p->PresecadoMaxPresionEje,$p->PresecadoPresionEjeAlertaNaranja).' >'.number_format($row->PresionEje,2,$decimal,'').'</td>';
                    
                    $rt =$rt.'<td id="preseTempCha1'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->TempCha1,$p->PresecadoMinTempCha1,$p->PresecadoMaxTempCha1,$p->PresecadoAlertaTempCha1).' >'.number_format($row->TempCha1,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="preseTempCha2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->TempCha2,$p->PresecadoMinTempCha2,$p->PresecadoMaxTempCha2,$p->PresecadoAlertaTempCha2).' >'.number_format($row->TempCha2,2,$decimal,'').'</td>';
                    
                    /*$rt =$rt.'<td id="presePreChaq'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->PresecadoMinPresionChaqueta,$p->PresecadoMaxPresionChaqueta,$p->PresecadoPresionChaquetaAlertaNaranja).' >'.number_format($row->PresionChaqueta,2,$decimal,'').'</td>';*/
                    $rt =$rt.'<td id="preseTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SCR,$p->PresecadoMinSCR,$p->PresecadoMaxSCR,$p->PresecadoSCRAlertaNaranja).' >'.number_format($row->SCR,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="preseHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->PresecadoMinPorcentajeHumedad,$p->PresecadoMaxPorcentajeHumedad,$p->PresecadoPorcentajeHumedadAlertaNaranja).' >'.number_format($row->PorcentajeHumedad,2,$decimal,'').'</td>';
                    $contadortabla++;
                    if($contadortabla>$count){
                            $contadortabla=1;
                            if($this->idrol()!=4 && $this->idrol()!=6):
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarpresecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                                if($this->idrol()==2):
                                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,7);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                                endif;
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,7);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                            endif;
                            $rt =$rt.'</tr>';
                    }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(7, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }

    public function actionSecado($fecha){
            $decimal = $this->decimal();
            $count= 0;
            $model = Rotatubo::model()->findAll('Estado = 1 and EstadoRotatubo=1 order by ID');
            /*Comienzo de tabla*/
            $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_secado">';
            /*FILA 1*/
            $rt = $rt.'<tr style="font-size:11px;">'
            . '<th style="font-size:11px;" rowspan="3"><center>Hora de Medición</center></th>';
            foreach($model as $row){
                $count++;
                $rt =$rt.'<th colspan="4" ><center>'.$row->Nombre.'</center></th>';
            }
            $rt = $rt.'</tr>';
            /*FILA 2*/
            $rt = $rt.'<tr style="font-size:11px;">';
            for($y = 0 ;$y<$count;$y++){
                $rt =$rt.'<th ><center>Amp</center></th><th colspan="1"><center>Presión PSI</center></th><th colspan="1" ><center>Temperatura</center></th><th colspan="1" ><center>%H</center></th>';
            }
            $rt = $rt.'</tr>';

            /*Contenido de la tabla*/
            $cri=new CDbCriteria();
            $cri->select ='t.*';
            $cri->with = array ('rotatubo');
            $cri->condition= "Fecha='".$fecha."' and t.Estado=1 and rotatubo.EstadoRotatubo =1";
            $cri->order ='Hora, codigofila, RotatuboID';
            $result = Secado::model()->findAll($cri);
            $contadortabla=1;
            /*FILA 3*/
            $rt = $rt.'<tr style="font-size:11px;">';
            foreach($model as $p){
                $rt =$rt.'<th><center>'.(float)$p->RotatuboMinAmperaje.' _ '.(float)$p->RotatuboMaxAmperaje.'</center></th><th><center>'.(float)$p->RotatuboMinPresionEje.' _ '.(float)$p->RotatuboMaxPresionEje.'</center></th><th><center>'.(float)$p->RotatuboMinSRC.' _ '.(float)$p->RotatuboMaxSRC.'</center></th><th ><center>'.(float)$p->RotatuboMinHumedad.' _ '.(float)$p->RotatuboMaxHumedad.'</center></th>';
            }
            $rt = $rt.'</tr>';
            foreach($result as $row){
                $p = $model[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                if($contadortabla==1){
                     $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                     $hora=  explode(':', $row->Hora);
                     $rt =$rt.'<th ><input  id="horasecado'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutossecado'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,8)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                }
                    $rt =$rt.'<td id="seAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->RotatuboMinAmperaje,$p->RotatuboMaxAmperaje,$p->RotatuboAmperajeAlertaNaranja).' >'.number_format($row->Amperaje,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="sePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->RotatuboMinPresionEje,$p->RotatuboMaxPresionEje,$p->RotatuboPresionEjeAlertaNaranja).' >'.number_format($row->PresionEje,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="seTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SRC,$p->RotatuboMinSRC,$p->RotatuboMaxSRC,$p->RotatuboSRCAlertaNaranja).' >'.number_format($row->SRC,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="seHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->RotatuboMinHumedad,$p->RotatuboMaxHumedad,$p->RotatuboHumedadAlertaNaranja).' >'.number_format($row->PorcentajeHumedad,2,$decimal,'').'</td>';
                    $contadortabla++;
                    if($contadortabla>$count){
                            $contadortabla=1;
                            if($this->idrol()!=4  && $this->idrol()!=6):
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarsecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                                if($this->idrol()==2):
                                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,8);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                                endif;
                                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,8);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                            endif;
                            $rt =$rt.'</tr>';
                    }
            }
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(8, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;
    }
    public function actionEmpaque($fecha){
            $decimal = $this->decimal();
            $result = Harina::model()->findAll("Fecha='".$fecha."' order by Hora , ID");
            $p= Parametros::model()->find();
            $rt = '<table id="tabla_fin" class="items table table-striped table-bordered table-condensed">';
            /*Fila 1*/
            $rt = $rt.'<tr style="font-size:10px;">'
                    . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
            $rt =$rt.'<th colspan=6><center>HARINA</center></th><th rowspan="4"><center>OBSERVACIONES</center></th>';
            $rt = $rt.'</tr>';
            /*Fila 2*/
            $rt = $rt.'<tr style="font-size:10px;">';
            $rt =$rt.'<th ><center>Temp</center></th><th ><center>Humedad</center></th><th ><center>A/O</center></th><th rowspan=3><center>Código inicio <br>Tarjeta</center></th><th rowspan=3><center>Código fin <br>Tarjeta</center></th><th rowspan=3><center>Sacos</center></th>';
            $rt = $rt.'</tr>';
            /*Fila 3*/
            $rt = $rt.'<tr style="font-size:10px;">';
            $rt =$rt.'<th ><center>°C</center></th><th ><center>%</center></th><th ><center>ppm</center></th>';
            $rt = $rt.'</tr>';
            /*Fila 4*/
            $rt = $rt.'<tr style="font-size:10px;">';
            $rt =$rt."<th ><center>".(float)$p->HarinaMinTemperatura." _ ".(float)$p->HarinaMaxTemperatura."</center></th>"
                    . "<th ><center>".(float)$p->HarinaMinHumedad." _ ".(float)$p->HarinaMaxHumedad."</center></th>"
                    . "<th ><center>".(float)$p->HarinappmMin." _ ".(float)$p->HarinappmMax."</center></th>";
                    /*. "<th ><center>".(float)$p->HarinaMinHumedad." _ ".(float)$p->HarinaMaxHumedad."</center></th>";*/
                    /*. "<th ><center>".(float)$p->HarinaTBVNmin." _ ".(float)$p->HarinaTBVNmax."</center></th>"
                    . "<th ><center>".(float)$p->HarinaprotMin." _ ".(float)$p->HarinaprotMax."</center></th>";*/
            $rt = $rt.'</tr>';
            /*CONTENIDO DE LA TABLA*/
            foreach($result as $row){
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0;  }
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                $rt = $rt.'<tr class ="odd" style="font-size:10px;">';
                $hora=  explode(':', $row->Hora);
                $rt =$rt.'<th ><div style="display:flex"><input  id="horahari'.$row->ID.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoshari'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,9)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button><div></th>';
                $rt =$rt.'<td id="hariTem'.$row->ID.'" '.$this->clases((float)$row->Temperatura,$p->HarinaMinTemperatura,$p->HarinaMaxTemperatura,$p->HarinaTemperaturaAlertaNaranja).' >'.number_format($row->Temperatura,2,$decimal,'').'</td>';-
                $rt =$rt.'<td id="hariPorH'.$row->ID.'" '.$this->clases((float)$row->PorcentajeHumedad,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->PorcentajeHumedad,2,$decimal,'').'</td>';
                $rt =$rt.'<td id="hariAO'.$row->ID.'" '.$this->clases((float)$row->AO,$p->HarinappmMin,$p->HarinappmMax,$p->HarinappmAlertaNaranja).' >'.number_format($row->AO,2,$decimal,'').'</td>';
                    /*$rt =$rt.'<td id="hariPeso'.$row->ID.'" '.$this->clases((float)$row->HumedadONline,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->HumedadONline,2,$decimal,'').'</td>';*/
                /*$rt =$rt.'<td id="hariTBVN'.$row->ID.'" '.$this->clases((float)$row->TBVN,$p->HarinaTBVNmin,$p->HarinaTBVNmax,$p->HarinaTBVNAlertaNaranja).' >'.number_format($row->TBVN,2,$decimal,'').'</td>';
                $rt =$rt.'<td id="hariPorP'.$row->ID.'" '.$this->clases((float)$row->PorcentajeProteina,$p->HarinaprotMin,$p->HarinaprotMax,$p->HarinaprotAlertaNaranja).' >'.number_format($row->PorcentajeProteina,2,$decimal,'').'</td>';*/
                $rt =$rt.'<td class="cini pasa" id="codini'.$row->ID.'"  >'.number_format($row->CodigoInicio,0,$decimal,'').'</td>';
                $rt =$rt.'<td class="cfin pasa" id="codfin'.$row->ID.'"  >'.number_format($row->CodigoFin,0,$decimal,'').'</td>';
                $rt =$rt.'<th class="saco pasa" id="hariSacos'.$row->ID.'" >'.number_format($row->SacoProducidos,0,$decimal,'').'</th>';
                /*$rt =$rt.'<td id="hariNumLot'.$row->ID.'" class="pasa" >'.number_format($row->NumeroLote,0,$decimal,'').'</td>';
                $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ClasificacionID,CHtml::listData(Clasificacion::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>' ','class'=>'form-control','style'=>"font-weight:initial; height:25px; font-size:12px;padding:4px;",'id'=>'hariClasif'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';*/
                $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ObservacionID,CHtml::listData(Observacion::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"font-weight:initial;height:25px; font-size:12px;padding:4px;",'id'=>'hariObser'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';
                if($this->idrol()!=4 ):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarharina(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,9);','rel'=>$row->ID,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,9);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                endif;
                $rt = $rt.'</tr>';
            }
            /*FIN DE LA TABLA*/
            $rt =$rt.'</table>';
            if($this->idrol()<3 || $this->idrol()==7 || $this->idrol()==6)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(9, "'.$fecha.'")',array('class'=>'btn btn-success'));
            print $rt;

    }
    public function actionRecepciones(){
        print $this->recepcion($_GET['fecha']);
    }
    function eliminarformato($decimal , $value){
        return str_replace($decimal, '.', $value);;
    }
    public function actionActualizarvalores(){
        $decimal = $this->decimal();
        $retorno = 0;
        $tabla = "recepcion";
        $campo ="ID";
        $id=$_GET['id'];
        $tolva=$_GET['tolva']=='nulo' ? 'NULL':$_GET['tolva'];
        $peso=$_GET['peso']=='nulo' ? 0 : $this->eliminarformato($decimal,$_GET['peso']);
        $descuento=$_GET['desc']=='nulo' ? 0 :$this->eliminarformato($decimal,$_GET['desc']);
        $tarra =$_GET['tarra']=='nulo' ? 0 :$this->eliminarformato($decimal,$_GET['tarra']);
        $pesoneto=$_GET['pesoneto']=='nulo' ? 0 :$this->eliminarformato($decimal,$_GET['pesoneto']);
        $especie=$_GET['especie']=='nulo' ? 'NULL':$_GET['especie'];
        $tbvn=$_GET['tbvn']=='nulo' ? 'NULL':$_GET['tbvn'];
        $prov = $_GET['prov']=='nulo' ? '':$_GET['prov'];
        $placa = $_GET['placa']=='nulo' ? '':$_GET['placa'];
        $secuencia = $_GET['secuencia']=='nulo' ? 0:$_GET['secuencia'];
        $chofer = $_GET['chofer']=='nulo' ? '':$_GET['chofer'];
        $hora = $_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $horatolva=$_GET['horatolva']=='nulo' ? '00:00':$_GET['horatolva'];
        $fechaproduccion = $_GET['fechaproduccion']=='nulo' ?  NULL: $_GET['fechaproduccion'];
        
        $tarra = (int)$tarra/1000;
        $peso = (int)$peso/1000;
        
        $status =Yii::app()->db->createCommand('select IdUsuario from recepcion where ID ='.$id)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0; }
        if($status and $rol==2 and $ban==0){
           $retorno = 2;
        }else{
            $model = Recepcion::model()->findByPk($id);
            $fechaAlterna = $model->fechaproduccion;
            $model->ProveedorId = $prov;
            $model->EspecieID= $especie;
            $model->Hora = $hora;
            $model->Horatolva = $horatolva;
            $model->TolvaID = $tolva;
            $model->PesoTonelada = $peso;
            $model->Descuento = $descuento;
            $model->Pesoneto = $pesoneto;
            $model->Chofer = $chofer;
            $model->Placa = $placa;
            $model->tarracarro = $tarra;
            $model->IdSecuencia = $secuencia;
            $model->fechaproduccion = $fechaproduccion;
            Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$id.",'".$campo."');")->execute();
            if($model->save(false)){
                if($fechaproduccion!=null):
                    $periodo = Periodo::model()->find("fecha = '$fechaproduccion'");
                    $sql = "select IFNULL(sum(Pesoneto),0) total from recepcion where fechaproduccion = '$fechaproduccion'";
                    $t = Yii::app()->db->createCommand($sql)->queryScalar();
                    $periodo->MPProcesada = $t;
                    $periodo->save(false);
                endif;
                if( $fechaAlterna != $fechaproduccion && $fechaAlterna!=NULL && $fechaAlterna!='' ){
                    $periodoAlterno = Periodo::model()->find("fecha = '$fechaAlterna'");
                    $sql = "select IFNULL(sum(Pesoneto),0) total from recepcion where fechaproduccion = '$fechaAlterna'";
                    $tA = Yii::app()->db->createCommand($sql)->queryScalar();
                    $periodoAlterno->MPProcesada = $tA;
                    $periodoAlterno->save(false);
                }
                
                $retorno = 1;
            }
        }
        print $retorno;
    }
    public function actionActualizarvalorescocina(){
        $decimal = $this->decimal();
        $sql= "select ID as CocinaID from cocina where Estado=1 and EstadoCocina=1 order by CocinaID;";
        $tabla = "coccion";
        $campo="codigofila";
        $Cocinas = Yii::app()->db->createCommand($sql)->queryColumn();
        $rpms = $_GET["rpms"];
        $ejes = $_GET["ejes"];
        $chaqs = $_GET["chaqs"];
        $temps = $_GET["temperaturas"];
        $total = $_GET["total"];
        $revision = $_GET['revision'];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $count=0;
        $status =Yii::app()->db->createCommand('select IdUsuario from coccion where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
            for($i=0;$i<$total;$i++){
                $rpm = $rpms[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$rpms[$i]);
                $eje = $ejes[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$ejes[$i]);
                $chaq = $chaqs[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$chaqs[$i]);
                $tem = $temps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$temps[$i]);
                $sql='update coccion set RPM='.$rpm.',PresionEje='.$eje.',PresionChaqueta='.$chaq.',Temperatura='.$tem.',Hora="'.$hora.'" where CocinaID='.$Cocinas[$i].' and codigofila='.$revision;
                Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
                $count =$count +Yii::app()->db->createCommand($sql)->execute();
            }
            
            $sqlnew = 'update coccion set Hora="'.$hora.'" where codigofila='.$revision;
            Yii::app()->db->createCommand($sqlnew)->execute();
            
            if($count>0){
                $count=1;
            }
        }
        echo $count;
    }
    public function actionActualizarprensado(){
        $decimal = $this->decimal();
        $campo="codigofila";	
        $tabla = "prensado";
        $Prensas = Prensa::model()->findAll('Estado =1 and EstadoPrensa=1 order by ID');
        $amps = $_GET["amps"];/*Array*/
        $hums = $_GET["hums"]; /*Array*/
        $solis = $_GET["lich"]=='nulo'?'NULL':$this->eliminarformato($decimal,$_GET["lich"]);
        $grasas = $_GET["licg"]=='nulo'?'NULL':$this->eliminarformato($decimal,$_GET["licg"]);
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $total = $_GET["total"];
        $concentrado = isset($_GET['concentrado']) ? $this->eliminarformato($decimal,$_GET['concentrado']) : 0;
        $revision = $_GET['revision'];
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from prensado where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0; }
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{

            for($i=0;$i<$total;$i++){
                    $amp = $amps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$amps[$i]);
                    $hum = $hums[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$hums[$i]);
                    $sql='update prensado set Amperaje='.$amp.', PorcentajeHumedad='.$hum.',Hora="'.$hora.'" where PrensaID='.$Prensas[$i]->ID.' and codigofila='.$revision;
                    $count =$count +Yii::app()->db->createCommand($sql)->execute();
            }
            
            $sqlnew = 'update prensado set Hora="'.$hora.'" where codigofila='.$revision;
            Yii::app()->db->createCommand($sqlnew)->execute();
            
            $sql='update licor set LicorPorcentajeSolidos='.$solis.',LicorPorcentajeGrasas='.$grasas.' , concentrado='.$concentrado.' where codigofila='.$revision;
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
            Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
           if($count>0){
            $count=1;
        }}
        echo $count;
    }
    public function actionActualizarvaloresrefinacion(){
        $decimal = $this->decimal();
        $campo="codigofila";
        $tabla = "refinacion";
        $Refinaci= Pulidora::model()->findAll('Estado=1 and EstadoPulidora=1 order by ID');
        $Reftemp = $_GET["Reftemp"];
        $Refhum = $_GET["Refhum"];
        $Refsol = $_GET["Refsol"];
        $Refaci = $_GET["Refaci"];
        $total = $_GET["total"];
        $revision = $_GET['revision'];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from refinacion where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol = $this->idrol();
        if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
        for($i=0;$i<$total;$i++){
            $Reftempf = $Reftemp[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Reftemp[$i]);
            $Refhumf = $Refhum[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Refhum[$i]);
            $Refsolf = $Refsol[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Refsol[$i]);
            $Refacif = $Refaci[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Refaci[$i]);
            $sql='update refinacion set AlimentacionTemperatura='.$Reftempf.',AceitePorcentajeHumedad='.$Refhumf.',AceitePorcentajeSolidos='.$Refsolf.',AceitePorcentajeAcidez='.$Refacif.',Hora="'.$hora.'" where PulidoraID='.$Refinaci[$i]->ID.' and codigofila='.$revision;
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
        }
        
        $sqlnew = 'update refinacion set Hora="'.$hora.'" where codigofila='.$revision;
        Yii::app()->db->createCommand($sqlnew)->execute();
        
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
       if($count>0){
            $count=1;
        }}
         echo $count;
    }

    public function actionActualizarvalorestratamiento(){
        $decimal = $this->decimal();
        $campo="codigofila";
        $tabla = "tratamiento";
        $Tratamiento= Planta::model()->findAll('Estado=1 and EstadoPlanta=1 order by ID');
        $Tratemps = $_GET["Tratemp"];
        $Trasolis = $_GET["Trasoli"];
        $Tratemp2s= $_GET["Tratemp2"];
        $Trapres = $_GET["Trapre"];
        $Tratemp3s= $_GET["Tratemp3"];
        $Tratemp4s= $_GET["Tratemp4"];
        $Tratemp5s= $_GET["Tratemp5"];
        $Trasoli2s= $_GET["Trasoli2"];
        $aguaArr =  $_GET["aguaarr"];
        $conArr =  $_GET["concentracionarr"];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $total = $_GET["total"];
        $revision = $_GET['revision'];
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from tratamiento where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol = $this->idrol();
            if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
        for($i=0;$i<$total;$i++){
            $Tratemp = $Tratemps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Tratemps[$i]);
            $Trasoli = $Trasolis[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Trasolis[$i]);
            $Tratemp2 = $Tratemp2s[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Tratemp2s[$i]);
            $Trapre = $Trapres[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Trapres[$i]);
            $Tratemp3 = $Tratemp3s[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Tratemp3s[$i]);
            $Tratemp4 = $Tratemp4s[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Tratemp4s[$i]);
            $Tratemp5 = $Tratemp5s[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Tratemp5s[$i]);
            $Trasoli2 = $Trasoli2s[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Trasoli2s[$i]);
            $agua = $this->eliminarformato($decimal,$aguaArr[$i]);
            $concentrado = $this->eliminarformato($decimal,$conArr[$i]);
            $sql='update tratamiento set AlimentacionAguaColaTemperatura='.$Tratemp.',AlimentacionAguaColaPorcentajeSolidos='.$Trasoli.',VahTemperatura='.$Tratemp2.',VacPeso='.$Trapre.',Temperatura1='.$Tratemp3.',Temperatura2='.$Tratemp4.',Temperatura3='.$Tratemp5.',SalidaPorcentajeSolidos='.$Trasoli2.',Hora="'.$hora.'", aguacola='.$agua.', concentrado='.$concentrado.' where PlantaID='.$Tratamiento[$i]->ID.' and codigofila='.$revision;
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
        }
        $sqlnew = 'update tratamiento set Hora="'.$hora.'" where codigofila='.$revision;
        Yii::app()->db->createCommand($sqlnew)->execute();
        
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
        if($count>0){
            $count=1;
        }}
         echo $count;
    }

    public function actionActualizarvaloresdecantacion(){
        $decimal = $this->decimal();
        $campo="codigofila";
        $tabla = "decantacion";
        $Tricanters = Tricanter::model()->findAll('Estado = 1 and EstadoTricanter=1 order by ID');
        $Detems = $_GET["Detems"];
        $Dehums = $_GET["Dehums"];
        $Desols = $_GET["Desols"];
        $Degras = $_GET["Degras"];
        $total = $_GET["total"];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $revision = $_GET['revision'];
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from decantacion where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
        for($i=0;$i<$total;$i++){
            $Detem = $Detems[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Detems[$i]);
            $Dehum = $Dehums[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Dehums[$i]);
            $Desol = $Desols[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Desols[$i]);
            $Degra = $Degras[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$Degras[$i]);
            $sql='update decantacion set AlimentacionTemperatura='.$Detem.',AlimentacionPorcentajeHumedad='.$Dehum.',AguaColaPorcentajeSolidos='.$Desol.',AguaColaPorcentajeGrasas='.$Degra.',Hora="'.$hora.'" where TricanterID='.$Tricanters[$i]->ID.' and codigofila='.$revision;
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
        }    
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
        if($count>0){
            $count=1;
        }}
        
        $sqlnew = 'update decantacion set Hora="'.$hora.'" where codigofila='.$revision;
        Yii::app()->db->createCommand($sqlnew)->execute();
         echo $count;
    }

    public function actionActualizarvalorespresecado(){
        $decimal = $this->decimal();
        $Rotadisks = Rotadisk::model()->findAll('Estado =1 and EstadoRotadisk=1 order by ID');
        $campo="codigofila";
        $tabla = "presecado";
        $PreAlimMins = $_GET["PreAlimMins"];
        $PreAlimMaxs = $_GET["PreAlimMaxs"];
        $PreAmps = $_GET["PreAmps"];
        $PrePreEjes = $_GET["PrePreEjes"];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $PreChaqs = $_GET["PreChaqs"];
        $PreTemps = $_GET["PreTemps"];
        $PreHums = $_GET["PreHums"];
        $total = $_GET["total"];
        $revision = $_GET['revision'];
        $temp1 = isset($_GET['tempcha1']) ? $_GET['tempcha1'] :[] ;
        $temp2 = isset($_GET['tempcha1']) ? $_GET['tempcha2'] :[] ;
                
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from presecado where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
        if ($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
            for($i=0;$i<$total;$i++){
                $PreAlimMin = $PreAlimMins[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreAlimMins[$i]);
                $PreAlimMax = $PreAlimMaxs[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreAlimMaxs[$i]);
                $PreAmp = $PreAmps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreAmps[$i]);
                $PrePreEje = $PrePreEjes[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PrePreEjes[$i]);
                $PreChaq = $PreChaqs[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreChaqs[$i]);
                $PreTemp = $PreTemps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreTemps[$i]);
                $PreHum = $PreHums[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$PreHums[$i]);
                $t1 = isset($temp1[$i]) ? $this->eliminarformato($decimal,$temp1[$i]) : 0 ;
                $t2 = isset($temp2[$i]) ? $this->eliminarformato($decimal,$temp2[$i])  : 0 ;
                $sql='update presecado set MinimoPorcentajeHumedad='.$PreAlimMin.',MaximoPresion='.$PreAlimMax.',Amperaje='.$PreAmp.',PresionEje='.$PrePreEje.',PresionChaqueta='.$PreChaq.',SCR='.$PreTemp.',PorcentajeHumedad='.$PreHum.',Hora="'.$hora.'", TempCha1 = '.$t1.', TempCha2 = '.$t2.' where RotadiskID='.$Rotadisks[$i]->ID.' and codigofila='.$revision;
                $count = $count +Yii::app()->db->createCommand($sql)->execute();
            }

            $sqlnew = 'update presecado set Hora="'.$hora.'" where codigofila='.$revision;
            Yii::app()->db->createCommand($sqlnew)->execute();
            
        
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
         if($count>0){
            $count=1;
        }}
         echo $count;
    }

    public function actionActualizarvaloressecado(){
        $decimal = $this->decimal();
        $Rotatubos =  Rotatubo::model()->findAll('Estado=1 and EstadoRotatubo=1 order by ID ');
        $campo="codigofila";
        $tabla = "secado";
        $SeAmps = $_GET["SeAmps"];
        $SePreEjes = $_GET["SePreEjes"];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $SeTemps = $_GET["SeTemps"];
        $SeHums = $_GET["SeHums"];
        $total = $_GET["total"];
        $revision = $_GET['revision'];
        $count=0;
        $status =Yii::app()->db->createCommand('select IdUsuario from secado where codigofila ='.$revision)->queryScalar();
        //$status = Yii::app()->user->id;
        $rol =$this->idrol();
        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
        for($i=0;$i<$total;$i++){
            $SeAmp = $SeAmps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$SeAmps[$i]);
            $SePreEje = $SePreEjes[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$SePreEjes[$i]);
            $SeTemp = $SeTemps[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$SeTemps[$i]);
            $SeHum = $SeHums[$i]=='nulo'?'NULL':$this->eliminarformato($decimal,$SeHums[$i]);
            $sql='update secado set Amperaje='.$SeAmp.',PresionEje='.$SePreEje.',SRC='.$SeTemp.',PorcentajeHumedad='.$SeHum.',Hora="'.$hora.'" where RotatuboID='.$Rotatubos[$i]->ID.' and codigofila='.$revision;
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
        }
        
        $sqlnew = 'update secado set Hora="'.$hora.'" where codigofila='.$revision;
        Yii::app()->db->createCommand($sqlnew)->execute();
        
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
         if($count>0){
            $count=1;
        }}
         echo $count;
    }
    public function actionActualizarvaloresharina(){
        $decimal = $this->decimal();
        $campo="ID";
        $tabla = "harina";
        $temp = $_GET["temp"]!="nulo" ? $this->eliminarformato($decimal,$_GET["temp"]):'NULL';
        $hum = $_GET["hum"]!="nulo" ? $this->eliminarformato($decimal,$_GET["hum"]):'NULL';
        $ao = $_GET["ao"]!="nulo" ? $this->eliminarformato($decimal,$_GET["ao"]):'NULL';
        $ini = $_GET["ini"]!="nulo" ? $this->eliminarformato($decimal,$_GET["ini"]):'NULL';
        $fin = $_GET["fin"]!="nulo" ? $this->eliminarformato($decimal,$_GET["fin"]):'NULL';
        $obser = $_GET["obser"]!="nulo" ? $_GET["obser"]:'NULL';
        $sacos = $_GET["sacos"]!="nulo" ? ($fin-$ini)+1:'NULL';
        $revision = $_GET['revision']!="nulo" ? $_GET["revision"]:'0';
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
        $count=0;

        $status =Yii::app()->db->createCommand('select IdUsuario from harina where ID ='.$revision)->queryScalar();
        /* $status = Yii::app()->user->id; */
        $rol = $this->idrol();
        if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
        if($status and $rol!=1 and $ban==0){
           $count = 2;
        }else{
        $sql='update harina set CodigoInicio='.$ini.', CodigoFin='.$fin.', Temperatura='.$temp.', PorcentajeHumedad='.$hum.', AO='.$ao.', SacoProducidos='.$sacos.',Hora="'.$hora.'" ,ObservacionID='.$obser.' where ID='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
         if($count>0){
            $count=1;
        }}
         echo $count;
    }
    public function clases($val,$min,$max,$naranja){
       $retoro_clase="";
       $p_naramin = ($min*$naranja)/100;
       $p_naramax = ($max*$naranja)/100;
       if($val>($min+$p_naramax) && $val<($max-$p_naramax) || $val==0){
           $retoro_clase ="pnormal";
       }else if($val>=$min && $val<=$max) {
           $retoro_clase ="pnaranja";
       }else{
           $retoro_clase ="projo";
       }

       return ' min="'.$min.'" max="'.$max.'" nara="'.$naranja.'" class="'.$retoro_clase.'" ';
    }
    public function actionAgregaregistro($pes, $fecha){
        switch($pes){
            case 1:
                $sql ="insert into recepcion (Fecha, FechaHoraReal) VALUES ('".$fecha."', now());";
            break;
            case 2:
                $sql = "insert into coccion(Fecha,CocinaID) select '".$fecha."',ID from cocina where Estado=1 order by ID";
                break;
        }
        Yii::app()->db->createCommand($sql)->execute();
        $this->redirect(array('index','fecha'=>$fecha));
    }

    public function actionBloquear($recepcion,$tabla){

        switch($tabla){
            case 1:
                    $status =Yii::app()->db->createCommand('select IdUsuario from recepcion where ID ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update recepcion set IdUsuario= Null where ID ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update recepcion set IdUsuario= '.Yii::app()->user->id.' where ID ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 2:
                $status =Yii::app()->db->createCommand('select IdUsuario from coccion where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update coccion set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update coccion set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 3:
                $status =Yii::app()->db->createCommand('select IdUsuario from prensado where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update prensado set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update prensado set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 4:
                $status =Yii::app()->db->createCommand('select IdUsuario from decantacion where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update decantacion set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update decantacion set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 5:
                $status =Yii::app()->db->createCommand('select IdUsuario from refinacion where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0;}
                    if($status and $ban==1){
                        $sql = 'update refinacion set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update refinacion set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 6:
                $status =Yii::app()->db->createCommand('select IdUsuario from tratamiento where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update tratamiento set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update tratamiento set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 7:
                $status =Yii::app()->db->createCommand('select IdUsuario from presecado where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update presecado set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update presecado set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 8:
                $status =Yii::app()->db->createCommand('select IdUsuario from secado where codigofila ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update secado set IdUsuario= Null where codigofila ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update secado set IdUsuario= '.Yii::app()->user->id.' where codigofila ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
            case 9:
                $status =Yii::app()->db->createCommand('select IdUsuario from harina where ID ='.$recepcion)->queryScalar();
                  if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==1){
                        $sql = 'update harina set IdUsuario= Null where ID ='.$recepcion;
                        $img = "images/candadoabierto.jpeg";
                    }
                    else if($status and $ban==0){
                        $sql= null;
                        $img = "images/candadocerrado.png";
                    }else {
                        $sql = 'update harina set IdUsuario= '.Yii::app()->user->id.' where ID ='.$recepcion;
                        $img ="images/candado.png";
                    }
                break;
        }
    if($sql != null)		{
    Yii::app()->db->createCommand($sql)->execute();}
                    echo $img;
    }
    public function actionIdrecepcion(){
        if(isset($_GET['id'])){
            $id= $_GET['id'];
        }else{
            echo '';
            return;
        }
        $modelo = InforFishIngreMatPri::model()->find('ImgresoMatPriId = '.$id);
        if ($modelo!=null):
            print $modelo->CantidadSG.'_'.$modelo->TipoMateriaPrima;
        else:
            print '.';
        endif;

    }
    public function actionVerpdfRango(){
            $d = isset($_GET['d']);
            $fechaini = $_GET['fechaini'];
            $fechafin = $_GET['fechafin'];
            $horaini = $_GET['horaini'];
            $proceso = $_GET['proceso'];
            if(strlen($horaini)<=4){$horaini='0'.$horaini;}
            $horafin = $_GET['horafin'];
            if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
//            if($fechaini!=$fechafin){
//                $horaini='00:00';
//                $horafin='23:59';
//            }
            //devuelve [pag]= cuantas paginacion voy a dar, [mayor] el valor mayor de las pentaña en ese dia
            
            $mPDF = Yii::app()->ePdf->mpdf('utf-8','A4','','',10,10,15,15,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF->useOnlyCoreFonts = true;
            $mPDF->SetTitle("Informe");
            $mPDF->showWatermarkText = true;
            $mPDF->watermark_font = 'DejaVuSansCondensed';
            $mPDF->watermarkTextAlpha = 0.1;
            $mPDF->SetDisplayMode('fullwidth');
            /*Comenzar a cargar datos*/
            
            $pdf = new pdfControlDiaria();
            switch ((int)$proceso){
                case 0:
                    $pdf::pdfGeneral($fechaini,$fechafin,$horaini,$horafin,$d,$this);
                    break;
                
                case 1:
                    $pdf::pdfRecepcion($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 2:
                    
                    $pdf::pdfCoccion($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 3:
                    $pdf::pdfPrensado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 4:
                    $pdf::pdfDecanter($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 5:
                    $pdf::pdfRefinado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 6:
                    $pdf::pdfTratamiento($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 7:
                    $pdf::pdfPresecado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 8:
                    $pdf::pdfSecado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
                case 9:
                    $pdf::pdfHarina($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF,$this);
                    break;
                
            } 
    }
    
    public function actionGenerarExcelprod(){
        $decimal = $this->decimal();
         $fechaini = $_GET['fechaini'];
            $fechafin = $_GET['fechafin'];
            $horaini = $_GET['horaini'];
            $recep = $_GET['hora'];
            if(strlen($horaini)<=4){$horaini='0'.$horaini;}
            $horafin = $_GET['horafin'];
            if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
            if($fechaini==$fechafin):
               $horaini = '00:00';
               $horafin = '23:59';
            endif;
            $proceso = $_GET['proceso'];
            
            $pdf = new excelControlDiaria();
            switch ((int)$proceso){
                case 0:
                    $pdf::pdfGeneral($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 1:
                    $pdf::pdfRecepcion($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 2:
                    
                    $pdf::pdfCoccion($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 3:
                    $pdf::pdfPrensado($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 4:
                    $pdf::pdfDecanter($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 5:
                    $pdf::pdfRefinado($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 6:
                    $pdf::pdfTratamiento($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 7:
                    $pdf::pdfPresecado($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 8:
                    $pdf::pdfSecado($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
                case 9:
                    $pdf::pdfHarina($fechaini,$fechafin,$horaini,$horafin,$this,$decimal);
                    break;
                
            }
    }
    
    public function actionGenerarExcelprodI($fechaini,$fechafin){
            $fechaini = $_GET['fechaini'];
            $fechafin = $_GET['fechafin'];
            $horaini = $_GET['horaini'];
            $recep = $_GET['hora'];
            if(strlen($horaini)<=4){$horaini='0'.$horaini;}
            $horafin = $_GET['horafin'];
            if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
            if($fechaini==$fechafin):
               $horaini = '00:00';
               $horafin = '23:59';
            endif;
            $proceso = $_GET['proceso'];
            //devuelve [pag]= cuantas paginacion voy a dar, [mayor] el valor mayor de las pentaña en ese dia

            /*Comenzar a cargar datos*/
            if($recep=='0'):

$cocina = Cocina::model()->findAll('Estado = 1 order by ID');
            $numerococina = count($cocina);

            /*Prensado Prensas*/
            $prensas = Prensa::model()->findAll('Estado = 1');
            $numeroprensas = count($prensas);

            /*Decantasion Tricanter*/
            $tricanter = Tricanter::model()->findAll('Estado = 1');
            $numerotricanter = count($tricanter);

            /*Refinacion Pulidora*/
            $pulidora = Pulidora::model()->findAll('Estado = 1');
            $numeropulidora = count($pulidora);
            /*Tratamiento Planta*/

            $planta = Planta::model()->findAll('Estado = 1');
            $numeroplantas=count($planta);

            $rotadisk = Rotadisk::model()->findAll('Estado = 1');
            $numerorotadisk = count($rotadisk);

            /*Secado Rotatubo*/
            $rotatubo = Rotatubo::model()->findAll('Estado = 1');
            $numerorotatubo = count($rotatubo);
            /*Parametros*/
            $p = Parametros::model()->find();
                $contenido =$this->renderPartial("ExcelRango", 
                        
                        array('horaini'=>$horaini,'horafin'=>$horafin,'fechainicio'=>$fechaini,'fechafinal'=>$fechafin,'p'=>$p,
                    'numerococina'=>$numerococina,'cocinas'=>$cocina,'numeroprensas'=>$numeroprensas,'prensas'=>$prensas,'numerotricanter'=>$numerotricanter,'tricanter'=>$tricanter,
                    'numeropulidora'=>$numeropulidora,'pulidora'=>$pulidora,'numeroplantas'=>$numeroplantas,'planta'=>$planta,'numerorotadisk'=>$numerorotadisk,'rotadisk'=>$rotadisk,'numerorotatubo'=>$numerorotatubo,'rotatubo'=>$rotatubo), true);
            Yii::app()->request->sendFile('ControlDeProcesoDeProduccion'.date('Y-m-d').'.xls', $contenido);
            else:
                    $contenido =$this->renderPartial("ExcelRecepcion", array('fechainicio'=>$fechaini,'fechafinal'=>$fechafin,), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccion'.date('Y-m-d').'.xls', $contenido);
            endif;
            exit;
            }
            public function actionGenerarExcelRecep($fechaini,$fechafin){
            $fechaini = $_GET['fechaini'];
            $fechafin = $_GET['fechafin'];
            $horaini = $_GET['horaini'];
            if(strlen($horaini)<=4){$horaini='0'.$horaini;}
            $horafin = $_GET['horafin'];
            if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
            //if($fechaini==$fechafin):
               $horaini = '00:00';
               $horafin = '23:59';
            //endif;
                $contenido =$this->renderPartial("ExcelRango", array('fechainicio'=>$fechaini,'fechafinal'=>$fechafin,), true);
            Yii::app()->request->sendFile('ControlDeProcesoDeProduccion'.date('Y-m-d').'.xls', $contenido);
            exit;
                }
    public function actionHoraActual(){
        echo date('H:i');
    }
    public function actionAddFila($fecha,$indice){
        $decimal = $this->decimal();
        $img = 'images/candadoabierto.jpeg';
        $rt='';
        switch($indice):
            case 1:
                $row = new Recepcion;
                $row->FechaHoraReal = Yii::app()->db->createCommand('select now();')->queryScalar();
                $row->Fecha = $fecha;
                if ($row->save()):
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:7px;">';
                    $rt =$rt.'<th id="gruphora'.$row->ID.'" rowspan="1"></th>';
                    $hora=  explode(':', $row->Hora);
                    $rt =$rt.'<th ><div style="display:flex;"><input onblur="cambiohora('.$row->ID.',this.value);" id="hora'.$row->ID.'"  value="'.$hora[0].'" class="hora" size="4" > : <input id="minutos'.$row->ID.'" value="'.$hora[1].'" class="minutos" size="4" ><button rel ="'.$row->ID.'" onclick="horaActual(this,1)" style=";margin-left:4px;" ><span class="glyphicon glyphicon-time"></span></button></div></th>';
                    $rt =$rt.'<th class="" >'.$row->ID.'</th>';
                    $rt =$rt.'<th class="prov" ><select id="prov'.$row->ID.'" class="form-control" style="height:18px; font-size:9px;padding:1px;margin:1px;">'.$this->Proveedores($row->ProveedorId).'</select></th>';
                    $rt =$rt.'<td class="placa text" id="placa'.$row->ID.'">'.$row->Placa.'</td>';
                    $rt =$rt.'<td class="chof text" id="chof'.$row->ID.'">'.$row->Chofer.'</td>';
                    $rt =$rt.'<td class="secu" id="secu'.$row->ID.'">'.$row->IdSecuencia.'</td>';
                    $rt =$rt.'<th ><select id= "especie'.$row->ID.'" class="form-control" style="height:18px; font-size:9px;padding:1px;margin:1px;">'.$this->Especie($row->EspecieID).'</select></th>';
                    $rt =$rt.'<td class="ton" id="peso'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);">'.$row->PesoTonelada.'</td>';
                    $rt =$rt.'<td class="tarra" id="tarra'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);">'.$row->tarracarro.'</td>';
                    $rt =$rt.'<td class="desc" id="desc'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.(float)$row->Descuento.'</td>';
                    $rt =$rt.'<th class="neto" id="pesoneto'.$row->ID.'">'.$row->Pesoneto.'</th>';
                    $rt =$rt.'<th class="neto" id="pesonetoton'.$row->ID.'">'.$row->Pesoneto.'</th>';
                    /*$rt =$rt.'<td id="tbvn'.$row->ID.'">'.$row->TBVN.'</td>';*/
                    $hora=  explode(':', $row->Horatolva);
                    $rt =$rt.'<th ><div style="display:flex;"><input id="horatolva'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutostolva'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActualTolva(this)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></div></th>';
                    $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->TolvaID,CHtml::listData(Tolva::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"height:18px; font-size:9px;padding:1px;margin:1px;",'id'=>'tolva'.$row->ID)).'</th>';
                    $rt = $rt.'<th style="padding:0px;"><input type="text" id="fechaproduccion'.$row->ID.'" class="fechaproduccion" style="width:80px;text-align:center;box-shadow: none;margin:0px;" value="'.$row->fechaproduccion.'"></th>';
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                    endif;
                    if($this->idrol()==1 || $this->idrol()==7):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                    endif;
                    $rt =$rt.'</tr>';
                endif;
                break;
            case 2:
                $cf = date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into coccion(Fecha,codigofila,CocinaID) select '".$fecha."','".$cf."',ID from cocina where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if ($r > 0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('cocina');
                    $cri->condition = "codigofila='$cf' and cocina.EstadoCocina=1 and cocina.Estado = 1";
                    $cri->order = 'CocinaID';
                    $m = Coccion::model()->findAll($cri);
                    $contadortabla=1;
                    $count= count($m);
                    $cocinas = Cocina::model()->findAll('Estado =1 and EstadoCocina=1 order by ID');
                    foreach($m as $row){
                        $p= $cocinas[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0; }//verificar si el Idusuario es el mismo de el user logeado
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);
                            $rt =$rt.'<th ><input  id="horacocc'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoscocc'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,2)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
                        }
                        $rt =$rt.'<td id="rpm'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->RPM,$p->CoccionMinRmp,$p->CoccionMaxRmp,$p->CoccionRpmAlertaNaranja).' >'.(float)$row->RPM.'</td>';
                        $rt =$rt.'<td id="presioneje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->CoccionMinPresionEje,$p->CoccionMaxPresionEje,$p->CoccionPresionEjeAlertaNaranja).'>'.(float)$row->PresionEje.'</td>';
                        $rt =$rt.'<td id="presionchaqueta'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->CoccionMinPresionChaqueta,$p->CoccionMaxPresionChaqueta,$p->CoccionPresionChaquetaAlertaNaranja).'>'.(float)$row->PresionChaqueta.'</td>';
                        $rt =$rt.'<td id="temperatura'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura,$p->CoccionMinTemperatura,$p->CoccionMaxTemperatura,$p->CoccionPresionTemperaturaAlertaNaranja).'>'.(float)$row->Temperatura.'</td>';
                        $contadortabla++;
                    }
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarcocinas(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                            if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,2);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                            endif;
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,2);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                            $rt =$rt.'</tr>';
                endif;
                break;
            case 3:
                $contadortabla=1;
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $prensas= Prensa::model()->findAll('1=1 order by ID');
                $sql = "insert into licor (codigofila) values ('".$cf."');";
                $r = Yii::app()->db->createCommand($sql)->execute();
                $sql = "insert into prensado(Fecha,codigofila,PrensaID) select '".$fecha."','".$cf."',ID from prensa where Estado=1 order by ID";
                $r += Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('prensa');
                    $cri->condition = "codigofila='$cf' and prensa.EstadoPrensa=1";
                    $cri->order = 'PrensaID';
                    $result= Prensado::model()->findAll($cri);
                    $licor = Licor::model()->find("codigofila=".$cf);
                    $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja,LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from parametros LIMIT 1";
                    $p = Yii::app()->db->createCommand($sql)->queryRow();
                    $count= count($result);
                    foreach($result as $row){
                        $pp =$prensas[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                            if($contadortabla==1){
                                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                                    $hora=  explode(':', $row->Hora);
                                    $rt =$rt.'<th ><input  id="horaprensa'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprensa'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,4)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                            }
                    $rt =$rt.'<td id="preAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje, $pp->PrensadoMinAmperaje, $pp->PrensadoMaxAmperaje, $pp->PrensadoAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
                    $rt =$rt.'<td id="preHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$pp->PrensadoMinPorcentajeHumedad, $pp->PrensadoMaxPorcentajeHumedad, $pp->PrensadoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
                    $contadortabla++;
                    if($contadortabla>$count){
                            $contadortabla=1;
                            $rt =$rt.'<td id="preLicH'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeSolidos, $p['LicorMinPorcentajeSolidos'], $p['LicorMaxPorcentajeSolidos'], $p['LicorPorcentajeSolidosAlertaNaranja']).' >'.(float)$licor->LicorPorcentajeSolidos.'</td>';
                            /*$rt =$rt.'<td id="preLicG'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeGrasas, $p['LicorMinPorcentajeGrasas'], $p['LicorMaxPorcentajeGrasas'], $p['LicorPorcentajeGrasasAlertaNaranja']).' >'.(float)$licor->LicorPorcentajeGrasas.'</td>';*/
                            $rt = $rt.'<td id="licCon'.$row->codigofila.'" class="new">'.number_format($licor->concentrado,2,$decimal,'').'</td>';
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Bloquear', array ('onclick'=>'guardarprensado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                            if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Actualizar', array ('onclick'=>'bloquear(this,3);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                            endif;
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,3);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                            $rt =$rt.'</tr>';
                    }
                    }
                endif;
                break;
            case 4:
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into decantacion (Fecha,codigofila,TricanterID) select '".$fecha."','".$cf."',ID from tricanter where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                $tricanter = Tricanter::model()->findAll('Estado=1 and EstadoTricanter=1 order by ID');
                $cri = new CDbCriteria();
                $cri->select = 't.*';
                $cri->with = array('tricanter');
                $cri->condition = "codigofila='$cf' and tricanter.EstadoTricanter=1";
                $cri->order = 'TricanterID';
                $result = Decantacion::model()->findAll($cri);
                $count= count($result);
                $contadortabla=1;
                foreach($result as $row){
                    $p =$tricanter[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){ $ban = 1; }  else {$ban =0; }
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);
                        $rt =$rt.'<th ><input  id="horadeca'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosdeca'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,3)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                    }
                    $rt =$rt.'<td id="Dtem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura,$p->DecantacionMinAlimentacionTemperatura,$p->DecantacionMaxAlimentacionTemperatura,$p->DecantacionAlimentacionTemperaturaAlertaNaranja).' >'.(float)$row->AlimentacionTemperatura.'</td>';
                    /*$rt =$rt.'<td id="Dhum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionPorcentajeHumedad, $p->DecantacionMinAlimentacionPorcentajeHumedad, $p->DecantacionMaxAlimentacionPorcentajeHumedad, $p->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja).' >'.(float)$row->AlimentacionPorcentajeHumedad.'</td>';*/
                    $rt =$rt.'<td id="Dsol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeSolidos,$p->DecantacionMinAguaColaPorcentajeSolidos, $p->DecantacionMaxAguaColaPorcentajeSolidos, $p->DecantacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->AguaColaPorcentajeSolidos.'</td>';
                    /*$rt =$rt.'<td id="Dgra'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeGrasas, $p->DecantacionMinAguaColaPorcentajeGrasas, $p->DecantacionMaxAguaColaPorcentajeGrasas, $p->DecantacionAguaColaPorcentajeGrasasAlertaNaranja).' >'.(float)$row->AguaColaPorcentajeGrasas.'</td>';*/
                    $contadortabla++;
                }
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardardecantacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,4);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,4);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
                endif;
                break;
            case 5:
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into refinacion (Fecha,codigofila,PulidoraID) select '".$fecha."','".$cf."',ID from pulidora where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('pkpulidora');
                    $cri->condition = "codigofila='$cf' and pkpulidora.EstadoPulidora=1";
                    $cri->order = 'PulidoraID';
                    $result = Refinacion::model()->findAll($cri);
                    $contadortabla=1;
                    $pulidora = Pulidora::model()->findAll('Estado=1 and EstadoPulidora=1 order by ID');
                    $count = count($result);
                    foreach($result as $row){
                        $p = $pulidora[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);                   
                            $rt =$rt.'<th ><input  id="horarefi'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosrefi'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,5)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
                        }
                        $rt =$rt.'<td id="retem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura, $p->RefinacionMinAlimentacionTemperatura, $p->RefinacionMaxAlimentacionTemperatura, $p->RefinacionTemperaturaAlertaNaranja).' >'.(float)$row->AlimentacionTemperatura.'</td>';
                        $rt =$rt.'<td id="rehum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeHumedad, $p->RefinacionMinAceitePorcentajeHumedad, $p->RefinacionMaxAceitePorcentajeHumedad, $p->RefinacionAceitePorcentajeHumedadAlertaNaranja).' >'.(float)$row->AceitePorcentajeHumedad.'</td>';
                        $rt =$rt.'<td id="resol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeSolidos, $p->RefinacionMinAceitePorcentajeSolidos, $p->RefinacionMaxAceitePorcentajeSolidos, $p->RefinacionAceitePorcentajeSolidosAlertaNaranja).' >'.(float)$row->AceitePorcentajeSolidos.'</td>';
                        $rt =$rt.'<td id="reAcid'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeAcidez, $p->RefinacionMinAceitePorcentajeAcidez, $p->RefinacionMaxAceitePorcentajeAcidez, $p->RefinacionAceitePorcentajeAcidezAlertaNaranja).' >'.(float)$row->AceitePorcentajeAcidez.'</td>';
                        $contadortabla++;
                    }
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarrefinacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,5);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,5);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
                endif;
                break;
            case 6:
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into tratamiento (Fecha,codigofila,PlantaID) select '".$fecha."','".$cf."',ID from planta where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('planta');
                    $cri->condition = "codigofila='$cf' and planta.EstadoPlanta=1";
                    $cri->order = 'PlantaID';
                    $result = Tratamiento::model()->findAll($cri);
                    $platas = Planta::model()->findAll('Estado = 1 and EstadoPlanta = 1 order by ID');
                    $contadortabla=1;
                    $count = count($result);
                    foreach($result as $row){
                        $p = $platas[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);                        
                            $rt =$rt.'<th ><input  id="horatrata'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutostrata'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,6)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
                        }
                        $rt =$rt.'<td id="tratemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaTemperatura,$p->TratamientoMinAlimentacionAguaColaTemperatura,$p->TratamientoMaxAlimentacionAguaColaTemperatura,$p->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja).' >'.(float)$row->AlimentacionAguaColaTemperatura.'</td>';
                        $rt =$rt.'<td id="trasoli'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->AlimentacionAguaColaPorcentajeSolidos.'</td>';
                        $rt =$rt.'<td id="tratemp2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VahTemperatura,$p->TratamientoMinVahTemperatura,$p->TratamientoMaxVahTemperatura,$p->TratamientoVahTemperaturaAlertaNaranja).' >'.(float)$row->VahTemperatura.'</td>';
                        $rt =$rt.'<td id="trapre'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VacPeso,$p->TratamientoMinVacPeso,$p->TratamientoMaxVacPeso,$p->TratamientoVacPesoAlertaNaranja).' >'.(float)$row->VacPeso.'</td>';
                        $rt =$rt.'<td id="tratemp3'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura1,$p->TratamientoMinTemperatura1,$p->TratamientoMaxTemperatura1,$p->TratamientoTemperatura1AlertaNaranja).' >'.(float)$row->Temperatura1.'</td>';
                        $rt =$rt.'<td id="tratemp4'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura2,$p->TratamientoMinTemperatura2,$p->TratamientoMaxTemperatura2,$p->TratamientoTemperatura2AlertaNaranja).' >'.(float)$row->Temperatura2.'</td>';
                        $rt =$rt.'<td id="tratemp5'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura3,$p->TratamientoMinTemperatura3,$p->TratamientoMaxTemperatura3,$p->TratamientoTemperatura3AlertaNaranja).' >'.(float)$row->Temperatura3.'</td>';
                        $rt =$rt.'<td id="trasoli2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SalidaPorcentajeSolidos,$p->TratamientoMinSalidaPorcentajeSolidos,$p->TratamientoMaxSalidaPorcentajeSolidos,$p->TratamientoSalidaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->SalidaPorcentajeSolidos.'</td>';
                        $rt =$rt.'<td id="aguacola'.$contadortabla.''.$row->codigofila.'" class="new" >'.number_format($row->aguacola,2,$decimal,'').'</td>';
                        $rt =$rt.'<td id="concentrado'.$contadortabla.''.$row->codigofila.'" class="new" >'.number_format($row->concentrado,2,$decimal,'').'</td>';
                        $contadortabla++;
                    }
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardartratamiento(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,6);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,6);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
                endif;
                break;
            case 7:
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into presecado (Fecha,codigofila,RotadiskID) select '".$fecha."','".$cf."',ID from rotadisk where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('rotadisk');
                    $cri->condition = "codigofila='$cf' and rotadisk.EstadoRotadisk=1";
                    $cri->order = 'RotadiskID';
                    $result = Presecado::model()->findAll($cri);
                    $rotadisk = Rotadisk::model()->findAll('Estado =1 and EstadoRotadisk = 1 order by ID');
                    $contadortabla=1;
                    $count = count($result);
                    foreach($result as $row){
                        $p = $rotadisk[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                            if($contadortabla==1){
                                $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                                $hora=  explode(':', $row->Hora);
                                $rt =$rt.'<th ><input  id="horaprese'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprese'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,7)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                            }
                            /*$rt =$rt.'<td id="preseAlimMin'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MinimoPorcentajeHumedad,$p->PresecadoMinMinimoPorcentajeHumedad,$p->PresecadoMaxMinimoPorcentajeHumedad,$p->PresecadoMinimoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->MinimoPorcentajeHumedad.'</td>';/*Columna Alimentacion*/
                            /*$rt =$rt.'<td id="preseAlimMax'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MaximoPresion,$p->PresecadoMinMaximoPresion,$p->PresecadoMaxMaximoPresion,$p->PresecadoMaximoPresionAlertaNaranja).' >'.(float)$row->MaximoPresion.'</td>';/*Columna Alimentacion*/
                            $rt =$rt.'<td id="preseAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->PresecadoMinAmperaje,$p->PresecadoMaxAmperaje,$p->PresecadoAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
                            $rt =$rt.'<td id="presePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->PresecadoMinPresionEje,$p->PresecadoMaxPresionEje,$p->PresecadoPresionEjeAlertaNaranja).' >'.(float)$row->PresionEje.'</td>';
                            
                            $rt =$rt.'<td id="preseTempCha1'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->TempCha1,$p->PresecadoMinTempCha1,$p->PresecadoMaxTempCha1,$p->PresecadoAlertaTempCha1).' >'.number_format($row->TempCha1,0,$decimal,'').'</td>';
                            $rt =$rt.'<td id="preseTempCha2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->TempCha2,$p->PresecadoMinTempCha2,$p->PresecadoMaxTempCha2,$p->PresecadoAlertaTempCha2).' >'.number_format($row->TempCha2,0,$decimal,'').'</td>';
                    
                            /*$rt =$rt.'<td id="presePreChaq'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->PresecadoMinPresionChaqueta,$p->PresecadoMaxPresionChaqueta,$p->PresecadoPresionChaquetaAlertaNaranja).' >'.(float)$row->PresionChaqueta.'</td>';*/
                            $rt =$rt.'<td id="preseTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SCR,$p->PresecadoMinSCR,$p->PresecadoMaxSCR,$p->PresecadoSCRAlertaNaranja).' >'.(float)$row->SCR.'</td>';
                            $rt =$rt.'<td id="preseHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->PresecadoMinPorcentajeHumedad,$p->PresecadoMaxPorcentajeHumedad,$p->PresecadoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
                            $contadortabla++;
                    } 
                     $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarpresecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,7);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,7);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
                endif;
                break;
            case 8:
                $cf =date('ymdHms').''.substr($micro_date = microtime(),2,2);//codigo de fila
                $sql = "insert into secado (Fecha,codigofila,RotatuboID) select '".$fecha."','".$cf."',ID from rotatubo where Estado=1 order by ID";
                $r = Yii::app()->db->createCommand($sql)->execute();
                if($r >0):
                    $cri = new CDbCriteria();
                    $cri->select = 't.*';
                    $cri->with = array('rotatubo');
                    $cri->condition = "codigofila='$cf' and rotatubo.EstadoRotatubo=1";
                    $cri->order = 'RotatuboID';
                    $result = Secado::model()->findAll($cri);
                    $rotatubo = Rotatubo::model()->findAll('Estado = 1 and EstadoRotatubo =1 order by ID');
                    $contadortabla=1;
                    $count =count($result);
                    foreach($result as $row){
                        $p = $rotatubo[$contadortabla-1];
                        $status = $row->IdUsuario;
                        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                            $img = 'images/candado.png';
                        }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                             $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                             $hora=  explode(':', $row->Hora);
                             $rt =$rt.'<th ><input  id="horasecado'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutossecado'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,8)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                        }
                            $rt =$rt.'<td id="seAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->RotatuboMinAmperaje,$p->RotatuboMaxAmperaje,$p->RotatuboAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
                            $rt =$rt.'<td id="sePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->RotatuboMinPresionEje,$p->RotatuboMaxPresionEje,$p->RotatuboPresionEjeAlertaNaranja).' >'.(float)$row->PresionEje.'</td>';
                            $rt =$rt.'<td id="seTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SRC,$p->RotatuboMinSRC,$p->RotatuboMaxSRC,$p->RotatuboSRCAlertaNaranja).' >'.(float)$row->SRC.'</td>';
                            $rt =$rt.'<td id="seHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->RotatuboMinHumedad,$p->RotatuboMaxHumedad,$p->RotatuboHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
                            $contadortabla++;
                    }
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarsecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,8);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,8);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
                endif;
            break;
            case 9:
                $row = new Harina();
                $row->Fecha=$fecha;
                if($row->save()):
                    $status = $row->IdUsuario;
                    $p = Parametros::model()->find();
                    if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0;  }
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    $rt = $rt.'<tr class ="odd" style="font-size:10px;">';
                    $hora=  explode(':', $row->Hora);
                    $rt =$rt.'<th ><div style="display:flex"><input  id="horahari'.$row->ID.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoshari'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,9)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></div></th>';
                    $rt =$rt.'<td id="hariTem'.$row->ID.'" '.$this->clases((float)$row->Temperatura,$p->HarinaMinTemperatura,$p->HarinaMaxTemperatura,$p->HarinaTemperaturaAlertaNaranja).' >'.number_format($row->Temperatura,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="hariPorH'.$row->ID.'" '.$this->clases((float)$row->PorcentajeHumedad,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->PorcentajeHumedad,2,$decimal,'').'</td>';
                    $rt =$rt.'<td id="hariAO'.$row->ID.'" '.$this->clases((float)$row->AO,$p->HarinappmMin,$p->HarinappmMax,$p->HarinappmAlertaNaranja).' >'.number_format($row->AO,2,$decimal,'').'</td>';
                    /*$rt =$rt.'<td id="hariPeso'.$row->ID.'" '.$this->clases((float)$row->HumedadONline,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->HumedadONline,2,$decimal,'').'</td>';
                    /*$rt =$rt.'<td id="hariTBVN'.$row->ID.'" '.$this->clases((float)$row->TBVN,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->TBVN,2,$decimal,'').'</td>';*/
                    /*$rt =$rt.'<td id="hariPorP'.$row->ID.'" '.$this->clases((float)$row->PorcentajeProteina,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja).' >'.number_format($row->PorcentajeProteina,2,$decimal,'').'</td>';*/
                    $rt =$rt.'<td class="cini pasa" id="codini'.$row->ID.'" >'.number_format($row->CodigoInicio,0,$decimal,'').'</td>';
                    $rt =$rt.'<td class="cfin pasa" id="codfin'.$row->ID.'" >'.number_format($row->CodigoFin,0,$decimal,'').'</td>';
                    $rt =$rt.'<th class="saco pasa" id="hariSacos'.$row->ID.'" >'.number_format($row->SacoProducidos,0,$decimal,'').'</th>';
                    $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ObservacionID,CHtml::listData(Observacion::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"font-weight:initial;height:25px; font-size:12px;padding:4px;",'id'=>'hariObser'.$row->ID)).'</th>';
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarharina(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,9);','rel'=>$row->ID,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,9);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                    $rt = $rt.'</tr>';
            endif;
        endswitch;
        echo $rt;
    }
    public function cla($val,$min,$max,$naranja){
       $retoro_clase="";
        $p_naramax = ($max*$naranja)/100;
        $p_naramin = ($min*$naranja)/100;
       if($val>($min+$p_naramax) && $val<($max-$p_naramax) || $val==0){
           $retoro_clase ="pnormal";
       }else if($val>=$min && $val<=$max) {
           $retoro_clase ="pnaranja";
       }else{
           $retoro_clase ="projo";
       }

       return 'class="'.$retoro_clase.'" ';
    }
    
    public function Proveedores($Iden){
        $res = Proveedores::model()->findAll('Estado =1 order by Nombre');
        $r='<option value="" selected >----</option>';;
        foreach($res as $row){
            if($row->ID==$Iden):
            $r.='<option value="'.$row->ID.'" selected >'.$row->Nombre.'</option>';
            else:
            $r.='<option value="'.$row->ID.'">'.$row->Nombre.'</option>';    
            endif;
        }
        return $r;
    }
    public function Especie($Iden){
        $res = Especie::model()->findAll('Estado =1');
        $r='<option value="" selected >----</option>';;
        foreach($res as $row){
            if($row->ID==$Iden):
            $r.='<option value="'.$row->ID.'" selected >'.$row->Nombre.'</option>';
            else:
            $r.='<option value="'.$row->ID.'">'.$row->Nombre.'</option>';    
            endif;
        }
        return $r;
    }
    
    public function actionGetEspecie($hora,$fecha, $sec){
        $sql = "select ImgresoMatPriId as Id,TipoMateriaPrima as Especie, CantidadSG as Peso from [InforFish.IngreMatPri] where ImgresoMatPriId=".$sec."";
        $res = Yii::app()->sqldb->createCommand($sql)->queryRow();
        $sql="select Secuencia from recepcion where RecepcionID =".$sec;

        $existe = Yii::app()->db->createCommand($sql)->queryColumn();
        $exi = count($existe)>0 ? $this->SecuenciaDato($existe[0]): 'no';
        $r="";
        $r=$res['Id'].'_'.$res['Especie'].'_'.$res['Peso'].'_'.$exi;
        echo $r;
    }
    public function actionEliminarregistro($id, $pest){
        $sql="";
        $rol = $this->idrol();
        $result = 1;
        $estadoRecepcion = false;
        switch ($pest):
            case 1:
                $sql = "delete from recepcion where ID=".$id;
                $sqluser = "select IdUsuario from recepcion where ID=".$id;
                $estadoRecepcion = true;
                break;
            case 2:
                $sql = "delete from coccion where codigofila=".$id;
                $sqluser = "select IdUsuario from coccion where codigofila=".$id;
                break;
            case 3:
                $sql = "delete from licor where codigofila=".$id;
                Yii::app()->db->createCommand($sql)->execute();
                $sql = "delete from prensado where codigofila=".$id;
                $sqluser = "select IdUsuario from coccion where codigofila=".$id;
                break;
            case 4:
                $sql = "delete from decantacion where codigofila=".$id;
                $sqluser = "select IdUsuario from decantacion where codigofila=".$id;
                break;
            case 5:
                $sql = "delete from refinacion where codigofila=".$id;
                $sqluser = "select IdUsuario from refinacion where codigofila=".$id;
                break;
            case 6:
                $sql = "delete from tratamiento where codigofila=".$id;
                $sqluser = "select IdUsuario from tratamiento where codigofila=".$id;
                break;
            case 7:
                $sql = "delete from presecado where codigofila=".$id;
                $sqluser = "select IdUsuario from presecado where codigofila=".$id;
                break;
            case 8:
                $sql = "delete from secado where codigofila=".$id;
                $sqluser = "select IdUsuario from secado where codigofila=".$id;
                break;
            case 9:
                $sql = "delete from harina where ID=".$id;
                $sqluser = "select IdUsuario from harina where ID=".$id;
                break;
        endswitch;
        
        if($estadoRecepcion){
            $fechaproduccion = Yii::app()->db->createCommand("select fechaproduccion from recepcion where ID= $id")->queryScalar();
            if($fechaproduccion!=null and trim($fechaproduccion)!="" ){
                $periodo = Periodo::model()->find("fecha = '$fechaproduccion'");
                $sql = "select IFNULL(sum(Pesoneto),0) total from recepcion where fechaproduccion = '$fechaproduccion'";
                $t = Yii::app()->db->createCommand($sql)->queryScalar();
                $periodo->MPProcesada = $t;
                $periodo->save(false);
            }
        }
        
        if($rol==1 || $rol==7):
            Yii::app()->db->createCommand($sql)->execute();
        elseif($rol ==2):
            $user = Yii::app()->db->createCommand($sqluser)->queryScalar();
            if($user == Yii::app()->user->id || $user == null):
                Yii::app()->db->createCommand($sql)->execute();
            else:
                $result =0;
            endif;
        endif;
        echo $result;
    }
    public function actionDetalles(){
        $fecha = isset($_GET['Fecha']) ? $_GET['Fecha'] : date('Y-m-d');
        $detalles = $this->totalesMP($fecha);
        print json_encode($detalles);
    }
    public function totalesMP($fecha){
        $decimal = $this->decimal();
        $grupos= Especiegrupo::model()->findAll('Velocidad =1');
        $grupospeso = Array();
        
        foreach ($grupos as $g):
            $sql = "SELECT especiegrupo.nombre as nombre, IFNULL(sum(pesoneto),0) as peso FROM recepcion join especie on especie.ID = recepcion.EspecieID join especiegrupo on especiegrupo.ID = especie.grupoID where fecha = '$fecha' and especie.grupoID = $g->ID GROUP by grupoID";
            $rowgrupo = Yii::app()->db->createCommand($sql)->queryRow();
            $grupospeso[$g->Nombre]= $rowgrupo!=null ? number_format($rowgrupo['peso'],3,$decimal,'') : number_format(0,3,$decimal,''); 
        endforeach;

        return $grupospeso;
    }
    function decimal(){
        $sql= "select signo_decimal from parametros ";
        $decimal = Yii::app()->db->createCommand($sql)->queryScalar();
        return $decimal;
    }
}
