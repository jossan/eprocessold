<?php

class pdfControlDiaria{
    
    public static function imagenLogo(){
        $img = file_exists('images/logopdf.png') ?  
            'images/logopdf.png' :
        (   file_exists('images/logopdf.jpg') ? 
                'images/logopdf.jpg': 
            file_exists('images/logopdf.jpeg') ?
                'images/logopdf.jpeg':"#"
        );
        return $img ;
    }
    
    public  static function pdfGeneral($fechaini,$fechafin,$horaini,$horafin,$d,$controller){
            $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
            $numerococina = count($cocina);

            /*Prensado Prensas*/
            $prensas = Prensa::model()->findAll('Estado = 1');
            $numeroprensas = count($prensas);

            /*Decantasion Tricanter*/
            $tricanter = Tricanter::model()->findAll('Estado = 1');
            $numerotricanter = count($tricanter);

            /*Refinacion Pulidora*/
            $pulidora = Pulidora::model()->findAll('Estado = 1');
            $numeropulidora = count($pulidora);
            /*Tratamiento Planta*/

            $planta = Planta::model()->findAll('Estado = 1');
            $numeroplantas=count($planta);

            $rotadisk = Rotadisk::model()->findAll('Estado = 1');
            $numerorotadisk = count($rotadisk);

            /*Secado Rotatubo*/
            $rotatubo = Rotatubo::model()->findAll('Estado = 1');
            $numerorotatubo = count($rotatubo);
            /*Parametros*/
            $p = Parametros::model()->find();
            /*Fin de cargar datos*/
            $PAGINADO = 12;
    //        $fechaaux=$model[0]->Fecha.' '.($model[0]->Hora?$model[0]->Hora:'00:00');
    //        $cont=0;

            //
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',5,5,10,10,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                $mPDF1->useOnlyCoreFonts = true;
                $mPDF1->SetTitle("Reporte");
                $mPDF1->SetAuthor("TADEL");
                //$mPDF1->SetWatermarkText("Pacientes");

                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullwidth');
                while($fechaini<=$fechafin):
                    $fechainicial = $fechaini.' '.$horaini;
                    $fechafinal = $fechaini.' '.$horafin;
                    $r = Yii::app()->db->createCommand("CALL valormayor ('".$fechainicial."','".$fechafinal."')")->queryRow();
                    $total=$r['mayor'];
                    for ($i = 0 ; $i < (int)$r['pag'];$i++):
                        $t = $total-($i*$PAGINADO);
                        if($r['pag']>0):
                            $mPDF1->AddPage();
                            $mPDF1->WriteHTML($controller->renderPartial('pdfRango', array(
                                'fechaini'=>$fechaini,
                                'cont'=>$i,
                                'limit'=>$t,
                                'horaini'=>$horaini,
                                'horafin'=>$horafin,
                                'p'=>$p,
                                'url_img'=> pdfControlDiaria::imagenLogo(),
                                'numerococina'=>$numerococina,
                                'cocinas'=>$cocina,
                                'numeroprensas'=>$numeroprensas,
                                'prensas'=>$prensas,
                                'numerotricanter'=>$numerotricanter,
                                'tricanter'=>$tricanter,
                                'numeropulidora'=>$numeropulidora,
                                'pulidora'=>$pulidora,
                                'numeroplantas'=>$numeroplantas,
                                'planta'=>$planta,
                                'numerorotadisk'=>$numerorotadisk,
                                'rotadisk'=>$rotadisk,
                                'numerorotatubo'=>$numerorotatubo,
                                'rotatubo'=>$rotatubo
                                    ), true));
                        endif;
                    endfor;
                    $nuevafecha = strtotime ( '+1 day' , strtotime ( $fechaini ) ) ;
                    $fechaini = date( 'Y-m-d' , $nuevafecha );
                endwhile;
            if($d)
                $mPDF1->Output('pdfRango'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else 
                $mPDF1->Output('pdfRango'.date('Y-m-d H:i').'.pdf','I');
            exit;
    }
    
    public static function pdfRecepcion($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $model = Recepcion::model()->findAll(
                "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini."' and '".$fechafin.' '.$horafin."' order by Fecha, Hora, ID ");
        
        $mPDF1->WriteHTML($controller->renderPartial('phpRecepcion', array(
            'fechaini'=>$fechaini,
            'fechafin'=>$fechafin,
            'horaini'=>$horaini,
            'horafin'=>$horafin,
            'model'=>$model,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), true));
        
        if ($d) $mPDF1->Output('ProduccionDiaria_recepción'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
        else    $mPDF1->Output('ProduccionDiaria_recepción'.date('Y-m-d H:i').'.pdf','I');
            
        exit;
    }
    
    public static function pdfCoccion($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
        $numerococina = count($cocina);
        
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini."' and '".$fechafin.' '.$horafin."'";
        $cri->order = 'Fecha, Hora, codigofila, ID';
        $coccions = Coccion::model()->findAll($cri);
        
        $mPDF1->WriteHTML($controller->renderPartial('phpCoccion', array(
            'result'=>$coccions,
            'model'=>$cocina,
            'url_img'=> pdfControlDiaria::imagenLogo(),
            ), 
                true));
        
        if($d)
                $mPDF1->Output('ProduccionDiaria_coccion'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else 
                $mPDF1->Output('ProduccionDiaria_coccion'.date('Y-m-d H:i').'.pdf','I');
            exit;
    }
    
    public static function pdfPrensado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $prensas = Prensa::model()->findAll('Estado = 1');        
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('prensa');
        $cri->order ='Fecha, Hora, codigofila, PrensaID';
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' ";
        $result = Prensado::model()->findAll($cri);
        $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja,LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from parametros LIMIT 1";
        $p = Yii::app()->db->createCommand($sql)->queryRow();
        
        $mPDF1->WriteHTML($controller->renderPartial('phpPrensado', array(
            'model'=>$prensas,
            'result'=>$result,
            'p'=>$p,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d) $mPDF1->Output('ProduccionDiaria_prensado'.date('Y-m-d H:i').'.pdf','D'); 
        else   $mPDF1->Output('ProduccionDiaria_prensado'.date('Y-m-d H:i').'.pdf','I');
        
        exit;
    }


    public static function pdfDecanter($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $model =  Tricanter::model()->findAll('Estado=1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('tricanter');
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' ";
        $cri->order ='Fecha, Hora, codigofila, TricanterID';
        $result = Decantacion::model()->findAll($cri);
        
        $mPDF1->WriteHTML($controller->renderPartial('phpDecanter', array(
            'model'=>$model,
            'result'=>$result,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d)
                $mPDF1->Output('ProduccionDiaria_decanter'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else 
                $mPDF1->Output('ProduccionDiaria_decanter'.date('Y-m-d H:i').'.pdf','I');
            exit;
    }
    public static function pdfRefinado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $model = Pulidora::model()->findAll('Estado = 1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('pkpulidora');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1";
        $cri->order ='Fecha, Hora, codigofila, PulidoraID';
        $result = Refinacion::model()->findAll($cri);
            
        $mPDF1->WriteHTML($controller->renderPartial('phpRefinado', array(
            'model'=>$model,
            'result'=>$result,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d) $mPDF1->Output('ProduccionDiaria_refinado'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
        else   $mPDF1->Output('ProduccionDiaria_refinado'.date('Y-m-d H:i').'.pdf','I');
            
        exit;
    }
    
    
    public static function pdfTratamiento($fechaini,$fechafin,$horaini,$horafin,$d, $mPDF1,$controller){
        
        $model = Planta::model()->findAll('Estado=1 order by ID');
        
        $cri = new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('planta');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1";
        $cri->order ='Fecha, Hora, codigofila, PlantaID';
        $result = Tratamiento::model()->findAll($cri);
        
        $mPDF1->WriteHTML($controller->renderPartial('phpConcentracion', array(
            'model'=>$model,
            'result'=>$result,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d)
                $mPDF1->Output('ProduccionDiaria_concentracion'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else 
                $mPDF1->Output('ProduccionDiaria_concentracion'.date('Y-m-d H:i').'.pdf','I');
            exit;
    }
    
    public static function pdfPresecado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $model = Rotadisk::model()->findAll('Estado=1 order by ID');
        
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->with = array ('rotadisk');
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' ";
        $cri->order = 'Fecha, Hora, codigofila, RotadiskID';
        $result = Presecado::model()->findAll($cri);
        
        $mPDF1->WriteHTML($controller->renderPartial('phpPresecado', array(
            'model'=>$model,
            'result'=>$result,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d) $mPDF1->Output('ProduccionDiaria_presecado'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
        else   $mPDF1->Output('ProduccionDiaria_presecado'.date('Y-m-d H:i').'.pdf','I');
        
        exit;
    }
    
    public static function pdfSecado($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        $model = Rotatubo::model()->findAll('Estado = 1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('rotatubo');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1 ";
        $cri->order ='Fecha, Hora, codigofila, RotatuboID';
        $result = Secado::model()->findAll($cri);
        
        $mPDF1->WriteHTML($controller->renderPartial('phpSecado', array(
            'model'=>$model,
            'result'=>$result,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d) $mPDF1->Output('ProduccionDiaria_secado'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
        else   $mPDF1->Output('ProduccionDiaria_secado'.date('Y-m-d H:i').'.pdf','I');
            
        exit;
    }
    
    public static function pdfHarina($fechaini,$fechafin,$horaini,$horafin,$d,$mPDF1,$controller){
        
        
        $result = Harina::model()->findAll("CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' order by Fecha, Hora , ID");
        $p= Parametros::model()->find();
        
        $mPDF1->WriteHTML($controller->renderPartial('phpHarina', array(
            'result'=>$result,
            'p'=>$p,
            'url_img'=> pdfControlDiaria::imagenLogo(),
                ), 
                true));
        
        if($d) $mPDF1->Output('ProduccionDiaria_Producto'.date('Y-m-d H:i').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
        else $mPDF1->Output('ProduccionDiaria_Producto'.date('Y-m-d H:i').'.pdf','I');
            
        exit;
    }
}
    
    ?>