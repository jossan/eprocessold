<?php

class ParametrosController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2SinOperaciones';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('aceiteD','index','admin','equivalencia','recibeimagen','decimal','coccionrpm','coccionPresioneje','coccionPresionchaq','coccionTemperatura','prensaAmp','prensadoH','licorS','licorG','decantasionTem','decantasionHum','decantasionaguaS','decantasionaguaG','refinacionTem','aceiteHum','aceiteSol','aceiteA','tratamientoAlimTem','tratamientoAlimSol','tratamientoVahTemp','tratamientoVacPeso','tratamientoTemperatura1','tratamientoTemperatura2','tratamientoTemperatura3','tratamientoSalSol','presecadoMinHum','presecadoMaxPresion','presecadoAmp','presecadoPresEje','presecadoPresChq','presecadoTempSrc','presecadoHumedad','secadoMinHum','secadoMaxPresion','secadoAmp','secadoPresPsi','secadoSrc','secadoHumedad','general','anual','anuala1','anuala2','anualf1','anualf2','anualf3','guardarequivalencias','objetivoHum','objetivoGal','objetivoTVC','generalH','anualH','anuala1H','anuala2H','anualf1H','anualf2H','anualf3H','harina','anualpbH','anuala1pbH','anuala2pbH','anualf1pbH','anualf2pbH','anualf3pbH','anualpb','anuala1pb','anuala2pb','anualf1pb','anualf2pb','anualf3pb','anualpbG','anuala1pbG','anuala2pbG','anualf1pbG','anualf2pbG','anualf3pbG','anualG','anuala1G','anuala2G','anualf1G','anualf2G','anualf3G','anualT','anuala1T','anuala2T','anualf1T','anualf2T','anualf3T','sistema'),
'users'=>CHtml::listData(Usuarios::model()->findAll("TipoRolID=1 or TipoRolID=7"),'Usuario','Usuario'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

public function actionEquivalencia()
{
    $model=new Especiegrupo;
    
    if(isset($_POST['Especiegrupo']))
    {
        $model->attributes=$_POST['Especiegrupo'];
        $model->Especie ="";
        $model->save();
        $this->render('equivalencia',array('model'=>new Especiegrupo));
    }else{
        $this->render('equivalencia',array('model'=>$model));
    }
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Parametros;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Parametros']))
{
$model->attributes=$_POST['Parametros'];
if($model->save())
$this->redirect(array('view','id'=>$model->ID));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Parametros']))
{
$model->attributes=$_POST['Parametros'];
if($model->save())
$this->redirect(array('view','id'=>$model->ID));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Parametros');
$this->render('index',array(
'dataProvider'=>$dataProvider
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
       $anio2 =2015;
       $anio1 =2016;
       $actual = 2017;
       $aniof1 =2018;
       $aniof2 =2019;
       $aniof3 =2020;
       $model=new Parametros('search');
       $model->unsetAttributes();  // clear any default values
       if(isset($_GET['Parametros']))
       $model->attributes=$_GET['Parametros'];

       $valora =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
       $valora1 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
       $valora2 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
       $valorf1 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
       $valorf2 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
       $valorf3 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

       $valorapb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
       $valora1pb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
       $valora2pb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
       $valorf1pb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
       $valorf2pb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
       $valorf3pb =Yii::app()->db->createCommand("Select baseRS from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();
              
       $this->render('admin',array(
       'model'=>$model,'anio2'=>$anio2,'anio1'=>$anio1,'aniof2'=>$aniof2,'aniof3'=>$aniof3,'aniof1'=>$aniof1,'actual'=>$actual,'valora'=>$valora,'valora1'=>$valora1,'valora2'=>$valora2,'valorf1'=>$valorf1,'valorf2'=>$valorf2,'valorf3'=>$valorf3,'valorapb'=>$valorapb,'valora1pb'=>$valora1pb,'valora2pb'=>$valora2pb,'valorf1pb'=>$valorf1pb,'valorf2pb'=>$valorf2pb,'valorf3pb'=>$valorf3pb
       ));
}

public function actionObjetivoHum()
{
   $anio2 =2015;
$anio1 =2016;
$actual = 2017;
$aniof1 =2018;
$aniof2 =2019;
$aniof3 =2020;
$model=new Parametros('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Parametros']))
$model->attributes=$_GET['Parametros'];

$valora =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1 =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2 =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1 =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2 =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
$valorf3 =Yii::app()->db->createCommand("Select ObjetivoHumedad from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

$valorapbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1pbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2pbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1pbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2pbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
$valorf3pbH =Yii::app()->db->createCommand("Select baseHumedad from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

$this->render('objetivoHum',array(
'model'=>$model,'anio2'=>$anio2,'anio1'=>$anio1,'aniof2'=>$aniof2,'aniof3'=>$aniof3,'aniof1'=>$aniof1,'actual'=>$actual,'valora'=>$valora,'valora1'=>$valora1,'valora2'=>$valora2,'valorf1'=>$valorf1,'valorf2'=>$valorf2,'valorf3'=>$valorf3,'valorapbH'=>$valorapbH,'valora1pbH'=>$valora1pbH,'valora2pbH'=>$valora2pbH,'valorf1pbH'=>$valorf1pbH,'valorf2pbH'=>$valorf2pbH,'valorf3pbH'=>$valorf3pbH
));
}

public function actionObjetivoTVC()
{
   $anio2 =2015;
$anio1 =2016;
$actual = 2017;
$aniof1 =2018;
$aniof2 =2019;
$aniof3 =2020;
$model=new Parametros('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Parametros']))
$model->attributes=$_GET['Parametros'];

$valora =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1 =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2 =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1 =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2 =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
$valorf3 =Yii::app()->db->createCommand("Select ObjetivoTVC from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

$this->render('objetivoTVC',array(
'model'=>$model,'anio2'=>$anio2,'anio1'=>$anio1,'aniof2'=>$aniof2,'aniof3'=>$aniof3,'aniof1'=>$aniof1,'actual'=>$actual,'valora'=>$valora,'valora1'=>$valora1,'valora2'=>$valora2,'valorf1'=>$valorf1,'valorf2'=>$valorf2,'valorf3'=>$valorf3
));
}

public function actionObjetivoGal()
{
  $anio2 =2015;
$anio1 =2016;
$actual = 2017;
$aniof1 =2018;
$aniof2 =2019;
$aniof3 =2020;
$model=new Parametros('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Parametros']))
$model->attributes=$_GET['Parametros'];

$valora =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1 =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2 =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1 =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2 =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
$valorf3 =Yii::app()->db->createCommand("Select ObjetivoGal from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

$valorapbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1pbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2pbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1pbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2pbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();
$valorf3pbG =Yii::app()->db->createCommand("Select baseGal from rendimientosemanal where Anio=".$aniof3." LIMIT 1")->queryScalar();

$this->render('objetivoGal',array(
'model'=>$model,'anio2'=>$anio2,'anio1'=>$anio1,'aniof2'=>$aniof2,'aniof3'=>$aniof3,'aniof1'=>$aniof1,'actual'=>$actual,'valora'=>$valora,'valora1'=>$valora1,'valora2'=>$valora2,'valorf1'=>$valorf1,'valorf2'=>$valorf2,'valorf3'=>$valorf3,'valorapbG'=>$valorapbG,'valora1pbG'=>$valora1pbG,'valora2pbG'=>$valora2pbG,'valorf1pbG'=>$valorf1pbG,'valorf2pbG'=>$valorf2pbG,'valorf3pbG'=>$valorf3pbG
));
}

function actionSistema(){
    $estado = 0;    
    
    if(isset($_FILES['uploadedfile'])){
        $name_img = "";
        if(isset($_POST['tipologo']) and $_POST['tipologo']=="sistema"){
            $name_img = 'brand';
        }elseif(isset($_POST['tipologo']) and $_POST['tipologo']=="pdf"){
            $name_img = "logopdf";
        }
        $estado = 1;
        $target_path = Yii::app()->basePath."/../images/";
        $nombre_arr = explode('/',$_FILES['uploadedfile']['type']);
        $target_path_full = $target_path.$name_img.'.'.$nombre_arr[1];

        if(file_exists($target_path.$name_img.'.jpg'))
            unlink($target_path.$name_img.'.jpg');
        if(file_exists($target_path.$name_img.'.jpeg'))
            unlink($target_path.$name_img.'.jpeg');
        if(file_exists($target_path.$name_img.'.png'))
            unlink($target_path.$name_img.'.png');
        if(file_exists($target_path.$name_img.'.gif'))
            unlink($target_path.$name_img.'.gif');

        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path_full)) {
            $this->redirect(array('sistema'));
        }else{
            echo "Ha ocurrido un error, trate de nuevo!";
        } 
    }
    
    if(isset($_POST['empresa'])){
        Yii::app()->db->createCommand("update parametros set  nombreempresa = '".$_POST['empresa']."';")->execute();;
    }
    $empresa = Yii::app()->db->createCommand("select nombreempresa from parametros")->queryScalar();
    $this->render('sistema',['estado'=>$estado,'empresa'=>$empresa]);
    
}

function actionRecibeimagen(){
    
}
/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Parametros::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='parametros-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

public function actionLicorS(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update parametros set LicorMinPorcentajeSolidos = '.$c1.',LicorMaxPorcentajeSolidos = '.$c2.',LicorPorcentajeSolidosAlertaNaranja = '.$c3)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAceiteD(){
    $c1= $_GET['c1'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update parametros set densidad_aceite = '.$c1)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionLicorG(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update parametros set LicorMinPorcentajeGrasas = '.
            $c1.',LicorMaxPorcentajeGrasas = '.$c2.',LicorPorcentajeGrasasAlertaNaranja = '.$c3)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}


public function actionAnual(){
$resul = 0;
$con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set Objetivo='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}

//PBOBJETIVO
public function actionAnualpb(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1pb(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2pb(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1pb(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2pb(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3pb(){
$resul = 0;
$con =0;
$aniof3 =2019;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseRS='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
//HUMEDAD

public function actionAnualH(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1H(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2H(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1H(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2H(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3H(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoHumedad='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualpbH(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1pbH(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2pbH(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1pbH(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2pbH(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3pbH(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set baseHumedad='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}

//GALONES


public function actionAnualG(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1G(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2G(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1G(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2G(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3G(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoGal='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualpbG(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1pbG(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2pbG(){
    $resul = 0;
    $con =0;
 $anio2 =2015;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1pbG(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2pbG(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3pbG(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set BaseGal='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}

//TVC

public function actionAnualT(){
$resul = 0;
    $con =0;
$actual = 2017;
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1T(){
$resul = 0;
    $con =0;
$anio1 =2016;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vanio1.' where Anio='.$anio1)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2T(){
    $resul = 0;
    $con =0;
$anio2 =2015;
$vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vanio2.' where Anio='.$anio2)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1T(){
    $resul = 0;
    $con =0;
$aniof1 =2018;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vaniof1.' where Anio='.$aniof1)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2T(){
$resul = 0;
    $con =0;
$aniof2 =2019;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vaniof2.' where Anio='.$aniof2)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}
public function actionAnualf3T(){
$resul = 0;
    $con =0;
$aniof3 =2020;
$vaniof3= $_GET['aniof3'];
        
$con =Yii::app()->db->createCommand( 'update rendimientosemanal set ObjetivoTVC='.$vaniof3.' where Anio='.$aniof3)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}

public function actionHarina($h1,$h2,$h3,$item){
    $sql = '';
    $con =0;
    switch($item):
        case 1: 
            $sql = "update parametros set HarinaMinTemperatura = $h1 , HarinaMaxTemperatura = $h2 , HarinaTemperaturaAlertaNaranja = $h3";
            break;
        case 2:
            $sql = "update parametros set HarinaMinHumedad = $h1 , HarinaMaxHumedad = $h2 , HarinaHumedadAlertaNaranja = $h3";
            break;
        case 3:
            $sql = "update parametros set HarinappmMin = $h1 , HarinappmMax = $h2 , HarinappmAlertaNaranja = $h3";
            break;
        case 4:
            $sql = "update parametros set HarinaprotMin = $h1 , HarinaprotMax = $h2 , HarinaprotAlertaNaranja = $h3";
            break;
        case 5:
            $sql = "update parametros set HarinaTBVNmin = $h1 , HarinaTBVNmax = $h2 , HarinaTBVNAlertaNaranja = $h3";
            break;
    endswitch;
    $con +=Yii::app()->db->createCommand($sql)->execute();
    print $con>0? 1 : 0;
}

public function actionGuardarequivalencias(){
    
    $especies = isset($_GET['grupos']) ? $_GET['grupos'] : [];
    Yii::app()->db->createCommand("update especie set grupoID = null;")->execute();
    foreach($especies as $especie):
        $modelEspecie = Especie::model()->findByPk($especie['especie']);
        $modelEspecie->grupoID = $especie['grupo'];
        $modelEspecie->save(false);
    endforeach;
    
    print 'si';
}

public function actionDecimal(){
    $signo = $_GET['signos'];
    $signoarr = ['p'=>'.','c'=>','];
    $decimal = $signoarr[$signo];
    if($decimal==null && $decimal==''){
        $decimal = ",";
        $mensaje = "Error al guardar";
    }
    else{
        $sql = "update parametros set signo_decimal = '$decimal'";
        Yii::app()->db->createCommand($sql)->execute();
        $mensaje = "Actualizado sastifactoriamente";
    }
    $this->redirect(['sistema','mensaje'=>$mensaje]);
    
}
function decimal(){
        $sql= "select signo_decimal from parametros ";
        $decimal = Yii::app()->db->createCommand($sql)->queryScalar();
        return $decimal;
}
}
