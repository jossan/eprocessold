<?php

class excelControlDiaria{
    
    public static function imagenLogo(){
        return './images/brand.png';
    }
    
    public  static function pdfGeneral($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
            //devuelve [pag]= cuantas paginacion voy a dar, [mayor] el valor mayor de las pentaña en ese dia

            /*Comenzar a cargar datos*/

            $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
            $numerococina = count($cocina);

            /*Prensado Prensas*/
            $prensas = Prensa::model()->findAll('Estado = 1');
            $numeroprensas = count($prensas);

            /*Decantasion Tricanter*/
            $tricanter = Tricanter::model()->findAll('Estado = 1');
            $numerotricanter = count($tricanter);

            /*Refinacion Pulidora*/
            $pulidora = Pulidora::model()->findAll('Estado = 1');
            $numeropulidora = count($pulidora);
            /*Tratamiento Planta*/

            $planta = Planta::model()->findAll('Estado = 1');
            $numeroplantas=count($planta);

            $rotadisk = Rotadisk::model()->findAll('Estado = 1');
            $numerorotadisk = count($rotadisk);

            /*Secado Rotatubo*/
            $rotatubo = Rotatubo::model()->findAll('Estado = 1');
            $numerorotatubo = count($rotatubo);
            /*Parametros*/
            $p = Parametros::model()->find();
                $contenido =$controller->renderPartial("ExcelRango", array(
                    'horaini'=>$horaini,
                    'horafin'=>$horafin,
                    'fechainicio'=>$fechaini,
                    'fechafinal'=>$fechafin,
                    'p'=>$p,
                    'lic'=>$p,
                    'numerococina'=>$numerococina,
                    'cocinas'=>$cocina,
                    'numeroprensas'=>$numeroprensas,
                    'prensas'=>$prensas,
                    'numerotricanter'=>$numerotricanter,
                    'tricanter'=>$tricanter,
                    'numeropulidora'=>$numeropulidora,
                    'pulidora'=>$pulidora,
                    'numeroplantas'=>$numeroplantas,
                    'planta'=>$planta,
                    'numerorotadisk'=>$numerorotadisk,
                    'rotadisk'=>$rotadisk,
                    'numerorotatubo'=>$numerorotatubo,
                    'rotatubo'=>$rotatubo,
                    'decimal'=>$decimal
                        ), true);
            Yii::app()->request->sendFile('ControlDeProcesoDeProduccion'.date('Y-m-d').'.xls', $contenido);
    }
    
    public static function pdfRecepcion($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model = Recepcion::model()->findAll(
                "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini."' and '".$fechafin.' '.$horafin."' order by Fecha, Hora, ID ");
        
        $contenido =$controller->renderPartial("excelRecepcion", array('fechaini'=>$fechaini,
            'fechafin'=>$fechafin,
            'horaini'=>$horaini,
            'horafin'=>$horafin,
            'model'=>$model,
            'decimal'=>$decimal,), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionRecepcion'.date('Y-m-d').'.xls', $contenido);
    }
    
    public static function pdfCoccion($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
        $numerococina = count($cocina);
        
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini."' and '".$fechafin.' '.$horafin."'";
        $cri->order = 'Fecha, Hora, codigofila, ID';
        $coccions = Coccion::model()->findAll($cri);
        
       
         $contenido =$controller->renderPartial("excelCoccion", array(
            'result'=>$coccions,
            'model'=>$cocina,
             'decimal'=>$decimal), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionCoccion'.date('Y-m-d').'.xls', $contenido);
        
    }
    
    public static function pdfPrensado($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $prensas = Prensa::model()->findAll('Estado = 1');        
        
       $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('prensa');
        $cri->order ='Fecha, Hora, codigofila, PrensaID';
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' ";
        $result = Prensado::model()->findAll($cri);
        $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja,LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from parametros LIMIT 1";
        $p = Yii::app()->db->createCommand($sql)->queryRow();
        
             
        $contenido =$controller->renderPartial("excelPrensado", array(
            'model'=>$prensas,
            'result'=>$result,
            'p'=>$p,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionPrensado'.date('Y-m-d').'.xls', $contenido);
    }


    public static function pdfDecanter($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model =  Tricanter::model()->findAll('Estado=1 and EstadoTricanter=1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('tricanter');
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and tricanter.EstadoTricanter =1";
        $cri->order ='Fecha, Hora, codigofila, TricanterID';
        $result = Decantacion::model()->findAll($cri);
        
        
    $contenido =$controller->renderPartial("excelDecanter", array(
            'model'=>$model,
            'result'=>$result,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionDecanter'.date('Y-m-d').'.xls', $contenido);
    }
    public static function pdfRefinado($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model = Pulidora::model()->findAll('Estado = 1 and EstadoPulidora=1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('pkpulidora');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1 and pkpulidora.EstadoPulidora =1";
        $cri->order ='Fecha, Hora, codigofila, PulidoraID';
        $result = Refinacion::model()->findAll($cri);
            
        $contenido =$controller->renderPartial("excelRefinado", array(
            'model'=>$model,
            'result'=>$result,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionRefinado'.date('Y-m-d').'.xls', $contenido);
    }
    
    
    public static function pdfTratamiento($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model = Planta::model()->findAll('Estado=1 and EstadoPlanta=1 order by ID');
        
        $cri = new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('planta');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1 and planta.EstadoPlanta =1";
        $cri->order ='Fecha, Hora, codigofila, PlantaID';
        $result = Tratamiento::model()->findAll($cri);
        
                
        $contenido =$controller->renderPartial("excelConcentracion", array(
            'model'=>$model,
            'result'=>$result,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionConcentracion'.date('Y-m-d').'.xls', $contenido);
    }
    
    public static function pdfPresecado($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model = Rotadisk::model()->findAll('Estado=1 and EstadoRotadisk=1 order by ID');
        
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->with = array ('rotadisk');
        $cri->condition = "t.Estado=1 and CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' ";
        $cri->order = 'Fecha, Hora, codigofila, RotadiskID';
        $result = Presecado::model()->findAll($cri);
        
             
        $contenido =$controller->renderPartial("excelPresecado", array(
            'model'=>$model,
            'result'=>$result,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionPresecado'.date('Y-m-d').'.xls', $contenido);
    }
    
    public static function pdfSecado($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        $model = Rotatubo::model()->findAll('Estado = 1 and EstadoRotatubo=1 order by ID');
        
        $cri=new CDbCriteria();
        $cri->select ='t.*';
        $cri->with = array ('rotatubo');
        $cri->condition= "CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' and t.Estado=1 and rotatubo.EstadoRotatubo =1";
        $cri->order ='Fecha, Hora, codigofila, RotatuboID';
        $result = Secado::model()->findAll($cri);
        
       
        $contenido =$controller->renderPartial("excelSecado", array(
            'model'=>$model,
            'result'=>$result,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionSecado'.date('Y-m-d').'.xls', $contenido);
    }
    
    public static function pdfHarina($fechaini,$fechafin,$horaini,$horafin,$controller,$decimal){
        
        
        $result = Harina::model()->findAll("CONCAT(Fecha,' ',Hora) between '".$fechaini.' '.$horaini.":00' and '".$fechafin.' '.$horafin."' order by Fecha, Hora , ID");
        $p= Parametros::model()->find();
        
        
        
      $contenido =$controller->renderPartial("excelHarina", array(
                   'result'=>$result,
            'p'=>$p,
            'decimal'=>$decimal
            ), true);
                    Yii::app()->request->sendFile('ControlDeProcesoDeProduccionHarina'.date('Y-m-d').'.xls', $contenido);
    }
}
    
    ?>