<?php

class UsuariosController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('create','delete','index','admin'),
'users'=>CHtml::listData(Usuarios::model()->findAll("TipoRolID=1"),'Usuario','Usuario'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('update','Contrasenia','view','Validarcontra','Guardarcontra','Guardarnueva','deshabilitar'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
    $model =$this->loadModel($id);
    $model->Contrasenia='******';
$this->render('view',array(
'model'=>$model
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Usuarios;

// Uncomment the following line if AJAX validation is needed
 $this->performAjaxValidation($model);

if(isset($_POST['Usuarios']))
{
$model->attributes=$_POST['Usuarios'];
if($model->save()){
    $model->Contrasenia=  md5($model->Contrasenia);
    Yii::app()->db->createCommand("update usuarios set Contrasenia = '".$model->Contrasenia."' where ID =".$model->ID)->execute();
    $this->redirect(array('view','id'=>$model->ID));

}
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
$this->performAjaxValidation($model);

if(isset($_POST['Usuarios']))
{
$model->attributes=$_POST['Usuarios'];

if($model->save()){
$this->redirect(array('view','id'=>$model->ID));

}
}

$this->render('update',array(
'model'=>$model,
));
}

public function actionContrasenia($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Usuarios']))
{
$model->attributes=$_POST['Usuarios'];

if($model->save()){
$this->redirect(array('view','id'=>$model->ID));

}
}

$this->render('Contrasenia',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$model=$this->loadModel($id)->delete();
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Usuarios');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Usuarios('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Usuarios']))
$model->attributes=$_GET['Usuarios'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Usuarios::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='usuarios-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

public function actionValidarcontra(){
    $var = $_GET['contra'];
    $Vvar = md5($var);
    $id = $_GET['idusu'];
            if ($var == NULL) {
                $var = 0;
            }
  $user = Usuarios::model()->find('ID='.$id);
  
  if($user->Contrasenia != $Vvar){
                 echo 'Su contraseña actual no es correcta';
            }else{
              
            }
}

public function actionGuardarcontra(){
    $resul = 0;/*Valor a retornar -> falso*/
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $id = $_GET['idusu'];
        
    if ($c1!=$c2){
    $resul = 0;/*Valor a retornar -> falso*/    
    }else {
    $pass = md5($c1);
    Yii::app()->db->createCommand("update usuarios set Contrasenia = '".$pass."' where ID =".$id)->execute();    
    $resul=1;
    }
    
        
    print $resul;/*Retorno "0" o "1"*/
}

public function actionGuardarnueva(){
    $resul = 0;/*Valor a retornar -> falso*/
    
    $id = $_GET['idusu'];
    $name = Yii::app()->db->createCommand("select Usuario from usuarios where ID =".$id)->queryScalar();
    $pass = md5($name);
    if(Yii::app()->db->createCommand("update usuarios set Contrasenia = '".$pass."' where ID =".$id)->execute()){
     $resul=1;   
    }   
    
 
        
    print $resul;/*Retorno "0" o "1"*/
}

public function actionDeshabilitar(){
    $id = $_GET['pk'];
    $model = Usuarios::model()->findByPk($id);
    $model->activo = !$model->activo;
    $model->save();
    
}
}

