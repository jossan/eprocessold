<?php

include 'pdfControlDiaria.php';

class ControlSemanalController extends Controller
{
    
    public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}
    public function accessRules()
{
return array(
    //funciones de control semanal
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('index','tabla','Actualizartabla','semanas','errorEquivalencia'),
'users'=>CHtml::listData(Usuarios::model()->findAll("TipoRolID=1 OR TipoRolID=7 or TipoRolID=6"),'Usuario','Usuario'),
),//funciones de informes
        array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('produccionSemana','unificadosacosexcel','unificadosacospdf','unificadosacos','produccionSemanaCliente','verpdfSemanalCliente','VerpdfSemanal','generarExcelprodSemanal','grafico','prueba','guardarimg','guardarimg2','eficienciaPlanta','compMateriaPrimaProcesada','ratioconsumo','variacionHumedad','final','guardarimgeficienciaPlanta','guardarimgFinal','guardarimgVariacionhumedad','guardarimgRatioconsumo','guardarimgCompmat','semanas','generarExcelprodSemanalCliente','volumenmensual','guardarimgVolumenmensual'),
'users'=>CHtml::listData(Usuarios::model()->findAll("TipoRolID=1 OR TipoRolID=7 or TipoRolID=4"),'Usuario','Usuario'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}
	public function actionIndex()
	{
            $decimal = $this->decimal();
            $densidad = $this->densidadaceite();
            
            if(isset($_GET['A']) and isset($_GET['M'])):
                $anio = $_GET['A'];
                $mes =$_GET['M'];
            else:
                $anio = date("Y");
                $mes = date("m"); 
            endif;
            $semana_rango = $this->min_max_semana($anio,$mes);

            $this->render('index',array('lim_sem'=>$semana_rango,'mes'=>$mes,'anio'=>$anio,'decimal'=>$decimal,'densidad'=>$densidad /*,'gesp'=>$grupospeso*/));
	}
        
        function min_max_semana($anio,$mes){
            $sql = "select Semana from periodo where Month(Fecha)=$mes and Anio = $anio group by Semana having count(*)>3";
            $resultado =Yii::app()->db->createCommand($sql)->queryColumn();
            return array('minimo'=>min($resultado),'maximo'=>max($resultado));
        }

        
        public function actionErrorEquivalencia(){
            $this->render('error_equivalencia',['mensaje'=>"Error"]);
        }
        public function actionGrafico(){
            if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
                $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            else:
                $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
                     
            $arrayValor = Array();$arraySemana = Array();$arrayObjetivo = Array();$arraybase = Array();
            $sql = Yii::app()->db->createCommand("select rendimientosemanal.semana, Valor from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and periodo.Anio = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and periodo.Anio between ".$anio1." and ".$anio." group by periodo.Anio,semana order by Fecha")->queryAll();
            $sqlobjetivo = Yii::app()->db->createCommand("select rendimientosemanal.semana, Objetivo,baseRS from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and periodo.Anio = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and periodo.Anio between ".$anio1." and ".$anio." group by periodo.Anio,semana order by Fecha")->queryAll();
            $sql2 =Yii::app()->db->createCommand("Select Semana,Anio as anio from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            $sql3 = Yii::app()->db->createCommand("Select Sum(MPProcesada) as SumaProcesada from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            foreach ($sql as $row):
                if ((float)$row['Valor']<=0){
                $arrayValor[] = null;   
                }else{
                $arrayValor[] = (float)$row['Valor'];}
            endforeach;
            foreach ($sqlobjetivo as $row):
                $arraybase[] = (float)$row['baseRS'];
                $arrayObjetivo[] = (float)$row['Objetivo'];
            endforeach;
            foreach ($sql2 as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['anio'];
            endforeach;
            $this->render('grafico',array('arraybase'=>$arraybase,'arrayObjetivo'=>$arrayObjetivo,'arrayValor'=>$arrayValor,'arraySemana'=>$arraySemana,'sql'=>$sql,'sql2'=>$sql2,'sql3'=>$sql3,'sql4'=>$sql4,'anio'=>$anio,'mes'=>$mes,'mes2'=>$mes2,'sqlobjetivo'=>$sqlobjetivo,'anio1'=>$anio1));
        }
        public function SemanaMayor($mes, $anio){
            $sql = "select Semana from periodo where Month(Fecha)=$mes and Anio = $anio group by Semana having count(*)>3 order by Fecha desc limit 1";
            $sem = Yii::app()->db->createCommand($sql)->queryScalar();       
            return ($sem<10 ? '0'.$sem: $sem);
        }
        public function SemanaMenor($mes, $anio){
            $sql = "select Semana from periodo where Month(Fecha)=$mes and Anio = $anio group by Semana having count(*)>3 order by Fecha asc limit 1";
            $sem = Yii::app()->db->createCommand($sql)->queryScalar();
            return ($sem<10 ? '0'.$sem: $sem); 
        }
        
        public function actionEficienciaPlanta(){
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] :date('Y')  ;
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
            $mesFin= isset($_GET['mes2']) ? $_GET['mes2'] : date('m');
            
            $arraySemana = Array();$C = Array();$V = Array();$T = Array();$TVC = Array();$arrayObjetivoPlanta=Array();
            
            $semIni =  $this->SemanaMenor($mesIni, $anioIni);
            $semFin = $this->SemanaMayor($mesFin, $anioFin);
            
            $sql = Yii::app()->db->createCommand("select rendimientosemanal.semana, ObjetivoTVC from rendimientosemanal where Concat(Anio,if(Semana<10,concat('0',Semana),Semana)) between $anioIni$semIni and $anioFin$semFin order by Anio, Semana")->queryAll();
            $sqlC = Yii::app()->db->createCommand("select ROUND(IFNULL((Sum(SacosProducidos)-Sum(SacosBajaCalidad))/Sum(SacosProducidos),0)*100) as C,Semana from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            $sqlV = Yii::app()->db->createCommand("select ROUND(IFNULL((Sum(SacosProducidos)/((Sum(TRTrabajo))*(select MIN(cocina.CapacidadCocina) from cocina WHERE cocina.EstadoCocina=1 and cocina.Estado=1)* Objetivo)),0)*100) as V from periodo join rendimientosemanal on periodo.Anio=rendimientosemanal.Anio and periodo.Semana=rendimientosemanal.Semana where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,periodo.Semana order by Fecha")->queryAll();
            $sqlT= Yii::app()->db->createCommand("select ROUND(IFNULL(Sum(TRTrabajo)/((((sum(HOUR(Concat(datediff(FechaFinProd,Fecha )*24,':00')))+Sum(HOUR(FinProd))+(Sum(MINUTE(Concat(datediff(FechaFinProd,Fecha )*24,':00'))/60)+Sum((MINUTE(FinProd))/60)))-(Sum(HOUR(InicioProd))+Sum(MINUTE(InicioProd)/60)))-(Sum(HOUR(ParadaProgramada))+Sum(MINUTE(ParadaProgramada)/60)))-(Sum(HOUR(ParadaTerceros))+Sum(MINUTE(ParadaTerceros)/60))),0)*100)as T from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();

            $sqlC1 = Yii::app()->db->createCommand("select IFNULL((Sum(SacosProducidos)-Sum(SacosBajaCalidad))/Sum(SacosProducidos),0) as C,Semana from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();
            $sqlV1 = Yii::app()->db->createCommand("select IFNULL((Sum(SacosProducidos)/((Sum(TRTrabajo))*(select MIN(cocina.CapacidadCocina) from cocina WHERE cocina.EstadoCocina=1 and cocina.Estado=1)* Objetivo)),0) as V from periodo join rendimientosemanal on periodo.Anio=rendimientosemanal.Anio and periodo.Semana=rendimientosemanal.Semana where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,periodo.Semana order by Fecha ")->queryAll();
            $sqlT1= Yii::app()->db->createCommand("select IFNULL(Sum(TRTrabajo)/((((sum(HOUR(Concat(datediff(FechaFinProd,Fecha )*24,':00')))+Sum(HOUR(FinProd))+(Sum(MINUTE(Concat(datediff(FechaFinProd,Fecha )*24,':00'))/60)+Sum((MINUTE(FinProd))/60)))-(Sum(HOUR(InicioProd))+Sum(MINUTE(InicioProd)/60)))-(Sum(HOUR(ParadaProgramada))+Sum(MINUTE(ParadaProgramada)/60)))-(Sum(HOUR(ParadaTerceros))+Sum(MINUTE(ParadaTerceros)/60))),0) as T from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,Semana order by Fecha")->queryAll();
            $semana =Yii::app()->db->createCommand("Select Semana,periodo.Anio as anio from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['anio'];
            endforeach;
            foreach ($sql as $row):
                $arrayObjetivoPlanta[]=(float)$row['ObjetivoTVC'];
            endforeach;
            foreach ($sqlC as $rowc):
                if((float)$rowc['C'] <=0){
                    $C[] = null;
                }else{
                    $C[] = (float)$rowc['C'];
                }
            endforeach;
            foreach ($sqlV as $rowv):   
                if((float)$rowv['V'] <=0){
                    $V[] = null;
                }else{
                    $V[] = (float)$rowv['V'];
                }
            endforeach;
            foreach ($sqlT as $rowt):
                if((float)$rowt['T'] <=0){
                $T[] = null;
                }else{
                $T[] = (float)$rowt['T'];    
                }
                
            endforeach;
            for ($i = 0; $i < count($sqlT1); $i++) {
                $Vcita =isset($sqlV1[$i]['V'])? (float)$sqlV1[$i]['V']:0;
                $valor =round((((float)$sqlC1[$i]['C'])*$Vcita*((float)$sqlT1[$i]['T']))*100);
                if($valor <=0){
                    $TVC[] = null;    
                }else {
                    $TVC[] = $valor;
                }
            }
            $this->render('eficienciaPlanta',array('sqlV1'=>$sqlV1,'sqlT1'=>$sqlT1,'sqlC1'=>$sqlC1,'sql2'=>$semana,'arrayObjetivoPlanta'=>$arrayObjetivoPlanta,'sqlV'=>$sqlV,'sqlT'=>$sqlT,'sqlC'=>$sqlC,'TVC'=>$TVC,'C'=>$C,'V'=>$V,'T'=>$T,'arraySemana'=>$arraySemana,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
        }
        public function actionCompMateriaPrimaProcesada(){
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date ('m');
            $mesFin= isset($_GET['mes2']) ? $_GET['mes2'] : date ('m');

            $semIni = $this->SemanaMenor($mesIni, $anioIni);
            $semFin = $this->SemanaMayor($mesFin, $anioFin);
            
            $arraySemana = Array();
            $rendimiento = Array();
            
            $semana = Yii::app()->db->createCommand("select Anio Y,semana Semana, Valor from rendimientosemanal where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Anio, Semana")->queryAll();
            $grupos = Especiegrupo::model()->findAll('velocidad = 1 order by ID');
                    
            foreach ($semana as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['Y'];
                if((float)$row['Valor']<=0)
                    $rendimiento[] = null;    
                else
                    $rendimiento[] = (float)$row['Valor'];
            endforeach;
            
            $this->render('compMateriaPrimaProcesada',array(
                'semanas'=>$semana,
                'rendimiento'=>$rendimiento,
                'arraySemana'=>$arraySemana,
                'anio'=>$anioFin,
                'mes'=>$mesIni,
                'mes2'=>$mesFin,
                'anio1'=>$anioIni,
                'totalgrupo'=>COUNT($grupos),
                'semIni'=>$semIni,
                'semFin'=>$semFin,
                'anioIni'=>$anioIni,
                'anioFin'=>$anioFin,
                'grupos'=>$grupos,
                'totalSemana'=>count($semana)
                    ));
        }
       
public function actionRatioconsumo(){
            $diario = isset($_GET['diario']) ? $_GET['diario'] : 0;
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
            $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
             $arrayValor = Array();
            $arraySemana = Array();
            $arrayObjetivo = Array(); 
            
            if($diario==1){
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesIni, $anioIni);
                
            $semana = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as anio,Fecha as Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, GalonesDiario as galones from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $sql = Yii::app()->db->createCommand("select rendimientosemanal.semana,Anio,GalonesSemana,ObjetivoGal,BaseGal from rendimientosemanal where concat(Anio,if(semana<10, concat('0',semana),semana)) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Anio,semana")->queryAll();
            $sqlgal = Yii::app()->db->createCommand("select Fecha,GalonesBunker as galones,(SacosProducidos)/20 as tn,(SacosProducidos) as sacos from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $objetivo = Yii::app()->db->createCommand("SELECT ObjetivoGal FROM rendimientosemanal WHERE Anio =".$anioIni)->queryScalar();
            $baseh = Yii::app()->db->createCommand("SELECT BaseGal FROM rendimientosemanal WHERE Anio =".$anioIni)->queryScalar();
                foreach ($semana as $row):
                $arraySemana[] = $row['Semana'];
                $arrayObjetivo[] = (float)$objetivo;
                $arrayBase[] = (float)$baseh;
                $arrayValor[] = (float)$row['galones'];
            endforeach;
            
            
                $this->render('ratioconsumo',array('diario'=>1,'sqlgal'=>$sqlgal,'sql2'=>$semana,'sql'=>$sql,'arrayBase'=>$arrayBase,'arrayObjetivo'=>$arrayObjetivo,'arrayValor'=>$arrayValor,'arraySemana'=>$arraySemana,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
            }else{
            $semIni = $this->SemanaMenor($mesIni, $anioIni);
            $semFin =  $this->SemanaMayor($mesFin, $anioFin);
            $sql = Yii::app()->db->createCommand("select rendimientosemanal.semana,Anio,GalonesSemana,ObjetivoGal,BaseGal from rendimientosemanal where concat(Anio,if(semana<10, concat('0',semana),semana)) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Anio,semana")->queryAll();

            $sqlgal = Yii::app()->db->createCommand("select Semana,Sum(GalonesBunker) as galones,Sum(SacosProducidos)/20 as tn,Sum(SacosProducidos) as sacos from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Fecha")->queryAll();
            
            $sql2 =Yii::app()->db->createCommand("Select distinct Anio as Y,Sum(SacosProducidos) as SumaSacos,Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Year(Fecha) as anio from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio, Semana order by Fecha")->queryAll();
            
            foreach ($sql2 as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['Y'];
            
            endforeach;
            foreach ($sql as $row):
                $arrayObjetivo[] = (float)$row['ObjetivoGal'];
                $arrayBase[] = (float)$row['BaseGal'];
                $arrayValor[] = (float)$row['GalonesSemana'];
            endforeach;
            $this->render('ratioconsumo',array('diario'=>0,'sqlgal'=>$sqlgal,'sql2'=>$sql2,'sql'=>$sql,'arrayBase'=>$arrayBase,'arrayObjetivo'=>$arrayObjetivo,'arrayValor'=>$arrayValor,'arraySemana'=>$arraySemana,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
            }
            
            
            
        }
        
        public function actionVariacionHumedad(){
            $diario = isset($_GET['diario']) ? $_GET['diario'] : 0;
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
            $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
            
            if($diario==1){
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesIni, $anioIni);
                $analiarr=Array();$porcentajearr = Array();$arraySemana =Array(); $aux = Array(); $base=Array();
            $semana = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as Y,Fecha as Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $semanayporcentaje =Yii::app()->db->createCommand("select Year(fecha) as Y,ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) as porcentaje, Fecha as Semana,Count(PorcentajeHumedad)as analisis from harina where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin and harina.PorcentajeHumedad > 0  group by Year(Fecha),Fecha order by Fecha")->queryAll();
            $objetivo = Yii::app()->db->createCommand("SELECT ObjetivoHumedad FROM rendimientosemanal WHERE Anio =".$anioIni)->queryScalar();
            $baseh = Yii::app()->db->createCommand("SELECT baseHumedad FROM rendimientosemanal WHERE Anio =".$anioIni)->queryScalar();
            $sqlobjetivo = null;    
                foreach ($semana as $row):
                $arraySemana[] = $row['Semana'];
                $porcentajearr[$row['Y'].''.$row['Semana']] = 0;
                $analiarr[$row['Y'].''.$row['Semana']] = 0;
                $arrayObjetivo[] = (float)$objetivo;
                $base[]= (float)$baseh;
            endforeach;
            
            foreach ($semanayporcentaje as $row):
                $porcentajearr[$row['Y'].''.$row['Semana']] = (float)$row['porcentaje'];
            $analiarr[$row['Y'].''.$row['Semana']] = (float)$row['analisis'];
            endforeach;
            
            foreach ($porcentajearr as $key=>$value):
                if($value<=0){
                $aux[]=null;    
            }else{
                $aux[]=$value;    
            }
            endforeach;
            foreach ($analiarr as $key=>$value):
                $aux2[]=$value;                
            endforeach;
            
            
                $this->render('variacionHumedad',array('diario'=>1,'sql3'=>$aux2,'sql2'=>$semana,'base'=>$base,'arrayObjetivo'=>$arrayObjetivo,'arraySemana'=>$arraySemana,'arrayPorcentaje'=>$aux,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
            }else{
            
            $semIni = $this->SemanaMenor($mesIni, $anioIni);
            $semFin =  $this->SemanaMayor($mesFin, $anioFin);
            
            $analiarr=Array();$porcentajearr = Array();$arraySemana =Array(); $aux = Array(); $base=Array();
            
            $semana = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as Y,Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            $semanayporcentaje =Yii::app()->db->createCommand("select Year(fecha) as Y,ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) as porcentaje, Week(Fecha,3) as Semana,Count(PorcentajeHumedad)as analisis from harina where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin and harina.PorcentajeHumedad > 0  group by Year(Fecha),Week(Fecha,3) order by Fecha")->queryAll();
            $sqlobjetivo = Yii::app()->db->createCommand("select anio ,semana, ObjetivoHumedad,baseHumedad from rendimientosemanal Where concat(Anio,if(semana<10,concat('0',semana),semana)) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Anio,semana")->queryAll();
            //$sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['Y'];
                $porcentajearr[$row['Y'].''.$row['Semana']] = 0;
                $analiarr[$row['Y'].''.$row['Semana']] = 0;
            endforeach;
            
            foreach ($semanayporcentaje as $row):
                $porcentajearr[$row['Y'].''.$row['Semana']] = (float)$row['porcentaje'];
            $analiarr[$row['Y'].''.$row['Semana']] = (float)$row['analisis'];
            endforeach;
            
            foreach ($porcentajearr as $key=>$value):
                if($value<=0){
                $aux[]=null;    
            }else{
                $aux[]=$value;    
            }
            endforeach;
            foreach ($analiarr as $key=>$value):
                $aux2[]=$value;                
            endforeach;
            
            foreach ($sqlobjetivo as $row):
                $arrayObjetivo[] = (float)$row['ObjetivoHumedad'];
                $base[]= (float)$row['baseHumedad'];
            endforeach;
            
            $this->render('variacionHumedad',array('diario'=>0,'sql3'=>$aux2,'sql2'=>$semana,'base'=>$base,'arrayObjetivo'=>$arrayObjetivo,'arraySemana'=>$arraySemana,'arrayPorcentaje'=>$aux,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
            
            }
            
        }
        public function actionFinal(){
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : 01;
            $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
            $mensual = isset($_GET['mensual']) ? $_GET['mensual'] : 0;
if($mensual){
    $anio = $anioIni;
      $arraympprocesada = Array(); $arraytnproducida = Array();$aux=Array();$aux2=Array();$acumulada=Array();$arraytnproducidaacumu=Array();
            $mpproce=0;$tnprodu=0;
            $semana = Yii::app()->db->createCommand("Select Month(Fecha) Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEAR(fecha) = $anio group by year(fecha),month(fecha) order by Fecha")->queryAll();
            $mpprocesadaytn =Yii::app()->db->createCommand("select Sum(MPProcesada) as mpprocesada,ROUND(Sum(SacosProducidos)/20,3) as tnproducida from periodo where YEAR(Fecha)= $anio group by year(fecha),Month(fecha) order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEAR(Fecha) = $anio group by year(fecha),Month(Fecha) order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $acumulada[$row['anio'].''.$row['Semana']] = '';
                $arraytnproducidaacumu[$row['anio'].''.$row['Semana']]= '';
            endforeach;
            
            $arraySemana = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Novienbre','Diciembre','Anual'];
            
            foreach ($mpprocesadaytn as $row):
                $arraympprocesada[] =(float)$row['mpprocesada'];
                $mpproce = $mpproce+$row['mpprocesada'];
                $arraytnproducida[]=(float)$row['tnproducida'];
                $tnprodu = $tnprodu+(float)$row['tnproducida'];
            endforeach;
            
            foreach ($acumulada as $key=>$value):
                $aux[]=$value;
            endforeach;
            $aux[]=$mpproce;
            
            foreach ($arraytnproducidaacumu as $key=>$value2):
                $aux2[]=$value2;
            endforeach;
            $aux2[]=$tnprodu;
            $this->render('final',array('mensual'=>$mensual,
                'sql2'=>$semana,'sql4'=>$sql4,
                'acumulada2'=>$aux2,'acumulada'=>$aux,
                'arraympprocesada'=>$arraympprocesada,'arraytnproducida'=>$arraytnproducida,
                'arraySemana'=>$arraySemana,'anio'=>$anio,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
            
}else{
            $semIni = $this->SemanaMenor($mesIni, $anioIni);
            $semFin =  $this->SemanaMayor($mesFin, $anioFin);

            $arraympprocesada = Array();$arraySemana =Array(); $arraytnproducida = Array();$aux=Array();$aux2=Array();$acumulada=Array();$arraytnproducidaacumu=Array();
            $mpproce=0;$tnprodu=0;
            $semana =Yii::app()->db->createCommand("Select Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            $mpprocesadaytn =Yii::app()->db->createCommand("select Sum(MPProcesada) as mpprocesada,ROUND(Sum(SacosProducidos)/20,3) as tnproducida from periodo where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['anio'];
                $acumulada[$row['anio'].''.$row['Semana']] = '';
                $arraytnproducidaacumu[$row['anio'].''.$row['Semana']]= '';
            endforeach;
            $arraySemana[]= -1;
            
            foreach ($mpprocesadaytn as $row):
                $arraympprocesada[] =(float)$row['mpprocesada'];
                $mpproce = $mpproce+$row['mpprocesada'];
                $arraytnproducida[]=(float)$row['tnproducida'];
                $tnprodu = $tnprodu+(float)$row['tnproducida'];
            endforeach;
            //$arraympprocesada[] = $mpproce;
            
            foreach ($acumulada as $key=>$value):
                $aux[]=$value;
            endforeach;
            $aux[]=$mpproce;
            
            foreach ($arraytnproducidaacumu as $key=>$value2):
                $aux2[]=$value2;
            endforeach;
            $aux2[]=$tnprodu;
            $this->render('final',array('mensual'=>$mensual,'sql2'=>$semana,'sql4'=>$sql4,'acumulada2'=>$aux2,'acumulada'=>$aux,'arraympprocesada'=>$arraympprocesada,'arraytnproducida'=>$arraytnproducida,'arraySemana'=>$arraySemana,'anio'=>$anioFin,'mes'=>$mesIni,'mes2'=>$mesFin,'anio1'=>$anioIni));
}
            
            
        }
        public function actionVolumenmensual(){
            $anio = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $arraympprocesada = Array(); $arraytnproducida = Array();$aux=Array();$aux2=Array();$acumulada=Array();$arraytnproducidaacumu=Array();
            $mpproce=0;$tnprodu=0;
            $semana = Yii::app()->db->createCommand("Select Month(Fecha) mes,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEAR(fecha) = $anio group by year(fecha),month(fecha) order by Fecha")->queryAll();
            $mpprocesadaytn =Yii::app()->db->createCommand("select Sum(MPProcesada) as mpprocesada,ROUND(Sum(SacosProducidos)/20,3) as tnproducida from periodo where YEAR(Fecha)= $anio group by year(fecha),Month(fecha) order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEAR(Fecha) = $anio group by year(fecha),Month(Fecha) order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $acumulada[$row['anio'].''.$row['mes']] = '';
                $arraytnproducidaacumu[$row['anio'].''.$row['mes']]= '';
            endforeach;
            
            $arraySemana = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Novienbre','Diciembre','Anual'];
            
            foreach ($mpprocesadaytn as $row):
                $arraympprocesada[] =(float)$row['mpprocesada'];
                $mpproce = $mpproce+$row['mpprocesada'];
                $arraytnproducida[]=(float)$row['tnproducida'];
                $tnprodu = $tnprodu+(float)$row['tnproducida'];
            endforeach;
            
            foreach ($acumulada as $key=>$value):
                $aux[]=$value;
            endforeach;
            $aux[]=$mpproce;
            
            foreach ($arraytnproducidaacumu as $key=>$value2):
                $aux2[]=$value2;
            endforeach;
            $aux2[]=$tnprodu;
            
            $this->render('volumenMensual',array(
                'sql2'=>$semana,
                'sql4'=>$sql4,
                'acumulada2'=>$aux2,
                'acumulada'=>$aux,
                'arraympprocesada'=>$arraympprocesada,
                'arraytnproducida'=>$arraytnproducida,
                'arraySemana'=>$arraySemana,
                'anio'=>$anio,
                ));
            
        }
        
        public function actionProduccionSemanaCliente()
	{
            $f1 = isset($_POST['inicio']) ? $_POST['inicio'] :  date('Y-m-').'01';
            $f2 = isset($_POST['fin']) ? $_POST['fin'] : date('Y-m-d');
            $cliente = isset($_POST['clientes']) ? $_POST['clientes'] : 0;
//            $AnioIni= isset($_POST['anioini']) ? $_POST['anioini'] : date('Y');
//            $AnioFin= isset($_POST['aniofin']) ? $_POST['aniofin'] : date('Y');
//            $mesIni =  date('m');
//            $mesFin = date('m') ;
//            
//            $semIni = isset($_POST['semini']) ? $_POST['semini'] :(int) $this->SemanaMenor($mesIni, $AnioIni);
//            $semFin = isset($_POST['semfin']) ? $_POST['semfin'] :(int) $this->SemanaMayor($mesFin, $AnioFin);
//            
            
            $this->render('porCliente',array('f1'=>$f1,'f2'=>$f2,'cliente'=>$cliente));
	}
        
        public function actionProduccionSemana()
	{
            $area = isset($_GET['area']) ?  $_GET['area'] : 1;
            $fechaini = isset($_GET['fechaini']) ? $_GET['fechaini'] :  date('Y-m-').'01';
            $fechafin = isset($_GET['fechafin']) ? $_GET['fechafin'] : date('Y-m-d');
            
            $this->render('Rango_Semanal',array(
                'f1'=>$fechaini,
                'f2'=>$fechafin,
                'area'=>$area
                    ));
	}
        
        public function actionTabla($a,$m,$s)
	{
            if($s<0):
                $rango_semana = $this->min_max_semana($a,$m);
            else:
                $rango_semana = array('minimo'=>$s,'maximo'=>$s);
            endif;
            echo $this->tabladatos($rango_semana,$a,$s);
	}
        function PNP($lista, $index=null, $id){
            return CHtml::dropDownList('idpnp'.$id, $index, 
                $lista,array('class'=>'motivo'));
        }
        public function tabladatos($lim_sem,$anio,$s){
            $jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
            $admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
            $calidad = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 6) ? true : false ;
            
            $lectura = 'th';
            $escritura = 'td';
            if($admin || $jefeprod):$filac = $escritura;else:$filac=$lectura;endif;
            if($calidad || $admin):$filasyc = $escritura;else:$filasyc=$lectura;endif;
            /*Separador Decimal*/
            $decimal = $this->decimal();
            /*CARGAR DATOS*/
            $s_min = $lim_sem['minimo'];/*Semana Minima*/
            $s_max = $lim_sem['maximo'];/*Semana Maxima*/
            $L_PNP = CHtml::listData(Paradasnoprogramadas::model()->findAll('Estado =1'),'id','nombre');/*Lista de parada no programada*/
            /*$para_no_prog = CHtml::dropDownList('categories', null, 
                CHtml::listData(Paradasnoprogramadas::model()->findAll('Estado =1'),'id','nombre'),
                array('empty' => '--'));*/
            $periodo = Periodo::model()->findAll("Anio = $anio and Semana BETWEEN $s_min and $s_max order by fecha");
            $grupos = Especiegrupo::model()->findAll('velocidad=1 order by ID');
            $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc, Nombre');
            $criventa=new CDbCriteria();
            $criventa->with = array ('periodo','cliente');
            $criventa->compare('cliente.Estado',1);
            $criventa->compare('periodo.Anio', $anio);
            $criventa->addBetweenCondition('periodo.Semana', $s_min, $s_max);
            $criventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
            $ptventa = PTVenta::model()->findAll($criventa);
            $sql = "select count(*) as total from periodo where Anio = $anio and Semana between $s_min and $s_max group by Semana order by Semana";
            $modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
            $totalClientes = count($clientes);
            $contadorPTVenta=0;
            if($s<0):
                //$sql = "select sum(Peso) from mprocesada join periodo on periodo.ID = mprocesada.PeriodoID join especiegrupo on especiegrupo.id =mprocesada.GrupoID where Anio = $anio and velocidad=1 and Semana between $s_min and $s_max";
                //$mensualtotalmp = Yii::app()->db->createCommand($sql)->queryScalar();
                $sql = "select sum(Valor) from periodo join ptventa on periodo.ID = ptventa.PeriodoID join clientes on clientes.ID = ptventa.clienteID where clientes.Estado=1 and Anio = $anio and Semana between $s_min and $s_max";
                $mensualCliTotal = Yii::app()->db->createCommand($sql)->queryScalar();
                $sql = "select sum(SacosHumedos) as SH, sum(SacosBajaCalidad) as SBC, sum(GalonesBunker)as GB from periodo where Anio = $anio and Semana between $s_min and $s_max";
                $mensualSacos = Yii::app()->db->createCommand($sql)->queryRow();
            endif;
            $tsemana = -1;$tsemana2 = -1;
           
            $peri=0;$peri2=0;

            /*Fin de cargar datos*/

            /*COMENZAR TABLA*/
            $tabla = "";
            $tabla = $tabla.'<table id="gridedi" class="items table table-bordered" ><tr >';
            /*PERIODO*/
            $tabla = $tabla.'<th rowspan="2" style="background:Turquoise;">FECHA</th><th rowspan="2" style="background:Turquoise;">DÍA</th><th rowspan="2" style="background:Turquoise;">SEM</th><th rowspan="2" ></th>';
            /*materia prima*/
            foreach($grupos as $grupo){
                $tabla = $tabla.'<th rowspan="2" class="mp" style="background:Turquoise;" >'.$grupo->Nombre.'</th>';
            }
            $tabla = $tabla.'<th rowspan="2" style="background:Turquoise;"><a href="javascript:visible(\'.mp\',visiblemp);">MP_PROCESADA<a></th>';
                          /*.'<th rowspan="2" style="background:LightSeaGreen;">P. PILOTO</th>';*/
            $tabla = $tabla.'<th rowspan="2" class="tr"  style="background:Wheat;">INICIO_PRODUCCIÓN<br >Materia prima</th>'
                    . '<th rowspan="2" style="background:Wheat;" class="tr" >FECHA_FIN PRODUCCIÓN</th>'
                    . '<th rowspan="2" style="background:Wheat;" class="tr" >FIN_DE_PRODUCCIÓN<BR>Materia prima</th>'
                    . '<th colspan="3" style="background:BurlyWood;" class="tr" >PARADA NO PROGRAMADAS</th>'
                    . '<th rowspan="2" style="background:Wheat;" class="tr" >PARADA_PROGRAMADA</th>'
                    . '<th rowspan="2" style="background:Wheat;" class="tr" >PARADA_TERCEROS</th>'
                    . '<th rowspan="2" style="background:Wheat;"><a href="javascript:visible(\'.tr\',visibletr)" >HORA REAL</a></th>'
                    . '<th rowspan="2" style="background:MediumSeaGreen;">VELOCIDAD PRODUCCIÓN</th>'
                    . '<th rowspan="2" style="background:burlywood;">CONCENTRADO DOSIFICACION M3</th>'
                    . '<th rowspan="2" style="background:burlywood;">% DOSIFICACION</th>'
                    . '<th rowspan="2" style="background:burlywood;">SALDO DE  CONCENTRADO ANTERIOR</th>'
                    . '<th rowspan="2" style="background:burlywood;">SALDO DE CONCENTRADO</th>'
                    . '<th rowspan="2" style="background:burlywood;">AGUA COLA PRODUCIDO</th>'
                    . '<th rowspan="2" style="background:burlywood;">SALDO AGUA COLA</th>';
            
            foreach($clientes as $row):
                if($row->Exportacion):
                    $tabla = $tabla.'<th rowspan="2" class="sp"  style="background:CornflowerBlue;">'.$row->Nombre.'</th>';
                else:
                    $tabla = $tabla.'<th rowspan="2" class="sp"  style="background:DarkGray;">'.$row->Nombre.'</th>';
                endif;
            endforeach;
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;"><a href="javascript:visible(\'.sp\', visiblesp)"> SACOS PRODUCIDOS<a></th>';
            $tabla = $tabla.'<th colspan="6" style="background:LightSalmon;">CALIDAD DEL PRODUCTO TERMINADO</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:Peru;">SACOS HUMEDOS</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:Peru;">SACOS BAJA CALIDAD</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:lightsalmon;"># DE LOTE</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:lightsalmon;">CLASIFICACION</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:DarkSeaGreen;">ACEITE PRODUCIDO LITROS</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:DarkSeaGreen;">ACEITE PRODUCIDO KILOS</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:DarkSeaGreen;">ACEITE DESCONTADO BORRA</th>';
            //$tabla = $tabla.'<th rowspan="2" style="background:DarkSeaGreen;">ACEITE 4</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">RENDIMIENTO DIARIO</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">RENDIMIENTO SEMANAL</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">GALONES BUNKER CALDEROS</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">GALONES BUNKER SECADO</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">GALONES BUNKER SUMATORIA</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">GL/Tn DÍA</th>';
            $tabla = $tabla.'<th rowspan="2" style="background:LightSalmon;">GL/Tn SEMANAL</th>';
            $tabla = $tabla.'</tr>';
            
            /******************/
            $tabla = $tabla.'<tr><th style="background:Wheat;" class="tr">PARADAS__OPERADOR</th ><th class="tr" style="background:Wheat;">PARADAS_MANTENIMIENTO</th>'
                    . '<th class="tr" style="background:Wheat;" >PARADAS_TERCEROS</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">% HUMEDAD</th>';
                       $tabla = $tabla.'<th style="background:LightSalmon;">% PROTEINA</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">% GRASA</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">% CENIZA</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">TOTAL</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">TBVN</th></tr>'; 
            /*COPIA*/
            $tabla = $tabla.'<tr width="0px;"><th ></th><th ></th><th ></th><th></th>';
            /*materia prima*/
            foreach($grupos as $grupo){
                $tabla = $tabla.'<th class="mp" ></th>';}
                $tabla = $tabla.'<th ></th>';
                /*$tabla = $tabla.'<th ></th>';*/
                $tabla = $tabla.'<th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr"></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                $tabla = $tabla.'<th></th>';
                foreach($clientes as $row):
                    $tabla = $tabla.'<th class="sp" ></th>';
                endforeach;
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                //$tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'</tr>';
                /*fin DE COPIA*/
            
            /*DATOS REALES*/
            foreach ($periodo as $row):
                if($row->Dia=='DO'):/*SI ES DOMINGO (se toma la filapara totales)*/
                    $tabla = $tabla.'<tr style="padding:0px;background-color:#EEEEEE;" rel='.$row->ID.' >';
                    $tabla = $tabla.'<th style="padding:0px;" ></th>';
                    $tabla = $tabla.'<th style="padding:0px;"></th>';
                    $tabla = $tabla.'<th style="padding:0px;" id="Semana'.$row->ID.'" name="Sem'.$row->Semana.'"></th>';
                    $tabla =$tabla.'<th style="padding:0px;padding-right:5.5px;" ></th>';
                    $c=0;

                    $sql = "select sum(mpprocesada) from periodo where Semana = $row->Semana and Anio=$anio";
                    $valorSemanal = Yii::app()->db->createCommand($sql)->queryScalar();
                    $sql = "select grupoID as grupo, IFNULL(sum(Pesoneto),0) as peso from recepcion join especie on especie.ID = EspecieID join periodo on periodo.fecha = recepcion.fechaproduccion where Semana = $row->Semana and Anio=$anio group by grupoID;";
                    $tt = Yii::app()->db->createCommand($sql)->queryAll();
                    $valgrupo = array();
                    foreach($tt as $t):
                        $valgrupo[$t['grupo']] = $t['peso'] ;
                    endforeach;
                    foreach($grupos as $grupo):
                        $totalespecie = isset($valgrupo[$grupo->ID]) ? $valgrupo[$grupo->ID] : 0;
                        $porcentaje = (($valorSemanal==0 ) ? 0 : ($totalespecie/$valorSemanal)*100);
                        $tabla = $tabla.'<th class="mp" style="padding:0px;" id="g'.$grupo->ID.''.$row->ID.'">'.number_format($porcentaje,1,'.','').'%</th>';
                    endforeach;
                    
                    $valores = Array();
                    $tabla = $tabla.'<th style="padding:0px;" id="MP'.$row->ID.'" >'.number_format($valorSemanal,3,$decimal,'').'</th>';
                    /*$tabla = $tabla.'<th style="padding:0px;"></th>';*/
                    $sql = "select sum(SacosHumedos) as SH, sum(SacosBajaCalidad) as SBC, sum(GalonesBunker)as GB from periodo where Semana = $row->Semana and Anio=$anio";
                    $totalSacos = Yii::app()->db->createCommand($sql)->queryRow();
                    $tabla = $tabla.'<th class="tr"></th><th class="tr"></th>';
                    $tabla = $tabla.'<th class="tr"></th><th class="tr"></th>';
                    $tabla = $tabla.'<th class="tr"></th><th class="tr"></th>';
                    $tabla = $tabla.'<th class="tr"></th>';
                    $tabla = $tabla.'<th class="tr"></th>';
                    $tabla = $tabla.'<th></th>';
                    $sqlvelocidadtotal = "SELECT IFNULL(round(sum( MPProcesada-piloto)/sum( TRTrabajo),2)  , 0) FROM periodo WHERE Anio = $row->Anio and semana = $row->Semana";
                    $totalvelocidad = Yii::app()->db->createCommand($sqlvelocidadtotal)->queryScalar();
                    $tabla = $tabla.'<th style="padding:1px;" >'.$totalvelocidad.'</th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $sql = "select sum(Valor) from periodo join ptventa on periodo.ID = ptventa.PeriodoID join clientes on clientes.ID = ptventa.clienteID where clientes.Estado=1 and periodo.Semana = $row->Semana and Anio=$anio group by clienteID order by Exportacion Desc, Nombre";
                    $CliTotal = Yii::app()->db->createCommand($sql)->queryColumn();
                    $T = 0;
                    for($i = 0;$i<($totalClientes);$i++):
                        $T +=$CliTotal[$i];
                        $V = number_format($CliTotal[$i],0,$decimal,'');
                        $tabla = $tabla.'<th class="sp"  style="padding:0px;" >'.$V.'</th>';$contadorPTVenta++;
                    endfor;
                    $tabla = $tabla.'<th style="padding:0px;">'.number_format($T,0,$decimal,'').'</th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th></th>';
                    $tabla = $tabla.'<th style="padding:0px;">'.number_format($totalSacos['SH'],0,$decimal,'').'</th>'
                            . '<th style="padding:0px;">'.number_format($totalSacos['SBC'],0,$decimal,'').'</th>'
                            . '<th></th>'
                            . '<th></th>'
                            . '<th></th>'
                            . '<th></th>'
                            . '<th></th>'
                            . '<th></th>'
                            //. '<th></th>'
                            . '<th></th>'
                            . '<th></th>'
                            . '<th style="padding:0px;">'.number_format($totalSacos['GB'],0,$decimal,'').'</th>'
                            . '<th></th>'; 
                else:
                    $tabla = $tabla.'<tr style="padding:0px;" rel='.$row->ID.'>';
                    $tabla = $tabla.'<th style="padding:0px;" id="Fecha'.$row->ID.'">'.$row->Fecha.'</th>';
                    $tabla = $tabla.'<th style="padding:0px;">'.$row->Dia.'</th>';
                    $tabla = $tabla.'<th style="padding:0px;" id="Semana'.$row->ID.'" name="Sem'.$row->Semana.'">'.$row->Semana.'</th>';
                    if($admin || $calidad || $jefeprod):
                        $tabla =$tabla.'<th style="padding:0px;padding-right:5.5px;" >'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row['ID'],'total'=>$totalClientes,'style'=>'margin:0px;')).'</th>';
                    else:
                        $tabla =$tabla.'<th style="padding:0px;padding-right:5.5px;" ></th>';
                    endif;
                    $c=0;
                    
                    /*MP_PROCESADA*/
                    $sql = "select grupoID as grupo, IFNULL(sum(Pesoneto),0) as peso from recepcion join especie on especie.ID = EspecieID where fechaproduccion = '$row->Fecha' group by grupoID;";
                    $tt = Yii::app()->db->createCommand($sql)->queryAll();
                    $valgrupo = array();
                    foreach($tt as $t):
                        $valgrupo[$t['grupo']] = $t['peso'] ;
                    endforeach;
                    foreach($grupos as $grupo):
                        $totalespecie = isset($valgrupo[$grupo->ID]) ? $valgrupo[$grupo->ID] : 0;
                        $tabla = $tabla.'<th class="mp" style="padding:0px;" id="g'.$grupo->ID.''.$row->ID.'">'.number_format($totalespecie,3,$decimal,'').'</th>';
                    endforeach;
                    /*FIN deGRUPOS*/
                    
                    $tabla = $tabla.'<th style="padding:0px;" id="MP'.$row->ID.'" >'.number_format($row->MPProcesada,3,$decimal,'').'</th>';
                    /*$tabla = $tabla.'<td id="pp'.$row->ID.'" class="pp" style="padding: 0px;" >'.number_format($row->piloto,3,$decimal,'').'</td>';*/
                    if($jefeprod || $admin):
                    $InicioProd=  explode(':', $row->InicioProd);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="inicioprodh'.$row->ID.'" class="hora" size="3" value="'.$InicioProd[0].'"> <input id="inicioprodm'.$row->ID.'" class="minutos" size="3" value="'.$InicioProd[1].'"></th>';
                    if($row->Dia=='DO'):
                        $tabla = $tabla.'<th class="tr" style="padding:0px;" ><input type="text" id="fechafin'.$row->ID.'" style="box-shadow:none;outline: none;text-align:center;background-color:#EEEEEE;" value="'.$row->FechaFinProd.'" class="fechafin"></th>';
                    else:
                        $tabla = $tabla.'<th class="tr" style="padding:0px;" ><input type="text" id="fechafin'.$row->ID.'" style="box-shadow:none;outline: none;text-align:center;" value="'.($row->FechaFinProd ? $row->FechaFinProd : $row->Fecha) .'" class="fechafin"></th>';
                    endif;
                    
                    $FinProd=  explode(':', $row->FinProd);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="finprodh'.$row->ID.'" class="hora" size="3" value="'.$FinProd[0].'"> <input id="finprodm'.$row->ID.'" class="minutos" size="3" value="'.$FinProd[1].'"></th>';
                    $ParadanoProgramada =  explode(':', $row->ParadanoProgramada);
                    $ParadanoProgramadaMant =  explode(':', $row->ParadanoProgramadaMant);
                    $ParadanoProgramadaTerc =  explode(':', $row->ParadanoProgramadaTerc);
                    $tabla = $tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="noprogoh'.$row->ID.'" class="hora" size="3" value="'.$ParadanoProgramada[0].'"> <input id="noprogom'.$row->ID.'" class="minutos" size="3" value="'.$ParadanoProgramada[1].'"></th>';
                    $tabla = $tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="noprogmh'.$row->ID.'" class="hora" size="3" value="'.$ParadanoProgramadaMant[0].'"> <input id="noprogmm'.$row->ID.'" class="minutos" size="3" value="'.$ParadanoProgramadaMant[1].'"></th>';
                    $tabla = $tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="noprogth'.$row->ID.'" class="hora" size="3" value="'.$ParadanoProgramadaTerc[0].'"> <input id="noprogtm'.$row->ID.'" class="minutos" size="3" value="'.$ParadanoProgramadaTerc[1].'"></th>';
                    //$tabla = $tabla."<th class='tr' style='padding:0px;width:130px;' >".$this->PNP($L_PNP,$row->idpnp,$row->ID)."</th>";
                    $ParadaProgramada=  explode(':', $row->ParadaProgramada);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="progh'.$row->ID.'" class="hora" size="3" value="'.$ParadaProgramada[0].'"> <input id="progm'.$row->ID.'" class="minutos" size="3" value="'.$ParadaProgramada[1].'"></th>';
                    
                    $ParadaTerceros=  explode(':', $row->ParadaTerceros);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;" ><input  id="tercerh'.$row->ID.'" class="hora" size="3" value="'.$ParadaTerceros[0].'"> <input id="tercerm'.$row->ID.'" class="minutos" size="3" value="'.$ParadaTerceros[1].'"></th>';
                    else:
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->InicioProd,0,5).'</th>';
                    if($row->Dia=='DO'):
                        $tabla = $tabla.'<th  class="tr" style="padding:0px;" >'.$row->FechaFinProd.'</th>';
                    else:
                        $tabla = $tabla.'<th  class="tr" style="padding:0px;" >'.($row->FechaFinProd ? $row->FechaFinProd : $row->Fecha) .'</th>';
                    endif;
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->FinProd,0,5).'</th>';
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->ParadanoProgramada,0,5).'</th>';
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->ParadanoProgramadaMant,0,5).'</th>';
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->ParadanoProgramadaTerc,0,5).'</th>';
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->ParadaProgramada,0,5).'</th>';
                    $tabla =$tabla.'<th  class="tr" style="padding:0px;width:130px;">'.substr($row->ParadaTerceros,0,5).'</th>';
                    endif;
                $tabla = $tabla.'<th style="padding:0px;" id="TR'.$row->ID.'" >'.number_format($row->TRTrabajo,2,$decimal,'').'</th>';
/*FIN DE HORA REAL */
                $vp = null;
                
                if ($row->TRTrabajo <=0):
                    $vp = 0;
                else:
                    $vp = ($row->MPProcesada-$row->piloto) / $row->TRTrabajo;
                endif;
                
                $tabla = $tabla.'<th id="VP'.$row->ID.'" style="padding: 0px;" >'.number_format(($vp),2,$decimal,'').'</th>';
                /*Inicio nuevas*/
                $conc = Yii::app()->db->createCommand("SELECT sum( concentrado )
FROM prensado
JOIN licor ON licor.codigofila = prensado.codigofila
WHERE fecha = '$row->Fecha'
GROUP BY PrensaID
LIMIT 1")->queryScalar()/1000;
                $produccioncon = Yii::app()->db->createCommand("select sum(concentrado) from tratamiento where fecha='$row->Fecha'")->queryScalar();;
                if($row->MPProcesada<=0):
                    $dosificacion =0;
                    else:
                        $dosificacion = ($conc/$row->MPProcesada)*100;
                endif;
                $aguaprocesada = Yii::app()->db->createCommand("select sum(aguacola) from tratamiento where fecha='$row->Fecha'")->queryScalar();
                $saldoconcentrado = ($row->concentradoanterior+$produccioncon)-$conc;
                $saldoaguacola = $row->aguacolaproducida-$aguaprocesada;
                $tabla = $tabla.'<th style="padding:0px;" class="condof" id="condof'.$row->ID.'" >'.number_format($conc,3,$decimal,'').'</th>';
                $tabla = $tabla.'<th style="padding:0px;" class="dosif" id="dosif'.$row->ID.'" >'.number_format($dosificacion,3,$decimal,'').'</th>';
                $tabla = $tabla.'<'.$filac.' style="padding:0px;" class="conant" id="conant'.$row->ID.'" >'.number_format($row->concentradoanterior,3,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" class="salcon" id="salcon'.$row->ID.'" >'.number_format($saldoconcentrado,3,$decimal,'').'</th>';
                $tabla = $tabla.'<'.$filac.' style="padding:0px;" class="aguacpro" id="aguacpro'.$row->ID.'" >'.number_format($row->aguacolaproducida,3,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" class="saldocola" id="saldocola'.$row->ID.'" >'.number_format($saldoaguacola,3,$decimal,'').'</th>';
                /*Fin nuevas*/
/*INICIO DE CLIENTES (SACOS PRODUCIDOS)*/
                for($i = 0;$i<($totalClientes);$i++):
                    $tabla = $tabla.'<'.$filasyc.' class="sp"  style="padding:0px;" id="valorc'.$i.''.$ptventa[$contadorPTVenta]->PeriodoID.'" >'.number_format($ptventa[$contadorPTVenta++]->Valor,0,$decimal,'').'</td>';
                endfor;
                $tabla = $tabla.'<th style="padding:0px;" id="SP'.$row->ID.'" >'.number_format($row->SacosProducidos,0,$decimal,'').'</th>';
/*FIN (SACOS PRODUCIDOS)*/
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="calpts" id="porhumedad'.$row->ID.'" >'.number_format($row->porhumedad,2,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="calpts" id="proteina'.$row->ID.'" >'.number_format($row->porproteina,2,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="calpts" id="grasa'.$row->ID.'" >'.number_format($row->porgrasa,2,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="calpts" id="ceniza'.$row->ID.'" >'.number_format($row->porceniza,2,$decimal,'').'</td>';
                $porsuma = $row->porhumedad+$row->porproteina+$row->porgrasa+$row->porceniza;
                $tabla = $tabla.'<th style="padding:0px;" class="calpt" id="porsuma'.$row->ID.'" >'.number_format($porsuma,2,$decimal,'').'</th>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="calpt" id="tbvn'.$row->ID.'" >'.number_format($row->tbvn,2,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="sac" id="sachumed'.$row->ID.'" >'.number_format($row->SacosHumedos,0,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="sac" id="sacbajacali'.$row->ID.'" >'.number_format($row->SacosBajaCalidad,0,$decimal,'').'</td>';
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="texto" id="numlote'.$row->ID.'" >'.$row->numlote.'</td>';
                if($calidad || $admin):
                $tabla = $tabla.'<'.$filasyc.' style="padding:0px;" class="clasif" id="clasif'.$row->ID.'" >'.CHtml::dropDownList('Lista',$row->ClasificacionID,
                        CHtml::listData(Clasificacion::model()->findAll('Estado=1 order by Nombre'),'ID','Nombre'),
                        array('empty'=>' ','class'=>'form-control','style'=>"font-weight:initial; height:25px; font-size:12px;padding:4px;",
                            'id'=>'hariClasif'.$row->ID,'onchange'=>'this.parent().parent().css("background-color","#CDF473");')).'</td>';
                else:
                    if($row->ClasificacionID):
                        $clasi = $row->clasificacion->Nombre;
                        else:
                        $clasi = '-';
                    endif;
                $tabla = $tabla.'<th style="padding:0px;" >'.$clasi.'</td>';    
                endif;
                $tabla = $tabla.'<'.$filac.' style="padding:0px;" class="ac" id="aceite1'.$row->ID.'" >'.number_format($row->aceite1,0,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" class="ac2" id="aceite2'.$row->ID.'" >'.number_format($row->aceite2,0,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" class="ac3" id="aceite3'.$row->ID.'" >'.number_format($row->aceite3,0,$decimal,'').'</td>';
                //$tabla = $tabla.'<th style="padding:0px;" class="ac4" id="aceite4'.$row->ID.'" >'.number_format($row->aceite4,2,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" id="RD'.$row->ID.'">'.number_format($row->RendimientoDiario,2,$decimal,'').'</th>';            
                if($tsemana!=$row->Semana):                    
                    $tsemana = $row->Semana;
                    $num = $modelPeriodo[$peri++];
                    $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$anio." and Semana=".$tsemana);
                    $tabla = $tabla.'<th id="RS'.$row->Semana.'" style="padding:0px;vertical-align:middle;font-size:13px;" class="RSemanal" rowspan="'.$num.'">'.number_format($modeloRendimiento->Valor,2,$decimal,'').'</th>';
                endif;
            $tabla = $tabla.'<'.$filac.' style="padding:0px;" class="glb" id="GLC'.$row->ID.'">'.number_format($row->GalCaldero,2,$decimal,'').'</td>';
            $tabla = $tabla.'<'.$filac.' style="padding:0px;" class="glb" id="GLS'.$row->ID.'">'.number_format($row->GalSecado,2,$decimal,'').'</td>';
                $tabla = $tabla.'<th style="padding:0px;" class="gl" id="GL'.$row->ID.'">'.number_format($row->GalonesBunker,2,$decimal,'').'</th>';
                $tabla = $tabla.'<th style="padding:0px;" id="RDGL'.$row->ID.'">'.number_format($row->GalonesDiario,3,$decimal,'').'</th>';
                if($tsemana2!=$row->Semana):                    
                    $tsemana2 = $row->Semana;
                    $num2 = $modelPeriodo[$peri2++];
                    $tabla = $tabla.'<th id="GS'.$tsemana2.'" style="padding:0px;vertical-align:middle;font-size:13px;" class="RSemanal" rowspan="'.$num2.'">'.number_format($modeloRendimiento->GalonesSemana,2,$decimal,'').'</th>';
                endif;
                endif; 
                $tabla = $tabla.'</tr>';
            endforeach;
            if($s<0):
             /*Fila de totales*/
             /*   $tabla = $tabla.'<tr style=" background-color: DimGray;color:white;" width="0px;"><th ></th><th ></th><th ></th><th></th>';
                /*materia prima*/
               /* foreach($grupos as $grupo){$tabla = $tabla.'<th class="mp" ></th>';}
                $tabla = $tabla.'<th style="padding:0px;">'.number_format(111111111,3,$decimal,'').'</th>';
                $tabla = $tabla.'<th class="tr"></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th>';
                $tabla = $tabla.'<th class="tr" ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                foreach($clientes as $row):
                    $tabla = $tabla.'<th class="sp" ></th>';
                endforeach;
                $tabla = $tabla.'<th style="padding:0px;">'.number_format($mensualCliTotal,0,$decimal,'').'</th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th style="padding:0px;">'.number_format($mensualSacos['SH'],0,$decimal,'').'</th>';
                $tabla = $tabla.'<th style="padding:0px;">'.number_format($mensualSacos['SBC'],0,$decimal,'').'</th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th style="padding:0px;" >'.number_format($mensualSacos['GB'],0,$decimal,'').'</th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'<th ></th>';
                $tabla = $tabla.'</tr>';
            /*fin DE Fila de totales*/
            endif;
            $tabla = $tabla.'</table>';
            return $tabla;
        }
        public function getSemanas($mes, $anio){
            $sql ="select distinct Semana from periodo where Anio=$anio and  Month(fecha) = $mes  group by semana having count(*) > 3 order by semana";
            return Yii::app()->db->createCommand($sql)->queryColumn();
        }
        public function actionSemanas($mes, $anio){
            print json_encode($this->getSemanas($mes, $anio));
        }
        
        public function actionActualizartabla(){
            /*Separador Decimal*/
            $decimal = $this->decimal();
            
            $idperiodo= $_GET['idper'];
            $pnp= isset($_GET['pnp']) ? $_GET['pnp'] : '0';
            $pp= isset($_GET['pp']) ? str_replace($decimal,'.',$_GET['pp']) : '0';
            $inicioprod=$_GET['inicioprod']=='nulo' ? '00:00':$_GET['inicioprod'];
            $fechafin = $_GET['fechafin']=='nulo' ? 'NULL': $_GET['fechafin'];
            $finprod=$_GET['finprod']=='nulo' ? '00:00':$_GET['finprod'];
            $noprogo = $_GET['noprogo']=='nulo' ? '00:00':$_GET['noprogo'];
            $noprogm = $_GET['noprogm']=='nulo' ? '00:00':$_GET['noprogm'];
            $noprogt = $_GET['noprogt']=='nulo' ? '00:00':$_GET['noprogt'];
            $prog=$_GET['prog']=='nulo' ? '00:00':$_GET['prog'];
            $tercer=$_GET['tercer']=='nulo' ? '00:00':$_GET['tercer'];
            $sachumed = $_GET['sachumed']=='nulo'?'0.0':$_GET['sachumed'];
            $sacbajacali = $_GET['sacbajacali']=='nulo'?'0.0':$_GET['sacbajacali'];
            $aceite1 = $_GET['aceite1']=='nulo'?'0':$_GET['aceite1'];
            $aguacpro = $_GET['aguacpro']=='nulo'?'0.0':$_GET['aguacpro'];
            $conant = $_GET['conant']=='nulo'?'0.0':$_GET['conant'];
            $gl = $_GET['gl']=='nulo'?'0.0':$_GET['gl'];
            $glc = $_GET['glc']=='nulo'?'0.0':$_GET['glc'];
            $gls = $_GET['gls']=='nulo'?'0.0':$_GET['gls'];
            
            $proteina = $_GET['proteina']=='nulo'?'0.0':$_GET['proteina'];
            $porhumedad = $_GET['porhumedad']=='nulo'?'0.0':$_GET['porhumedad'];
            $grasa = $_GET['grasa']=='nulo'?'0.0':$_GET['grasa'];
            $ceniza = $_GET['ceniza']=='nulo'?'0.0':$_GET['ceniza'];
            $tbvn = $_GET['tbvn']=='nulo'?'0.0':$_GET['tbvn'];
                        
            $numlote = $_GET["numlote"]!="nulo" ? $_GET["numlote"]:'NULL';
            $clasif = $_GET["clasif"]!="nulo" ? $_GET["clasif"]:'NULL';
            
            $valores = $_GET["valores"];
            $total = $_GET["total"];
            
            $count=0;
            $contg=0;
            $glc = str_replace($decimal,'.',$glc);
            $gls = str_replace($decimal,'.',$gls);
            $gl = str_replace($decimal,'.',$gl);
            
            $aguacpro = str_replace($decimal,'.',$aguacpro);
            $conant = str_replace($decimal,'.',$conant);
            $proteina = str_replace($decimal,'.',$proteina);
            $porhumedad = str_replace($decimal,'.',$porhumedad);
            $grasa = str_replace($decimal,'.',$grasa);
            $ceniza = str_replace($decimal,'.',$ceniza);
            $tbvn = str_replace($decimal,'.',$tbvn);

            $sql='update periodo set InicioProd="'.$inicioprod.
                    '",FechaFinProd="'.$fechafin.
                    '" ,FinProd="'.$finprod.
                    '",ParadanoProgramada="'.$noprogo.
                    '",ParadanoProgramadaMant="'.$noprogm.
                    '",ParadanoProgramadaTerc="'.$noprogt.
                    '",ParadaProgramada="'.$prog.
                    '",ParadaTerceros="'.$tercer.
                    '",SacosHumedos='.$sachumed.
                    ', SacosBajaCalidad='.$sacbajacali.
                    ', aguacolaproducida='.$aguacpro.
                    ', aceite1='.$aceite1.
                    ', concentradoanterior='.$conant.
                    ', GalCaldero='.$glc.
                    ', GalSecado='.$gls.
                    ', GalonesBunker='.$gl.
                    ', porhumedad='.$porhumedad.
                    ', porproteina='.$proteina.
                    ', porgrasa='.$grasa.
                    ', porceniza='.$ceniza.
                    ', tbvn='.$tbvn.
                    ', numlote="'.$numlote.
                    '", ClasificacionID='.$clasif.
                    ' , idpnp='.$pnp.
                    ' , piloto = '.$pp.
                    ' where ID='.$idperiodo;
            
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
            $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc, Nombre');
             for($i=0;$i<$total;$i++){
                $valor = $valores[$i]=='nulo'?'0':$valores[$i];
                $valor = str_replace($decimal,'.',$valor);
                $sql='update ptventa set Valor='.$valor.' where ClienteID='.$clientes[$i]->Id.' and PeriodoID='.$idperiodo;
                $count =$count +Yii::app()->db->createCommand($sql)->execute();
             }
             
             if($count>0){
                 $count=1;
             }
             Yii::app()->db->createCommand("call RendimientoDiario (".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call actualizatiempos (".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call RendimientoSemanal(".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call galones (".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call GalonesSemanal(".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call aceites(".$idperiodo.")")->execute();
             $fecha = Yii::app()->db->createCommand("select fecha from periodo where ID='$idperiodo'")->queryScalar();
             $aguaprocesada = Yii::app()->db->createCommand("select sum(aguacola) from tratamiento where fecha='$fecha'")->queryScalar();
             $saldoaguacola = $aguacpro-$aguaprocesada;
             $produccioncon = Yii::app()->db->createCommand("select sum(concentrado) from tratamiento where fecha='$fecha'")->queryScalar();
             $conc = Yii::app()->db->createCommand("SELECT sum( concentrado )
FROM prensado
JOIN licor ON licor.codigofila = prensado.codigofila
WHERE fecha = '$fecha'
GROUP BY PrensaID
LIMIT 1")->queryScalar()/1000;
             $saldoconcentrado = ($conant+$produccioncon)-$conc;
             $sql = 'select MPProcesada as Mp, GalonesDiario as galond,aceite2 as Ac2,aceite4 as Ac4,aceite3 as Ac3, TRTrabajo as Tr, IFNULL((MPProcesada - piloto)/TRTrabajo,0) as Vp ,SacosProducidos as Sp, Calidad as Ca, RendimientoDiario as Rd , Valor as Rs,GalonesDiario as Gd,GalonesSemana as Gs from periodo join rendimientosemanal on rendimientosemanal.Anio = periodo.Anio and rendimientosemanal.Semana = periodo.Semana where periodo.ID = '.$idperiodo;
             
             $arr = Yii::app()->db->createCommand($sql)->queryRow();
             $arr['Mp']=  number_format($arr['Mp'],3,$decimal,'');
             $arr['Tr']=  number_format($arr['Tr'],2,$decimal,'');
             $arr['Sp']=  number_format($arr['Sp'],0,$decimal,'');
             $arr['Rd']=  number_format($arr['Rd'],2,$decimal,'');
             $arr['Ac2']=  number_format($arr['Ac2'],0,$decimal,'');
             $arr['Ac3']=  number_format($arr['Ac3'],0,$decimal,'');
             //$arr['Ac4']=  number_format($arr['Ac4'],2,$decimal,'');
             $arr['Rs']=  number_format($arr['Rs'],2,$decimal,'');
             $arr['Gs']=  number_format($arr['Gs'],2,$decimal,'');
             $arr['Gd']=  number_format($arr['Gd'],2,$decimal,'');
             $arr['Vp']=  number_format($arr['Vp'],2,$decimal,'');
             $arr['SAC']=  number_format($saldoaguacola,3,$decimal,'');
             $arr['SALDOC']=  number_format($saldoconcentrado,3,$decimal,'');
             
             $arr['galond']=number_format($arr['galond'],2,$decimal,'');
             
             $arr ['status']=$count;
             echo json_encode($arr);
        }
        
        
        public function actionVerpdfSemanal($f1,$f2){
            $area = isset($_GET['area']) ? $_GET['area'] : '1';
            $d = isset($_GET['d']);
            if(isset($_GET['f1']) and isset($_GET['f2'])):
                $f1= $_GET['f1'];
                $f2= $_GET['f2'];
             else:
                $f1= date('Y-m-').'01';
                $f2= date('Y-m-t');
             endif;
             switch ($area){
                 case '1':
                     $file = 'pdfRangoSemanal';
                     break;
                 case '2':
                     $file = 'pdfRangoSemanal_Resumen';
                     break;
                 case '3':
                     $file = 'pdfRangoSamanal_MP';
                     break;
                 case '4':
                     $file = 'pdfRangoSemanal_Tiempo';
                     break;
                 case '5':
                     $file = 'pdfRangoSamanal_Sacos';
                     break;
                 case '7':
                     $file = 'pdfRangoSamanal_Aceite';
                     break;
                 case '6':
                     $file = 'pdfRangoSamanal_Calidad';
                     break;
                 case '8':
                     $file = 'pdfRangoSamanal_Concentrado';
                     break;
                  case '9':
                     $file = 'pdfRangoSamanal_Bunker';
                     break;
             }
             
             
            $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',5,5,14,10,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF1->useOnlyCoreFonts = true;
            $mPDF1->SetTitle("Reporte");
            $mPDF1->SetAuthor("TADEL");
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermark_font = 'DejaVuSansCondensed';
            $mPDF1->watermarkTextAlpha = 0.1;
            $mPDF1->SetDisplayMode('fullwidth');
            $mPDF1->WriteHTML($this->renderPartial($file, array(
                'f1'=>$f1,
                'f2'=>$f2,
                'area'=>$area,
                'url_img'=>$this->imagenLogo()
                    ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
            if($d)
                $mPDF1->Output('pdfSemanal'.date('Y-m-d').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else
                $mPDF1->Output('pdfSemanal'.date('Y-m-d').'.pdf','I');
            exit;
}

public function actionVerpdfSemanalCliente(){
            
            $d = isset($_GET['d']);
            
            $f1= isset($_GET['f1']) ? $_GET['f1'] : date('Y-m-').'01';
            $f2= isset($_GET['f2']) ? $_GET['f2'] : date('Y-m-t');

            $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',5,5,10,10,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF1->useOnlyCoreFonts = true;
            $mPDF1->SetTitle("Reporte");
            $mPDF1->SetAuthor("TADEL");
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermark_font = 'DejaVuSansCondensed';
            $mPDF1->watermarkTextAlpha = 0.1;
            $mPDF1->SetDisplayMode('fullwidth');
            $sql = "select distinct clienteid  from ptventa join periodo on ptventa.periodoId = periodo.id where periodo.Fecha between '$f1' and '$f2' and Valor > 0";
            $resul = Yii::app()->db->createCommand($sql)->queryColumn();
            $clientesPermitidos = array();
            foreach( $resul as $c):
                $clientesPermitidos[$c] = 1; 
            endforeach;
            /*hacemos un render partial a una vista preparada, en este caso es la vista pdfReport*/
            $mPDF1->WriteHTML($this->renderPartial('pdfSemanasCliente', array(
                'f1'=>$f1,
                'f2'=>$f2,
                'cliente'=>$cliente,
                'clientesPermitidos'=>$clientesPermitidos,
                'url_img'=> $this->imagenLogo()
                ), true)); 
            if($d)
                $mPDF1->Output('pdfSemanal'.date('Y-m-d').'.pdf','D');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            else
                $mPDF1->Output('pdfSemanal'.date('Y-m-d').'.pdf','I');
            exit;
}

public function actionUnificadosacos(){
    
    $f1 = isset($_GET['f1']) ? $_GET['f1'] : date('Y-m-1');
    $f2 = isset($_GET['f2']) ? $_GET['f2'] : date('Y-m-d'); 
    
    $this->render('sacosunificados', [
        'f1'=>$f1,
        'f2'=>$f2,
        
    ]);
    
}
public function actionUnificadosacospdf(){
    $linea  = Yii::app()->params['linea'];
    $f1 = isset($_GET['f1']) ? $_GET['f1'] : date('Y-m-1');
    $f2 = isset($_GET['f2']) ? $_GET['f2'] : date('Y-m-d'); 
    $download = isset($_GET['d']) ? true : false;
    $sql = "select dia, sacosproducidos as sacos, fecha, semana, anio from periodo where fecha BETWEEN '$f1' and '$f2' order by fecha";
    if($linea==1){
        $model1 = Yii::app()->db->createCommand($sql)->queryAll();
        $model2 = Yii::app()->db2->createCommand($sql)->queryAll();
    }else{
        $model1 = Yii::app()->db2->createCommand($sql)->queryAll();
        $model2 = Yii::app()->db->createCommand($sql)->queryAll();
    }
    
    $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,25,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
    $mPDF1->useOnlyCoreFonts = true;
    $mPDF1->SetTitle("Reporte");
    $mPDF1->SetAuthor("Resultec");
    $mPDF1->showWatermarkText = true;
    $mPDF1->watermark_font = 'DejaVuSansCondensed';
    $mPDF1->watermarkTextAlpha = 0.1;
    $mPDF1->SetDisplayMode('fullpage');
    $mPDF1->WriteHTML($this->renderPartial('pdfUnificadoSacos', array(
            'linea'=>$linea,
            'ml1'=>$model1,
            'ml2'=>$model2,
            'f1'=>$f1,
            'f2'=>$f2,
            'url_img'=> $this->imagenLogo()
        ), true)); 

    $mPDF1->Output('Informe Sacos Unicado '.date('Y-m-d').'.pdf',$download ? 'D':'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
    exit;
        
}

public function actionUnificadosacosexcel(){
    $linea  = Yii::app()->params['linea'];
    $f1 = isset($_GET['f1']) ? $_GET['f1'] : date('Y-m-1');
    $f2 = isset($_GET['f2']) ? $_GET['f2'] : date('Y-m-d'); 
    $sql = "select dia, sacosproducidos as sacos, fecha, semana, anio from periodo where fecha BETWEEN '$f1' and '$f2' order by fecha";
    if($linea==1){
        $model1 = Yii::app()->db->createCommand($sql)->queryAll();
        $model2 = Yii::app()->db2->createCommand($sql)->queryAll();
    }else{
        $model1 = Yii::app()->db2->createCommand($sql)->queryAll();
        $model2 = Yii::app()->db->createCommand($sql)->queryAll();
    }
    $contenido = $this->renderPartial('pdfUnificadoSacos', 
            array(
                'linea'=>$linea,
                'ml1'=>$model1,
                'ml2'=>$model2,
                'f1'=>$f1,
                'f2'=>$f2,
                'url_img'=> $this->imagenLogo()
            ), true);
    Yii::app()->request->sendFile('Produccion Sacos | Unificados '.date('Y-m-d H:m').'.xls', $contenido);
    exit;
}
public function actionGenerarExcelprodSemanal($f1,$f2){
    $area = isset($_GET['area']) ? $_GET['area'] : '1';
            if(isset($_GET['f1']) and isset($_GET['f2'])):
                $f1= $_GET['f1'];
                $f2= $_GET['f2'];
             else:
                $f1= date('Y-m-').'01';
                $f2= date('Y-m-t');
             endif;
             
              switch ($area){
                 case '1':
                     $file = 'ExcelRangoSemanal';
                     break;
                 case '2':
                     $file = 'excelRangoSemanal_Resumen';
                     break;
                 case '3':
                     $file = 'excelRangoSamanal_MP';
                     break;
                 case '4':
                     $file = 'excelRangoSemanal_Tiempo';
                     break;
                 case '5':
                     $file = 'excelRangoSamanal_Sacos';
                     break;
                    case '7':
                     $file = 'excelRangoSamanal_Aceite';
                     break;
                 case '6':
                     $file = 'excelRangoSamanal_Calidad';
                     break;
                 case '8':
                     $file = 'excelRangoSamanal_Concentrado';
                     break;
                  case '9':
                     $file = 'excelRangoSamanal_Bunker';
                     break;
             }
            $contenido = $this->renderPartial($file, array('f1'=>$f1,'f2'=>$f2,'area'=>$area,
                'url_img'=>$this->imagenLogo()), true);
            Yii::app()->request->sendFile('ReporteRangoSemanalExcel'.date('Y-m-d H:m').'.xls', $contenido);
            exit;
            }
public function actionGenerarExcelprodSemanalCliente(){
    $f1 = isset($_GET['f1']) ? $_GET['f1'] :date('Y-m-').'01';
    $f2 = isset($_GET['f2']) ? $_GET['f2'] :date('Y-m-t');
    $contenido = $this->renderPartial("excelSemanasCliente", array('f1'=>$f1,'f2'=>$f2), true);
    Yii::app()->request->sendFile('ReporteRangoSemanalExcel'.date('Y-m-d H:m').'.xls', $contenido);
    exit;
}
            
            public function actionPrueba(){
                $this->render('prueba');
            }
            
            public function actionGuardarimg2(){

                $data = str_replace(' ', '+', $_POST['bin_data']);
                $data = base64_decode($data);
                $fileName = 'images/IMGPDF.png';
                $im = imagecreatefromstring($data);
                
                if ($im !== false) {
                     //Save image in the specified location
                    imagepng($im, $fileName);
                    imagedestroy($im);
                    echo $fileName;
            }
            }
            
            public function actionGuardarimg(){
                if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
                $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            else:
                $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            $filename = $_GET['filename'];
            $sql = Yii::app()->db->createCommand("select rendimientosemanal.semana, Valor from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and periodo.Anio = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and periodo.Anio between ".$anio1." and ".$anio." group by periodo.Anio,semana order by Fecha")->queryAll();
            $sqlobjetivo = Yii::app()->db->createCommand("select rendimientosemanal.semana, Objetivo,baseRS from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and periodo.Anio = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and periodo.Anio between ".$anio1." and ".$anio." group by periodo.Anio,semana order by Fecha")->queryAll();
            $sql2 =Yii::app()->db->createCommand("Select Semana,Anio as anio from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            $sql3 = Yii::app()->db->createCommand("Select Sum(MPProcesada) as SumaProcesada from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and Anio between ".$anio1." and ".$anio." group by Anio,Semana order by Fecha")->queryAll();
            $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF1->useOnlyCoreFonts = true;
            $mPDF1->SetTitle("Reporte");
            $mPDF1->SetAuthor("TADEL");
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermark_font = 'DejaVuSansCondensed';
            $mPDF1->watermarkTextAlpha = 0.1;
            $mPDF1->SetDisplayMode('fullpage');
            $mPDF1->WriteHTML($this->renderPartial('pdfGrafico', array(
                'filename'=>$filename,
                'sql'=>$sql,
                'sql2'=>$sql2,
                'sql3'=>$sql3,
                'sql4'=>$sql4,
                'sqlobjetivo'=>$sqlobjetivo,
                'fechaini'=>$fechaini,
                'fechafin'=>$fechafin,
                'url_img'=> $this->imagenLogo()
                ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
            $mPDF1->Output('RendimientoSemanal'.date('Y-m-d H:i').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            exit;
        }
            
            public function actionGuardarimgeficienciaPlanta(){             
            
                $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
                $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] :date('Y')  ;
                $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
                $mesFin= isset($_GET['mes2']) ? $_GET['mes2'] : date('m');
                
                $semIni =  $this->SemanaMenor($mesIni, $anioIni);
                $semFin = $this->SemanaMayor($mesFin, $anioFin);
                $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
                
                $filename = $_GET['filename'];
                
                $sqlC = Yii::app()->db->createCommand("select ROUND(IFNULL((Sum(SacosProducidos)-Sum(SacosBajaCalidad))/Sum(SacosProducidos),0)*100) as C,Semana from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
                $sqlV = Yii::app()->db->createCommand("select ROUND(IFNULL((Sum(SacosProducidos)/((Sum(TRTrabajo))*(select MIN(cocina.CapacidadCocina) from cocina WHERE cocina.EstadoCocina=1 and cocina.Estado=1)* Objetivo)),0)*100) as V from periodo join rendimientosemanal on periodo.Anio=rendimientosemanal.Anio and periodo.Semana=rendimientosemanal.Semana where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,periodo.Semana order by Fecha")->queryAll();
                $sqlT= Yii::app()->db->createCommand("select ROUND(IFNULL(Sum(TRTrabajo)/((((sum(HOUR(Concat(datediff(FechaFinProd,Fecha )*24,':00')))+Sum(HOUR(FinProd))+(Sum(MINUTE(Concat(datediff(FechaFinProd,Fecha )*24,':00'))/60)+Sum((MINUTE(FinProd))/60)))-(Sum(HOUR(InicioProd))+Sum(MINUTE(InicioProd)/60)))-(Sum(HOUR(ParadaProgramada))+Sum(MINUTE(ParadaProgramada)/60)))-(Sum(HOUR(ParadaTerceros))+Sum(MINUTE(ParadaTerceros)/60))),0)*100)as T from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();

                $sqlC1 = Yii::app()->db->createCommand("select IFNULL((Sum(SacosProducidos)-Sum(SacosBajaCalidad))/Sum(SacosProducidos),0) as C,Semana from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();
                $sqlV1 = Yii::app()->db->createCommand("select IFNULL((Sum(SacosProducidos)/((Sum(TRTrabajo))*(select MIN(cocina.CapacidadCocina) from cocina WHERE cocina.EstadoCocina=1 and cocina.Estado=1)* Objetivo)),0) as V from periodo join rendimientosemanal on periodo.Anio=rendimientosemanal.Anio and periodo.Semana=rendimientosemanal.Semana where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,periodo.Semana order by Fecha ")->queryAll();
                $sqlT1= Yii::app()->db->createCommand("select IFNULL(Sum(TRTrabajo)/((((sum(HOUR(Concat(datediff(FechaFinProd,Fecha )*24,':00')))+Sum(HOUR(FinProd))+(Sum(MINUTE(Concat(datediff(FechaFinProd,Fecha )*24,':00'))/60)+Sum((MINUTE(FinProd))/60)))-(Sum(HOUR(InicioProd))+Sum(MINUTE(InicioProd)/60)))-(Sum(HOUR(ParadaProgramada))+Sum(MINUTE(ParadaProgramada)/60)))-(Sum(HOUR(ParadaTerceros))+Sum(MINUTE(ParadaTerceros)/60))),0) as T from periodo where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio ,Semana order by Fecha")->queryAll();
                $semana =Yii::app()->db->createCommand("Select Semana,periodo.Anio as anio from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by periodo.Anio,Semana order by Fecha")->queryAll();               
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                $mPDF1->useOnlyCoreFonts = true;
                $mPDF1->SetTitle("Reporte");
                $mPDF1->SetAuthor("TADEL");
                //$mPDF1->SetWatermarkText("Pacientes");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('pdfEficienciaPlanta', array(
                    'sqlV1'=>$sqlV1,
                    'sqlT1'=>$sqlT1,
                    'sqlC1'=>$sqlC1,
                    'sql2'=>$semana,
                    'semana'=>$semana,
                    'sqlV'=>$sqlV,
                    'sqlT'=>$sqlT,
                    'sqlC'=>$sqlC,
                    'filename'=>$filename,
                    'fechaini'=>$fechaini,
                    'fechafin'=>$fechafin,
                    'url_img'=> $this->imagenLogo()
                    ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport

                $mPDF1->Output('EficienciaPlanta'.date('Y-m-d').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;
            }
            
            public function actionGuardarimgFinal(){
                $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
                $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
                $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : 01;
                $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
                $filename = $_GET['filename'];
$mensual = isset($_GET['mensual']) ? $_GET['mensual'] : 0;
if($mensual){
    
    $anio = $anioIni;
      $arraympprocesada = Array(); $arraytnproducida = Array();$aux=Array();$aux2=Array();$acumulada=Array();$arraytnproducidaacumu=Array();
            $mpproce=0;$tnprodu=0;
            $semana = Yii::app()->db->createCommand("Select Month(Fecha) Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEAR(fecha) = $anio group by year(fecha),month(fecha) order by Fecha")->queryAll();
            $mpprocesadaytn =Yii::app()->db->createCommand("select Sum(MPProcesada) as mpprocesada,ROUND(Sum(SacosProducidos)/20,3) as tnproducida from periodo where YEAR(Fecha)= $anio group by year(fecha),Month(fecha) order by Fecha")->queryAll();
            $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEAR(Fecha) = $anio group by year(fecha),Month(Fecha) order by Fecha")->queryAll();
            
            foreach ($semana as $row):
                $acumulada[$row['anio'].''.$row['Semana']] = '';
                $arraytnproducidaacumu[$row['anio'].''.$row['Semana']]= '';
            endforeach;
            
            $fechaini = $anioIni;
                $fechafin = $anioIni;
            
            foreach ($mpprocesadaytn as $row):
                $arraympprocesada[] =(float)$row['mpprocesada'];
                $mpproce = $mpproce+$row['mpprocesada'];
                $arraytnproducida[]=(float)$row['tnproducida'];
                $tnprodu = $tnprodu+(float)$row['tnproducida'];
            endforeach;
            
            foreach ($acumulada as $key=>$value):
                $aux[]=$value;
            endforeach;
            $aux[]=$mpproce;
            
            foreach ($arraytnproducidaacumu as $key=>$value2):
                $aux2[]=$value2;
            endforeach;
            $aux2[]=$tnprodu;
            
            
}else{
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesFin, $anioFin);
                
                $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
                
                $semana =Yii::app()->db->createCommand("Select Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
                $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where YEARWEEK(Fecha, 3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();

                
}
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                $mPDF1->useOnlyCoreFonts = true;
                $mPDF1->SetTitle("Reporte");
                $mPDF1->SetAuthor("TADEL");
                //$mPDF1->SetWatermarkText("Pacientes");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('pdfFinal', array(
                    'mensual'=>$mensual,
                    'sql2'=>$semana,
                    'sql4'=>$sql4,
                    'filename'=>$filename,
                    'fechaini'=>$fechaini,
                    'fechafin'=>$fechafin,
                    'url_img'=> $this->imagenLogo()
                        ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
                $mPDF1->Output('Final'.date('Y-m-d H:i').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;
                
            }

            public function actionGuardarimgVolumenmensual(){
                $anio = isset($_GET['anio']) ? $_GET['anio'] : date('Y');

                $semIni = $this->SemanaMenor('01', $anio);
                $semFin =  $this->SemanaMayor('12', $anio);
                
                $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anio")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anio")->queryScalar();
                
                $semana =Yii::app()->db->createCommand("Select Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Anio as anio from periodo Where YEAR(Fecha) = $anio group by Year(Fecha),Month(Fecha) order by Fecha")->queryAll();
                $sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from periodo Where year(Fecha) = $anio group by Year(Fecha),Month(Fecha) order by Fecha")->queryAll();

                $filename = $_GET['filename'];
                         
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                $mPDF1->useOnlyCoreFonts = true;
                $mPDF1->SetTitle("Reporte");
                $mPDF1->SetAuthor("TADEL");
                //$mPDF1->SetWatermarkText("Pacientes");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('pdfVolumenmensual', array(
                    'sql2'=>$semana,
                    'sql4'=>$sql4,
                    'filename'=>$filename,
                    'fechaini'=>$fechaini,
                    'fechafin'=>$fechafin,
                    'url_img'=>$this->imagenLogo()
                        ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
                $mPDF1->Output('Final'.date('Y-m-d H:i').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;
                
            }
            
            public function actionGuardarimgVariacionhumedad(){
                                
            $diario = isset($_GET['diario']) ? $_GET['diario'] : 0;
            $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
            $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
            $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
            $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
            $filename = $_GET['filename'];
            
            if($diario==1){
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesIni, $anioIni);
                $analiarr=Array();$porcentajearr = Array();$arraySemana =Array(); $aux = Array(); $base=Array();
            $semana = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as Y,Fecha as Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $semanayporcentaje =Yii::app()->db->createCommand("select Year(fecha) as Y,ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) as porcentaje, Fecha as Semana,Count(PorcentajeHumedad)as analisis from harina where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin and harina.PorcentajeHumedad > 0  group by Year(Fecha),Fecha order by Fecha")->queryAll();
            $diario = 1;
            $baseh = Yii::app()->db->createCommand("SELECT baseHumedad FROM rendimientosemanal WHERE Anio =".$anioIni)->queryScalar();
            $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
                foreach ($semana as $row):
                $arraySemana[] = $row['Semana'];
                $porcentajearr[$row['Y'].''.$row['Semana']] = 0;
                $analiarr[$row['Y'].''.$row['Semana']] = 0;
                
                $base[]= (float)$baseh;
            endforeach;
            
            foreach ($semanayporcentaje as $row):
                $porcentajearr[$row['Y'].''.$row['Semana']] = (float)$row['porcentaje'];
            $analiarr[$row['Y'].''.$row['Semana']] = (float)$row['analisis'];
            endforeach;
            
            foreach ($porcentajearr as $key=>$value):
                if($value<=0){
                $aux[]=null;    
            }else{
                $aux[]=$value;    
            }
            endforeach;
            foreach ($analiarr as $key=>$value):
                $aux2[]=$value;                
            endforeach;
            
            
            }else{
            
            $semIni = $this->SemanaMenor($mesIni, $anioIni);
            $semFin =  $this->SemanaMayor($mesFin, $anioFin);
            
            $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
            $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
                
            $analiarr=Array();
            $aux2 = Array();
            
            $semana = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as Y,Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Semana order by Fecha")->queryAll();
            $semanayporcentaje =Yii::app()->db->createCommand("select Year(fecha) as Y,ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) as porcentaje, Week(Fecha,3) as Semana,Count(PorcentajeHumedad)as analisis from harina where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin and harina.PorcentajeHumedad > 0  group by Year(Fecha),Week(Fecha,3) order by Fecha")->queryAll();
            
            
            
            foreach ($semana as $row):
                $analiarr[$row['Y'].''.$row['Semana']] = 0;
            endforeach;
            
            foreach ($semanayporcentaje as $row):
                $analiarr[$row['Y'].''.$row['Semana']] = (float)$row['analisis'];
            endforeach;
            
            foreach ($analiarr as $key => $value):
                $aux2[]=$value;    
            $diario = 0;
            endforeach;
            }
            $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF1->useOnlyCoreFonts = true;
            $mPDF1->SetTitle("Reporte");
            $mPDF1->SetAuthor("TADEL");
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermark_font = 'DejaVuSansCondensed';
            $mPDF1->watermarkTextAlpha = 0.1;
            $mPDF1->SetDisplayMode('fullpage');
            $mPDF1->WriteHTML($this->renderPartial('pdfVariacionhumedad', 
                    array('diario'=>$diario,
                        'sql3'=>$aux2,
                        'sql2'=>$semana,
                        'filename'=>$filename,
                        'fechaini'=>$fechaini,
                        'fechafin'=>$fechafin,
                        'url_img'=>$this->imagenLogo()
                    ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
            $mPDF1->Output('VariacionHumedad'.date('Y-m-d H:i').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            exit;
    

            }
            public function actionGuardarimgRatioconsumo(){
                $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
                $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
                $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : date('m');
                $mesFin = isset($_GET['mes2']) ? $_GET['mes2'] : date('m') ;
                $diario = isset($_GET['diario']) ? $_GET['diario'] : 0;
                $filename = $_GET['filename'];
if($diario==1){
    $diari = 1;
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesIni, $anioIni);
                $sql2 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos, Anio as anio,Fecha as Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, GalonesDiario as galones from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $sqlgal = Yii::app()->db->createCommand("select Fecha,GalonesBunker as galones,(SacosProducidos)/20 as tn,(SacosProducidos) as sacos from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,Fecha order by Fecha")->queryAll();
            $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
            }else{
                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin =  $this->SemanaMayor($mesFin, $anioFin);
                
                $fechaini = Yii::app()->db->createCommand("select min(Fecha) from periodo where semana = $semIni and Anio = $anioIni")->queryScalar();
                $fechafin = Yii::app()->db->createCommand("select max(Fecha) from periodo where semana = $semFin and Anio = $anioFin")->queryScalar();
                 $sqlgal = Yii::app()->db->createCommand("select Semana,Sum(GalonesBunker) as galones,Sum(SacosProducidos)/20 as tn,Sum(SacosProducidos) as sacos from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Fecha")->queryAll();
$diari = 0;
                $sql2 =Yii::app()->db->createCommand("Select distinct Anio as Y,Semana,Min(Concat(Day(Fecha),'/',Month(Fecha)))as ini, Year(Fecha) as anio from periodo Where YEARWEEK(Fecha,3) between $anioIni$semIni and $anioFin$semFin group by Anio, Semana order by Fecha")->queryAll();
            }
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                $mPDF1->useOnlyCoreFonts = true;
                $mPDF1->SetTitle("Reporte");
                $mPDF1->SetAuthor("TADEL");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('pdfRatioconsumo', 
                        array(
                            'diario'=>$diari,
                            'sqlgal'=>$sqlgal,
                            'sql2'=>$sql2,
                            'filename'=>$filename,
                            'fechaini'=>$fechaini,
                            'fechafin'=>$fechafin,
                            'url_img'=>$this->imagenLogo()
                        ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
                $mPDF1->Output('Ratioconsumo'.date('Y-m-d H:i').'pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;
    

            }
            public function actionGuardarimgCompmat(){
                $anioFin = isset($_GET['anio']) ? $_GET['anio'] : date('Y');
                $anioIni = isset($_GET['anio1']) ? $_GET['anio1'] : date('Y');
                $mesIni = isset($_GET['mes1']) ? $_GET['mes1'] : 1;
                $mesFin= isset($_GET['mes2']) ? $_GET['mes2'] : date ('m');
                $filename = $_GET['filename'];

                $semIni = $this->SemanaMenor($mesIni, $anioIni);
                $semFin = $this->SemanaMayor($mesFin, $anioFin);

                $arraySemana = Array();
            $rendimiento = Array();
            
            $semana = Yii::app()->db->createCommand("select Anio Y,semana Semana, Valor from rendimientosemanal where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin group by Anio,semana order by Anio, Semana")->queryAll();
            
            $grupos = Especiegrupo::model()->findAll('velocidad = 1 order by ID');
                    
            foreach ($semana as $row):
                $arraySemana[] = (float)$row['Semana'].''.(float)$row['Y'];
                if((float)$row['Valor']<=0)
                    $rendimiento[] = null;    
                else
                    $rendimiento[] = (float)$row['Valor'];
            endforeach;
                 
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("TADEL");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
        $mPDF1->WriteHTML($this->renderPartial('pdfCompmat', array(
             'filename'=>$filename,
             'semanas'=>$semana,
            'rendimiento'=>$rendimiento,
            'arraySemana'=>$arraySemana,
            'anio'=>$anioFin,
            'mes'=>$mesIni,
            'mes2'=>$mesFin,
            'anio1'=>$anioIni,
            'totalgrupo'=>COUNT($grupos),
            'semIni'=>$semIni,
            'semFin'=>$semFin,
            'anioIni'=>$anioIni,
            'anioFin'=>$anioFin,
            'grupos'=>$grupos,
            'totalSemana'=>count($semana),
             'url_img'=>$this->imagenLogo()
                 ), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('CompMateriaPrimaProc'.date('Y-m-d').'pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
        }
        
    function decimal(){
        $sql= "select signo_decimal from parametros ";
        $decimal = Yii::app()->db->createCommand($sql)->queryScalar();
        return $decimal;
    }
    
    function densidadaceite(){
        $sql= "select densidad_aceite from parametros ";
        $da = Yii::app()->db->createCommand($sql)->queryScalar();
        return $da;
    }
    function sumarHoras($h1,$h2,$h3){
        $hora1 =(int) date('H',  strtotime($h1));
        $hora2 = (int) date('H',  strtotime($h2));
        $hora3 = (int) date('H',  strtotime($h3));
        $hora = $hora1+$hora2+$hora3;
        $minutos1 =(int) date('i',  strtotime($h1));
        $minutos2 = (int) date('i',  strtotime($h2));
        $minutos3 = (int) date('i',  strtotime($h3));
        $minutos= $minutos1+$minutos2+$minutos3;
        $hora += floor($minutos/60);
        $minutos = $minutos%60>9 ? $minutos%60 : '0'.$minutos%60;
        
        return $hora.':'.$minutos;
    }
    
    function imagenLogo(){
        return pdfControlDiaria::imagenLogo();
    }
}

