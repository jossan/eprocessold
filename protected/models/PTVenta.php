<?php

/**
 * This is the model class for table "PTVenta".
 *
 * The followings are the available columns in table 'PTVenta':
 * @property string $ID
 * @property string $PeriodoID
 * @property integer $ClienteID
 * @property double $Valor
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Clientes $cliente
 * @property Periodo $periodo
 */
class PTVenta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ptventa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PeriodoID', 'required'),
			array('ClienteID, Estado', 'numerical', 'integerOnly'=>true),
			array('Valor', 'numerical'),
			array('PeriodoID', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, PeriodoID, ClienteID, Valor, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cliente' => array(self::BELONGS_TO, 'Clientes', 'ClienteID'),
			'periodo' => array(self::BELONGS_TO, 'Periodo', 'PeriodoID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'PeriodoID' => 'Periodo',
			'ClienteID' => 'Cliente',
			'Valor' => 'Valor',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('PeriodoID',$this->PeriodoID,true);
		$criteria->compare('ClienteID',$this->ClienteID);
		$criteria->compare('Valor',$this->Valor);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PTVenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
