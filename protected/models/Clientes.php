<?php

/**
 * This is the model class for table "Clientes".
 *
 * The followings are the available columns in table 'Clientes':
 * @property integer $Id
 * @property string $Nombre
 * @property integer $Exportacion
 * @property string $Descripcion
 * @property integer $Estado
 * @property integer $Email
 * @property integer $Telefono
 * The followings are the available model relations:
 * @property PTVenta[] $pTVentas
 */
class Clientes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre', 'required'),
			array('Exportacion, Estado', 'numerical', 'integerOnly'=>true),
                        array('Telefono', 'length', 'max'=>50),
			array('Nombre,Email', 'length', 'max'=>100),
			array('Descripcion', 'length', 'max'=>400),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Id, Nombre, Exportacion, Descripcion, Estado, Email, Telefono', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pTVentas' => array(self::HAS_MANY, 'PTVenta', 'ClienteID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'Nombre' => 'Nombre',
			'Exportacion' => 'Tipo de venta',
			'Descripcion' => 'Descripción',
			'Estado' => 'Estado',
                        'Telefono'=>'Telefono',
                        'Email'=>'Email'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Exportacion',$this->Exportacion);
		$criteria->compare('Descripcion',$this->Descripcion,true);
                $criteria->compare('Telefono',$this->Telefono,true);
                $criteria->compare('Email',$this->Email,true);
		$criteria->compare('Estado',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clientes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
