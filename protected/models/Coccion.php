<?php

/**
 * This is the model class for table "Coccion".
 *
 * The followings are the available columns in table 'Coccion':
 * @property integer $ID
 * @property string $RevisionID
 * @property integer $CocinaID
 * @property double $RPM
 * @property double $PresionEje
 * @property double $PresionChaqueta
 * @property double $Temperatura
 * @property string $codigofila
 * @property integer $Estado
 *@property  date $Fecha
 * @property time $Hora
 * The followings are the available model relations:
 * @property Recepcion $revision
 * @property Cocina $cocina
 */
class Coccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'coccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CocinaID', 'required'),
			array('CocinaID, Estado', 'numerical', 'integerOnly'=>true),
			array('RPM, PresionEje, PresionChaqueta, Temperatura', 'numerical'),
			array('length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha, CocinaID, RPM, PresionEje, PresionChaqueta, Temperatura, Estado, Hora, codigofila,IdUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cocina' => array(self::BELONGS_TO, 'Cocina', 'CocinaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Fecha' => 'Fecha',
			'CocinaID' => 'Cocina',
			'RPM' => 'Rpm',
			'PresionEje' => 'Presion Eje',
			'PresionChaqueta' => 'Presion Chaqueta',
			'Temperatura' => 'Temperatura',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('CocinaID',$this->CocinaID);
		$criteria->compare('RPM',$this->RPM);
		$criteria->compare('PresionEje',$this->PresionEje);
		$criteria->compare('PresionChaqueta',$this->PresionChaqueta);
		$criteria->compare('Temperatura',$this->Temperatura);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Coccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
