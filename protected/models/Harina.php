<?php

/**
 * This is the model class for table "harina".
 *
 * The followings are the available columns in table 'harina':
 * @property string $ID
 * @property string $NumeroLote
 * @property double $Temperatura
 * @property double $PorcentajeHumedad
 * @property double $AO
 * @property double $HumedadONline
 * @property double $TBVN
 * @property double $PorcentajeProteina
 * @property integer $ClasificacionID
 * @property integer $Estado
 * @property double $SacoProducidos
 * @property integer $IdUsuario
 * @property string $FechaModificado
 * @property string $Fecha
 * @property string $Hora
 * @property integer $ObservacionID
 * @property integer $CodigoInicio
 * @property integer $CodigoFin
 *
 * The followings are the available model relations:
 * @property Observacion $observacion
 * @property Clasificacion $clasificacion
 */
class Harina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'harina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fecha', 'required'),
			array('ClasificacionID, Estado, IdUsuario, ObservacionID, CodigoInicio, CodigoFin', 'numerical', 'integerOnly'=>true),
			array('Temperatura, PorcentajeHumedad, AO, HumedadONline, TBVN, PorcentajeProteina, SacoProducidos', 'numerical'),
			array('NumeroLote', 'length', 'max'=>20),
			array('FechaModificado, Hora', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NumeroLote, Temperatura, PorcentajeHumedad, AO, HumedadONline, TBVN, PorcentajeProteina, ClasificacionID, Estado, SacoProducidos, IdUsuario, FechaModificado, Fecha, Hora, ObservacionID, CodigoInicio, CodigoFin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'obser' => array(self::BELONGS_TO, 'Observacion', 'ObservacionID'),
			'clasi' => array(self::BELONGS_TO, 'Clasificacion', 'ClasificacionID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NumeroLote' => 'Numero Lote',
			'Temperatura' => 'Temperatura',
			'PorcentajeHumedad' => 'Porcentaje Humedad',
			'AO' => 'Ao',
			'HumedadONline' => 'Humedad Online',
			'TBVN' => 'Tbvn',
			'PorcentajeProteina' => 'Porcentaje Proteina',
			'ClasificacionID' => 'Clasificacion',
			'Estado' => 'Estado',
			'SacoProducidos' => 'Saco Producidos',
			'IdUsuario' => 'Id Usuario',
			'FechaModificado' => 'Fecha Modificado',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
			'ObservacionID' => 'Observacion',
			'CodigoInicio' => 'Codigo Inicio',
			'CodigoFin' => 'Codigo Fin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('NumeroLote',$this->NumeroLote,true);
		$criteria->compare('Temperatura',$this->Temperatura);
		$criteria->compare('PorcentajeHumedad',$this->PorcentajeHumedad);
		$criteria->compare('AO',$this->AO);
		$criteria->compare('HumedadONline',$this->HumedadONline);
		$criteria->compare('TBVN',$this->TBVN);
		$criteria->compare('PorcentajeProteina',$this->PorcentajeProteina);
		$criteria->compare('ClasificacionID',$this->ClasificacionID);
		$criteria->compare('Estado',$this->Estado);
		$criteria->compare('SacoProducidos',$this->SacoProducidos);
		$criteria->compare('IdUsuario',$this->IdUsuario);
		$criteria->compare('FechaModificado',$this->FechaModificado,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora,true);
		$criteria->compare('ObservacionID',$this->ObservacionID);
		$criteria->compare('CodigoInicio',$this->CodigoInicio);
		$criteria->compare('CodigoFin',$this->CodigoFin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Harina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
