<?php

/**
 * This is the model class for table "Prensado".
 *
 * The followings are the available columns in table 'Prensado':
 * @property string $ID
 * @property string $RevisionID
 * @property integer $PrensaID
 * @property double $Amperaje
 * @property double $PorcentajeHumedad
 * @property integer $Estado
 * @property time $Hora
 * @property string $Fecha
 * @property string $codigofila
 * The followings are the available model relations:
 * @property Recepcion $revision
 * @property Prensa $prensa
 */
class Prensado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prensado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PrensaID', 'required'),
			array('PrensaID, Estado', 'numerical', 'integerOnly'=>true),
			array('Amperaje, PorcentajeHumedad', 'numerical'),
			array('length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha,Hora,codigofila,IdUsuario, PrensaID, Amperaje, PorcentajeHumedad, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prensa' => array(self::BELONGS_TO, 'Prensa', 'PrensaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'PrensaID' => 'Prensa',
			'Amperaje' => 'Amperaje',
			'PorcentajeHumedad' => 'Porcentaje Humedad',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('PrensaID',$this->PrensaID);
		$criteria->compare('Amperaje',$this->Amperaje);
		$criteria->compare('PorcentajeHumedad',$this->PorcentajeHumedad);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prensado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
