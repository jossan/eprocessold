<?php

/**
 * This is the model class for table "Recepcion".
 *
 * The followings are the available columns in table 'Recepcion':
 * @property integer $ID
 * @property string $Fecha
 * @property string $Hora
 * @property string $FechaHoraReal
 * @property integer $TolvaID
 * @property string $Horatolva
 * @property string $EspecieID
 * @property integer $TBVN
 * @property integer $ObservacionID
 * @property integer $Estado
 * @property double $PesoTonelada
 * @property double $Descuento
 * @property double $Pesoneto
 * @property integer $IdUsuario
 * @property string $FechaModificado
 * @property integer $ProveedorId
 * @property string $Placa
 * @property string $Chofer
 *
 * The followings are the available model relations:
 * @property Proveedores $proveedor
 * @property Tolva $tolva
 * @property Observacion $observacion
 */
class Recepcion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recepcion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TolvaID, ObservacionID, Estado, ProveedorId, EspecieID, IdSecuencia', 'numerical', 'integerOnly'=>true),
                    
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                        array('fechaproduccion,Fecha,FechaHoraReal,Horatolva, PesoTonelada, Descuento, Pesoneto, Placa, Chofer, tarracarro', 'safe'),
                        array('Placa', 'length', 'max'=>20),
                        array('Chofer', 'length', 'max'=>150),
                    
                        array('ID,fechaproduccion, Fecha, Hora, FechaHoraReal, TolvaID, Horatolva, EspecieID, TBVN, ObservacionID, Estado, PesoTonelada, Descuento, Pesoneto, IdUsuario, FechaModificado, ProveedorId, Placa, Chofer, IdSecuencia, tarracarro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'proveedor' => array(self::BELONGS_TO, 'Proveedores', 'ProveedorId'),
			'observacion' => array(self::BELONGS_TO, 'Observacion', 'ObservacionID'),
			'especie' => array(self::BELONGS_TO, 'Especie', 'EspecieID'),
			'tolva' => array(self::BELONGS_TO, 'Tolva', 'TolvaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
			'FechaHoraReal' => 'Fecha Hora Real',
			'TolvaID' => 'Tolva',
			'EspecieID' => 'Especie',
			'RecepcionID' => 'Recepción',
			'TBVN' => 'Tbvn',
			'ObservacionID' => 'Observación',
			'Estado' => 'Estado',
                        'fechaproduccion'=>'Fecha Produccion'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($f_ini,$f_fin)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora);
                $criteria->compare('Pesoneto',$this->Pesoneto,true);
		$criteria->compare('FechaHoraReal',$this->FechaHoraReal,true);
		$criteria->compare('TolvaID',$this->TolvaID);
		$criteria->compare('EspecieID',$this->EspecieID);
		$criteria->compare('RecepcionID',$this->RecepcionID,true);
		$criteria->compare('TBVN',$this->TBVN,true);
		$criteria->compare('ObservacionID',$this->ObservacionID);
		$criteria->compare('Estado',$this->Estado);
                $criteria->compare('ProveedorId',$this->ProveedorId);
                $criteria->compare('Placa',$this->Placa,true);
                $criteria->compare('Chofer',$this->Chofer,true);
                if($f_ini && $f_fin)
                    $criteria->addBetweenCondition("CONCAT (Fecha, ' ',Hora)", $f_ini, $f_fin);
                
                $session=new CHttpSession;
                $session->open();
                $session['reporteRango']=$criteria;  //Esto para guardar la criteria en la sesión actual para usarlo posteriormente.

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recepcion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
