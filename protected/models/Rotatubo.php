<?php

/**
 * This is the model class for table "rotatubo".
 *
 * The followings are the available columns in table 'rotatubo':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoRotatubo
 * @property integer $Estado
 * @property double $RotatuboMinAmperaje
 * @property double $RotatuboMaxAmperaje
 * @property double $RotatuboAmperajeAlertaNaranja
 * @property double $RotatuboMinPresionEje
 * @property double $RotatuboMaxPresionEje
 * @property double $RotatuboPresionEjeAlertaNaranja
 * @property double $RotatuboMinSRC
 * @property double $RotatuboMaxSRC
 * @property double $RotatuboSRCAlertaNaranja
 * @property double $RotatuboMinHumedad
 * @property double $RotatuboMaxHumedad
 * @property double $RotatuboHumedadAlertaNaranja
 *
 * The followings are the available model relations:
 * @property Secado[] $secados
 */
class Rotatubo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rotatubo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, RotatuboMinAmperaje, RotatuboMaxAmperaje, RotatuboAmperajeAlertaNaranja, RotatuboMinPresionEje, RotatuboMaxPresionEje, RotatuboPresionEjeAlertaNaranja, RotatuboMinSRC, RotatuboMaxSRC, RotatuboSRCAlertaNaranja, RotatuboMinHumedad, RotatuboMaxHumedad, RotatuboHumedadAlertaNaranja', 'required'),
		/*array('Nombre','unique', 'message' => 'Nombre del Rotatubo ya existe'),	*/
                    array('EstadoRotatubo, Estado', 'numerical', 'integerOnly'=>true),
			array('RotatuboMinAmperaje, RotatuboMaxAmperaje, RotatuboAmperajeAlertaNaranja, RotatuboMinPresionEje, RotatuboMaxPresionEje, RotatuboPresionEjeAlertaNaranja, RotatuboMinSRC, RotatuboMaxSRC, RotatuboSRCAlertaNaranja, RotatuboMinHumedad, RotatuboMaxHumedad, RotatuboHumedadAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>100),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoRotatubo, Estado, RotatuboMinAmperaje, RotatuboMaxAmperaje, RotatuboAmperajeAlertaNaranja, RotatuboMinPresionEje, RotatuboMaxPresionEje, RotatuboPresionEjeAlertaNaranja, RotatuboMinSRC, RotatuboMaxSRC, RotatuboSRCAlertaNaranja, RotatuboMinHumedad, RotatuboMaxHumedad, RotatuboHumedadAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'secados' => array(self::HAS_MANY, 'Secado', 'RotatuboID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoRotatubo' => 'Estado',
			'Estado' => 'Estado de eliminación',
			'RotatuboMinAmperaje' =>  'Valor Mínimo:',
			'RotatuboMaxAmperaje' => 'Valor Máximo: ',
			'RotatuboAmperajeAlertaNaranja' =>  'Porcentaje de Alerta Naranja:',
			'RotatuboMinPresionEje' =>  'Valor Mínimo:',
			'RotatuboMaxPresionEje' => 'Valor Máximo: ',
			'RotatuboPresionEjeAlertaNaranja' =>  'Porcentaje de Alerta Naranja:',
			'RotatuboMinSRC' =>  'Valor Mínimo:',
			'RotatuboMaxSRC' => 'Valor Máximo: ',
			'RotatuboSRCAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'RotatuboMinHumedad' => 'Valor Mínimo:',
			'RotatuboMaxHumedad' => 'Valor Máximo: ',
			'RotatuboHumedadAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoRotatubo',$this->EstadoRotatubo);
		$criteria->compare('Estado',1);
		$criteria->compare('RotatuboMinAmperaje',$this->RotatuboMinAmperaje);
		$criteria->compare('RotatuboMaxAmperaje',$this->RotatuboMaxAmperaje);
		$criteria->compare('RotatuboAmperajeAlertaNaranja',$this->RotatuboAmperajeAlertaNaranja);
		$criteria->compare('RotatuboMinPresionEje',$this->RotatuboMinPresionEje);
		$criteria->compare('RotatuboMaxPresionEje',$this->RotatuboMaxPresionEje);
		$criteria->compare('RotatuboPresionEjeAlertaNaranja',$this->RotatuboPresionEjeAlertaNaranja);
		$criteria->compare('RotatuboMinSRC',$this->RotatuboMinSRC);
		$criteria->compare('RotatuboMaxSRC',$this->RotatuboMaxSRC);
		$criteria->compare('RotatuboSRCAlertaNaranja',$this->RotatuboSRCAlertaNaranja);
		$criteria->compare('RotatuboMinHumedad',$this->RotatuboMinHumedad);
		$criteria->compare('RotatuboMaxHumedad',$this->RotatuboMaxHumedad);
		$criteria->compare('RotatuboHumedadAlertaNaranja',$this->RotatuboHumedadAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rotatubo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
