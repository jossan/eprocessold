<?php

/**
 * This is the model class for table "tricanter".
 *
 * The followings are the available columns in table 'tricanter':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoTricanter
 * @property integer $Estado
 * @property double $DecantacionMinAlimentacionTemperatura
 * @property double $DecantacionMaxAlimentacionTemperatura
 * @property double $DecantacionAlimentacionTemperaturaAlertaNaranja
 * @property double $DecantacionMinAlimentacionPorcentajeHumedad
 * @property double $DecantacionMaxAlimentacionPorcentajeHumedad
 * @property double $DecantacionAlimentacionPorcentajeHumedadAlertaNaranja
 * @property double $DecantacionMinAguaColaPorcentajeSolidos
 * @property double $DecantacionMaxAguaColaPorcentajeSolidos
 * @property double $DecantacionAguaColaPorcentajeSolidosAlertaNaranja
 * @property double $DecantacionMinAguaColaPorcentajeGrasas
 * @property double $DecantacionMaxAguaColaPorcentajeGrasas
 * @property double $DecantacionAguaColaPorcentajeGrasasAlertaNaranja
 *
 * The followings are the available model relations:
 * @property Decantacion[] $decantacions
 */
class Tricanter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tricanter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, DecantacionMinAlimentacionTemperatura, DecantacionMaxAlimentacionTemperatura, DecantacionAlimentacionTemperaturaAlertaNaranja, DecantacionMinAlimentacionPorcentajeHumedad, DecantacionMaxAlimentacionPorcentajeHumedad, DecantacionAlimentacionPorcentajeHumedadAlertaNaranja, DecantacionMinAguaColaPorcentajeSolidos, DecantacionMaxAguaColaPorcentajeSolidos, DecantacionAguaColaPorcentajeSolidosAlertaNaranja, DecantacionMinAguaColaPorcentajeGrasas, DecantacionMaxAguaColaPorcentajeGrasas, DecantacionAguaColaPorcentajeGrasasAlertaNaranja', 'required'),
                    /*array('Nombre','unique', 'message' => 'Nombre del tricanter ya existe'),*/
			array('EstadoTricanter, Estado', 'numerical', 'integerOnly'=>true),
			array('DecantacionMinAlimentacionTemperatura, DecantacionMaxAlimentacionTemperatura, DecantacionAlimentacionTemperaturaAlertaNaranja, DecantacionMinAlimentacionPorcentajeHumedad, DecantacionMaxAlimentacionPorcentajeHumedad, DecantacionAlimentacionPorcentajeHumedadAlertaNaranja, DecantacionMinAguaColaPorcentajeSolidos, DecantacionMaxAguaColaPorcentajeSolidos, DecantacionAguaColaPorcentajeSolidosAlertaNaranja, DecantacionMinAguaColaPorcentajeGrasas, DecantacionMaxAguaColaPorcentajeGrasas, DecantacionAguaColaPorcentajeGrasasAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoTricanter, Estado, DecantacionMinAlimentacionTemperatura, DecantacionMaxAlimentacionTemperatura, DecantacionAlimentacionTemperaturaAlertaNaranja, DecantacionMinAlimentacionPorcentajeHumedad, DecantacionMaxAlimentacionPorcentajeHumedad, DecantacionAlimentacionPorcentajeHumedadAlertaNaranja, DecantacionMinAguaColaPorcentajeSolidos, DecantacionMaxAguaColaPorcentajeSolidos, DecantacionAguaColaPorcentajeSolidosAlertaNaranja, DecantacionMinAguaColaPorcentajeGrasas, DecantacionMaxAguaColaPorcentajeGrasas, DecantacionAguaColaPorcentajeGrasasAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'decantacions' => array(self::HAS_MANY, 'Decantacion', 'TricanterID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoTricanter' => 'Estado',
			'Estado' => 'Estado de Eliminacion',
			'DecantacionMinAlimentacionTemperatura' => 'Valor Mínimo:',
			'DecantacionMaxAlimentacionTemperatura' => 'Valor Máximo:',
			'DecantacionAlimentacionTemperaturaAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'DecantacionMinAlimentacionPorcentajeHumedad' => 'Valor Mínimo:',
			'DecantacionMaxAlimentacionPorcentajeHumedad' => 'Valor Máximo:',
			'DecantacionAlimentacionPorcentajeHumedadAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'DecantacionMinAguaColaPorcentajeSolidos' => 'Valor Mínimo:',
			'DecantacionMaxAguaColaPorcentajeSolidos' => 'Valor Máximo:',
			'DecantacionAguaColaPorcentajeSolidosAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'DecantacionMinAguaColaPorcentajeGrasas' => 'Valor Mínimo:',
			'DecantacionMaxAguaColaPorcentajeGrasas' => 'Valor Máximo:',
			'DecantacionAguaColaPorcentajeGrasasAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoTricanter',$this->EstadoTricanter);
		$criteria->compare('Estado',1);
		$criteria->compare('DecantacionMinAlimentacionTemperatura',$this->DecantacionMinAlimentacionTemperatura);
		$criteria->compare('DecantacionMaxAlimentacionTemperatura',$this->DecantacionMaxAlimentacionTemperatura);
		$criteria->compare('DecantacionAlimentacionTemperaturaAlertaNaranja',$this->DecantacionAlimentacionTemperaturaAlertaNaranja);
		$criteria->compare('DecantacionMinAlimentacionPorcentajeHumedad',$this->DecantacionMinAlimentacionPorcentajeHumedad);
		$criteria->compare('DecantacionMaxAlimentacionPorcentajeHumedad',$this->DecantacionMaxAlimentacionPorcentajeHumedad);
		$criteria->compare('DecantacionAlimentacionPorcentajeHumedadAlertaNaranja',$this->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja);
		$criteria->compare('DecantacionMinAguaColaPorcentajeSolidos',$this->DecantacionMinAguaColaPorcentajeSolidos);
		$criteria->compare('DecantacionMaxAguaColaPorcentajeSolidos',$this->DecantacionMaxAguaColaPorcentajeSolidos);
		$criteria->compare('DecantacionAguaColaPorcentajeSolidosAlertaNaranja',$this->DecantacionAguaColaPorcentajeSolidosAlertaNaranja);
		$criteria->compare('DecantacionMinAguaColaPorcentajeGrasas',$this->DecantacionMinAguaColaPorcentajeGrasas);
		$criteria->compare('DecantacionMaxAguaColaPorcentajeGrasas',$this->DecantacionMaxAguaColaPorcentajeGrasas);
		$criteria->compare('DecantacionAguaColaPorcentajeGrasasAlertaNaranja',$this->DecantacionAguaColaPorcentajeGrasasAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tricanter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
