<?php

/**
 * This is the model class for table "pulidora".
 *
 * The followings are the available columns in table 'pulidora':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoPulidora
 * @property integer $Estado
 * @property double $RefinacionMinAlimentacionTemperatura
 * @property double $RefinacionMaxAlimentacionTemperatura
 * @property double $RefinacionTemperaturaAlertaNaranja
 * @property double $RefinacionMinAceitePorcentajeHumedad
 * @property double $RefinacionMaxAceitePorcentajeHumedad
 * @property double $RefinacionAceitePorcentajeHumedadAlertaNaranja
 * @property double $RefinacionMinAceitePorcentajeSolidos
 * @property double $RefinacionMaxAceitePorcentajeSolidos
 * @property double $RefinacionAceitePorcentajeSolidosAlertaNaranja
 * @property double $RefinacionMinAceitePorcentajeAcidez
 * @property double $RefinacionMaxAceitePorcentajeAcidez
 * @property double $RefinacionAceitePorcentajeAcidezAlertaNaranja
 */
class Pulidora extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pulidora';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, RefinacionMinAlimentacionTemperatura, RefinacionMaxAlimentacionTemperatura, RefinacionTemperaturaAlertaNaranja, RefinacionMinAceitePorcentajeHumedad, RefinacionMaxAceitePorcentajeHumedad, RefinacionAceitePorcentajeHumedadAlertaNaranja, RefinacionMinAceitePorcentajeSolidos, RefinacionMaxAceitePorcentajeSolidos, RefinacionAceitePorcentajeSolidosAlertaNaranja, RefinacionMinAceitePorcentajeAcidez, RefinacionMaxAceitePorcentajeAcidez, RefinacionAceitePorcentajeAcidezAlertaNaranja', 'required'),
			array('EstadoPulidora, Estado', 'numerical', 'integerOnly'=>true),
                    /*array('Nombre','unique', 'message' => 'Nombre de la pulidora ya existe'),*/
			array('RefinacionMinAlimentacionTemperatura, RefinacionMaxAlimentacionTemperatura, RefinacionTemperaturaAlertaNaranja, RefinacionMinAceitePorcentajeHumedad, RefinacionMaxAceitePorcentajeHumedad, RefinacionAceitePorcentajeHumedadAlertaNaranja, RefinacionMinAceitePorcentajeSolidos, RefinacionMaxAceitePorcentajeSolidos, RefinacionAceitePorcentajeSolidosAlertaNaranja, RefinacionMinAceitePorcentajeAcidez, RefinacionMaxAceitePorcentajeAcidez, RefinacionAceitePorcentajeAcidezAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoPulidora, Estado, RefinacionMinAlimentacionTemperatura, RefinacionMaxAlimentacionTemperatura, RefinacionTemperaturaAlertaNaranja, RefinacionMinAceitePorcentajeHumedad, RefinacionMaxAceitePorcentajeHumedad, RefinacionAceitePorcentajeHumedadAlertaNaranja, RefinacionMinAceitePorcentajeSolidos, RefinacionMaxAceitePorcentajeSolidos, RefinacionAceitePorcentajeSolidosAlertaNaranja, RefinacionMinAceitePorcentajeAcidez, RefinacionMaxAceitePorcentajeAcidez, RefinacionAceitePorcentajeAcidezAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoPulidora' => 'Estado Pulidora',
			'Estado' => 'Estado de Eliminación',
			'RefinacionMinAlimentacionTemperatura' => 'Valor Mínimo:',
			'RefinacionMaxAlimentacionTemperatura' => 'Valor Máximo: ',
			'RefinacionTemperaturaAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'RefinacionMinAceitePorcentajeHumedad' => 'Valor Mínimo:',
			'RefinacionMaxAceitePorcentajeHumedad' => 'Valor Máximo: ',
			'RefinacionAceitePorcentajeHumedadAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'RefinacionMinAceitePorcentajeSolidos' => 'Valor Mínimo:',
			'RefinacionMaxAceitePorcentajeSolidos' => 'Valor Máximo: ',
			'RefinacionAceitePorcentajeSolidosAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'RefinacionMinAceitePorcentajeAcidez' => 'Valor Mínimo:',
			'RefinacionMaxAceitePorcentajeAcidez' => 'Valor Máximo: ',
			'RefinacionAceitePorcentajeAcidezAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoPulidora',$this->EstadoPulidora);
		$criteria->compare('Estado',1);
		$criteria->compare('RefinacionMinAlimentacionTemperatura',$this->RefinacionMinAlimentacionTemperatura);
		$criteria->compare('RefinacionMaxAlimentacionTemperatura',$this->RefinacionMaxAlimentacionTemperatura);
		$criteria->compare('RefinacionTemperaturaAlertaNaranja',$this->RefinacionTemperaturaAlertaNaranja);
		$criteria->compare('RefinacionMinAceitePorcentajeHumedad',$this->RefinacionMinAceitePorcentajeHumedad);
		$criteria->compare('RefinacionMaxAceitePorcentajeHumedad',$this->RefinacionMaxAceitePorcentajeHumedad);
		$criteria->compare('RefinacionAceitePorcentajeHumedadAlertaNaranja',$this->RefinacionAceitePorcentajeHumedadAlertaNaranja);
		$criteria->compare('RefinacionMinAceitePorcentajeSolidos',$this->RefinacionMinAceitePorcentajeSolidos);
		$criteria->compare('RefinacionMaxAceitePorcentajeSolidos',$this->RefinacionMaxAceitePorcentajeSolidos);
		$criteria->compare('RefinacionAceitePorcentajeSolidosAlertaNaranja',$this->RefinacionAceitePorcentajeSolidosAlertaNaranja);
		$criteria->compare('RefinacionMinAceitePorcentajeAcidez',$this->RefinacionMinAceitePorcentajeAcidez);
		$criteria->compare('RefinacionMaxAceitePorcentajeAcidez',$this->RefinacionMaxAceitePorcentajeAcidez);
		$criteria->compare('RefinacionAceitePorcentajeAcidezAlertaNaranja',$this->RefinacionAceitePorcentajeAcidezAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pulidora the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
