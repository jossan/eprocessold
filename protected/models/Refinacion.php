<?php

/**
 * This is the model class for table "Refinacion".
 *
 * The followings are the available columns in table 'Refinacion':
 * @property string $ID
 * @property string $RevisionID
 * @property integer $PulidoraID
 * @property double $AlimentacionTemperatura
 * @property double $AceitePorcentajeHumedad
 * @property double $AceitePorcentajeSolidos
 * @property double $AceitePorcentajeAcidez
 * @property integer $Estado
  * @property time $Hora
 * @property string $Fecha
 * @property string $codigofila
 */
class Refinacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'refinacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RevisionID, PulidoraID', 'required'),
			array('PulidoraID, Estado', 'numerical', 'integerOnly'=>true),
			array('AlimentacionTemperatura, AceitePorcentajeHumedad, AceitePorcentajeSolidos, AceitePorcentajeAcidez', 'numerical'),
			array('RevisionID', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha,Hora,codigofila,IdUsuario, PulidoraID, AlimentacionTemperatura, AceitePorcentajeHumedad, AceitePorcentajeSolidos, AceitePorcentajeAcidez, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pkpulidora' => array(self::BELONGS_TO, 'Pulidora', 'PulidoraID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'PulidoraID' => 'Pulidora',
			'AlimentacionTemperatura' => 'Alimentacion Temperatura',
			'AceitePorcentajeHumedad' => 'Aceite Porcentaje Humedad',
			'AceitePorcentajeSolidos' => 'Aceite Porcentaje Solidos',
			'AceitePorcentajeAcidez' => 'Aceite Porcentaje Acidez',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('PulidoraID',$this->PulidoraID);
		$criteria->compare('AlimentacionTemperatura',$this->AlimentacionTemperatura);
		$criteria->compare('AceitePorcentajeHumedad',$this->AceitePorcentajeHumedad);
		$criteria->compare('AceitePorcentajeSolidos',$this->AceitePorcentajeSolidos);
		$criteria->compare('AceitePorcentajeAcidez',$this->AceitePorcentajeAcidez);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Refinacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
