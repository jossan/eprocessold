<?php

/**
 * This is the model class for table "prensa".
 *
 * The followings are the available columns in table 'prensa':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoPrensa
 * @property integer $Estado
 * @property double $PrensadoMinAmperaje
 * @property double $PrensadoMaxAmperaje
 * @property double $PrensadoAmperajeAlertaNaranja
 * @property double $PrensadoMinPorcentajeHumedad
 * @property double $PrensadoMaxPorcentajeHumedad
 * @property double $PrensadoPorcentajeHumedadAlertaNaranja
 *
 * The followings are the available model relations:
 * @property Prensado[] $prensados
 */
class Prensa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prensa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, PrensadoMinAmperaje, PrensadoMaxAmperaje, PrensadoAmperajeAlertaNaranja, PrensadoMinPorcentajeHumedad, PrensadoMaxPorcentajeHumedad, PrensadoPorcentajeHumedadAlertaNaranja', 'required'),
		/*array('Nombre','unique', 'message' => 'Nombre de la prensa ya existe'),	*/
                    array('EstadoPrensa, Estado', 'numerical', 'integerOnly'=>true),
			array('PrensadoMinAmperaje, PrensadoMaxAmperaje, PrensadoAmperajeAlertaNaranja, PrensadoMinPorcentajeHumedad, PrensadoMaxPorcentajeHumedad, PrensadoPorcentajeHumedadAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoPrensa, Estado, PrensadoMinAmperaje, PrensadoMaxAmperaje, PrensadoAmperajeAlertaNaranja, PrensadoMinPorcentajeHumedad, PrensadoMaxPorcentajeHumedad, PrensadoPorcentajeHumedadAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prensados' => array(self::HAS_MANY, 'Prensado', 'PrensaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoPrensa' => 'Estado',
			'Estado' => 'Estado',
			'PrensadoMinAmperaje' => 'Valor Mínimo:',
			'PrensadoMaxAmperaje' => 'Valor Máximo:',
			'PrensadoAmperajeAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'PrensadoMinPorcentajeHumedad' => 'Valor Mínimo:',
			'PrensadoMaxPorcentajeHumedad' => 'Valor Máximo:',
			'PrensadoPorcentajeHumedadAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoPrensa',$this->EstadoPrensa);
		$criteria->compare('Estado',1);
		$criteria->compare('PrensadoMinAmperaje',$this->PrensadoMinAmperaje);
		$criteria->compare('PrensadoMaxAmperaje',$this->PrensadoMaxAmperaje);
		$criteria->compare('PrensadoAmperajeAlertaNaranja',$this->PrensadoAmperajeAlertaNaranja);
		$criteria->compare('PrensadoMinPorcentajeHumedad',$this->PrensadoMinPorcentajeHumedad);
		$criteria->compare('PrensadoMaxPorcentajeHumedad',$this->PrensadoMaxPorcentajeHumedad);
		$criteria->compare('PrensadoPorcentajeHumedadAlertaNaranja',$this->PrensadoPorcentajeHumedadAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prensa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
