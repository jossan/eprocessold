<?php

/**
 * This is the model class for table "parametros".
 *
 * The followings are the available columns in table 'parametros':
 * @property integer $ID
 * @property double $LicorMinPorcentajeSolidos
 * @property double $LicorMaxPorcentajeSolidos
 * @property double $LicorPorcentajeSolidosAlertaNaranja
 * @property double $LicorMinPorcentajeGrasas
 * @property double $LicorMaxPorcentajeGrasas
 * @property double $LicorPorcentajeGrasasAlertaNaranja
 * @property double $HarinaMinTemperatura
 * @property double $HarinaMaxTemperatura
 * @property double $HarinaTemperaturaAlertaNaranja
 * @property double $HarinaMinHumedad
 * @property double $HarinaMaxHumedad
 * @property double $HarinaHumedadAlertaNaranja
 * @property double $HarinappmMin
 * @property double $HarinappmMax
 * @property double $HarinappmAlertaNaranja
 * @property double $HarinapesoMin
 * @property double $HarinapesoMax
 * @property double $HarinapesoAlertaNaranja
 * @property double $HarinaprotMin
 * @property double $HarinaprotMax
 * @property double $HarinaprotAlertaNaranja
 * @property double $HarinaTBVNmin
 * @property double $HarinaTBVNmax
 * @property double $HarinaTBVNAlertaNaranja
 */
class Parametros extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parametros';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja, LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja', 'required'),
			array('LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja, LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja, nombreempresa,HarinaMinTemperatura, HarinaMaxTemperatura, HarinaTemperaturaAlertaNaranja, HarinaMinHumedad, HarinaMaxHumedad, HarinaHumedadAlertaNaranja, HarinappmMin, HarinappmMax, HarinappmAlertaNaranja, HarinapesoMin, HarinapesoMax, HarinapesoAlertaNaranja, HarinaprotMin, HarinaprotMax, HarinaprotAlertaNaranja, HarinaTBVNmin, HarinaTBVNmax, HarinaTBVNAlertaNaranja', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, nombreempresa, LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja, LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja, HarinaMinTemperatura, HarinaMaxTemperatura, HarinaTemperaturaAlertaNaranja, HarinaMinHumedad, HarinaMaxHumedad, HarinaHumedadAlertaNaranja, HarinappmMin, HarinappmMax, HarinappmAlertaNaranja, HarinapesoMin, HarinapesoMax, HarinapesoAlertaNaranja, HarinaprotMin, HarinaprotMax, HarinaprotAlertaNaranja, HarinaTBVNmin, HarinaTBVNmax, HarinaTBVNAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LicorMinPorcentajeSolidos' => 'Licor Min Porcentaje Solidos',
			'LicorMaxPorcentajeSolidos' => 'Licor Max Porcentaje Solidos',
			'LicorPorcentajeSolidosAlertaNaranja' => 'Licor Porcentaje Solidos Alerta Naranja',
			'LicorMinPorcentajeGrasas' => 'Licor Min Porcentaje Grasas',
			'LicorMaxPorcentajeGrasas' => 'Licor Max Porcentaje Grasas',
			'LicorPorcentajeGrasasAlertaNaranja' => 'Licor Porcentaje Grasas Alerta Naranja',
			'HarinaMinTemperatura' => 'Harina Min Temperatura',
			'HarinaMaxTemperatura' => 'Harina Max Temperatura',
			'HarinaTemperaturaAlertaNaranja' => 'Harina Temperatura Alerta Naranja',
			'HarinaMinHumedad' => 'Harina Min Humedad',
			'HarinaMaxHumedad' => 'Harina Max Humedad',
			'HarinaHumedadAlertaNaranja' => 'Harina Humedad Alerta Naranja',
			'HarinappmMin' => 'Harinappm Min',
			'HarinappmMax' => 'Harinappm Max',
			'HarinappmAlertaNaranja' => 'Harinappm Alerta Naranja',
			'HarinapesoMin' => 'Harinapeso Min',
			'HarinapesoMax' => 'Harinapeso Max',
			'HarinapesoAlertaNaranja' => 'Harinapeso Alerta Naranja',
			'HarinaprotMin' => 'Harinaprot Min',
			'HarinaprotMax' => 'Harinaprot Max',
			'HarinaprotAlertaNaranja' => 'Harinaprot Alerta Naranja',
			'HarinaTBVNmin' => 'Harina Tbvnmin',
			'HarinaTBVNmax' => 'Harina Tbvnmax',
			'HarinaTBVNAlertaNaranja' => 'Harina Tbvnalerta Naranja',
                        'nombreempresa'=>'Nombre empresa'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LicorMinPorcentajeSolidos',$this->LicorMinPorcentajeSolidos);
		$criteria->compare('LicorMaxPorcentajeSolidos',$this->LicorMaxPorcentajeSolidos);
		$criteria->compare('LicorPorcentajeSolidosAlertaNaranja',$this->LicorPorcentajeSolidosAlertaNaranja);
		$criteria->compare('LicorMinPorcentajeGrasas',$this->LicorMinPorcentajeGrasas);
		$criteria->compare('LicorMaxPorcentajeGrasas',$this->LicorMaxPorcentajeGrasas);
		$criteria->compare('LicorPorcentajeGrasasAlertaNaranja',$this->LicorPorcentajeGrasasAlertaNaranja);
		$criteria->compare('HarinaMinTemperatura',$this->HarinaMinTemperatura);
		$criteria->compare('HarinaMaxTemperatura',$this->HarinaMaxTemperatura);
		$criteria->compare('HarinaTemperaturaAlertaNaranja',$this->HarinaTemperaturaAlertaNaranja);
		$criteria->compare('HarinaMinHumedad',$this->HarinaMinHumedad);
		$criteria->compare('HarinaMaxHumedad',$this->HarinaMaxHumedad);
		$criteria->compare('HarinaHumedadAlertaNaranja',$this->HarinaHumedadAlertaNaranja);
		$criteria->compare('HarinappmMin',$this->HarinappmMin);
		$criteria->compare('HarinappmMax',$this->HarinappmMax);
		$criteria->compare('HarinappmAlertaNaranja',$this->HarinappmAlertaNaranja);
		$criteria->compare('HarinapesoMin',$this->HarinapesoMin);
		$criteria->compare('HarinapesoMax',$this->HarinapesoMax);
		$criteria->compare('HarinapesoAlertaNaranja',$this->HarinapesoAlertaNaranja);
		$criteria->compare('HarinaprotMin',$this->HarinaprotMin);
		$criteria->compare('HarinaprotMax',$this->HarinaprotMax);
		$criteria->compare('HarinaprotAlertaNaranja',$this->HarinaprotAlertaNaranja);
		$criteria->compare('HarinaTBVNmin',$this->HarinaTBVNmin);
		$criteria->compare('HarinaTBVNmax',$this->HarinaTBVNmax);
		$criteria->compare('HarinaTBVNAlertaNaranja',$this->HarinaTBVNAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Parametros the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
