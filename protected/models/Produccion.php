<?php

/**
 * This is the model class for table "produccion".
 *
 * The followings are the available columns in table 'produccion':
 * @property string $ID
 * @property string $RecepcionID
 * @property double $PesoProduccion
 * @property string $FechaProduccion
 * @property string $Status
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Recepcion $recepcion
 */
class Produccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'produccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Estado', 'numerical', 'integerOnly'=>true),
			array('PesoProduccion', 'numerical'),
			array('RecepcionID', 'length', 'max'=>11),
			array('Status', 'length', 'max'=>50),
			array('FechaProduccion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, RecepcionID, PesoProduccion, FechaProduccion, Status, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'recep' => array(self::BELONGS_TO, 'Recepcion', 'RecepcionID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'RecepcionID' => 'Recepcion',
			'PesoProduccion' => 'Peso Produccion',
			'FechaProduccion' => 'Fecha Produccion',
			'Status' => 'Status',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('RecepcionID',$this->RecepcionID,true);
		$criteria->compare('PesoProduccion',$this->PesoProduccion);
		$criteria->compare('FechaProduccion',$this->FechaProduccion,true);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Produccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
