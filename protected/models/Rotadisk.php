<?php

/**
 * This is the model class for table "rotadisk".
 *
 * The followings are the available columns in table 'rotadisk':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoRotadisk
 * @property integer $Estado
 * @property double $PresecadoMinAmperaje
 * @property double $PresecadoMaxAmperaje
 * @property double $PresecadoAmperajeAlertaNaranja
 * @property double $PresecadoMinPresionEje
 * @property double $PresecadoMaxPresionEje
 * @property double $PresecadoPresionEjeAlertaNaranja
 * @property double $PresecadoMinPresionChaqueta
 * @property double $PresecadoMaxPresionChaqueta
 * @property double $PresecadoPresionChaquetaAlertaNaranja
 * @property double $PresecadoMinSCR
 * @property double $PresecadoMaxSCR
 * @property double $PresecadoSCRAlertaNaranja
 * @property double $PresecadoMinPorcentajeHumedad
 * @property double $PresecadoMaxPorcentajeHumedad
 * @property double $PresecadoPorcentajeHumedadAlertaNaranja
 *
 * The followings are the available model relations:
 * @property Presecado[] $presecados
 */
class Rotadisk extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rotadisk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, PresecadoMinAmperaje, PresecadoMaxAmperaje, PresecadoAmperajeAlertaNaranja, PresecadoMinPresionEje, PresecadoMaxPresionEje, PresecadoPresionEjeAlertaNaranja, PresecadoMinPresionChaqueta, PresecadoMaxPresionChaqueta, PresecadoPresionChaquetaAlertaNaranja, PresecadoMinSCR, PresecadoMaxSCR, PresecadoSCRAlertaNaranja, PresecadoMinPorcentajeHumedad, PresecadoMaxPorcentajeHumedad, PresecadoPorcentajeHumedadAlertaNaranja,PresecadoMinTempCha1,PresecadoMaxTempCha1,PresecadoMinTempCha2,PresecadoAlertaTempCha1,PresecadoAlertaTempCha2,PresecadoMaxTempCha2', 'required'),
		 /*array('Nombre','unique', 'message' => 'Nombre del rotadisk ya existe'),	*/
                    array('EstadoRotadisk, Estado', 'numerical', 'integerOnly'=>true),
			array('PresecadoMinAmperaje, PresecadoMaxAmperaje, PresecadoAmperajeAlertaNaranja, PresecadoMinPresionEje, PresecadoMaxPresionEje, PresecadoPresionEjeAlertaNaranja, PresecadoMinPresionChaqueta, PresecadoMaxPresionChaqueta, PresecadoPresionChaquetaAlertaNaranja, PresecadoMinSCR, PresecadoMaxSCR, PresecadoSCRAlertaNaranja, PresecadoMinPorcentajeHumedad, PresecadoMaxPorcentajeHumedad, PresecadoPorcentajeHumedadAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoRotadisk, Estado, PresecadoMinAmperaje, PresecadoMaxAmperaje, PresecadoAmperajeAlertaNaranja, PresecadoMinPresionEje, PresecadoMaxPresionEje, PresecadoPresionEjeAlertaNaranja, PresecadoMinPresionChaqueta, PresecadoMaxPresionChaqueta, PresecadoPresionChaquetaAlertaNaranja, PresecadoMinSCR, PresecadoMaxSCR, PresecadoSCRAlertaNaranja, PresecadoMinPorcentajeHumedad, PresecadoMaxPorcentajeHumedad, PresecadoPorcentajeHumedadAlertaNaranja,PresecadoMinTempCha1,PresecadoMaxTempCha1,PresecadoMinTempCha2,PresecadoAlertaTempCha1,PresecadoAlertaTempCha2,PresecadoMaxTempCha2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'presecados' => array(self::HAS_MANY, 'Presecado', 'RotadiskID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoRotadisk' => 'Estado',
			'Estado' => 'Estado de Eliminación',
			'PresecadoMinAmperaje' =>  'Valor Mínimo:',
			'PresecadoMaxAmperaje' => 'Valor Máximo: ',
			'PresecadoAmperajeAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'PresecadoMinPresionEje' =>  'Valor Mínimo:',
			'PresecadoMaxPresionEje' => 'Valor Máximo: ',
			'PresecadoPresionEjeAlertaNaranja' =>  'Porcentaje de Alerta Naranja:',
			'PresecadoMinPresionChaqueta' =>  'Valor Mínimo:',
			'PresecadoMaxPresionChaqueta' =>'Valor Máximo: ',
			'PresecadoPresionChaquetaAlertaNaranja' =>  'Porcentaje de Alerta Naranja:',
			'PresecadoMinSCR' =>  'Valor Mínimo:',
			'PresecadoMaxSCR' => 'Valor Máximo: ',
			'PresecadoSCRAlertaNaranja' => 'Porcentaje de alerta Naranja',
			'PresecadoMinPorcentajeHumedad' =>  'Valor Mínimo:',
			'PresecadoMaxPorcentajeHumedad' => 'Valor Máximo: ',
			'PresecadoPorcentajeHumedadAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoRotadisk',$this->EstadoRotadisk);
		$criteria->compare('Estado',1);
		$criteria->compare('PresecadoMinAmperaje',$this->PresecadoMinAmperaje);
		$criteria->compare('PresecadoMaxAmperaje',$this->PresecadoMaxAmperaje);
		$criteria->compare('PresecadoAmperajeAlertaNaranja',$this->PresecadoAmperajeAlertaNaranja);
		$criteria->compare('PresecadoMinPresionEje',$this->PresecadoMinPresionEje);
		$criteria->compare('PresecadoMaxPresionEje',$this->PresecadoMaxPresionEje);
		$criteria->compare('PresecadoPresionEjeAlertaNaranja',$this->PresecadoPresionEjeAlertaNaranja);
		$criteria->compare('PresecadoMinPresionChaqueta',$this->PresecadoMinPresionChaqueta);
		$criteria->compare('PresecadoMaxPresionChaqueta',$this->PresecadoMaxPresionChaqueta);
		$criteria->compare('PresecadoPresionChaquetaAlertaNaranja',$this->PresecadoPresionChaquetaAlertaNaranja);
		$criteria->compare('PresecadoMinSCR',$this->PresecadoMinSCR);
		$criteria->compare('PresecadoMaxSCR',$this->PresecadoMaxSCR);
		$criteria->compare('PresecadoSCRAlertaNaranja',$this->PresecadoSCRAlertaNaranja);
		$criteria->compare('PresecadoMinPorcentajeHumedad',$this->PresecadoMinPorcentajeHumedad);
		$criteria->compare('PresecadoMaxPorcentajeHumedad',$this->PresecadoMaxPorcentajeHumedad);
		$criteria->compare('PresecadoPorcentajeHumedadAlertaNaranja',$this->PresecadoPorcentajeHumedadAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rotadisk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
