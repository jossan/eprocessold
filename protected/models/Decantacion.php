<?php

/**
 * This is the model class for table "Decantacion".
 *
 * The followings are the available columns in table 'Decantacion':
 * @property string $ID
 * @property string $RevisionID
 * @property integer $TricanterID
 * @property double $AlimentacionTemperatura
 * @property double $AlimentacionPorcentajeHumedad
 * @property double $AguaColaPorcentajeSolidos
 * @property double $AguaColaPorcentajeGrasas
 * @property integer $Estado
 * @property date $Fecha
 * @property time $Hora
 * @property string codigofila
 * The followings are the available model relations:
 * @property Recepcion $revision
 * @property Tricanter $tricanter
 */
class Decantacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'decantacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TricanterID', 'required'),
			array('TricanterID, Estado', 'numerical', 'integerOnly'=>true),
			array('AlimentacionTemperatura, AlimentacionPorcentajeHumedad, AguaColaPorcentajeSolidos, AguaColaPorcentajeGrasas', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha,Hora,codigofila,IdUsuario, TricanterID, AlimentacionTemperatura, AlimentacionPorcentajeHumedad, AguaColaPorcentajeSolidos, AguaColaPorcentajeGrasas, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tricanter' => array(self::BELONGS_TO, 'Tricanter', 'TricanterID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'RevisionID' => 'Revision',
			'TricanterID' => 'Tricanter',
			'AlimentacionTemperatura' => 'Alimentacion Temperatura',
			'AlimentacionPorcentajeHumedad' => 'Alimentacion Porcentaje Humedad',
			'AguaColaPorcentajeSolidos' => 'Agua Cola Porcentaje Solidos',
			'AguaColaPorcentajeGrasas' => 'Agua Cola Porcentaje Grasas',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('TricanterID',$this->TricanterID);
		$criteria->compare('AlimentacionTemperatura',$this->AlimentacionTemperatura);
		$criteria->compare('AlimentacionPorcentajeHumedad',$this->AlimentacionPorcentajeHumedad);
		$criteria->compare('AguaColaPorcentajeSolidos',$this->AguaColaPorcentajeSolidos);
		$criteria->compare('AguaColaPorcentajeGrasas',$this->AguaColaPorcentajeGrasas);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Decantacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
