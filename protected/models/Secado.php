<?php

/**
 * This is the model class for table "Secado".
 *
 * The followings are the available columns in table 'Secado':
 * @property string $ID
 * @property integer $RotatuboID
 * @property double $Amperaje
 * @property double $PresionEje
 * @property double $SRC
 * @property double $PorcentajeHumedad
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Recepcion $revision
 * @property Rotatubo $rotatubo
 */
class Secado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'secado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RotatuboID', 'required'),
			array('RotatuboID, Estado', 'numerical', 'integerOnly'=>true),
			array('MinimoPorcentajeHumedad, MaximoPresion, Amperaje, PresionEje, SRC, PorcentajeHumedad', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha,Hora,codigofila,IdUsuario, RotatuboID, Amperaje, PresionEje, SRC, PorcentajeHumedad, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rotatubo' => array(self::BELONGS_TO, 'Rotatubo', 'RotatuboID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'RotatuboID' => 'Rotatubo',
			'Amperaje' => 'Amperaje',
			'PresionEje' => 'Presion Eje',
			'SRC' => 'Src',
			'PorcentajeHumedad' => 'Porcentaje Humedad',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('RotatuboID',$this->RotatuboID);
		$criteria->compare('Amperaje',$this->Amperaje);
		$criteria->compare('PresionEje',$this->PresionEje);
		$criteria->compare('SRC',$this->SRC);
		$criteria->compare('PorcentajeHumedad',$this->PorcentajeHumedad);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Secado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
