<?php

/**
 * This is the model class for table "Tratamiento".
 *
 * The followings are the available columns in table 'Tratamiento':
 * @property string $ID
 * @property string $RevisionID
 * @property integer $PlantaID
 * @property double $AlimentacionAguaColaTemperatura
 * @property double $AlimentacionAguaColaPorcentajeSolidos
 * @property double $VahTemperatura
 * @property double $VacPeso
 * @property double $Temperatura1
 * @property double $Temperatura2
 * @property double $Temperatura3
 * @property double $SalidaPorcentajeSolidos
 * @property integer $Estado
 * @property time $Hora
 * @property string $Fecha
 * @property string $codigofila
 * The followings are the available model relations:
 * @property Planta $planta
 */
class Tratamiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tratamiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PlantaID', 'required'),
			array('PlantaID, Estado', 'numerical', 'integerOnly'=>true),
			//array('AlimentacionAguaColaTemperatura, AlimentacionAguaColaPorcentajeSolidos, VahTemperatura, VacPeso, Temperatura1, Temperatura2, Temperatura3, SalidaPorcentajeSolidos', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID,aguacola,concentrado, Fecha,Hora,codigofila,IdUsuario, PlantaID, AlimentacionAguaColaTemperatura, AlimentacionAguaColaPorcentajeSolidos, VahTemperatura, VacPeso, Temperatura1, Temperatura2, Temperatura3, SalidaPorcentajeSolidos, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'planta' => array(self::BELONGS_TO, 'Planta', 'PlantaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'PlantaID' => 'Planta',
			'AlimentacionAguaColaTemperatura' => 'Alimentacion Agua Cola Temperatura',
			'AlimentacionAguaColaPorcentajeSolidos' => 'Alimentacion Agua Cola Porcentaje Solidos',
			'VahTemperatura' => 'Vah Temperatura',
			'VacPeso' => 'Vac Peso',
			'Temperatura1' => 'Temperatura1',
			'Temperatura2' => 'Temperatura2',
			'Temperatura3' => 'Temperatura3',
			'SalidaPorcentajeSolidos' => 'Salida Porcentaje Solidos',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('PlantaID',$this->PlantaID);
		$criteria->compare('AlimentacionAguaColaTemperatura',$this->AlimentacionAguaColaTemperatura);
		$criteria->compare('AlimentacionAguaColaPorcentajeSolidos',$this->AlimentacionAguaColaPorcentajeSolidos);
		$criteria->compare('VahTemperatura',$this->VahTemperatura);
		$criteria->compare('VacPeso',$this->VacPeso);
		$criteria->compare('Temperatura1',$this->Temperatura1);
		$criteria->compare('Temperatura2',$this->Temperatura2);
		$criteria->compare('Temperatura3',$this->Temperatura3);
		$criteria->compare('SalidaPorcentajeSolidos',$this->SalidaPorcentajeSolidos);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tratamiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
