<?php

/**
 * This is the model class for table "Usuarios".
 *
 * The followings are the available columns in table 'Usuarios':
 * @property integer $ID
 * @property string $NombreCompleto
 * @property string $Usuario
 * @property string $Contrasenia
 * @property integer $TipoRolID
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property UsuarioRol $tipoRol
 */
class Usuarios extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
     public $repeat_contrasenia;
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario, Contrasenia, TipoRolID, Estado, activo', 'required'),
                    array('Usuario','unique', 'message' => 'Nombre del usuario ya existe'),
			array('TipoRolID, Estado', 'numerical', 'integerOnly'=>true),
			array('NombreCompleto, Usuario, Contrasenia', 'length', 'max'=>100),
                    array('repeat_contrasenia','ComprobarPassword','on' => 'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, activo, NombreCompleto, nombrerol,Usuario, Contrasenia, TipoRolID, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoRol' => array(self::BELONGS_TO, 'UsuarioRol', 'TipoRolID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NombreCompleto' => 'Nombre Completo',
			'Usuario' => 'Usuario',
			'Contrasenia' => 'Contraseña',
			'TipoRolID' => 'Tipo Rol',
			'Estado' => 'Estado',
                    'repeat_contraseina' => 'Repetir Contraseña',
                    'activo'=>'Estado'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $nombrerol ;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->compare('ID',$this->ID);
		$criteria->compare('NombreCompleto',$this->NombreCompleto,true);
		$criteria->compare('Usuario',$this->Usuario,true);
		$criteria->compare('TipoRolID',$this->TipoRolID);
                $criteria->with= array('tipoRol');
                $criteria->compare('tipoRol.NombreRol',$this->nombrerol,true);
		$criteria->compare('t.Estado',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
        
        public function ComprobarPassword($attribute,$params){
                        $var=$this->repeat_contrasenia;
                        $con2 = $this->Contrasenia;
                        if($var == $con2){
                            
                        }else{
                            $this->addError($attribute,'Contraseñas no coinciden');
                        }
                    }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
          
}
