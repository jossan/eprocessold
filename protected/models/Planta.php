<?php

/**
 * This is the model class for table "planta".
 *
 * The followings are the available columns in table 'planta':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoPlanta
 * @property integer $Estado
 * @property double $TratamientoMinAlimentacionAguaColaTemperatura
 * @property double $TratamientoMaxAlimentacionAguaColaTemperatura
 * @property double $TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja
 * @property double $TratamientoMinAlimentacionAguaColaPorcentajeSolidos
 * @property double $TratamientoMaxAlimentacionAguaColaPorcentajeSolidos
 * @property double $TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja
 * @property double $TratamientoMinVahTemperatura
 * @property double $TratamientoMaxVahTemperatura
 * @property double $TratamientoVahTemperaturaAlertaNaranja
 * @property double $TratamientoMinVacPeso
 * @property double $TratamientoMaxVacPeso
 * @property double $TratamientoVacPesoAlertaNaranja
 * @property double $TratamientoMinTemperatura1
 * @property double $TratamientoMaxTemperatura1
 * @property double $TratamientoTemperatura1AlertaNaranja
 * @property double $TratamientoMinTemperatura2
 * @property double $TratamientoMaxTemperatura2
 * @property double $TratamientoTemperatura2AlertaNaranja
 * @property double $TratamientoMinTemperatura3
 * @property double $TratamientoMaxTemperatura3
 * @property double $TratamientoTemperatura3AlertaNaranja
 * @property double $TratamientoMinSalidaPorcentajeSolidos
 * @property double $TratamientoMaxSalidaPorcentajeSolidos
 * @property double $TratamientoSalidaPorcentajeSolidosAlertaNaranja
 *
 * The followings are the available model relations:
 * @property Tratamiento[] $tratamientos
 */
class Planta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'planta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, TratamientoMinAlimentacionAguaColaTemperatura, TratamientoMaxAlimentacionAguaColaTemperatura, TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja, TratamientoMinAlimentacionAguaColaPorcentajeSolidos, TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja, TratamientoMinVahTemperatura, TratamientoMaxVahTemperatura, TratamientoVahTemperaturaAlertaNaranja, TratamientoMinVacPeso, TratamientoMaxVacPeso, TratamientoVacPesoAlertaNaranja, TratamientoMinTemperatura1, TratamientoMaxTemperatura1, TratamientoTemperatura1AlertaNaranja, TratamientoMinTemperatura2, TratamientoMaxTemperatura2, TratamientoTemperatura2AlertaNaranja, TratamientoMinTemperatura3, TratamientoMaxTemperatura3, TratamientoTemperatura3AlertaNaranja, TratamientoMinSalidaPorcentajeSolidos, TratamientoMaxSalidaPorcentajeSolidos, TratamientoSalidaPorcentajeSolidosAlertaNaranja', 'required'),
			array('EstadoPlanta, Estado', 'numerical', 'integerOnly'=>true),
                    /*array('Nombre','unique', 'message' => 'Nombre de la planta ya existe'),*/
			array('TratamientoMinAlimentacionAguaColaTemperatura, TratamientoMaxAlimentacionAguaColaTemperatura, TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja, TratamientoMinAlimentacionAguaColaPorcentajeSolidos, TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja, TratamientoMinVahTemperatura, TratamientoMaxVahTemperatura, TratamientoVahTemperaturaAlertaNaranja, TratamientoMinVacPeso, TratamientoMaxVacPeso, TratamientoVacPesoAlertaNaranja, TratamientoMinTemperatura1, TratamientoMaxTemperatura1, TratamientoTemperatura1AlertaNaranja, TratamientoMinTemperatura2, TratamientoMaxTemperatura2, TratamientoTemperatura2AlertaNaranja, TratamientoMinTemperatura3, TratamientoMaxTemperatura3, TratamientoTemperatura3AlertaNaranja, TratamientoMinSalidaPorcentajeSolidos, TratamientoMaxSalidaPorcentajeSolidos, TratamientoSalidaPorcentajeSolidosAlertaNaranja', 'numerical'),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoPlanta, Estado, TratamientoMinAlimentacionAguaColaTemperatura, TratamientoMaxAlimentacionAguaColaTemperatura, TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja, TratamientoMinAlimentacionAguaColaPorcentajeSolidos, TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja, TratamientoMinVahTemperatura, TratamientoMaxVahTemperatura, TratamientoVahTemperaturaAlertaNaranja, TratamientoMinVacPeso, TratamientoMaxVacPeso, TratamientoVacPesoAlertaNaranja, TratamientoMinTemperatura1, TratamientoMaxTemperatura1, TratamientoTemperatura1AlertaNaranja, TratamientoMinTemperatura2, TratamientoMaxTemperatura2, TratamientoTemperatura2AlertaNaranja, TratamientoMinTemperatura3, TratamientoMaxTemperatura3, TratamientoTemperatura3AlertaNaranja, TratamientoMinSalidaPorcentajeSolidos, TratamientoMaxSalidaPorcentajeSolidos, TratamientoSalidaPorcentajeSolidosAlertaNaranja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tratamientos' => array(self::HAS_MANY, 'Tratamiento', 'PlantaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoPlanta' => 'Estado',
			'Estado' => 'Estado de Eliminación',
			'TratamientoMinAlimentacionAguaColaTemperatura' => 'Valor Mínimo:',
			'TratamientoMaxAlimentacionAguaColaTemperatura' =>'Valor Máximo: ',
			'TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja' =>'Porcentaje de Alerta Naranja:',
			'TratamientoMinAlimentacionAguaColaPorcentajeSolidos' => 'Valor Mínimo:',
			'TratamientoMaxAlimentacionAguaColaPorcentajeSolidos' =>'Valor Máximo: ',
			'TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinVahTemperatura' => 'Valor Mínimo:',
			'TratamientoMaxVahTemperatura' => 'Valor Máximo: ',
			'TratamientoVahTemperaturaAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinVacPeso' => 'Valor Mínimo:',
			'TratamientoMaxVacPeso' => 'Valor Máximo: ',
			'TratamientoVacPesoAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinTemperatura1' =>'Valor Mínimo:',
			'TratamientoMaxTemperatura1' =>'Valor Máximo: ',
			'TratamientoTemperatura1AlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinTemperatura2' =>'Valor Mínimo:',
			'TratamientoMaxTemperatura2' => 'Valor Máximo: ',
			'TratamientoTemperatura2AlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinTemperatura3' =>'Valor Mínimo:',
			'TratamientoMaxTemperatura3' => 'Valor Máximo: ',
			'TratamientoTemperatura3AlertaNaranja' => 'Porcentaje de Alerta Naranja:',
			'TratamientoMinSalidaPorcentajeSolidos' => 'Valor Mínimo:',
			'TratamientoMaxSalidaPorcentajeSolidos' => 'Valor Máximo: ',
			'TratamientoSalidaPorcentajeSolidosAlertaNaranja' => 'Porcentaje de Alerta Naranja:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoPlanta',$this->EstadoPlanta);
		$criteria->compare('Estado',1);
		$criteria->compare('TratamientoMinAlimentacionAguaColaTemperatura',$this->TratamientoMinAlimentacionAguaColaTemperatura);
		$criteria->compare('TratamientoMaxAlimentacionAguaColaTemperatura',$this->TratamientoMaxAlimentacionAguaColaTemperatura);
		$criteria->compare('TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja',$this->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja);
		$criteria->compare('TratamientoMinAlimentacionAguaColaPorcentajeSolidos',$this->TratamientoMinAlimentacionAguaColaPorcentajeSolidos);
		$criteria->compare('TratamientoMaxAlimentacionAguaColaPorcentajeSolidos',$this->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos);
		$criteria->compare('TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja',$this->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja);
		$criteria->compare('TratamientoMinVahTemperatura',$this->TratamientoMinVahTemperatura);
		$criteria->compare('TratamientoMaxVahTemperatura',$this->TratamientoMaxVahTemperatura);
		$criteria->compare('TratamientoVahTemperaturaAlertaNaranja',$this->TratamientoVahTemperaturaAlertaNaranja);
		$criteria->compare('TratamientoMinVacPeso',$this->TratamientoMinVacPeso);
		$criteria->compare('TratamientoMaxVacPeso',$this->TratamientoMaxVacPeso);
		$criteria->compare('TratamientoVacPesoAlertaNaranja',$this->TratamientoVacPesoAlertaNaranja);
		$criteria->compare('TratamientoMinTemperatura1',$this->TratamientoMinTemperatura1);
		$criteria->compare('TratamientoMaxTemperatura1',$this->TratamientoMaxTemperatura1);
		$criteria->compare('TratamientoTemperatura1AlertaNaranja',$this->TratamientoTemperatura1AlertaNaranja);
		$criteria->compare('TratamientoMinTemperatura2',$this->TratamientoMinTemperatura2);
		$criteria->compare('TratamientoMaxTemperatura2',$this->TratamientoMaxTemperatura2);
		$criteria->compare('TratamientoTemperatura2AlertaNaranja',$this->TratamientoTemperatura2AlertaNaranja);
		$criteria->compare('TratamientoMinTemperatura3',$this->TratamientoMinTemperatura3);
		$criteria->compare('TratamientoMaxTemperatura3',$this->TratamientoMaxTemperatura3);
		$criteria->compare('TratamientoTemperatura3AlertaNaranja',$this->TratamientoTemperatura3AlertaNaranja);
		$criteria->compare('TratamientoMinSalidaPorcentajeSolidos',$this->TratamientoMinSalidaPorcentajeSolidos);
		$criteria->compare('TratamientoMaxSalidaPorcentajeSolidos',$this->TratamientoMaxSalidaPorcentajeSolidos);
		$criteria->compare('TratamientoSalidaPorcentajeSolidosAlertaNaranja',$this->TratamientoSalidaPorcentajeSolidosAlertaNaranja);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Planta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
