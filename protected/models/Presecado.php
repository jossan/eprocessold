<?php

/**
 * This is the model class for table "Presecado".
 *
 * The followings are the available columns in table 'Presecado':
 * @property string $ID
 * @property string $RevisionID
 * @property integer $RotadiskID
 * @property double $MinimoPorcentajeHumedad
 * @property double $MaximoPresion
 * @property double $Amperaje
 * @property double $PresionEje
 * @property double $PresionChaqueta
 * @property double $SCR
 * @property double $PorcentajeHumedad
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Rotadisk $rotadisk
 * @property Recepcion $revision
 */
class Presecado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'presecado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' RotadiskID', 'required'),
			array('RotadiskID, Estado', 'numerical', 'integerOnly'=>true),
			array('MinimoPorcentajeHumedad, MaximoPresion, Amperaje, PresionEje, PresionChaqueta, SCR, PorcentajeHumedad', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha,Hora,codigofila,IdUsuario, RotadiskID, MinimoPorcentajeHumedad, MaximoPresion, Amperaje, PresionEje, PresionChaqueta, SCR, PorcentajeHumedad, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rotadisk' => array(self::BELONGS_TO, 'Rotadisk', 'RotadiskID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'RotadiskID' => 'Rotadisk',
			'MinimoPorcentajeHumedad' => 'Minimo Porcentaje Humedad',
			'MaximoPresion' => 'Maximo Presion',
			'Amperaje' => 'Amperaje',
			'PresionEje' => 'Presion Eje',
			'PresionChaqueta' => 'Presion Chaqueta',
			'SCR' => 'Scr',
			'PorcentajeHumedad' => 'Porcentaje Humedad',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('RotadiskID',$this->RotadiskID);
		$criteria->compare('MinimoPorcentajeHumedad',$this->MinimoPorcentajeHumedad);
		$criteria->compare('MaximoPresion',$this->MaximoPresion);
		$criteria->compare('Amperaje',$this->Amperaje);
		$criteria->compare('PresionEje',$this->PresionEje);
		$criteria->compare('PresionChaqueta',$this->PresionChaqueta);
		$criteria->compare('SCR',$this->SCR);
		$criteria->compare('PorcentajeHumedad',$this->PorcentajeHumedad);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Presecado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
