<?php

/**
 * This is the model class for table "Periodo".
 *
 * The followings are the available columns in table 'Periodo':
 * @property string $ID
 * @property string $Fecha
 * @property string $Dia
 * @property integer $Semana
 * @property double $MPProcesada
 * @property string $InicioProd
 * @property string $FinProd
 * @property string $ParadanoProgramada
 * @property string $ParadaProgramada
 * @property string $TRTrabajo
 * @property double $SacosProducidos
 * @property double $SacosHumedos
 * @property integer $Calidad
 * @property double $RendimientoDiario
 *
 * The followings are the available model relations:
 * @property MateriaPrima[] $materiaPrimas
 * @property PTVenta[] $pTVentas
 */
class Periodo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'periodo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fecha', 'required'),
			array('Semana, Calidad', 'numerical', 'integerOnly'=>true),
			array('MPProcesada, SacosProducidos, SacosHumedos, RendimientoDiario', 'numerical'),
			array('Dia', 'length', 'max'=>20),
			array('InicioProd, FinProd, ParadanoProgramada, ParadaProgramada, TRTrabajo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Fecha, Dia, Semana, MPProcesada, InicioProd,FechaFinProd, FinProd, ParadanoProgramada, ParadaProgramada, TRTrabajo, SacosProducidos, SacosHumedos, Calidad, RendimientoDiario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materiaPrimas' => array(self::HAS_MANY, 'MateriaPrima', 'PeriodoID'),
			'pTVentas' => array(self::HAS_MANY, 'PTVenta', 'PeriodoID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Fecha' => 'Fecha',
			'Dia' => 'Dia',
			'Semana' => 'Semana',
			'MPProcesada' => 'Mpprocesada',
			'InicioProd' => 'Inicio Prod',
			'FinProd' => 'Fin Prod',
			'ParadanoProgramada' => 'Paradano Programada',
			'ParadaProgramada' => 'Parada Programada',
			'TRTrabajo' => 'Trtrabajo',
			'SacosProducidos' => 'Sacos Producidos',
			'SacosHumedos' => 'Sacos Humedos',
			'Calidad' => 'Calidad',
			'RendimientoDiario' => 'Rendimiento Diario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($f_ini, $f_fin)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Dia',$this->Dia,true);
		$criteria->compare('Semana',$this->Semana);
		$criteria->compare('MPProcesada',$this->MPProcesada);
		$criteria->compare('InicioProd',$this->InicioProd,true);
		$criteria->compare('FinProd',$this->FinProd,true);
		$criteria->compare('ParadanoProgramada',$this->ParadanoProgramada,true);
		$criteria->compare('ParadaProgramada',$this->ParadaProgramada,true);
		$criteria->compare('TRTrabajo',$this->TRTrabajo,true);
		$criteria->compare('SacosProducidos',$this->SacosProducidos);
		$criteria->compare('SacosHumedos',$this->SacosHumedos);
		$criteria->compare('Calidad',$this->Calidad);
		$criteria->compare('RendimientoDiario',$this->RendimientoDiario);

                if($f_ini && $f_fin)
                    $criteria->addBetweenCondition("Fecha", $f_ini, $f_fin);
                
                $session=new CHttpSession;
                $session->open();
                $session['reporteSemanal']=$criteria;  //Esto para guardar la criteria en la sesión actual para usarlo posteriormente.
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Periodo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
