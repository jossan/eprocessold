<?php
/*Signo Decimal*/
$decimal = $this->decimal();

$PAGINADO =12 ;
$LIMIT = $PAGINADO;
$OFFSET = $cont * $PAGINADO;
$LIMITCOCCION = $PAGINADO*$numerococina;
$OFFSETCOCCION = ($cont * $PAGINADO)*$numerococina;
$LIMITPRENSADO = $PAGINADO*$numeroprensas;
$OFFSETPRENSADO = ($cont * $PAGINADO)*$numeroprensas;
$LIMITDECANTACION = $PAGINADO*$numerotricanter;
$OFFSETDECANTACION = ($cont * $PAGINADO)*$numerotricanter;
$LIMITREFINADO = $PAGINADO*$numeropulidora;
$OFFSETREFINADO = ($cont * $PAGINADO)*$numeropulidora;
$LIMITTRATAMIENTO = $PAGINADO*$numeroplantas;
$OFFSETTRATAMIENTO = ($cont * $PAGINADO)*$numeroplantas;
$LIMITPRESECADO = $PAGINADO*$numerorotadisk;
$OFFSETPRESECADO = ($cont * $PAGINADO)*$numerorotadisk;
$LIMITSECADO = $PAGINADO*$numerorotatubo;
$OFFSETSECADO = ($cont * $PAGINADO)*$numerorotatubo;

$model = Recepcion::model()->findAll("Fecha='".$fechaini."' and Hora between '$horaini' and '$horafin' order by Fecha, Hora, ID LIMIT ".$LIMIT." OFFSET ".$OFFSET);
$cri = new CDbCriteria();
$cri->select = "*";
$cri->condition = "t.Estado=1 and Fecha = '$fechaini'";
$cri->addBetweenCondition("Hora", $horaini, $horafin);
$cri->order = 'Fecha, Hora, codigofila, ID';
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$crili= new CDbCriteria();
$crili->select = "distinct(t.codigofila), t.*";
$crili->join = "join prensado on t.codigofila = prensado.codigofila";
$crili->condition = "Fecha ='$fechaini'";
$crili->addBetweenCondition("Hora",$horaini, $horafin);
$crili->order = 'Fecha, Hora, ID';
$crili->limit = $LIMITCOCCION;
$crili->offset = $OFFSETCOCCION;
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$licors = Licor::model()->findAll($crili);
$cricoc = new $cri;
$cricoc->with = array ('cocina');
$cricoc->condition = "t.Estado=1 and Fecha = '$fechaini' and cocina.Estado=1";
$cricoc->order = 'Fecha, Hora, codigofila,CocinaID';
$cricoc->limit = $LIMITCOCCION;
$cricoc->offset = $OFFSETCOCCION;
$coccions = Coccion::model()->findAll($cricoc);
$cri->order = 'Fecha, Hora, codigofila, PrensaID';
$cri->limit = $LIMITPRENSADO;
$cri->offset = $OFFSETPRENSADO;
$prensados = Prensado::model()->findAll($cri);

$cricoc = new $cri;
$cricoc->with = array ('tricanter');
$cricoc->condition = "t.Estado=1 and Fecha = '$fechaini' and tricanter.Estado=1";
$cricoc->order = 'Fecha,Hora, codigofila, TricanterID';
$cricoc->limit = $LIMITDECANTACION;
$cricoc->offset = $OFFSETDECANTACION;
$decantasions = Decantacion::model()->findAll($cricoc);

$cri->order = 'Fecha, Hora, codigofila, PulidoraID';
$cri->limit = $LIMITREFINADO;
$cri->offset = $OFFSETREFINADO;
$refinados = Refinacion::model()->findAll($cri);
$cri->order = 'Fecha, Hora, codigofila, PlantaID';
$cri->limit = $LIMITTRATAMIENTO;
$cri->offset = $OFFSETTRATAMIENTO;
$tratamientos= Tratamiento::model()->findAll($cri);
$cri->order = 'Fecha, Hora, codigofila, RotadiskID';
$cri->limit = $LIMITPRESECADO;
$cri->offset = $OFFSETPRESECADO;
$presecados = Presecado::model()->findAll($cri);
$cri->order = 'Fecha, Hora, codigofila, RotatuboID';
$cri->limit = $LIMITSECADO;
$cri->offset = $OFFSETSECADO;
$secados = Secado::model()->findAll($cri);
//$cri = new CDbCriteria();
//$cri->select = "*";
//$cri->condition = "Fecha ='$fechaini'";
//$cri->addBetweenCondition("Hora", $horaini, $horafin);
$cri->order = 'Fecha, Hora, ID';
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$harina = Harina::model()->findAll($cri);
$dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.08mm solid #000000;
 border-right: 0.08mm solid #000000;
 }
 td{
    text-align: center; 
 }
 table thead td { background-color: #EEEEEE;
 
 border: 0.08mm solid #000000;
 vertical-align: middle;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-bottom: 0.08mm solid gray;
  border-left: 0.08mm solid gray;
  border-right: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.08mm solid #000000;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="25px;"></td>
 <td width="50%" style="text-align: center;font-size: 12pt;"><b>Control de proceso de producción</b></td>
 <td width="50%" style="text-align: right;"><b>Fecha: </b><?php echo $dias[date('N', strtotime($fechaini))].', '.$fechaini; ?><br></td>
 

 </tr>

</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 8pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
 <table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="4">
 <thead>
 <tr>
 <!--Pestaña recepcion-->
 <td colspan="14">Recepción</td>
 <td colspan="<?php print ($numerococina*4)+1?>">Cocción</td>
 <td colspan="<?php print($numeroprensas*2)+3;?>">Prensado</td>
 <td colspan="<?php print ($numerotricanter*2)+1;?>">Decantación</td> 
 <td colspan="<?php print ($numeropulidora*4)+1;?>">Centrifugado</td>
 
 </tr>
 <!--Pestaña Coccion-->
 <tr>
 <td rowspan="4">H<br>O<br>R<br>A</td>
 <td rowspan="4">Id<br> recepción</td>
 <td rowspan="4">Proveedor</td>
 <td rowspan="4">Placa</td>
 <td rowspan="4">Chofer</td>
 <td rowspan="4">Secuencia</td>
 <td rowspan="4">Especie</td>
 <td rowspan="4">Peso(T)</td>
 <td rowspan="4">Tara carro</td>
 <td rowspan="4">Desc(T)</td>
 <td rowspan="4">Peso Neto(T)</td>
 <td rowspan="4">Hora de entrada a tolva</td>
 <td rowspan="4">TOLVA</td>
 <td rowspan="4"> Fecha<br>producción</td>
 <!-- Coccion -->
 <td rowspan="4">Hora</td>
 <?php foreach ($cocinas as $cocina):
     if($cocina->EstadoCocina):?>
        <td colspan="4"><?= $cocina->Nombre; ?></td>
     <?php else: ?>
        <td colspan="4" style="background-color: #C1C1C1;"><?php print $cocina->Nombre.' (inactiva)';?></td>
     <?php endif; ?>
 <?php endforeach;?>
 
 <td rowspan="4">Hora</td>
 <?php foreach ($prensas as $prensa):
    if($prensa->EstadoPrensa):?>   
       <td colspan="2"><?= $prensa->Nombre ?></td>
    <?php else: ?>
       <td colspan="2" style="background-color: #C1C1C1;" ><?php print $prensa->Nombre.' (inactiva)'; ?></td>
   <?php endif; ?>
 <?php endforeach;?>
 <td colspan="1">Licor</td>
 <td colspan="1" rowspan="4">Dosificación de concentrado (l)</td>
 
 <td rowspan="4">Hora</td>
 <?php foreach ($tricanter as $trica):
    if($trica->EstadoTricanter):?>
       <td colspan="2"><?= $trica->Nombre ?></td>
    <?php else: ?>
       <td colspan="2   " style="background-color: #C1C1C1;"><?php print $trica->Nombre.' (inactiva)'; ?></td>
   <?php endif; ?>
 <?php endforeach;?>
 <td rowspan="4">Hora</td>
 <?php foreach ($pulidora as $pulidor):
     if($pulidor->EstadoPulidora):?>  
        <td colspan="4"><?= $pulidor->Nombre ?></td>
    <?php else: ?>
        <td colspan="4" style="background-color: #C1C1C1;" ><?php print $pulidor->Nombre.' (inactiva)'; ?></td>
    <?php endif; ?>
 
 <?php endforeach;?>
 
 </tr>
 <!--Fila # 3-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td rowspan="2">RPM</td><td colspan="2">P. PSI</td><td rowspan="2">T°C</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroprensas;$i++):?>
 <td rowspan="2">Amp</td><td rowspan="2">%H</td>
 <?php endfor;?>
 <td rowspan="2">%S</td>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td colspan="1">Alim</td>
 <td colspan="1">A cola</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td >Alim</td><td colspan="3">Aceite</td>
 <?php endfor;?>
 
 </tr>
 <!--Fila # 4-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
    <td >Eje</td><td >Chaq</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
    <td >T°C</td> <td >%S</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td >T°C</td><td >%H</td><td >%S</td><td >%A</td>
 <?php endfor;?>
 
 </tr>
  <!--Fila # 5-->
 <tr >
<?php foreach($cocinas as $pa):?>
     <td style="font-size: 6px;"><?php print (float)$pa->CoccionMinRmp.'_'.(float)$pa->CoccionMaxRmp?></td><td style="font-size: 6px;"><?php print (float)$pa->CoccionMinPresionEje.' _ '.(float)$pa->CoccionMaxPresionEje?></td><td style="font-size: 6px;"><?php print (float)$pa->CoccionMinPresionChaqueta.' _ '.(float)$pa->CoccionMaxPresionChaqueta?></td><td style="font-size: 6px;"><?php print (float)$pa->CoccionMinTemperatura.' _ '.(float)$pa->CoccionMaxTemperatura?></td>
<?php endforeach;?>
 <?php foreach($prensas as $pre):?>
 <td style="font-size: 6px;"><?php print (float)$pre->PrensadoMinAmperaje.' _ '.(float)$pre->PrensadoMaxAmperaje?></td><td style="font-size: 6px;"><?php print (float)$pre->PrensadoMinPorcentajeHumedad.' _ '.(float)$pre->PrensadoMaxPorcentajeHumedad?></td>
<?php endforeach;?>
 <td style="font-size: 6px;"><?php print (float)$lic['LicorMinPorcentajeSolidos'].' _ '.(float)$lic['LicorMaxPorcentajeSolidos']?></td>
 <?php foreach($tricanter as $tri):?>
    <td style="font-size: 6px;"><?php print (float)$tri->DecantacionMinAlimentacionTemperatura.' _ '.(float)$tri->DecantacionMaxAlimentacionTemperatura?></td>
    <td style="font-size: 6px;"><?php print (float)$tri->DecantacionMinAguaColaPorcentajeSolidos.' _ '.(float)$tri->DecantacionMaxAguaColaPorcentajeSolidos?></td>
<?php endforeach;?>
 <?php foreach($pulidora as $pul):?>
 <td style="font-size: 6px;"><?php print (float)$pul->RefinacionMinAlimentacionTemperatura.' _ '.(float)$pul->RefinacionMaxAlimentacionTemperatura?></td><td style="font-size: 6px;" ><?php print (float)$pul->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$pul->RefinacionMaxAceitePorcentajeHumedad?></td><td style="font-size: 6px;"><?php print (float)$pul->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$pul->RefinacionMaxAceitePorcentajeSolidos?></td><td style="font-size: 6px;"><?php print (float)$pul->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$pul->RefinacionMaxAceitePorcentajeAcidez ?></td>
<?php endforeach;?>
  
 </tr>
 </thead>
 <tbody>
     
 <!-- ITEMS -->
 <?php 
 $control =0;
 $countcocina=0;
 $countprensa=0;
 $countlicor=0;
 $countdecan=0;
 $countrefinado=0;
 $counttrata=0;
 $countrotadisk=0;
 $countsecado=0;
 $countharina=0;
 $fin = $limit >= $PAGINADO ? $PAGINADO : $limit;
 for($i=0;$i<$fin;$i++):
    $rowRecepcion = $model[$i] ? $model[$i] : NULL; ?>
    <tr>
    <td ><?php echo isset($rowRecepcion->Hora)? substr($rowRecepcion->Hora,0,5) : '-' ;?></td>
    <td ><?php echo isset($rowRecepcion->ID) ? $rowRecepcion->ID : '-';?></td>
    
    <td ><?= $rowRecepcion->proveedor ? $rowRecepcion->proveedor->Nombre : "";?></td>
    <td ><?= $rowRecepcion->Placa ;?></td>
    <td ><?= $rowRecepcion->Chofer ;?></td>
    <td ><?= $rowRecepcion->IdSecuencia ;?></td>
    <td  ><?php print isset($rowRecepcion->especie) ? $rowRecepcion->especie->Nombre : '-';?> 
    </td><td align="right"><?php print $rowRecepcion->PesoTonelada ? number_format($rowRecepcion->PesoTonelada,3,$decimal,'') : '-'; ?>
    </td><td align="right"><?php print $rowRecepcion->tarracarro ? number_format($rowRecepcion->tarracarro,3,$decimal,'') : '-';  ?>
    </td><td align="right"><?php print $rowRecepcion->Descuento ? number_format($rowRecepcion->Descuento,0,$decimal,'').' %' : '-';?>
    </td><td align="right"><?php print $rowRecepcion->Pesoneto ? number_format($rowRecepcion->Pesoneto,3,$decimal,'') : '-';?></td>
    <td align="right"><?php print $rowRecepcion->Horatolva ? substr($rowRecepcion->Horatolva,0,5) : '-';?> </td>
    <td ><?php print $rowRecepcion->TolvaID ? $rowRecepcion->tolva->Nombre : '-';?> </td>
    <td > <?= $rowRecepcion->fechaproduccion ?> </td>
 <!--Coccion -->
   <?php $x=0;foreach($cocinas as $c):
    $coccion =$coccions[$countcocina] ? $coccions[$countcocina] : NULL;
    $countcocina++;
    if ($x++<=0):?>
    <td  ><?php echo $coccion->Hora ? substr($coccion->Hora,0,5) : '-';?></td>
    <?php endif;?>
    <td  <?php print $this->cla($coccion->RPM, $c->CoccionMinRmp,$c->CoccionMaxRmp, $c->CoccionRpmAlertaNaranja)?>><?php echo $coccion->RPM ? number_format($coccion->RPM,2,$decimal,'') : '-';?></td>
    <td  <?php print $this->cla($coccion->PresionEje, $c->CoccionMinPresionEje,$c->CoccionMaxPresionEje, $c->CoccionPresionEjeAlertaNaranja)?> ><?php print $coccion->PresionEje ? number_format($coccion->PresionEje,2,$decimal,'') : '-';?> </td>
    <td  <?php print $this->cla($coccion->PresionChaqueta, $c->CoccionMinPresionChaqueta,$c->CoccionMaxPresionChaqueta, $c->CoccionPresionChaquetaAlertaNaranja)?>><?php print $coccion->PresionChaqueta ? number_format($coccion->PresionChaqueta,2,$decimal,'') : '-';?> </td>
    <td   <?php print $this->cla($coccion->Temperatura, $c->CoccionMinTemperatura,$c->CoccionMaxTemperatura, $c->CoccionPresionTemperaturaAlertaNaranja)?> ><?php print $coccion->Temperatura ? number_format($coccion->Temperatura,2,$decimal,'') : '-' ;?> </td>
    <?php endforeach; ?>
 
    <!--Prensado -->
    <?php $x =0;
    foreach ($prensas as $pn):
        $prensado = $prensados[$countprensa]? $prensados[$countprensa] : NULL;
        $countprensa++;
        if ($x++<=0): ?>
        <td  ><?php echo $prensado->Hora ? substr($prensado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td  <?php print $this->cla($prensado->Amperaje, $pn->PrensadoMinAmperaje,$pn->PrensadoMaxAmperaje, $pn->PrensadoAmperajeAlertaNaranja)?>><?php echo $prensado->Amperaje ? number_format($prensado->Amperaje,2,$decimal,'') : '-';?></td>
        <td  <?php print $this->cla($prensado->PorcentajeHumedad, $pn->PrensadoMinPorcentajeHumedad,$pn->PrensadoMaxPorcentajeHumedad, $pn->PrensadoPorcentajeHumedadAlertaNaranja)?>><?php print $prensado->PorcentajeHumedad ? number_format($prensado->PorcentajeHumedad,2,$decimal,'') : '-';?> </td>
    <?php endforeach;
    $licor = $licors[$countlicor]? $licors[$countlicor] :NULL; $countlicor++?>
    <!-- Licor -->
    <td  <?php print $this->cla($licor->LicorPorcentajeSolidos, $p->LicorMinPorcentajeSolidos,$p->LicorMaxPorcentajeSolidos, $p->LicorPorcentajeSolidosAlertaNaranja)?>><?php echo $licor->LicorPorcentajeSolidos ? number_format($licor->LicorPorcentajeSolidos,2,$decimal,'') : '-';?></td>
    <td  ><?php print $licor->concentrado ? number_format($licor->concentrado,2,$decimal,'') : '-';?> </td>
    <!--Decantasion -->
    <?php $x = 0;
    foreach ($tricanter as $tc):
        $decantasion =$decantasions[$countdecan] ? $decantasions[$countdecan] : NULL;$countdecan++;
        if ($x++<=0): ?>
        <td  ><?php echo $decantasion->Hora ? substr($decantasion->Hora,0,5) :'-';?></td>
        <?php endif;?>
        <td  <?php print $this->cla($decantasion->AlimentacionTemperatura, $tc->DecantacionMinAlimentacionTemperatura,$tc->DecantacionMaxAlimentacionTemperatura, $tc->DecantacionAlimentacionTemperaturaAlertaNaranja)?>><?php echo $decantasion->AlimentacionTemperatura? number_format($decantasion->AlimentacionTemperatura,2,$decimal,'') : '-';?></td>
        <td  <?php print $this->cla($decantasion->AguaColaPorcentajeSolidos, $tc->DecantacionMinAguaColaPorcentajeSolidos,$tc->DecantacionMaxAguaColaPorcentajeSolidos, $tc->DecantacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print $decantasion->AguaColaPorcentajeSolidos ? number_format($decantasion->AguaColaPorcentajeSolidos,2,$decimal,'') :'-';?> </td>
    <?php endforeach; ?>
    <!--Refinacion -->
    <?php $x = 0;
    foreach($pulidora as $pl):
        $refinado= $refinados[$countrefinado]?$refinados[$countrefinado]:NULL;$countrefinado++;
        if ($x++<=0): ?>
        <td  ><?php echo $refinado->Hora ? substr($refinado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td  <?php print $this->cla($refinado->AlimentacionTemperatura, $pl->RefinacionMinAlimentacionTemperatura,$pl->RefinacionMaxAlimentacionTemperatura, $pl->RefinacionTemperaturaAlertaNaranja)?>><?php echo $refinado->AlimentacionTemperatura? number_format($refinado->AlimentacionTemperatura,2,$decimal,'') : '-';?></td>
        <td  <?php print $this->cla($refinado->AceitePorcentajeHumedad, $pl->RefinacionMinAceitePorcentajeHumedad,$pl->RefinacionMaxAceitePorcentajeHumedad, $pl->RefinacionAceitePorcentajeHumedadAlertaNaranja)?>><?php print $refinado->AceitePorcentajeHumedad ? number_format($refinado->AceitePorcentajeHumedad,2,$decimal,'') : '-';?> </td>
        <td  <?php print $this->cla($refinado->AceitePorcentajeSolidos, $pl->RefinacionMinAceitePorcentajeSolidos,$pl->RefinacionMaxAceitePorcentajeSolidos, $pl->RefinacionAceitePorcentajeSolidosAlertaNaranja)?>><?php print $refinado->AceitePorcentajeSolidos ? number_format($refinado->AceitePorcentajeSolidos,2,$decimal,'') : '-';?> </td>
        <td  <?php print $this->cla($refinado->AceitePorcentajeAcidez, $pl->RefinacionMinAceitePorcentajeAcidez,$pl->RefinacionMaxAceitePorcentajeAcidez, $pl->RefinacionAceitePorcentajeAcidezAlertaNaranja)?>><?php print $refinado->AceitePorcentajeAcidez ? number_format($refinado->AceitePorcentajeAcidez,2,$decimal,'') : '-';?> </td>
    <?php endforeach; ?>
    
    </tr>
 <?php endfor; ?>
 <!-- FIN ITEMS -->
 </tbody>
 </table>
<!--TABLA 2-->


 <table class="items" width="100%" style="font-size: 7pt; margin-top: 5px; border-collapse: collapse;" cellpadding="5">
 <thead>
 <tr>
 <!--Pestaña recepcion-->
 <td colspan="<?php print ($numeroplantas*10)+1;?>">Concentración</td>
 <td colspan="<?php print ($numerorotadisk*6)+1?>">Presecado</td>
 <td colspan="<?php print ($numerorotatubo*4)+1;?>">Secado</td> 
 <td colspan="7">Producto Terminado</td><td rowspan="5">Observaciones</td>
 </tr>
 <!--Pestaña Presecado-->
 <tr>
     
 <td rowspan="4">Hora</td>-->
 <?php foreach ($planta as $plant):
     if($plant->EstadoPlanta):?>    
        <td colspan="10"><?= $plant->Nombre ?></td>
    <?php else: ?>
        <td colspan="10" style="background-color: #C1C1C1;"><?php $plant->Nombre.' (inactiva)'; ?></td>
    <?php endif; ?>
 <?php endforeach;?>
     
 <td rowspan="4">Hora</td>
 <?php foreach ($rotadisk as $rotadiskItem):?>
    <?php if($rotadiskItem->EstadoRotadisk):?>   
        <td colspan="6"><?= $rotadiskItem->Nombre; ?></td>
    <?php else: ?>
        <td colspan="6" style="background-color: #C1C1C1;" ><?= $rotadiskItem->Nombre.' (inactiva)'; ?></td>
    <?php endif; ?>
 <?php endforeach;?>
 <td rowspan="4">Hora</td>
 <?php foreach ($rotatubo as $rotatuboItem):
     if($rotatuboItem->EstadoRotatubo):?>    
        <td colspan="4"><?= $rotatuboItem->Nombre ?></td>
    <?php else: ?>
        <td colspan="4" style="background-color: #C1C1C1;"><?= $rotatuboItem->Nombre.' (inactiva)'; ?></td>
    <?php endif; ?>
 <?php endforeach;?>
<td rowspan="4">Hora</td>
<td rowspan="4">Temp</td>
<td rowspan="4">Humedad</td>
<td rowspan="4">A/O</td>
<td rowspan="4">Cod. Inicio tarjeta</td>
<td rowspan="4">Cod. Fin tarjeta</td>
<td rowspan="4">Sacos</td>

 </tr>
 <!--Fila # 3-->
 <tr>
<?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td colspan="2" >A.Cola</td>
 <td colspan="1" >VAH</td>
 <td colspan="1" >Presión</td>
 <td colspan="1" >1°E</td>
 <td colspan="1" >2°E</td>
 <td colspan="1" >3°E</td>
 <td colspan="1" >CON</td>
 <td rowspan="3">AGUA DE COLA PROCESADA (m3)</td>
 <td rowspan="3">CONCENTRADO PRODUCIDO (m3) </td>
 <?php endfor; ?>
     
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td rowspan="2">Amp</td>
 <td colspan="1">P. PSI</td>
 <td rowspan="1" colspan="3">Temperatura</td>
    <td rowspan="2">%H</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerorotatubo;$i++):?>
 <td rowspan="2">Amp</td><td rowspan="2">PresiónPSI</td><td rowspan="2">Temperatura</td><td rowspan="2">%H</td>
 <?php endfor;?>

 </tr>
 <!--Fila # 4-->
 <tr>
     
  <?php for ($i=0;$i<$numeroplantas;$i++):?>
    <td>T°C</td>
    <td >%S</td>
    <td>T°C</td>
    <td>p°</td>
    <td>T°C</td>
    <td >__</td>
    <td>T°C</td>
    <td>%S</td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
    <td >Eje</td>
    <td >Chaq 1</td>
    <td >Chaq 2</td>
    <td >Temp</td>
 <?php endfor;?>

 </tr>
 <!--Fila # 5-->
 <tr>
  
  <?php  foreach($planta as $pla):?>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaTemperatura?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinVahTemperatura.' _ '.(float)$pla->TratamientoMaxVahTemperatura?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinVacPeso.' _ '.(float)$pla->TratamientoMaxVacPeso?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinTemperatura1.' _ '.(float)$pla->TratamientoMaxTemperatura1?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinTemperatura2.' _ '.(float)$pla->TratamientoMaxTemperatura2?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinTemperatura3.' _ '.(float)$pla->TratamientoMaxTemperatura3?></td>
 <td style="font-size: 6px;"><?php print (float)$pla->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxSalidaPorcentajeSolidos?></td>
<?php endforeach; ?>

 
 <?php foreach($rotadisk as $rot):?>
<td style="font-size: 6px;"><?php print (float)$rot->PresecadoMinAmperaje.' _ '.(float)$rot->PresecadoMaxAmperaje?></td>
 <td style="font-size: 6px;"><?php print (float)$rot->PresecadoMinPresionEje.' _ '.(float)$rot->PresecadoMaxPresionEje?></td>
 <td style="font-size: 6px;"> <?= (float)$rot->PresecadoMinTempCha1.' _ '.(float)$rot->PresecadoMaxTempCha1 ?> </td>
 <td style="font-size: 6px;"> <?= (float)$rot->PresecadoMinTempCha2.' _ '.(float)$rot->PresecadoMaxTempCha2 ?> </td>
 <td style="font-size: 6px;"><?php print (float)$rot->PresecadoMinSCR.' _ '.(float)$rot->PresecadoMaxSCR?></td>
 <td style="font-size: 6px;"><?php print (float)$rot->PresecadoMinPorcentajeHumedad.' _ '.(float)$rot->PresecadoMaxPorcentajeHumedad?></td>
<?php endforeach;?>
 
 <?php foreach($rotatubo as $rotb):?>
 <td style="font-size: 6px;"><?php print (float)$rotb->RotatuboMinAmperaje.' _ '.(float)$rotb->RotatuboMaxAmperaje?></td>
 <td style="font-size: 6px;"><?php print (float)$rotb->RotatuboMinPresionEje.' _ '.(float)$rotb->RotatuboMaxPresionEje?></td>
 <td style="font-size: 6px;"><?php print (float)$rotb->RotatuboMinSRC.' _ '.(float)$rotb->RotatuboMaxSRC?></td>
 <td style="font-size: 6px;"><?php print (float)$rotb->RotatuboMinHumedad.' _ '.(float)$rotb->RotatuboMaxHumedad?></td>
<?php endforeach;?>
 </tr>
 </thead>
 <tbody>
 <!-- ITEMS -->
 <?php  for($i=0;$i<$fin;$i++): ?>
    <tr>
      <!--Tratamiento -->
    <?php $x=0;
    foreach($planta as $pt):
        $tratamiento = $tratamientos[$counttrata]?$tratamientos[$counttrata]:NULL;$counttrata++;
        if ($x++<=0): ?>
        <td  ><?php echo $tratamiento->Hora ? substr($tratamiento->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td  <?= $this->cla($tratamiento->AlimentacionAguaColaTemperatura, $pt->TratamientoMinAlimentacionAguaColaTemperatura,$pt->TratamientoMaxAlimentacionAguaColaTemperatura, $pt->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja)?>><?php echo $tratamiento->AlimentacionAguaColaTemperatura ? number_format($tratamiento->AlimentacionAguaColaTemperatura,2,$decimal,'') :'-';?></td>
        <td  <?= $this->cla($tratamiento->AlimentacionAguaColaPorcentajeSolidos, $pt->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$pt->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, $pt->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print $tratamiento->AlimentacionAguaColaPorcentajeSolidos ? number_format($tratamiento->AlimentacionAguaColaPorcentajeSolidos,2,$decimal,'') : '-';?> </td>
        <td  <?= $this->cla($tratamiento->VahTemperatura, $pt->TratamientoMinVahTemperatura,$pt->TratamientoMaxVahTemperatura, $pt->TratamientoVahTemperaturaAlertaNaranja)?>><?php print $tratamiento->VahTemperatura ? number_format($tratamiento->VahTemperatura,2,$decimal,'') :  '-';?> </td>-
        <td  <?= $this->cla($tratamiento->VacPeso, $pt->TratamientoMinVacPeso,$pt->TratamientoMaxVacPeso, $pt->TratamientoVacPesoAlertaNaranja)?>><?php print $tratamiento->VacPeso ? number_format($tratamiento->VacPeso,2,$decimal,'') : '-';?> </td>
        <td  <?= $this->cla($tratamiento->Temperatura1, $pt->TratamientoMinTemperatura1,$pt->TratamientoMaxTemperatura1, $pt->TratamientoTemperatura1AlertaNaranja)?>><?php echo $tratamiento->Temperatura1 ? number_format($tratamiento->Temperatura1,2,$decimal,'') : '-';?></td>
        <td  <?= $this->cla($tratamiento->Temperatura2, $pt->TratamientoMinTemperatura2,$pt->TratamientoMaxTemperatura2, $pt->TratamientoTemperatura2AlertaNaranja)?>><?php print $tratamiento->Temperatura2 ? number_format($tratamiento->Temperatura2,2,$decimal,'') : '-';?> </td>
        <td  <?= $this->cla($tratamiento->Temperatura3, $pt->TratamientoMinTemperatura3,$pt->TratamientoMaxTemperatura3, $pt->TratamientoTemperatura3AlertaNaranja)?>><?php print $tratamiento->Temperatura3 ? number_format($tratamiento->Temperatura3,2,$decimal,'') : '-';?> </td>
        <td  <?= $this->cla($tratamiento->SalidaPorcentajeSolidos, $pt->TratamientoMinSalidaPorcentajeSolidos,$pt->TratamientoMaxSalidaPorcentajeSolidos, $pt->TratamientoSalidaPorcentajeSolidosAlertaNaranja)?> ><?php print $tratamiento->SalidaPorcentajeSolidos ? number_format($tratamiento->SalidaPorcentajeSolidos,2,$decimal,'') : '-';?> </td>
        <td  ><?= $tratamiento->aguacola ? number_format($tratamiento->aguacola,2,$decimal,'') : '-'?></td>
        <td  ><?= $tratamiento->concentrado ? number_format($tratamiento->concentrado,2,$decimal,''):'-' ?></td>
    <?php endforeach; ?>
 <!--Presecado -->
    <?php 
    $x=0;
    foreach ($rotadisk as $rt):
        $presecado = $presecados[$countrotadisk]? $presecados[$countrotadisk] : NULL ;$countrotadisk++;
        if ($x++<=0): ?>
        <td  ><?php echo $presecado->Hora? substr($presecado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td  <?php print $this->cla($presecado->Amperaje, $rt->PresecadoMinAmperaje,$rt->PresecadoMaxAmperaje, $rt->PresecadoAmperajeAlertaNaranja)?>><?php echo $presecado->Amperaje ? number_format($presecado->Amperaje,2,$decimal,'') : '-';?></td>
        <td  <?php print $this->cla($presecado->PresionEje, $rt->PresecadoMinPresionEje,$rt->PresecadoMaxPresionEje, $rt->PresecadoPresionEjeAlertaNaranja)?>><?php print $presecado->PresionEje ? number_format($presecado->PresionEje,2,$decimal,'') : '-';?> </td>
        
        <td <?= $this->clases($presecado->TempCha1,$rt->PresecadoMinTempCha1,$rt->PresecadoMaxTempCha1,$rt->PresecadoAlertaTempCha1) ?> > <?= $presecado->TempCha1 ? number_format($presecado->TempCha1,2,$decimal,'') : '-' ?></td>
        <td <?= $this->clases($presecado->TempCha2,$rt->PresecadoMinTempCha2,$rt->PresecadoMaxTempCha2,$rt->PresecadoAlertaTempCha2) ?> > <?= $presecado->TempCha2 ? number_format($presecado->TempCha2,2,$decimal,'') : '-' ?></td>
                    
        <td  <?php print $this->cla($presecado->SCR, $rt->PresecadoMinSCR,$rt->PresecadoMaxSCR, $rt->PresecadoSCRAlertaNaranja)?>><?php print $presecado->SCR ? number_format($presecado->SCR,2,$decimal,'') : '-';?> </td>
        <td  <?php print $this->cla($presecado->PorcentajeHumedad, $rt->PresecadoMinPorcentajeHumedad,$rt->PresecadoMaxPorcentajeHumedad, $rt->PresecadoPorcentajeHumedadAlertaNaranja)?>><?php print $presecado->PorcentajeHumedad ? number_format($presecado->PorcentajeHumedad,2,$decimal,'') : '-';?> </td>
    <?php endforeach; ?>
    <!--Secado-->
    <?php $x=0;
    foreach($rotatubo as $rtb):
        $row = $secados[$countsecado] ? $secados[$countsecado] : NULL;$countsecado++;
        if ($x++<=0): ?>
        <td  ><?php echo $row->Hora ? substr($row->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td  <?php print $this->cla($row->Amperaje, $rtb->RotatuboMinAmperaje,$rtb->RotatuboMaxAmperaje, $rtb->RotatuboAmperajeAlertaNaranja)?>><?php echo $row->Amperaje? number_format($row->Amperaje,2,$decimal,'') : '-';?></td>
        <td  <?php print $this->cla($row->PresionEje, $rtb->RotatuboMinPresionEje,$rtb->RotatuboMaxPresionEje, $rtb->RotatuboPresionEjeAlertaNaranja)?>><?php print $row->PresionEje ? number_format($row->PresionEje,2,$decimal,'') : '-';?> </td>
        <td  <?php print $this->cla($row->SRC, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?>><?php print $row->SRC ? number_format($row->SRC,2,$decimal,'') : '-';?> </td>
        <td  <?php print $this->cla($row->PorcentajeHumedad, $rtb->RotatuboMinHumedad,$rtb->RotatuboMaxHumedad, $rtb->RotatuboHumedadAlertaNaranja)?>><?php print $row->PorcentajeHumedad ? number_format($row->PorcentajeHumedad,2,$decimal,'') : '-'; ?> </td>
    <?php endforeach; ?>
    <!--ProductoFinal -->
    <?php $har = $harina[$countharina]? $harina[$countharina] : NULL; $countharina++; ?>
    <td ><?php print $har->Hora ? substr($har->Hora,0,5) : '-';?> </td>
    <td  <?php print $this->cla((float)$har->Temperatura,$p->HarinaMinTemperatura,$p->HarinaMaxTemperatura,$p->HarinaTemperaturaAlertaNaranja);?> ><?php echo $har->Temperatura ? number_format($har->Temperatura,2,$decimal,'') : '-';?></td>
    <td  <?php print $this->cla((float)$har->PorcentajeHumedad, $p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja);?> ><?php print $har->PorcentajeHumedad? number_format($har->PorcentajeHumedad,2,$decimal,'') : '-';?> </td>
    <td  <?php print $this->cla((float)$har->AO, $p->HarinappmMin,$p->HarinappmMax,$p->HarinappmAlertaNaranja)?> ><?php print $har->AO ? number_format($har->AO,2,$decimal,'') : '-';?> </td>
    <td ><?php print $har->CodigoInicio ? number_format($har->CodigoInicio,0,$decimal,'') : '-';?> </td>
    <td ><?php print $har->CodigoFin ? number_format($har->CodigoFin,0,$decimal,'') : '-';?> </td>
    <td ><?php print $har->SacoProducidos ? number_format($har->SacoProducidos,0,$decimal,'') : '-';?> </td>
    <td ><?php print $har->ObservacionID ? $har->obser->Nombre:'-';?> </td>
    </tr>
 <?php endfor; ?>
 <!-- FIN ITEMS -->
 </tbody>
 </table>

 </body>
 </html>
