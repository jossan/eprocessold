<html>
<head>
<style>
 body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>


 <table width="100%"><tr>
    
    <td width="33%" style="text-align: center;font-size: 10px;"><strong>PRODUCTO TERMINADO</strong></td>
    <td width="33%" style="text-align: right;"><br></td>
 </tr>
</table>

<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
 
<?php
    $decimal = $this->decimal();
    
    ?>
    <table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
    <thead>
    <tr >
        <td style="font-size:10px;" rowspan="4"><center>Fecha</center></td>
        <td style="font-size:10px;" rowspan="4"><center>Hora de Medición</center></td>
        <td colspan=6 ><center>HARINA</center></td>
        <td rowspan="4"><center>OBSERVACIONES</center></td>
    </tr>
    
    <tr style="font-size:10px;">
        <td ><center>Temp</center></td>
        <td ><center>Humedad</center></td>
        <td ><center>A/O</center></td>
        <td rowspan=3><center>Código inicio <br>Tarjeta</center></td>
        <td rowspan=3><center>Código fin <br>Tarjeta</center></td>
        <td rowspan=3><center>Sacos</center></td>
    </tr>
    <tr style="font-size:10px;">
        <td ><center>°C</center></td>
        <td ><center>%</center></td>
        <td ><center>ppm</center></td>
    </tr>

    <tr style="font-size:10px;">
        <td ><center><?= (float)$p->HarinaMinTemperatura." _ ".(float)$p->HarinaMaxTemperatura; ?></center></td>
        <td ><center> <?= (float)$p->HarinaMinHumedad." _ ".(float)$p->HarinaMaxHumedad ?></center></td>
        <td ><center> <?= (float)$p->HarinappmMin." _ ".(float)$p->HarinappmMax ?></center></td>
    </tr>
    </thead>
    
    <?php
    foreach($result as $row): ?>
        <tr class ="odd" style="font-size:10px;">
        <?php $hora=  explode(':', $row->Hora); ?>
        <td > <?= $row->Fecha ?></td>
            <td > <?= $hora[0] ?>: <?= $hora[1] ?></td>
        <td <?= $this->clases((float)$row->Temperatura,$p->HarinaMinTemperatura,$p->HarinaMaxTemperatura,$p->HarinaTemperaturaAlertaNaranja) ?> >
            <?=number_format($row->Temperatura,2,$decimal,'')?></td>
        <td <?= $this->clases((float)$row->PorcentajeHumedad,$p->HarinaMinHumedad,$p->HarinaMaxHumedad,$p->HarinaHumedadAlertaNaranja) ?> >
            <?=number_format($row->PorcentajeHumedad,2,$decimal,'')?></td>
        <td <?= $this->clases((float)$row->AO,$p->HarinappmMin,$p->HarinappmMax,$p->HarinappmAlertaNaranja) ?> >
            <?= number_format($row->AO,2,$decimal,'')?></td>
        
        <td class="cini pasa" > <?= number_format($row->CodigoInicio,0,$decimal,'') ?></td>
        <td class="cfin pasa" > <?= number_format($row->CodigoFin,0,$decimal,'') ?></td>
        <td class="saco pasa" > <?= number_format($row->SacoProducidos,0,$decimal,'') ?></td>
        <td> <?= isset($row->obser) ? $row->obser->Descripcion : ''?> </td>
        </tr>
    <?php endforeach;?>
    
    </table>';
     
 </body>
 </html>

