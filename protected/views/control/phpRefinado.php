<html>
<head>
<style>
 body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
    <td width="33%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="25px;"></td>
    <td width="33%" style="text-align: center;font-size: 10px;"><strong>PROCESO DE CENTRIFUGADO</strong></td>
    <td width="33%" style="text-align: right;"></td>
 </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 8pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
 <table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
<?php
     $decimal = $this->decimal();
    $count= 0;
    ?>
     <thead>
    <tr >
        <td style="font-size:10px;" rowspan="4"><center>Fecha</center></td>
        <td style="font-size:10px;" rowspan="4"><center>Hora de Medición</center></td>
    
    <?php foreach ($model as $row):
            $count++;
           if($row->EstadoPulidora): ?>
               <td colspan="4" ><center><?= $row->Nombre; ?></center></td>
            <?php else: ?>
               <td colspan="4" style="background-color: #C1C1C1;"><center><?php print $row->Nombre.' (inactiva)';?></center></td>
            <?php endif; ?>
        <?php endforeach;?>
    </tr>
    <tr style="font-size:11px;">
        <?php for($y = 0 ;$y<$count;$y++):?>
            <td colspan="1" ><center>ALIM</center></td>
            <td colspan="3" ><center>ACEITE</center></td>';
        <?php endfor; ?>
    </tr>
    
    <tr style="font-size:11px;">
    <?php for($y = 0 ;$y<$count;$y++):?>
        <td><center>T °C</center></td>
        <td ><center>%H</center></td>
        <td><center>%S</center></td>
        <td><center>%A</center></td>';
    <?php endfor; ?>
 
    </tr>
    <?php 
    
    $contadortabla=1;?>
    <tr style="font-size:11px;">
        <?php foreach($model as $p): ?>
            <td><center> <?= (float)$p->RefinacionMinAlimentacionTemperatura.' _ '.(float)$p->RefinacionMaxAlimentacionTemperatura; ?></center></td>
            <td ><center><?= (float)$p->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$p->RefinacionMaxAceitePorcentajeHumedad; ?></center></td>
            <td><center><?= (float)$p->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$p->RefinacionMaxAceitePorcentajeSolidos ;?></center></td>
            <td><center><?= (float) $p->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$p->RefinacionMaxAceitePorcentajeAcidez ?></center></td>
        <?php endforeach; ?>
    </tr>
    </thead>
    <?php
    
    foreach($result as $row):
        
        if($contadortabla==1):?>
            <tr class ="odd" style="font-size:11px;height:10px;">
            <?php $hora=  explode(':', $row->Hora); ?>
            <td > <?= $row->Fecha ?></td>
            <td > <?= $hora[0]; ?> :<?= $hora[1]; ?></td>';                        
        <?php endif;?>
        <td <?= $this->clases($row->AlimentacionTemperatura, $p->RefinacionMinAlimentacionTemperatura, $p->RefinacionMaxAlimentacionTemperatura, $p->RefinacionTemperaturaAlertaNaranja)?> > <?= number_format($row->AlimentacionTemperatura,2,$decimal,'') ?></td>
        <td <?= $this->clases($row->AceitePorcentajeHumedad, $p->RefinacionMinAceitePorcentajeHumedad, $p->RefinacionMaxAceitePorcentajeHumedad, $p->RefinacionAceitePorcentajeHumedadAlertaNaranja)?> > <?= number_format($row->AceitePorcentajeHumedad,2,$decimal,'') ?></td>
        <td <?= $this->clases($row->AceitePorcentajeSolidos, $p->RefinacionMinAceitePorcentajeSolidos, $p->RefinacionMaxAceitePorcentajeSolidos, $p->RefinacionAceitePorcentajeSolidosAlertaNaranja)?> ><?=number_format($row->AceitePorcentajeSolidos,2,$decimal,'') ?></td>
        <td <?= $this->clases($row->AceitePorcentajeAcidez, $p->RefinacionMinAceitePorcentajeAcidez, $p->RefinacionMaxAceitePorcentajeAcidez, $p->RefinacionAceitePorcentajeAcidezAlertaNaranja)?> > <?= number_format($row->AceitePorcentajeAcidez,2,$decimal,'') ?></td>
        <?php $contadortabla++;
        if($contadortabla>$count):
            $contadortabla=1;?>
            </tr>
        <?php endif;?>
    <?php endforeach;?>
    </table>
     
     
 </body>
 </html>

