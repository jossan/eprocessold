<html>
<head>
<style>
 body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
    <td width="33%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="20px;"></td>
    <td width="33%" style="text-align: center;font-size: 10px;"><strong>PROCESO DE PRENSADO</strong></td>
    <td width="33%" style="text-align: right;"><br></td>
 </tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 8pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
<?php
    $decimal = $this->decimal();
    $count= 0;
?>
<table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
    <thead>
    <tr>
        <td style="font-size:10px;" rowspan="3"><center>Fecha</center></td>
        <td style="font-size:10px;" rowspan="3"><center>Hora de Medición</center></td>
        <?php foreach ($model as $row):
            $count++;
           if($row->EstadoPrensa): ?>
               <td colspan="2"><center><?= $row->Nombre; ?></center></td>
            <?php else: ?>
               <td colspan="2" style="background-color: #C1C1C1;"><center><?php print $row->Nombre.' (inactiva)';?></center></td>
            <?php endif; ?>
        <?php endforeach;?>

        <td colspan="1"><center>Licor</center></td>
        <td colspan="1" rowspan="3"><center>Dosificación de concentrado (l)</center></td>
    </tr>
    <tr style="font-size:11px;">
    
        <?php for($y = 0 ;$y<$count;$y++): ?>
            <td><center>Amp</center></td>
            <td><center>%H</center></td>
        <?php endfor; ?>

        <td><center>%S</center></td>
    </tr>
    
    <?php
    
    $contadortabla=1;
    ?>
    
    <tr style="font-size:11px;">
        
    <!--tbody-->
    
    <?php foreach($model as $pp): ?>
        <td><center><?= (float)$pp->PrensadoMinAmperaje.' _ '.(float)$pp->PrensadoMaxAmperaje ?></center></td>
        <td><center><?= (float)$pp->PrensadoMinPorcentajeHumedad.' _ '.(float)$pp->PrensadoMaxPorcentajeHumedad ?> </center></td>
    <?php endforeach;?>
    <td><center><?= (float)$p['LicorMinPorcentajeSolidos'].' _ '.(float)$p['LicorMaxPorcentajeSolidos'] ?></center></td>
    </tr>
    </thead>
    
    <!--tbody-->
    <?php
        foreach($result as $row):
            $pp=$model[$contadortabla-1];
            if($contadortabla==1):?>
                <tr class ="odd" style="font-size:11px;height:10px;">
                <?php $hora=  explode(':', $row->Hora); ?>
                    <td ><?= $row->Fecha; ?></td>
                <td style="text-align: center"><?= $hora[0] ?> : <?= $hora[1] ?></td>
            <?php endif; ?>
            <td <?= $this->clases($row->Amperaje, $pp->PrensadoMinAmperaje, $pp->PrensadoMaxAmperaje, $pp->PrensadoAmperajeAlertaNaranja)?> > <?= number_format($row->Amperaje,2,$decimal,'') ?> </td>
            <td <?= $this->clases($row->PorcentajeHumedad,$pp->PrensadoMinPorcentajeHumedad, $pp->PrensadoMaxPorcentajeHumedad, $pp->PrensadoPorcentajeHumedadAlertaNaranja) ?> > <?= number_format($row->PorcentajeHumedad,2,$decimal,'') ?></td>
            <?php 
            $contadortabla++;
            if($contadortabla>$count):
                    $contadortabla=1;
                    $licor = Licor::model()->find('codigofila ='.$row->codigofila);
                    ?>
                    <td  <?= $this->clases($licor->LicorPorcentajeSolidos, $p['LicorMinPorcentajeSolidos'], $p['LicorMaxPorcentajeSolidos'], $p['LicorPorcentajeSolidosAlertaNaranja'])?> > <?= number_format($licor->LicorPorcentajeSolidos,2,$decimal,'') ?> </td>
                    <td><center> <?= number_format($licor->concentrado,2,$decimal,'') ?> </center></td>
                    </tr>

            <?php endif; ?>
        <?php endforeach ?>
        </table>
    
    </body>
 </html>

