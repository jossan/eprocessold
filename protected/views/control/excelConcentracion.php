<html>
<head>
<style>
 body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>


 <table width="100%"><tr>
 
<td width="33%" style="text-align: center;font-size: 10px;"><strong>PROCESO DE CONCENTRACIÓN</strong></td>
<td width="33%" style="text-align: right;"><br></td>
 

 </tr>

</table>
</htmlpageheader>

<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
<?php
    $decimal = $this->decimal();
    $count= 0;
?>
<table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
    <thead>
        <tr >
            <td style="font-size:10px;" rowspan="4"><center>Fecha</center></td>
            <td style="font-size:10px;" rowspan="4"><center>Hora de Medición</center></td>

            <?php foreach ($model as $row):
                $count++;
               if($row->EstadoPlanta): ?>
                   <td colspan="10"><center><?= $row->Nombre; ?></center></td>
                <?php else: ?>
                   <td colspan="10" style="background-color: #C1C1C1;"><center><?php print $row->Nombre.' (inactiva)';?></center></td>
                <?php endif; ?>

            <?php endforeach;?>
        </tr>
    <tr style="font-size:11px;">';
        <?php for($y = 0 ;$y<$count;$y++): ?>
            <td colspan="2" ><center>ALIM A.Cola</center></td>
            <td colspan="1" ><center>VAH</center></td>
            <td colspan="1" ><center>Vacio</center></td>
            <td colspan="1" ><center>1°E</center></td>
            <td colspan="1" ><center>2°E</center></td>
            <td colspan="1" ><center>3°E</center></td>
            <td colspan="1" ><center>CON</center></td>
            <td rowspan="3" > AGUA DE COLA PROCESADA (m3) </td>
            <td rowspan="3" > CONCENTRADO PRODUCIDO (m3)  </td>
        <?php endfor;?>
    </tr>
    <tr style="font-size:11px;">
        <?php for($y = 0 ;$y<$count;$y++): ?>
            <td><center>T °C</center></td>
            <td ><center>%S</center></td>
            <td><center>T°C</center></td>
            <td><center>p</center></td>
            <td><center>T °C</center></td>
            <td ><center>__</center></td>
            <td><center>T°C</center></td>
            <td><center>%S</center></td>
        <?php endfor; ?>
    </tr>
    <?php 
    
    $contadortabla=1;
    ?>
    <tr >        
    <?php foreach($model as $p): ?>
        <td><center><?= (float)$p->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaTemperatura; ?></center></td>
        <td ><center><?= (float)$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos; ?></center></td>
        <td><center><?= (float)$p->TratamientoMinVahTemperatura.' _ '.(float)$p->TratamientoMaxVahTemperatura ?> </center></td>
        <td><center><?= (float)$p->TratamientoMinVacPeso.' _ '.(float)$p->TratamientoMaxVacPeso; ?></center></td>
        <td><center><?= (float)$p->TratamientoMinTemperatura1.' _ '.(float)$p->TratamientoMaxTemperatura1 ?></center></td>
        <td ><center><?= (float)$p->TratamientoMinTemperatura2.' _ '.(float)$p->TratamientoMaxTemperatura2 ?></center></td>
        <td><center><?= (float)$p->TratamientoMinTemperatura3.' _ '.(float)$p->TratamientoMaxTemperatura3 ?> </center></td>
        <td><center> <?= (float)$p->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxSalidaPorcentajeSolidos ?></center></td>
    <?php endforeach; ?>
    </tr>
    </thead>
    
        <?php foreach($result as $row):
        $p= $model[$contadortabla-1];
        
        if($contadortabla==1):?>
            <tr class ="odd" style="font-size:11px;height:10px;">
            <?php $hora=  explode(':', $row->Hora);?>
            <td > <?= $row->Fecha ?></td>   
            <td > <?= $hora[0] ;?> : <?= $hora[1];?></td>
        <?php endif;?>
            <td  <?= $this->clases($row->AlimentacionAguaColaTemperatura,$p->TratamientoMinAlimentacionAguaColaTemperatura,$p->TratamientoMaxAlimentacionAguaColaTemperatura,$p->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja) ?> > <?= number_format($row->AlimentacionAguaColaTemperatura,2,$decimal,'') ?></td>
            <td  <?= $this->clases($row->AlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja)?> ><?= number_format($row->AlimentacionAguaColaPorcentajeSolidos,2,$decimal,'') ?></td>
            <td  <?= $this->clases($row->VahTemperatura,$p->TratamientoMinVahTemperatura,$p->TratamientoMaxVahTemperatura,$p->TratamientoVahTemperaturaAlertaNaranja)?> ><?= number_format($row->VahTemperatura,2,$decimal,'')?></td>
            <td  <?= $this->clases($row->VacPeso,$p->TratamientoMinVacPeso,$p->TratamientoMaxVacPeso,$p->TratamientoVacPesoAlertaNaranja) ?> > <?= number_format($row->VacPeso,2,$decimal,'') ?></td>
            <td  <?= $this->clases($row->Temperatura1,$p->TratamientoMinTemperatura1,$p->TratamientoMaxTemperatura1,$p->TratamientoTemperatura1AlertaNaranja) ?> > <?= number_format($row->Temperatura1,2,$decimal,'') ?></td>
            <td  <?= $this->clases($row->Temperatura2,$p->TratamientoMinTemperatura2,$p->TratamientoMaxTemperatura2,$p->TratamientoTemperatura2AlertaNaranja) ?> ><?= number_format($row->Temperatura2,2,$decimal,'') ?></td>';
            <td  <?= $this->clases($row->Temperatura3,$p->TratamientoMinTemperatura3,$p->TratamientoMaxTemperatura3,$p->TratamientoTemperatura3AlertaNaranja) ?> ><?= number_format($row->Temperatura3,2,$decimal,'') ?></td>';
            <td  <?= $this->clases($row->SalidaPorcentajeSolidos,$p->TratamientoMinSalidaPorcentajeSolidos,$p->TratamientoMaxSalidaPorcentajeSolidos,$p->TratamientoSalidaPorcentajeSolidosAlertaNaranja) ?> > <?= number_format($row->SalidaPorcentajeSolidos,2,$decimal,'') ?></td>
            <td><?= number_format($row->aguacola,2,$decimal,'') ?></td>
            <td><?= number_format($row->concentrado,2,$decimal,'') ?></td>
            <?php $contadortabla++;
            if($contadortabla>$count):
                $contadortabla=1;?>
                
                </tr>
            <?php endif;?>
    <?php endforeach;?>
    </table>
    
    </body>
 </html>