<html>
<head>
<style>
 body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>


 <table width="100%"><tr>
    
    <td width="33%" style="text-align: center;font-size: 10px;"><strong>PROCESO DE SECADO</strong></td>
    <td width="33%" style="text-align: right;"></td>
 </tr>
</table>

<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->

<?php 
    $decimal = $this->decimal();
    $count= 0;
?>
 <table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
     <thead>
     <tr >
        <td style="font-size:10px;" rowspan="3"><center>Fecha</center></td>
        <td style="font-size:10px;" rowspan="3"><center>Hora de Medición</center></td>
        <?php foreach ($model as $row):
            $count++;
            if($row->EstadoRotatubo): ?>
                <td colspan="4" ><center><?= $row->Nombre ?></center></td>
            <?php else: ?>
                <td colspan="4" style="background-color: #C1C1C1;"><center><?php print $row->Nombre.' (inactiva)';?></center></td>
            <?php endif; ?>
        <?php endforeach;?>
    </tr>
    
    <tr style="font-size:11px;">
        <?php for($y = 0 ;$y<$count;$y++):?>
            <td ><center>Amp</center></td>
            <td colspan="1"><center>Presión PSI</center></td>
            <td colspan="1" ><center>Temperatura</center></td>
            <td colspan="1" ><center>%H</center></td>
        <?php endfor;?>
    </tr>

    <?php 
    $contadortabla=1;
    ?>
    <tr style="font-size:11px;">
    <?php foreach($model as $p): ?>
        <td><center><?= (float)$p->RotatuboMinAmperaje.' _ '.(float)$p->RotatuboMaxAmperaje ?></center></td>
        <td><center><?= (float)$p->RotatuboMinPresionEje.' _ '.(float)$p->RotatuboMaxPresionEje ?></center></td>
        <td><center> <?= (float)$p->RotatuboMinSRC.' _ '.(float)$p->RotatuboMaxSRC ?></center></td>
        <td ><center> <?= (float)$p->RotatuboMinHumedad.' _ '.(float)$p->RotatuboMaxHumedad ?></center></td>
    <?php endforeach;?>
    </tr>
    </thead>
    <?php 
    foreach($result as $row):
        $p = $model[$contadortabla-1];
        if($contadortabla==1): ?>
            <tr class ="odd" style="font-size:11px;height:10px;">
            <?php $hora=  explode(':', $row->Hora);?>
                <td ><?= $row->Fecha ?></td>';
                <td ><?= $hora[0] ?> : <?= $hora[1]?></td>';
        <?php endif;?>
            <td <?= $this->clases($row->Amperaje,$p->RotatuboMinAmperaje,$p->RotatuboMaxAmperaje,$p->RotatuboAmperajeAlertaNaranja) ?> >
                <?= number_format($row->Amperaje,2,$decimal,'') ?></td>
            <td <?= $this->clases($row->PresionEje,$p->RotatuboMinPresionEje,$p->RotatuboMaxPresionEje,$p->RotatuboPresionEjeAlertaNaranja) ?> >
                <?= number_format($row->PresionEje,2,$decimal,'') ?></td>
            <td <?= $this->clases($row->SRC,$p->RotatuboMinSRC,$p->RotatuboMaxSRC,$p->RotatuboSRCAlertaNaranja) ?>>
                <?= number_format($row->SRC,2,$decimal,'') ?></td>
            <td <?= $this->clases($row->PorcentajeHumedad,$p->RotatuboMinHumedad,$p->RotatuboMaxHumedad,$p->RotatuboHumedadAlertaNaranja) ?> > 
                <?= number_format($row->PorcentajeHumedad,2,$decimal,'') ?></td>
            <?php 
            $contadortabla++;
            if($contadortabla>$count):
                    $contadortabla=1; ?>
                    </tr>
            <?php endif; ?>
    <?php endforeach; ?>
    </table>

     
     
 </body>
 </html>

