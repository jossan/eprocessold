<?php /*echo $form->dateRangeGroup($model,'Fecha',array('widgetOptions' => array(
'callback' => 'js:function(start, end){console.log(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));}'),
'wrapperHtmlOptions' => array('class' => 'col-sm-5','placeholder'=>'Rango de fecha'),'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
)); */

?>
<fieldset>
<legend style='margin: 0px;padding: 0px;margin-bottom: 0px;'>Recepción de materia prima</legend>
<center>
    <table style="width: 100%">
    <tr>
        <td class="fechas">
            Fecha Inicial
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'inicio',
    'value'=>$f1,
        'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> 'linked',
        )
    )
    );
 ?>
        </td>
          <td class="fechas">
            Fecha final
            
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fin',
    'value'=>$f2,
        'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> 'linked',
        )
    )
    );
 ?>
        </td>
        <td class="hora">

     Hora Inicial <br>
     <?php   $this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horainic',
    'id' => 'horainic',
    'value'=>$horaini,
        'noAppend' => true,
        'options'=>array(
        'size' => 'small',
            'minuteStep'=>2,
    'showMeridian'=> false,
        ),
        
    )
    );
    ?>
            
            	
        </td >
        <td class="hora">
            Hora Final <br>
            <?php   $this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horafinc',
        'id'=>'horafinc',
        'noAppend' => true,
    'value'=>$horafin,
        'options'=>array(
          'minuteStep'=>2,
    'showMeridian'=> false,
        )
    )
    );
 ?>
        </td>
        <td>
            <br>
        <?php
        echo CHtml::link('Actualizar','index.php?r=/control/produccionRecepcion',array('id'=>'btnact','class'=>'btn btn-info'));
        ?>
        </td>
        <td style='text-align:right;'>
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/pdf_icon.png","Descargar reporte PDF",array("title"=>"Exportar a PDF",'height'=>'40px')),
                'index.php?r=/control/generarExcelprod',array('id'=>'btnpdf')
        );?>
        </td><td>
        <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel_ico.png","Descargar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                'index.php?r=/control/generarExcelprod',array('id'=>'btnexcel')
        );?>
        </td>
    </tr>
</table>
</center>
</fieldset>

<?php
//$m = $model->search($f1,$f2);
?>
<iframe src="index.php?r=/control/VerpdfRango&fechaini=<?php print $f1; ?>&fechafin=<?php print $f2; ?>&horaini=<?php print $horaini; ?>&horafin=<?php print $horafin; ?>&hora=<?php print $hora; ?>" width="100%" height="600px;" >
<p>Tu Navegador no soporta iframe</p>
</iframe>

<script type="text/javascript">
    var recepcion = <?php print $hora;?>;
    function enlistar(){
        alert($('#rangofecha').val());
    }
    $("#btnact").click(function (){
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/produccionRecepcion';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&hora='+recepcion;
        $(this).attr('href',enlace);
    } );
    
    $("#btnexcel").click(function (){
        
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/generarExcelprod';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&hora='+recepcion;
        $(this).attr('href',enlace);
    } );
    $("#btnpdf").click(function (){
        
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/VerpdfRango';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&d='+1+'&hora='+recepcion;;
        $(this).attr('href',enlace);
    } );
    $('.ct-form-control').each(function(){
        $(this).attr('class','form-control');
    });
    $('.fechas').change(function(){
        var ini = $('#inicio').val();
        var fin = $('#fin').val();
        if(ini != fin){
            $('.hora').hide();
        }else{
            $('.hora').show();
        }
    });
    $('.fechas').change();
</script>