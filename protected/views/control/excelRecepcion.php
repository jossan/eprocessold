<html>
<head>
<style>
body {
    font-family: sans-serif;
    font-size: 5pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
    border-left: 0.08mm solid gray;
    border-right:0.08mm solid gray;
 }
 table thead td { 
     background-color: #EEEEEE;
    text-align: center;
    border: 0.08mm solid gray;
    vertical-align: middle;
 }
 table.items{
     border-top:  0.08mm solid gray;
     border-left: 0.08mm solid gray;
     border-right: 0.08mm solid gray;
 }
 .items tr {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
    text-align: right;
    border: 0.08mm solid gray;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>


 <table width="100%"><tr>
 
<td width="33%" style="text-align: center;font-size: 10px;"><strong>RECEPCIÓN DE MATERIA PRIMA</strong></td>
<td width="33%" style="text-align: right;"><br></td>
 

 </tr>

</table>

 <table class="items" width="100%" style="font-size: 6pt; border-collapse: collapse;" cellpadding="5"  >
 <thead>
 <!--Pestaña Coccion-->
 <tr>
 <td rowspan="1">FECHA</td>
 <td rowspan="1">HORA</td>
 <td rowspan="1">Id recepción</td>
 <td rowspan="1">Proveedor</td>
 <td rowspan="1">Placa</td>
 <td rowspan="1">Chofer</td>
 <td rowspan="1">Secuencia</td>
 <td rowspan="1">Especie</td>
 <td rowspan="1">Peso(T)</td>
 <td rowspan="1">Tara carro(T)</td>
 <td rowspan="1">Desc(%)</td>
 <td rowspan="1">Peso Neto(T)</td>
<!-- <td rowspan="1">TBVN</td>-->
 <td rowspan="1">Hora de entrada a tolva</td>
 <td rowspan="1">TOLVA</td>
 <td rowspan="1">Fecha producción</td>
 </tr>
 
 </thead>
 <tbody>
     
 <!-- ITEMS -->
 <?php 
 $contat = 0;
 $contad = 0;
 $contan = 0;
 $contatarra = 0;
foreach ($model as $rowRecepcion):  ?>
    <tr>
    <td align="center"><?php echo $rowRecepcion->Fecha;?></td>
    <td align="center"><?php echo $rowRecepcion->Hora? substr($rowRecepcion->Hora,0,5) : '-' ; ?></td>
    <td align="center"><?php echo $rowRecepcion->ID ;?></td>
    <td align="center"><?= $rowRecepcion->proveedor ? $rowRecepcion->proveedor->Nombre : "";?></td>
    <td align="center"><?= $rowRecepcion->Placa ;?></td>
    <td align="center"><?= $rowRecepcion->Chofer ;?></td>
    <td align="center"><?= $rowRecepcion->IdSecuencia ;?></td>
    <td  align="center"><?php print isset($rowRecepcion->especie) ? $rowRecepcion->especie->Nombre : '-';?> 
    </td><td align="center"><?php print $rowRecepcion->PesoTonelada ? number_format($rowRecepcion->PesoTonelada,3,$decimal,'') : '-'; $contat+=$rowRecepcion->PesoTonelada; ?>
    </td><td align="center"><?php print $rowRecepcion->tarracarro ? number_format($rowRecepcion->tarracarro,3,$decimal,'') : '-'; $contatarra+=$rowRecepcion->tarracarro; ?>
    </td><td align="center"><?php print $rowRecepcion->Descuento ? number_format($rowRecepcion->Descuento,0,$decimal,'').' %' : '-';$contad+=$rowRecepcion->Descuento;?>
    </td><td align="center"><?php print $rowRecepcion->Pesoneto ? number_format($rowRecepcion->Pesoneto,3,$decimal,'') : '-';$contan+=$rowRecepcion->Pesoneto;?></td>
<!--<td align="right"><?php //print $rowRecepcion->TBVN ? $rowRecepcion->TBVN : '-';?> </td>-->
    <td align="right"><?php print $rowRecepcion->Horatolva ? substr($rowRecepcion->Horatolva,0,5) : '-';?> </td>
    <td align="left"><?php print $rowRecepcion->TolvaID ? $rowRecepcion->tolva->Nombre : '-';?> </td>
    <td style="text-align: center;"><?= $rowRecepcion->fechaproduccion ?></td>
  </tr>
 <?php endforeach; ?>
  <tr style='background-color: #333;'>
      <td colspan="8" style='text-align: right;color: white;'>TOTALES:</td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($contat,3,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($contat,3,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($contad,3,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($contan,3,$decimal,'');?></td>
      <td colspan="3" ></td>
  </tr>
 <!-- FIN ITEMS -->
 </tbody>
 </table>
 </body>
 </html>

