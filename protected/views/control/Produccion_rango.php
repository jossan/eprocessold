<?php /*echo $form->dateRangeGroup($model,'Fecha',array('widgetOptions' => array(
'callback' => 'js:function(start, end){console.log(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));}'),
'wrapperHtmlOptions' => array('class' => 'col-sm-5','placeholder'=>'Rango de fecha'),'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
)); */
$js = "";
if((int)$proceso==1){
    $js="sector_inicio";
}elseif((int)$proceso>1 and (int)$proceso<9) {
    $js= "sector_centro";
}elseif((int)$proceso==9){
    $js= "sector_final";
}

?>
<form method="get" action="index.php" >
    <input value="control/produccionRango" name="r" type="hidden">
<fieldset>
<legend style='margin: 0px;padding: 0px;margin-bottom: 0px;'>Control de proceso de producción</legend>
    <table style="width: 100%">
    <tr>
        <td class="fechas">
            Fecha Inicial
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fechaini',
    'value'=>$f1,
        'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> 'linked',
        )
    )
    );
 ?>
        </td>
          <td class="fechas">
            Fecha final
            
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fechafin',
    'value'=>$f2,
    'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> '    linked',
        )
    )
    );
 ?>
        </td>
        <td class="hora">

     Hora Inicial <br>
     <?php   $this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horaini',
    'id' => 'horainic',
    'value'=>$horaini,
        'noAppend' => true,
        'options'=>array(
        'size' => 'small',
            'minuteStep'=>2,
            'showMeridian'=> false,
        ),
        
    )
    );
    ?>
            
            	
        </td >
        <td class="hora">
            Hora Final <br>
            <?php   $this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horafin',
        'id'=>'horafinc',
        'noAppend' => true,
        'value'=>$horafin,
        'options'=>array(
            'minuteStep'=>2,
            'showMeridian'=> false,
        )
    )
    );
 ?>
        </td>
        <td>
            <br>
        <?php
        
         CHtml::link('Actualizar','index.php?r=/control/produccionRango',array('id'=>'btnact','class'=>'btn btn-info'));
         
        ?>
            <button type="sumit" class="btn btn-info">Actualizar</button>
        </td>
        <td style='text-align:right;'>
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/pdf_icon.png","Descargar reporte PDF",array("title"=>"Descargar reporte PDF",'height'=>'40px')),
                'index.php?r=/control/generarExcelprod',array('id'=>'btnpdf')
        );?>
        </td><td>
        <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel_ico.png","Descargar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                'index.php?r=/control/generarExcelprod',array('id'=>'btnexcel')
        );?>
        </td>
    </tr>
</table>

</fieldset>

    <div style="font-size:10px;text-transform: uppercase;">
    <div><label>Filtros por proceso:</label></div>
    <label><input type="radio" name="proceso" value="0" <?= ($proceso=='0') ? "checked" :"";?> onclick="enabled_mas_filtros('')" >Sin filtros</label>
    <label><input type="radio" name="proceso" value="1" <?= ($proceso=='1') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_inicio')" >Recepción</label>
    <label><input type="radio" name="proceso" value="2" <?= ($proceso=='2') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Coccion</label>
    <label><input type="radio" name="proceso" value="3" <?= ($proceso=='3') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Prensado</label>
    <label><input type="radio" name="proceso" value="4" <?= ($proceso=='4') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Decantación</label>
    <label><input type="radio" name="proceso" value="5" <?= ($proceso=='5') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Centrifugado</label>
    <label><input type="radio" name="proceso" value="6" <?= ($proceso=='6') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Concentración</label>
    <label><input type="radio" name="proceso" value="7" <?= ($proceso=='7') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Presecado</label>
    <label><input type="radio" name="proceso" value="8" <?= ($proceso=='8') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_centro')" >Secado</label>
    <label><input type="radio" name="proceso" value="9" <?= ($proceso=='9') ? "checked" :"";?> onclick="enabled_mas_filtros('sector_final')" >Producto terminado</label>
    
</div>
    <input type="hidden" name="hora" value="<?php print $hora;?>">
    <button style="display:none" onclick="masfiltros()" type="button">Mas filtros 
        <span id="right" class="glyphicon glyphicon-chevron-right"></span>
        <span id="down" style="display:none" class="glyphicon glyphicon-chevron-down"></span>
    </button>
<div id="filtros_container" style="display:none">
    <div id="sector_inicio">
        
        
        |
        
        Proveedor <select></select>
        
        |
        
        Especie <select></select>
        
        |
        
        Tolva
        <select >
            
        </select>
    </div>   
        <div id="sector_centro">
            <input type="checkbox" >fila con alertas naranja|
            <input type="checkbox" >fila con alertas rojo
            <input type="checkbox" >fila sin alertas
        </div>
        
        <div id="sector_final">
            |Clasificacion <select></select>| 
            Observación<select></select>
        </div>
    
</div>
    
</form>
<?php
//$m = $model->search($f1,$f2);
?>

<iframe src="index.php?r=/control/VerpdfRango&fechaini=<?php print $f1; ?>&fechafin=<?php print $f2; ?>&horaini=<?php print $horaini; ?>&horafin=<?php print $horafin; ?>&proceso=<?php print $proceso; ?>" width="100%" height="600px;" style="border:0px;">
<p>Tu Navegador no soporta iframe</p>
</iframe>

<script type="text/javascript">
    var filtros_estado = false;
    var recepcion = <?php print $hora;?>;
    function enlistar(){
        alert($('#rangofecha').val());
    }
    /*
    $("#btnact").click(function (){
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/produccionRango';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&hora='+recepcion;
        $(this).attr('href',enlace);
    } );
    */
    $("#btnexcel").click(function (){
        
        var ini ="<?php print $f1; ?>";
        var fin ="<?php print $f2; ?>";
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/generarExcelprod';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&hora='+recepcion+'&proceso='+<?php print $proceso; ?>;
        $(this).attr('href',enlace);
    } );
    $("#btnpdf").click(function (){
        
        var ini ="<?php print $f1; ?>";
        var fin ="<?php print $f2; ?>";
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/VerpdfRango';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin+'&d='+1+'&hora='+recepcion+'&proceso='+<?php print $proceso; ?>;
        $(this).attr('href',enlace);
    } );
    $('.ct-form-control').each(function(){
        $(this).attr('class','form-control');
    });
    $('.fechas').change(function(){
        var ini = $('#inicio').val();
        var fin = $('#fin').val();
        if(ini != fin){
            $('.hora').hide();
        }else{
            $('.hora').show();
        }
    });
    $('.fechas').change();
    
    
    function masfiltros(){
        filtros_estado = !filtros_estado;
        if(filtros_estado){
            $('#right').hide();
            $('#down').show();
            $('#filtros_container').show('show');
        }else{
            $('#down').hide();
            $('#right').show();
            $('#filtros_container').hide('show');
        }
    }
    
    function enabled_mas_filtros(sector){
        $('input[type="checkbox"]').prop('checked',false);
        $('#filtros_container').find('div').hide();
        $('#'+sector).show();
    }
    
    enabled_mas_filtros('<?= $js; ?>');
</script>