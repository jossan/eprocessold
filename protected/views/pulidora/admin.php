<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear centrifuga','url'=>array('create'),'visible'=>$admin || $jefeprod),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pulidora-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Centrifugas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'pulidora-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    array('name'=>'EstadoPulidora',
        'value'=>'$data->EstadoPulidora ? "Activo":"Inactivo"'
        ),
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
   'template'=>'{view} {update} {delete}',
    'buttons'=>array(
        'update'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
        'delete'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
    )
),
),
)); ?>
