<?php
$this->breadcrumbs=array(
	'Pulidoras'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de centrifugas','url'=>array('admin')),
	array('label'=>'Crear centrifuga','url'=>array('create')),
	array('label'=>'Ver centrifuga','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar centrifuga</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
