<?php
$this->breadcrumbs=array(
	'Lista'=>array('admin'),
	'Nueva',
);

$this->menu=array(
array('label'=>'Lista de centrifuga','url'=>array('admin')),

);
?>

<h3>Nueva centrifuga</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
