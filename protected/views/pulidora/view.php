<?php

$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Pulidoras'=>array('admin'),
	$model->Nombre,
);

$this->menu=array(
array('label'=>'Lista de centrifugas','url'=>array('admin')),
array('label'=>'Crear centrifuga','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar centrifuga','url'=>array('update','id'=>$model->ID),'visible'=>$admin || $jefeprod),
array('label'=>'Borrar centrifuga','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles de centrifuga</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    array( 'name'=>'EstadoPulidora',
        'value'=>$model->EstadoPulidora ? "Activo" : "Inactivo"
        ),
	
		//'Estado',
),
)); ?>
<?php 
      $model->RefinacionMinAlimentacionTemperatura = (float)$model->RefinacionMinAlimentacionTemperatura;
      $model->RefinacionMaxAlimentacionTemperatura = (float)$model->RefinacionMaxAlimentacionTemperatura;
      $model->RefinacionTemperaturaAlertaNaranja = (float)$model->RefinacionTemperaturaAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeHumedad = (float)$model->RefinacionMinAceitePorcentajeHumedad;
      $model->RefinacionMaxAceitePorcentajeHumedad = (float)$model->RefinacionMaxAceitePorcentajeHumedad;
      $model->RefinacionAceitePorcentajeHumedadAlertaNaranja = (float)$model->RefinacionAceitePorcentajeHumedadAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeSolidos = (float)$model->RefinacionMinAceitePorcentajeSolidos;
      $model->RefinacionMaxAceitePorcentajeSolidos = (float)$model->RefinacionMaxAceitePorcentajeSolidos;
      $model->RefinacionAceitePorcentajeSolidosAlertaNaranja = (float)$model->RefinacionAceitePorcentajeSolidosAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeAcidez = (float)$model->RefinacionMinAceitePorcentajeAcidez;
      $model->RefinacionMaxAceitePorcentajeAcidez = (float)$model->RefinacionMaxAceitePorcentajeAcidez;
      $model->RefinacionAceitePorcentajeAcidezAlertaNaranja = (float)$model->RefinacionAceitePorcentajeAcidezAlertaNaranja;      
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="refinacionTem">
               
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionMinAlimentacionTemperatura, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionMaxAlimentacionTemperatura, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionTemperaturaAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="AceiteHum">
        <div class="form-group" style="margin: 0px;padding: 0px;">
           
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->RefinacionMinAceitePorcentajeHumedad, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionMaxAceitePorcentajeHumedad, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionAceitePorcentajeHumedadAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
       
          <div class="aceiteSol">
        <div class="form-group" style="margin: 0px;padding: 0px;">
           
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->RefinacionMinAceitePorcentajeSolidos, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionMaxAceitePorcentajeSolidos, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionAceitePorcentajeSolidosAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
            <div class="aceiteA">
        <div class="form-group" style="margin: 0px;padding: 0px;">
          
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->RefinacionMinAceitePorcentajeAcidez, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionMaxAceitePorcentajeAcidez, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RefinacionAceitePorcentajeAcidezAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
     </form>
    </div>
    
    </div>