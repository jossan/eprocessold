<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pulidora-form',
    'type'=>'horizontal',
			'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

<?php echo $form->switchGroup($model, 'EstadoPulidora',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
        //'handleWith' => 100,
),
            ))); ?>
<?php 
      $model->RefinacionMinAlimentacionTemperatura = (float)$model->RefinacionMinAlimentacionTemperatura;
      $model->RefinacionMaxAlimentacionTemperatura = (float)$model->RefinacionMaxAlimentacionTemperatura;
      $model->RefinacionTemperaturaAlertaNaranja = (float)$model->RefinacionTemperaturaAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeHumedad = (float)$model->RefinacionMinAceitePorcentajeHumedad;
      $model->RefinacionMaxAceitePorcentajeHumedad = (float)$model->RefinacionMaxAceitePorcentajeHumedad;
      $model->RefinacionAceitePorcentajeHumedadAlertaNaranja = (float)$model->RefinacionAceitePorcentajeHumedadAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeSolidos = (float)$model->RefinacionMinAceitePorcentajeSolidos;
      $model->RefinacionMaxAceitePorcentajeSolidos = (float)$model->RefinacionMaxAceitePorcentajeSolidos;
      $model->RefinacionAceitePorcentajeSolidosAlertaNaranja = (float)$model->RefinacionAceitePorcentajeSolidosAlertaNaranja;
      
      $model->RefinacionMinAceitePorcentajeAcidez = (float)$model->RefinacionMinAceitePorcentajeAcidez;
      $model->RefinacionMaxAceitePorcentajeAcidez = (float)$model->RefinacionMaxAceitePorcentajeAcidez;
      $model->RefinacionAceitePorcentajeAcidezAlertaNaranja = (float)$model->RefinacionAceitePorcentajeAcidezAlertaNaranja;      
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        
            <div class="refinacionTem"><div style="margin-left:230px;">
        
           <?php echo $form->textFieldGroup($model,'RefinacionMinAlimentacionTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionMaxAlimentacionTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionTemperaturaAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
</div></div>
        <div class="AceiteHum"><div style="margin-left:230px;">
       
            <?php echo $form->textFieldGroup($model,'RefinacionMinAceitePorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionMaxAceitePorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionAceitePorcentajeHumedadAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
        <div class="aceiteSol"><div style="margin-left:230px;">
             <?php echo $form->textFieldGroup($model,'RefinacionMinAceitePorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionMaxAceitePorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionAceitePorcentajeSolidosAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
                <div class="aceiteA"><div style="margin-left:230px;">
        
          <?php echo $form->textFieldGroup($model,'RefinacionMinAceitePorcentajeAcidez',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionMaxAceitePorcentajeAcidez',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RefinacionAceitePorcentajeAcidezAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
     
    </div>
    
    </div>
	
	
	
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>