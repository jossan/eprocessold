<?php
$this->breadcrumbs=array(
	'Tolvas'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Tolva','url'=>array('admin')),

);
?>

<h2>Crear Tolva</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
