<?php
$this->breadcrumbs=array(
	'Tolvas'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Tolvas','url'=>array('admin')),
	array('label'=>'Crear Tolva','url'=>array('create')),
	array('label'=>'Ver Tolva','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Tolva</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
