<?php
$this->breadcrumbs=array(
	'Tolvas'=>array('admin'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Lista de Tolvas','url'=>array('admin')),
array('label'=>'Crear Tolva','url'=>array('create')),
array('label'=>'Actualizar Tolva','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Tolva','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h3>Detalles</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		//'Estado',
),
)); ?>
