<?php
$this->breadcrumbs=array(

	'Tolvas',
);

$this->menu=array(

array('label'=>'Crear Tolva','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tolva-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Tolvas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tolva-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
