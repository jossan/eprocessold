<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'paradasnoprogramadas-form',
	'enableAjaxValidation'=>false,
    'type'=>'horizontal'
)); ?>
<br/>
<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
