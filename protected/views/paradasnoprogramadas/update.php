<?php
$this->breadcrumbs=array(
	'Paradas no programadas'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de paradas no programadas','url'=>array('admin')),
	array('label'=>'Crear parada no programada','url'=>array('create')),
	);
	?>

	<h3>Actualizar </h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>