<?php

$this->menu=array(
array('label'=>'Crear parada no programada','url'=>array('create')),
);
?>

<h2>Paradas no programadas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'paradasnoprogramadas-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		'nombre',
array(
'class'=>'booster.widgets.TbButtonColumn',
     'template'=>'{update} {delete}'
),
),
)); ?>
