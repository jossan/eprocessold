<?php
$this->breadcrumbs=array(
	'Paradas no programadas'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista paradas no programadas','url'=>array('admin')),
);
?>

<h3>Nueva parada no programada</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>