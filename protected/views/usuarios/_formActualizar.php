<?php 
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'usuarios-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>
<div style="float:right;">
<?php 
if($admin){
$this->widget('booster.widgets.TbButton',array('label' => 'Reestablecer contraseña del usuario','context' => 'success','buttonType'=>'button','htmlOptions'=>array('id'=>'enviar')));
}
?>
</div>
<br>
<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>
<br>
<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

<?php echo $form->textFieldGroup($model,'NombreCompleto',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
<?php echo $form->textFieldGroup($model,'Usuario',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
<?php $rol = CHtml::listData(UsuarioRol::model()->findAll('Estado = 1'),'ID','NombreRol'); ?>

        <?php
        if($admin){
        echo"<div class='form-group'>
    <label class='col-sm-3 control-label required' >Rol del usuario <span class='required'> *</span></label>
    <div class='col-sm-5'>". $form->dropdownList($model,'TipoRolID', $rol, array('prompt'=>'--Seleccione rol--','class'=>'form-control'))."</div>
       </div>";
        }
        ?><br>
        
<?php echo $form->switchGroup($model, 'activo',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
),
            ))); ?>
        
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<br>



<?php $this->endWidget(); ?>
<script type="text/javascript">
$('#enviar').click(function(){
confirmar=confirm("¿Seguro desea reestablecer la contraseña?");
if (confirmar){
    $.ajax({
            url:'index.php?r=/Usuarios/guardarnueva',
            type:'GET',
            data: {idusu:<?php echo $model->ID ?>},
            success: function (resp) {
                if(resp==1){
        js:$.notify("Contraseña reestablecida", "success");//el valor es 1
        
    }else{
        js:$.notify("Ocurrio un error", "error");//el valor es 0
    }
            }
        });

}

});
</script>