<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'usuarios-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'NombreCompleto',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'Usuario',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

	<?php echo $form->passwordFieldGroup($model,'Contrasenia',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
        <?php echo $form->passwordFieldGroup($model,'repeat_contrasenia',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
<?php $rol = CHtml::listData(UsuarioRol::model()->findAll('Estado = 1'),'ID','NombreRol'); ?>
	
<div class="form-group">
    <label class="col-sm-3 control-label required" >Rol del usuario <span class="required"> *</span></label>
    <div class="col-sm-5">
        <?php echo $form->dropdownList($model,'TipoRolID', $rol, array('prompt'=>'--Seleccione rol--','class'=>'form-control')); ?><br>
        </div>
       </div>



<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
