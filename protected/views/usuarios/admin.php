<?php
$this->breadcrumbs=array(
	'Usuarios',
	
);

$this->menu=array(

array('label'=>'Crear Usuarios','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('usuarios-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Usuarios</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'usuarios-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'ID',
		'NombreCompleto',
		'Usuario',
		
  
		array(
                'name'=>'TipoRolID',
                'value'=>'$data->tipoRol->NombreRol' ,
                'filter'=>CHtml::activeTextField($model, 'nombrerol',array('class'=>'form-control')),
                ),
                ARRAY(
                   'name'=>'activo',
                    'value'=>'$data->activo ? "<span class=\'text-success\'>Activo</span>": "<span class=\'text-danger\'>Inactivo</span>"',
                    'type'=>'raw'
                ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
