<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	'Actualizar',
);
$ADMIN = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
	$this->menu=array(
	array('label'=>'Lista de Usuarios','url'=>array('admin'),'visible'=>$ADMIN),
	array('label'=>'Crear Usuarios','url'=>array('create'),'visible'=>$ADMIN),
	array('label'=>'Ver Usuarios','url'=>array('view','id'=>$model->ID),'visible'=>$ADMIN),
	
	);
	?>

	<h2>Actualizar Usuario </h2>

<?php echo $this->renderPartial('_formActualizar',array('model'=>$model)); ?>