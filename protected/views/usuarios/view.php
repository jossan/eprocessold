<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	
);
$ADMIN = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->menu=array(
array('label'=>'Lista de Usuarios','url'=>array('admin'),'visible'=>$ADMIN),
array('label'=>'Crear Usuarios','url'=>array('create'),'visible'=>$ADMIN),
array('label'=>'Actualizar Usuarios','url'=>array('update','id'=>$model->ID),'visible'=>$ADMIN),
array('label'=>'Borrar Usuarios','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$ADMIN),

);
?>

<h3>Detalles de usuario</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'NombreCompleto',
		'Usuario',
                array(
                'name'=>'TipoRolID',
                'value'=>$model->tipoRol->NombreRol,
                ),
                array(
                    'label'=>'Estado',
                    'value'=>$model->activo ? "Activo" : "Inactivo"
                )
		
		//'Estado',
),
)); ?>
