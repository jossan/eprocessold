<?php

$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Cocinadores'=>array('admin'),
	$model->Nombre,
);

$this->menu=array(
array('label'=>'Lista de Cocinadores','url'=>array('admin')),
array('label'=>'Crear Cocinador','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar Cocinador','url'=>array('update','id'=>$model->ID),'visible'=>$admin || $jefeprod),
array('label'=>'Borrar Cocinador','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles del cocinador</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    'CapacidadCocina',
                array('name'=>'EstadoCocina',
                    'value'=>$model->EstadoCocina?'Activo':'Inactivo'
                    )
		
		//'Estado',
),
)); ?>

<?php 
      $model->CoccionMinRmp = (float)$model->CoccionMinRmp;
      $model->CoccionMaxRmp = (float)$model->CoccionMaxRmp;
      $model->CoccionRpmAlertaNaranja = (float)$model->CoccionRpmAlertaNaranja;
      $model->CoccionMinPresionEje = (float)$model->CoccionMinPresionEje;
      $model->CoccionMaxPresionEje = (float)$model->CoccionMaxPresionEje;
      $model->CoccionPresionEjeAlertaNaranja = (float)$model->CoccionPresionEjeAlertaNaranja;
      $model->CoccionMinPresionChaqueta = (float)$model->CoccionMinPresionChaqueta;
      $model->CoccionMaxPresionChaqueta = (float)$model->CoccionMaxPresionChaqueta;
      $model->CoccionPresionChaquetaAlertaNaranja = (float)$model->CoccionPresionChaquetaAlertaNaranja;
      $model->CoccionMinTemperatura = (float)$model->CoccionMinTemperatura;
      $model->CoccionMaxTemperatura = (float)$model->CoccionMaxTemperatura;
      $model->CoccionPresionTemperaturaAlertaNaranja = (float)$model->CoccionPresionTemperaturaAlertaNaranja;
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="framecoccionrpm">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMinRmp, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMaxRmp, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionRpmAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="framecoccionPresioneje">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->CoccionMinPresionEje, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMaxPresionEje, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionPresionEjeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        <div class="framecoccionPresionchaq">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMinPresionChaqueta, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMaxPresionChaqueta, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionPresionChaquetaAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
             
        </div>
                <div class="framecocciontemperatura">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMinTemperatura, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionMaxTemperatura, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->CoccionPresionTemperaturaAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
     </form>
    </div>
    
    </div>
