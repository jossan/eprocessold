<?php
$this->breadcrumbs=array(
	'Cocinadores'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Cocinadores','url'=>array('admin')),

);
?>

<h3>Nuevo Cocinador</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
