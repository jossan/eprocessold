<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;


$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Cocinador','url'=>array('create'),'visible'=>$admin || $jefeprod),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('cocina-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Cocinadores</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'cocina-grid',
             'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
	//	'ID',
		'Nombre',
		'Descripcion',
    'CapacidadCocina',
                array('name'=>'EstadoCocina',
                    'value'=>'$data->EstadoCocina?"Activo":"Inactivo"'
                    ),
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
 'template'=>'{view} {update} {delete}',
    'buttons'=>array(
        'update'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
        'delete'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
    )
),
),
)); ?>
