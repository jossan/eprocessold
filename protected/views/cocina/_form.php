<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'cocina-form',
    'type'=>'horizontal',
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
<?php echo $form->textFieldGroup($model,'CapacidadCocina',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off')))); ?>

	<?php echo $form->switchGroup($model, 'EstadoCocina',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
        'handleWith' => 100,
),
            ))); ?>



<?php 
      $model->CoccionMinRmp = (float)$model->CoccionMinRmp;
      $model->CoccionMaxRmp = (float)$model->CoccionMaxRmp;
      $model->CoccionRpmAlertaNaranja = (float)$model->CoccionRpmAlertaNaranja;
      $model->CoccionMinPresionEje = (float)$model->CoccionMinPresionEje;
      $model->CoccionMaxPresionEje = (float)$model->CoccionMaxPresionEje;
      $model->CoccionPresionEjeAlertaNaranja = (float)$model->CoccionPresionEjeAlertaNaranja;
      $model->CoccionMinPresionChaqueta = (float)$model->CoccionMinPresionChaqueta;
      $model->CoccionMaxPresionChaqueta = (float)$model->CoccionMaxPresionChaqueta;
      $model->CoccionPresionChaquetaAlertaNaranja = (float)$model->CoccionPresionChaquetaAlertaNaranja;
      $model->CoccionMinTemperatura = (float)$model->CoccionMinTemperatura;
      $model->CoccionMaxTemperatura = (float)$model->CoccionMaxTemperatura;
      $model->CoccionPresionTemperaturaAlertaNaranja = (float)$model->CoccionPresionTemperaturaAlertaNaranja;
?>



	
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        Parámetros
  </h4>
    </div>
    <div class="panel-body">    
            <div class="framecoccionrpm"><div style="margin-left:230px;">
            <?php echo $form->textFieldGroup($model,'CoccionMinRmp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
        <?php echo $form->textFieldGroup($model,'CoccionMaxRmp',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
        <?php echo $form->textFieldGroup($model,'CoccionRpmAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
</div></div>
        <div class="framecoccionPresioneje"><div style="margin-left:230px;">
            <?php echo $form->textFieldGroup($model,'CoccionMinPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
             <?php echo $form->textFieldGroup($model,'CoccionMaxPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
        <?php echo $form->textFieldGroup($model,'CoccionPresionEjeAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
        <div class="framecoccionPresionchaq"><div style="margin-left:230px;">
             <?php echo $form->textFieldGroup($model,'CoccionMinPresionChaqueta',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            <?php echo $form->textFieldGroup($model,'CoccionMaxPresionChaqueta',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
<?php echo $form->textFieldGroup($model,'CoccionPresionChaquetaAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
                <div class="framecocciontemperatura"><div style="margin-left:230px;">
         <?php echo $form->textFieldGroup($model,'CoccionMinTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
             <?php echo $form->textFieldGroup($model,'CoccionMaxTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
         <?php echo $form->textFieldGroup($model,'CoccionPresionTemperaturaAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div> </div>    
    </div>    
    </div>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $.fn.bootstrapSwitch.defaults.handleWidth = 100;
    
    function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>