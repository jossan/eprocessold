<?php
$this->breadcrumbs=array(
	'Rotadisks',
);

$this->menu=array(
array('label'=>'Crear Rotadisk','url'=>array('create')),
array('label'=>'Administrar Rotadisk','url'=>array('admin')),
);
?>

<h2>Rotadisks</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
