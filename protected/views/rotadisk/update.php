<?php
$this->breadcrumbs=array(
	'Rotatubos'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Rotatubos','url'=>array('admin')),
	array('label'=>'Crear Rotatubo','url'=>array('create')),
	array('label'=>'Ver Rotatubo','url'=>array('view','id'=>$model->ID)),
	);
	?>

	<h3>Actualizar Rotatubo</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
