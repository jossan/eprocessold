<?php

$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Rotatubos'=>array('admin'),
	'Detalles',
);

$this->menu=array(
array('label'=>'Lista de Rotatubos','url'=>array('admin')),
array('label'=>'Crear Rotatubo','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar Rotatubo','url'=>array('update','id'=>$model->ID),'visible'=>$admin || $jefeprod),
array('label'=>'Borrar Rotatubo','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles de Rotatubo</h3>
<?php 
      //1
      $model->PresecadoMinAmperaje = (float)$model->PresecadoMinAmperaje;
      $model->PresecadoMaxAmperaje = (float)$model->PresecadoMaxAmperaje;
      $model->PresecadoAmperajeAlertaNaranja = (float)$model->PresecadoAmperajeAlertaNaranja;
      //2
      $model->PresecadoMinPresionEje = (float)$model->PresecadoMinPresionEje;
      $model->PresecadoMaxPresionEje = (float)$model->PresecadoMaxPresionEje;
      $model->PresecadoPresionEjeAlertaNaranja = (float)$model->PresecadoPresionEjeAlertaNaranja;
      //3
      $model->PresecadoMinPresionChaqueta = (float)$model->PresecadoMinPresionChaqueta;
      $model->PresecadoMaxPresionChaqueta = (float)$model->PresecadoMaxPresionChaqueta;
      $model->PresecadoPresionChaquetaAlertaNaranja = (float)$model->PresecadoPresionChaquetaAlertaNaranja;
      //4
      $model->PresecadoMinSCR = (float)$model->PresecadoMinSCR;
      $model->PresecadoMaxSCR = (float)$model->PresecadoMaxSCR;
      $model->PresecadoSCRAlertaNaranja = (float)$model->PresecadoSCRAlertaNaranja;    
      //5
      $model->PresecadoMinPorcentajeHumedad = (float)$model->PresecadoMinPorcentajeHumedad;
      $model->PresecadoMaxPorcentajeHumedad = (float)$model->PresecadoMaxPorcentajeHumedad;
      $model->PresecadoPorcentajeHumedadAlertaNaranja = (float)$model->PresecadoPorcentajeHumedadAlertaNaranja;
      ?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		array('name'=>'EstadoRotadisk',
                    'value'=>$model->EstadoRotadisk ? "Activo":"Inactivo"
                    )
		//'Estado',
),
)); ?>



<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="presecadoAmp">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMinAmperaje, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxAmperaje, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoAmperajeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="presecadoPresEje">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->PresecadoMinPresionEje, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxPresionEje, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoPresionEjeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        <div class="presecadoTempChq1">
            <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMinTempCha1, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxTempCha1, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoAlertaTempCha1, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        </div>
        
        <div class="presecadoTempChq2">
            
               <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMinTempCha2, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxTempCha2, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoAlertaTempCha2, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        </div>
                <div class="presecadoTempSrc">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMinSCR, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxSCR, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoSCRAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
            
              <div class="presecadoHumedad">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMinPorcentajeHumedad, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoMaxPorcentajeHumedad, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PresecadoPorcentajeHumedadAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
     </form>
    </div>
    
    </div>
