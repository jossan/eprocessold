<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'rotadisk-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

    <?php echo $form->switchGroup($model, 'EstadoRotadisk',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
        'handleWith' => 100,
),
            ))); ?>
<?php 
      //1
      $model->PresecadoMinAmperaje = (float)$model->PresecadoMinAmperaje;
      $model->PresecadoMaxAmperaje = (float)$model->PresecadoMaxAmperaje;
      $model->PresecadoAmperajeAlertaNaranja = (float)$model->PresecadoAmperajeAlertaNaranja;
      //2
      $model->PresecadoMinPresionEje = (float)$model->PresecadoMinPresionEje;
      $model->PresecadoMaxPresionEje = (float)$model->PresecadoMaxPresionEje;
      $model->PresecadoPresionEjeAlertaNaranja = (float)$model->PresecadoPresionEjeAlertaNaranja;
      //3
      $model->PresecadoMinPresionChaqueta = (float)$model->PresecadoMinPresionChaqueta;
      $model->PresecadoMaxPresionChaqueta = (float)$model->PresecadoMaxPresionChaqueta;
      $model->PresecadoPresionChaquetaAlertaNaranja = (float)$model->PresecadoPresionChaquetaAlertaNaranja;
      //4
      $model->PresecadoMinSCR = (float)$model->PresecadoMinSCR;
      $model->PresecadoMaxSCR = (float)$model->PresecadoMaxSCR;
      $model->PresecadoSCRAlertaNaranja = (float)$model->PresecadoSCRAlertaNaranja;    
      //5
      $model->PresecadoMinPorcentajeHumedad = (float)$model->PresecadoMinPorcentajeHumedad;
      $model->PresecadoMaxPorcentajeHumedad = (float)$model->PresecadoMaxPorcentajeHumedad;
      $model->PresecadoPorcentajeHumedadAlertaNaranja = (float)$model->PresecadoPorcentajeHumedadAlertaNaranja;
      ?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        Parámetros
  </h4>
    </div>
    <div class="panel-body">    
            <div class="presecadoAmp"><div style="margin-left:25%;">
           <?php echo $form->textFieldGroup($model,'PresecadoMinAmperaje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoMaxAmperaje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoAmperajeAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
</div></div>
        <div class="presecadoPresEje"><div style="margin-left:25%;">
           <?php echo $form->textFieldGroup($model,'PresecadoMinPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoMaxPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoPresionEjeAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
        <div class="presecadoTempChq1">
            <div style="margin-left:25%;">
                <?php echo $form->textFieldGroup($model,'PresecadoMinTempCha1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoMaxTempCha1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoAlertaTempCha1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            </div>
        </div>
        
        <div class="presecadoTempChq2">
            <div style="margin-left:25%;">
                <?php echo $form->textFieldGroup($model,'PresecadoMinTempCha2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoMaxTempCha2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoAlertaTempCha2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            </div>
        </div>
        
        <div class="presecadoTempSrc">
            <div style="margin-left:25%;">
                <?php echo $form->textFieldGroup($model,'PresecadoMinSCR',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoMaxSCR',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
                <?php echo $form->textFieldGroup($model,'PresecadoSCRAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            </div> 
        </div>    
        
        <div class="presecadoHumedad"><div style="margin-left:25%;">
         <?php echo $form->textFieldGroup($model,'PresecadoMinPorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoMaxPorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'PresecadoPorcentajeHumedadAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div> </div>    
    </div>    
    </div>
	
	
	
	
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    
    
      function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>