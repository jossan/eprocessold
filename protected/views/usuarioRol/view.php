<?php
$this->breadcrumbs=array(
	'Roles de Usuario'=>array('admin'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Roles de Usuario','url'=>array('admin')),
array('label'=>'Crear Usuario Rol','url'=>array('create')),
array('label'=>'Actualizar Usuario Rol','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Usuario Rol','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h2>Ver Usuario Rol #<?php echo $model->ID; ?></h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'NombreRol',
		'Descripcion',
		//'Estado',
),
)); ?>
