<?php
$this->breadcrumbs=array(
	'Roles de Usuario'=>array('admin'),
	$model->ID=>array('view','id'=>$model->ID),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Roles de Usuario','url'=>array('admin')),
	array('label'=>'Crear Usuario Rol','url'=>array('create')),
	array('label'=>'Ver Usuario Rol','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h2>Actualizar Usuario Rol <?php echo $model->ID; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>