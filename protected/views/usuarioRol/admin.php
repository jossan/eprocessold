<?php
$this->breadcrumbs=array(
	'Roles de Usuario'=>array('admin'),
	'Administración',
);

$this->menu=array(

);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('usuario-rol-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Usuario Roles</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'usuario-rol-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'ID',
		'NombreRol',
		'Descripcion',
		//'Estado',

),
)); ?>
