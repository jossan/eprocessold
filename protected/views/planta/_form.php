<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'planta-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

<?php echo $form->switchGroup($model, 'EstadoPlanta',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
        'handleWith' => 100,
),
            ))); ?>
<?php 
      //1
      $model->TratamientoMinAlimentacionAguaColaTemperatura = (float)$model->TratamientoMinAlimentacionAguaColaTemperatura;
      $model->TratamientoMaxAlimentacionAguaColaTemperatura = (float)$model->TratamientoMaxAlimentacionAguaColaTemperatura;
      $model->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja = (float)$model->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja;
      //2
      $model->TratamientoMinAlimentacionAguaColaPorcentajeSolidos = (float)$model->TratamientoMinAlimentacionAguaColaPorcentajeSolidos;
      $model->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos = (float)$model->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos;
      $model->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja = (float)$model->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja;
      //3
      $model->TratamientoMinVahTemperatura = (float)$model->TratamientoMinVahTemperatura;
      $model->TratamientoMaxVahTemperatura = (float)$model->TratamientoMaxVahTemperatura;
      $model->TratamientoVahTemperaturaAlertaNaranja = (float)$model->TratamientoVahTemperaturaAlertaNaranja;
      //4
      $model->TratamientoMinVacPeso = (float)$model->TratamientoMinVacPeso;
      $model->TratamientoMaxVacPeso = (float)$model->TratamientoMaxVacPeso;
      $model->TratamientoVacPesoAlertaNaranja = (float)$model->TratamientoVacPesoAlertaNaranja;      
      //5
      $model->TratamientoMinTemperatura1 = (float)$model->TratamientoMinTemperatura1;
      $model->TratamientoMaxTemperatura1 = (float)$model->TratamientoMaxTemperatura1;
      $model->TratamientoTemperatura1AlertaNaranja = (float)$model->TratamientoTemperatura1AlertaNaranja;      
      //6
      $model->TratamientoMinTemperatura2 = (float)$model->TratamientoMinTemperatura2;
      $model->TratamientoMaxTemperatura2 = (float)$model->TratamientoMaxTemperatura2;
      $model->TratamientoTemperatura2AlertaNaranja = (float)$model->TratamientoTemperatura2AlertaNaranja;      
      //7
      $model->TratamientoMinTemperatura3 = (float)$model->TratamientoMinTemperatura3;
      $model->TratamientoMaxTemperatura3 = (float)$model->TratamientoMaxTemperatura3;
      $model->TratamientoTemperatura3AlertaNaranja = (float)$model->TratamientoTemperatura3AlertaNaranja;      
      //8
      $model->TratamientoMinSalidaPorcentajeSolidos = (float)$model->TratamientoMinSalidaPorcentajeSolidos;
      $model->TratamientoMaxSalidaPorcentajeSolidos = (float)$model->TratamientoMaxSalidaPorcentajeSolidos;
      $model->TratamientoSalidaPorcentajeSolidosAlertaNaranja = (float)$model->TratamientoSalidaPorcentajeSolidosAlertaNaranja;      
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        Parámetros
  </h4>
    </div>
    <div class="panel-body">    
            <div class="tratamientoAlimTem">
            <div style="margin-left:230px;">
        <!-- 1 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinAlimentacionAguaColaTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxAlimentacionAguaColaTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
</div></div>
        <div class="tratamientoAlimSol"><div style="margin-left:230px;">
        <!-- 2 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinAlimentacionAguaColaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxAlimentacionAguaColaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>  
        <div class="tratamientoVahTemp"><div style="margin-left:230px;">
        <!-- 3 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinVahTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxVahTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoVahTemperaturaAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>  
        <div class="tratamientoVacPeso"><div style="margin-left:230px;">
        <!-- 4 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinVacPeso',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxVacPeso',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoVacPesoAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div>  </div>  
        <div class="tratamientoTemperatura1"><div style="margin-left:230px;">
        <!-- 5 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinTemperatura1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxTemperatura1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoTemperatura1AlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div>  </div>  
        
         <div class="tratamientoTemperatura2"><div style="margin-left:230px;">
        <!-- 6 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinTemperatura2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxTemperatura2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoTemperatura2AlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div>  </div>  
         <div class="tratamientoTemperatura3"><div style="margin-left:230px;">
        <!-- 7 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinTemperatura3',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxTemperatura3',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoTemperatura3AlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div> </div>   
         <div class="tratamientoSalSol"><div style="margin-left:230px;">
        <!-- 8 -->
        <?php echo $form->textFieldGroup($model,'TratamientoMinSalidaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoMaxSalidaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'TratamientoSalidaPorcentajeSolidosAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div>  </div>  
    </div>    
    </div>
	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>