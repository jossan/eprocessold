<?php
$this->breadcrumbs=array(
	'Plantas Evaporadoras'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de planta concentrado','url'=>array('admin')),
	array('label'=>'Crear planta concentrado','url'=>array('create')),
	array('label'=>'Ver planta concentrado','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar planta concentrado</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
