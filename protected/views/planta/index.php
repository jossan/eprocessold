<?php
$this->breadcrumbs=array(
	'Plantas',
);

$this->menu=array(
array('label'=>'Crear planta concentrado','url'=>array('create')),
array('label'=>'Administrar planta concentrado','url'=>array('admin')),
);
?>

<h2>Plantas</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
