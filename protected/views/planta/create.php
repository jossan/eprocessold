<?php
$this->breadcrumbs=array(
	'Plantas Evaporadoras'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de plantas','url'=>array('admin')),

);
?>

<h3>Crear planta concentrado</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
