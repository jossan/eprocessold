<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Centrifugadores'=>array('index')
);

$this->menu=array(
array('label'=>'Lista de planta concentrado','url'=>array('admin')),
array('label'=>'Crear planta concentrado','url'=>array('create'),'url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar planta concentrado','url'=>array('update','id'=>$model->ID),'url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Eliminar planta concentrado','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'url'=>array('create'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles de planta concentrado</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
                'Descripcion',
                array('name'=>'EstadoPlanta',
                    'value'=>$model->EstadoPlanta ? "Activo":"Inactivo"
                    ),
		
		//'Estado',
),
)); ?>
<?php 
      //1
      $model->TratamientoMinAlimentacionAguaColaTemperatura = (float)$model->TratamientoMinAlimentacionAguaColaTemperatura;
      $model->TratamientoMaxAlimentacionAguaColaTemperatura = (float)$model->TratamientoMaxAlimentacionAguaColaTemperatura;
      $model->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja = (float)$model->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja;
      //2
      $model->TratamientoMinAlimentacionAguaColaPorcentajeSolidos = (float)$model->TratamientoMinAlimentacionAguaColaPorcentajeSolidos;
      $model->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos = (float)$model->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos;
      $model->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja = (float)$model->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja;
      //3
      $model->TratamientoMinVahTemperatura = (float)$model->TratamientoMinVahTemperatura;
      $model->TratamientoMaxVahTemperatura = (float)$model->TratamientoMaxVahTemperatura;
      $model->TratamientoVahTemperaturaAlertaNaranja = (float)$model->TratamientoVahTemperaturaAlertaNaranja;
      //4
      $model->TratamientoMinVacPeso = (float)$model->TratamientoMinVacPeso;
      $model->TratamientoMaxVacPeso = (float)$model->TratamientoMaxVacPeso;
      $model->TratamientoVacPesoAlertaNaranja = (float)$model->TratamientoVacPesoAlertaNaranja;      
      //5
      $model->TratamientoMinTemperatura1 = (float)$model->TratamientoMinTemperatura1;
      $model->TratamientoMaxTemperatura1 = (float)$model->TratamientoMaxTemperatura1;
      $model->TratamientoTemperatura1AlertaNaranja = (float)$model->TratamientoTemperatura1AlertaNaranja;      
      //6
      $model->TratamientoMinTemperatura2 = (float)$model->TratamientoMinTemperatura2;
      $model->TratamientoMaxTemperatura2 = (float)$model->TratamientoMaxTemperatura2;
      $model->TratamientoTemperatura2AlertaNaranja = (float)$model->TratamientoTemperatura2AlertaNaranja;      
      //7
      $model->TratamientoMinTemperatura3 = (float)$model->TratamientoMinTemperatura3;
      $model->TratamientoMaxTemperatura3 = (float)$model->TratamientoMaxTemperatura3;
      $model->TratamientoTemperatura3AlertaNaranja = (float)$model->TratamientoTemperatura3AlertaNaranja;      
      //8
      $model->TratamientoMinSalidaPorcentajeSolidos = (float)$model->TratamientoMinSalidaPorcentajeSolidos;
      $model->TratamientoMaxSalidaPorcentajeSolidos = (float)$model->TratamientoMaxSalidaPorcentajeSolidos;
      $model->TratamientoSalidaPorcentajeSolidosAlertaNaranja = (float)$model->TratamientoSalidaPorcentajeSolidosAlertaNaranja;      
?>
<br>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="tratamientoAlimTem">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinAlimentacionAguaColaTemperatura, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxAlimentacionAguaColaTemperatura, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="tratamientoAlimSol">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->TratamientoMinAlimentacionAguaColaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        <div class="tratamientoVahTemp">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinVahTemperatura, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxVahTemperatura, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoVahTemperaturaAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
             
        </div>
                <div class="tratamientoVacPeso">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinVacPeso, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxVacPeso, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoVacPesoAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
            
            <div class="tratamientoTemperatura1">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinTemperatura1, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxTemperatura1, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoTemperatura1AlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
            
            <div class="tratamientoTemperatura2">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinTemperatura2, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxTemperatura2, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoTemperatura2AlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
            
            <div class="tratamientoTemperatura3">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinTemperatura3, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxTemperatura3, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoTemperatura3AlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
            
            <div class="tratamientoSalSol">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMinSalidaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoMaxSalidaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->TratamientoSalidaPorcentajeSolidosAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
     </form>
    </div>
    
    </div>