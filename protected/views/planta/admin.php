<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Centrifugadoras'=>array('admin'),
	'Lista',
);

$this->menu=array(

array('label'=>'Crear planta concentrado','url'=>array('create'),'visible'=>$admin || $jefeprod),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('planta-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Plantas concentrado</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'planta-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
                array('name'=>'EstadoPlanta',
                    'value'=>'$data->EstadoPlanta? "Activo":"Inactivo"'
                    ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
 'template'=>'{view} {update} {delete}',
    'buttons'=>array(
        'update'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
        'delete'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
    )
),
),
)); ?>
