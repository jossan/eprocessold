<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Nosotros';
$this->breadcrumbs=array(
	'Nosotros',
);
?>

<style type="text/css">
#mision {
       border: 2px solid #a1a1a1;
    padding: 10px 40px; 
    background: #dddddd;
    width: 300px;
    border-radius: 25px;
    
}

#vision {
       border: 2px solid #a1a1a1;
    padding: 10px 40px; 
    background: #dddddd;
    width: 300px;
    border-radius: 25px;
    
}

#ambiente {
       border: 2px solid #a1a1a1;
    padding: 10px 40px; 
    background: #dddddd;
    width: 290px;
    border-radius: 25px;
    
}

div {
    
}
</style>
<center>
    <h2>TADEL S. A.</h2>
<h4>Procesadora de Harina de Pescado secada al vapor y Aceite de Pescado. </h4>

<i>Posee una amplia experiencia en la elaboración de harina de pescado secada al vapor y aceites de pescado. La harina y los aceites de pescado, por ser fuentes nutricionales y energéticas, son direccionadas a la industria del balanceado alimenticio animal bajo las normas internacionales. </i><br>

<br>
<TABLE>

<TD >
<div id="mision">
<b>MISIÓN </b><br><p align="justify">Producir harina de pescado y derivados secados al vapor, bajo altos estándares de calidad, seguridad y cuidado ambiental; con Ia participación activa y profesional de sus trabajadores, que satisfaga las exigencias del mercado nacional e internacional.</p>    
</div>
</TD>

<TD>
<div id="vision">
<b>VISIÓN</b><br><p align="justify"> Ser una empresa líder en el mercado, cumpliendo las leyes y reglamentos. Utilizar tecnología de punta para propiciar el crecimiento técnico de trabajadores. Innovar permanentemente y satisfacerlos requerimientos del mercado.</p>    
</div>
</TD>

<TD>
<div id="ambiente">
<b>COMPROMISO AMBIENTAL</b><br><p align="justify">Comprometidos con el medio ambiente, TADEL, las aguas de limpieza de proceso las somete a tratamientos para ser utilizadas en arborizar áreas aledañas a nuestras instalaciones, aportando armonía y oxígeno al ambiente.</p>    
</div>
</TD>


</TABLE>

<br>



<br>


<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3391.894644083437!2d-80.63557627894775!3d-0.9737854262173624!3m2!1i1024!2i768!4f13.1!5e1!3m2!1ses-419!2sus!4v1425272584965" width="450" height="180" frameborder="0" style="border:0"></iframe>
<br>
<b>Dirección</b>
Km 9 1/2, vía Manta- Rocafuerte, calles 13 y 14 , Edf. Depto. 3 Of. 302 - Jaramijo
<br>
<b>Telfs.</b> (05) 262-3420 

</center>