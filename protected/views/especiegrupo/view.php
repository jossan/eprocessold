<?php
/* @var $this EspeciegrupoController */
/* @var $model Especiegrupo */

$this->breadcrumbs=array(
	'Grupo de especie'=>array('index'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Lista de grupos de especia','url'=>array('admin')),
array('label'=>'Crear grupo de especie','url'=>array('create')),
array('label'=>'Actualizar grupo de especie','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar grupo de especie','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h3>Detalles</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		
		'Nombre',
		'Especie',
		
	),
)); ?>
