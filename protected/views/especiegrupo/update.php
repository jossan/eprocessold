<?php
/* @var $this EspeciegrupoController */
/* @var $model Especiegrupo */

$this->breadcrumbs=array(
	'Grupos de especies'=>array('admin'),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Lista grupos de especies','url'=>array('admin')),
	array('label'=>'Crear grupo de especie','url'=>array('create')),
	array('label'=>'Ver grupo de especie','url'=>array('view','id'=>$model->ID)),
);
?>

<h3>Actualizar grupo de especie</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>