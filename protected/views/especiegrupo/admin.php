<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear grupo de especie','url'=>array('create')),
);
?>
<h2>Grupos de especies</h2>



<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'especiegrupo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
	'columns'=>array(
		
		'Nombre',
		
		
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
