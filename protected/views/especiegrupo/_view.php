<?php
/* @var $this EspeciegrupoController */
/* @var $data Especiegrupo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombre')); ?>:</b>
	<?php echo CHtml::encode($data->Nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Especie')); ?>:</b>
	<?php echo CHtml::encode($data->Especie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('velocidad')); ?>:</b>
	<?php echo CHtml::encode($data->velocidad); ?>
	<br />


</div>