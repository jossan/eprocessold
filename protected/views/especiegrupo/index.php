<?php
/* @var $this EspeciegrupoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Especiegrupos',
);

$this->menu=array(
	array('label'=>'Create Especiegrupo', 'url'=>array('create')),
	array('label'=>'Manage Especiegrupo', 'url'=>array('admin')),
);
?>

<h1>Especiegrupos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
