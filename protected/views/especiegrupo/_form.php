<?php
/* @var $this EspeciegrupoController */
/* @var $model Especiegrupo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'especiegrupo-form',
    'type'=>'horizontal',
			
)); ?>

	<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	
	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->