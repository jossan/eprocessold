<?php
/* @var $this EspeciegrupoController */
/* @var $model Especiegrupo */

$this->breadcrumbs=array(
	'Grupos de especies'=>array('admin'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Lista grupos de especies','url'=>array('admin')),
);
?>

<h2>Crear grupo de especie</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>