<?php
$this->breadcrumbs=array(
	'Prensas'=>array('admin'),
	'Nueva',
);

$this->menu=array(
array('label'=>'Lista de Prensas','url'=>array('admin')),

);
?>

<h3>Nueva Prensa</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
