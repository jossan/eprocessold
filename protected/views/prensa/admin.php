<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Prensas'=>array('admin'),
	'Lista',
);

$this->menu=array(

array('label'=>'Crear Prensa','url'=>array('create'),'visible'=>$admin || $jefeprod),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('prensa-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Prensas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'prensa-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		array('name'=>'EstadoPrensa',
                    'value'=>'$data->EstadoPrensa?"Activo":"Inactivo"'
                    ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{view} {update} {delete}',
    'buttons'=>array(
        'update'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
        'delete'=>array(
            'visible'=>'Yii::app()->user->idrol == 7 || Yii::app()->user->idrol == 1'
        ),
    )
),
),
)); ?>
