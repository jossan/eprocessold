<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Prensas'=>array('admin'),
	$model->Nombre,
);

$this->menu=array(
array('label'=>'Lista de Prensas','url'=>array('admin')),
array('label'=>'Crear Prensa','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar Prensa','url'=>array('update','id'=>$model->ID),'visible'=>$admin || $jefeprod),
array('label'=>'Borrar Prensa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles de prensa</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		array('name'=>'EstadoPrensa',
                    'value'=>$model->EstadoPrensa?'Activo':'Inactivo'
                    )
		//'Estado',
),
)); ?>
<?php 
      $model->PrensadoMinAmperaje = (float)$model->PrensadoMinAmperaje;
      $model->PrensadoMaxAmperaje = (float)$model->PrensadoMaxAmperaje;
      $model->PrensadoAmperajeAlertaNaranja = (float)$model->PrensadoAmperajeAlertaNaranja;
      $model->PrensadoMinPorcentajeHumedad = (float)$model->PrensadoMinPorcentajeHumedad;
      $model->PrensadoMaxPorcentajeHumedad = (float)$model->PrensadoMaxPorcentajeHumedad;
      $model->PrensadoPorcentajeHumedadAlertaNaranja = (float)$model->PrensadoPorcentajeHumedadAlertaNaranja;
?>


<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="prensaamp">
                
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PrensadoMinAmperaje, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PrensadoMaxAmperaje, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PrensadoAmperajeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="PrensadoH">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->PrensadoMinPorcentajeHumedad, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PrensadoMaxPorcentajeHumedad, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->PrensadoPorcentajeHumedadAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        
              
     </form>
    </div>
    
    </div>
