<?php
$this->breadcrumbs=array(
	'Prensas',
);

$this->menu=array(
array('label'=>'Crear Prensa','url'=>array('create')),
array('label'=>'Administrar Prensa','url'=>array('admin')),
);
?>

<h2>Prensas</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
