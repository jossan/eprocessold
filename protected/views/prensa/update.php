<?php
$this->breadcrumbs=array(
	'Prensas'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Prensas','url'=>array('admin')),
	array('label'=>'Crear Prensa','url'=>array('create')),
	array('label'=>'Ver Prensa','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Prensa</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
