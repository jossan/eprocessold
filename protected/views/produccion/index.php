<?php
/* @var $this ProduccionController */

$this->breadcrumbs=array(
	'Produccion',
);

$producciones = Produccion::model()->findAll('Estado = 1 order by ID');
$dias=['Domingo','Lunes','Martes','Mercoles','Jueves','Viernes','Sabado','Domingo'];
?>
<script src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="js/numeric-input-example.js"></script>
<link href="css/index.css" rel="stylesheet">
<style type="text/css">
    tbody th {
        font-weight: normal;
    }
</style>
<h3>Control de Producción</h3>
<div style="text-align: right;display: block;margin-top: 12px;">
    <p>Fecha: <input type="text" id="datepicker" style="height: 22px;width: 100px;text-align: center;display: inline;">
    <?php print CHtml::link('Ir', 'index.php?r=/control/index&fecha='.date('Y-m-d'), array('class' => 'btn btn-success','style'=>'padding:1px 5px;','id'=>'ir'));?></p>
</div>
<table class="table table-bordered table-striped" id='tabla'>
    <thead>
        <tr>
            <th>Fecha Recepción</th>
            <th>Secuencia</th>
            <th>Descripción</th>
            <th>Peso Neto(T)</th>
            <th>Peso Producido(T)</th>
            <th>Fecha Produccion</th>
        </tr>
    </thead>
    <tbody class='small'>
        <?php foreach($producciones as $produccion ):?>
        <tr>
        <th><?php print $dias[date('w',strtotime($produccion->recep->Fecha))].', '.$produccion->recep->Fecha.' a las '.substr($produccion->recep->Hora, 0,5);?></th>
        <th><?php print $produccion->recep->Secuencia;?></th>
        <th><?php print 'Asignada en la <strong>'.$produccion->recep->tolva->Nombre.'</strong>: '.$produccion->recep->EspecieID;?></th>
        <th><?php print $produccion->recep->Pesoneto;?></th>
        <td><?php print $produccion->PesoProduccion;?></td>
        <th><input type="text" /><?php //print $produccion->Status;?></th>
        </tr>
        <?php endforeach;  ?>
    </tbody>
</table> 

<script type="text/javascript">
    $('#tabla').editableTableWidget().numericInputExample().find('td:first').focus();
</script>