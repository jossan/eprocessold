<?php
$this->breadcrumbs=array(
	'Especies'=>array('admin'),
	
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Especies','url'=>array('admin')),
	array('label'=>'Crear Especie','url'=>array('create')),
	array('label'=>'Ver Especie','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar especie</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
