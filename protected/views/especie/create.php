<?php
$this->breadcrumbs=array(
	'Especies'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Especies','url'=>array('admin')),

);
?>

<h2>Crear Especie</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
