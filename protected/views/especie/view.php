<?php
$this->breadcrumbs=array(
	'Especies'=>array('admin'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Lista de Especies','url'=>array('admin')),
array('label'=>'Crear Especie','url'=>array('create')),
array('label'=>'Actualizar Especie','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Especie','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h3>Detalles</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
//		'Estado',
),
)); ?>
