<?php
$this->breadcrumbs=array(
	'Especies',
);

$this->menu=array(
array('label'=>'Crear Especie','url'=>array('create')),
array('label'=>'Administrar Especie','url'=>array('admin')),
);
?>

<h2>Especies</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
