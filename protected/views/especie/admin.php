<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Especie','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('especie-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Especies</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'especie-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
	//	'ID',
		'Nombre',
		'Descripcion',
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
