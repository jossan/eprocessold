<?php
$this->breadcrumbs=array(
	'Decanter'=>array('admin'),
	'Nuevo',
);

$this->menu=array(
array('label'=>'Lista de decanter','url'=>array('admin')),

);
?>

<h3>Nuevo decanter</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
