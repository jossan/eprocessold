<?php
$this->breadcrumbs=array(
	'Decanter'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de decanter','url'=>array('admin')),
	array('label'=>'Crear decanter','url'=>array('create')),
	array('label'=>'Ver decanter','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar decanter</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
