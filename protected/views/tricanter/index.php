<?php
$this->breadcrumbs=array(
	'Decanter',
);

$this->menu=array(
array('label'=>'Crear decanter','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Administrar decanter','url'=>array('admin'),'visible'=>$admin || $jefeprod),
);
?>

<h2>Decanter</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
