<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tricanter-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

        <?php echo $form->switchGroup($model, 'EstadoTricanter',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
),
            ))); ?>

<?php 
      $model->DecantacionMinAlimentacionTemperatura = (float)$model->DecantacionMinAlimentacionTemperatura;
      $model->DecantacionMaxAlimentacionTemperatura = (float)$model->DecantacionMaxAlimentacionTemperatura;
      $model->DecantacionAlimentacionTemperaturaAlertaNaranja = (float)$model->DecantacionAlimentacionTemperaturaAlertaNaranja;
      /*$model->DecantacionMinAlimentacionPorcentajeHumedad = (float)$model->DecantacionMinAlimentacionPorcentajeHumedad;
      $model->DecantacionMaxAlimentacionPorcentajeHumedad = (float)$model->DecantacionMaxAlimentacionPorcentajeHumedad;
      $model->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja = (float)$model->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja;*/
      $model->DecantacionMinAguaColaPorcentajeSolidos = (float)$model->DecantacionMinAguaColaPorcentajeSolidos;
      $model->DecantacionMaxAguaColaPorcentajeSolidos = (float)$model->DecantacionMaxAguaColaPorcentajeSolidos;
      $model->DecantacionAguaColaPorcentajeSolidosAlertaNaranja = (float)$model->DecantacionAguaColaPorcentajeSolidosAlertaNaranja;
      /*$model->DecantacionMinAguaColaPorcentajeGrasas = (float)$model->DecantacionMinAguaColaPorcentajeGrasas;
      $model->DecantacionMaxAguaColaPorcentajeGrasas = (float)$model->DecantacionMaxAguaColaPorcentajeGrasas;
      $model->DecantacionAguaColaPorcentajeGrasasAlertaNaranja = (float)$model->DecantacionAguaColaPorcentajeGrasasAlertaNaranja;      */
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <div class="decantasionTem">
            <div style="margin-left:230px;">
            <?php echo $form->textFieldGroup($model,'DecantacionMinAlimentacionTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            <?php echo $form->textFieldGroup($model,'DecantacionMaxAlimentacionTemperatura',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            <?php echo $form->textFieldGroup($model,'DecantacionAlimentacionTemperaturaAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
            </div>
        </div>
     
 <?php /*
        <div class="decantasionHum"><div style="margin-left:230px;">
       
            <?php echo $form->textFieldGroup($model,'DecantacionMinAlimentacionPorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionMaxAlimentacionPorcentajeHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionAlimentacionPorcentajeHumedadAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
*/?>      
        <div class="decantasionaguaS"><div style="margin-left:230px;">
             <?php echo $form->textFieldGroup($model,'DecantacionMinAguaColaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionMaxAguaColaPorcentajeSolidos',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionAguaColaPorcentajeSolidosAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
<?php /*       
                <div class="decantasionaguaG"><div style="margin-left:230px;">
        
           <?php echo $form->textFieldGroup($model,'DecantacionMinAguaColaPorcentajeGrasas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionMaxAguaColaPorcentajeGrasas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'DecantacionAguaColaPorcentajeGrasasAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
*/?>  
    </div>
    
    </div>
	
	
	
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>