<?php
$this->breadcrumbs=array(
	'Parametroses'=>array('index'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Parametros','url'=>array('index')),
array('label'=>'Administrar Parametros','url'=>array('admin')),
);
?>

<h2>Crear Parametros</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>