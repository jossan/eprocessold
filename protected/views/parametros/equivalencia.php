<?php

$this->menu=array(
array('label'=>'Lista de Parametros','url'=>array('index')),
array('label'=>'Crear Parametros','url'=>array('create')),
array('label'=>'Administrar Parametros','url'=>array('admin')),
);
?>

<h3>Tabla de asociación de especies</h3>
<div class="text-right">
<button class="btn btn-link" onclick="habilitar()">Nuevo Grupo</button>
</div>
<div class="row" id="grupocrear" style="display:none; margin-bottom: 15px;">
    <?= CHtml::beginForm(); ?>
    <div class="col-sm-8"></div>
    <div class="col-sm-3">
        <?= CHtml::activeTextField($model,'Nombre' , ['class'=>'form-control','autocomplete'=>'off']) ?>
    </div>
    <div class="col-sm-1">
        <?= CHtml::submitButton('Crear', ['class'=>'btn - btn-success']); ?>
    </div>
    <?= CHtml::endForm(); ?>
    </div>

<?php 

$id =1;
$grupos = Especiegrupo::model()->findAll(
array("condition"=>"velocidad =  $id"));

/*$sql= "select * from especie where Estado=1";
$especie = Yii::app()->db->createCommand($sql)->queryAll();*/
$especie = Especie::model()->findAll('Estado=1');

?>
<table class='items table table-striped table-bordered table-condensed'> 
    <thead>
    <tr>
        <th>GRUPOS >></th>
    <?php foreach($grupos as $grupo):?>
        <th style="font-size:12px;" >
            <?php print $grupo->Nombre; 
                        $grupojson[] =$grupo->Nombre;
            ?>
        </th>
    <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
<?php foreach($especie as $row): ?>
        
        <?php $value = $row['Nombre'];?>
        <?php $id_especie = $row['ID'];?>
        <tr>
            <td >
                <?php print $value?>
            </td>
<?php foreach ($grupos as $grupo): ?>
            <?php 
            $clase='btn btn-default';
            if($row->grupoID==$grupo->ID):
                $clase='btn btn-success';
            endif;
            
            ?>
            <td style='text-align: center;'>
                <button style="width: 100%; height: 100%;margin: 0px;" class="<?php print $clase;?>" especie='<?= $row->ID ;?>' grupo="<?php print $grupo->ID?>"></button>
            </td>
       <?php endforeach; ?>
    </tr>
    
<?php endforeach; ?>
    <tbody>
</table>
<div style='text-align: right;margin-top: 10px;'>
    <button id='guardar' class='btn btn-primary' >GUARDAR </button>
</div>

<script type="text/javascript">
    var grupos = <?php print json_encode($grupojson);?>;
    var valorgrupo = new Array();
    var val ='';
    $('table .btn').click(function(){
        var index =$(this).parent().index()-1;
        //$('table').find('tr').each(function(){ $(this).find('button:eq('+index+')').attr('class','btn btn-default') ;});
        $(this).parent().parent().find('.btn').attr('class','btn btn-default');
        $(this).attr('class','btn btn-success');
    });
    $('#guardar').click(function(){
        valorgrupo = new Array();
        $('table').find('.btn-success').each(
                function(index,valor){
                    valorgrupo.push({especie:$(valor).attr('especie'),grupo:$(valor).attr('grupo')});
                });
        $.ajax({
            url:"index.php?r=/parametros/guardarequivalencias",
            type: 'GET',
            data:{'grupos':valorgrupo},
        }).done(function(){
            $.notify("Actualizado Correctamente ", "success");
        });
    });
    var estado = true;
    function habilitar(){
        if(estado){
            $('#grupocrear').show('show');
            $('#grupocrear input').focus();
        }
        else{
            $('#grupocrear').hide('show');
        }
        estado = !estado;
    }
</script>
    
