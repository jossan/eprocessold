<?php 
    $decimal = $this->decimal(); 
?>



<h3>Configuración del sistema</h3>

<hr>
<div class="row">
<div class="col-md-6">
<form enctype="multipart/form-data" action="index.php?r=parametros/sistema" method="POST" class="form-vertical  ">
    <div class="form-group">
        <input type="hidden" name="tipologo" value="sistema" />
        <label class="control-label">Cambiar logo del sistema</label>
        <input name="uploadedfile" type="file" required="required   " />
        <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
    </div>
    <button type="submit" class="btn btn-primary">
        Subir y cambiar Logo
    </button>
</form> 
</div>
<div class="col-md-2 text-center">
    <label>Logo actual</label>
    <?= file_exists('images/brand.png') ?  
        CHtml::image('images/brand.png', "LOGO",['style'=>'width: 100%;']) :
        (   file_exists('images/brand.jpg') ? 
                        CHtml::image('images/brand.jpg','LOGO',['style'=>'width: 100%']) : 
            file_exists('images/brand.jpeg') ?
                        CHtml::image('images/brand.jpeg','LOGO',['style'=>'width: 100%']):""
        ) ?>
</div>
<div class="col-md-4"></div>
</div>
<hr>

<div class="row">
<div class="col-md-6">
<form enctype="multipart/form-data" action="index.php?r=parametros/sistema" method="POST" class="form-vertical  ">
    <div class="form-group">
        <input type="hidden" name="tipologo" value="pdf" />
        <label class="control-label">Cambiar logo para informes</label>
        <input name="uploadedfile" type="file" required="required   " />
        <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
    </div>
    <button type="submit" class="btn btn-primary">
        Subir y cambiar Logo
    </button>
</form> 
</div>
<div class="col-md-2 text-center">
    <label>Logo pdf actual</label>
    <?= file_exists('images/logopdf.png') ?  
        CHtml::image('images/logopdf.png', "LOGO",['style'=>'width: 100%;']) :
        (   file_exists('images/logopdf.jpg') ? 
                        CHtml::image('images/logopdf.jpg','LOGO',['style'=>'width: 100%']) : 
            file_exists('images/logopdf.jpeg') ?
                        CHtml::image('images/logopdf.jpeg','LOGO',['style'=>'width: 100%']):""
        ) ?>
</div>
<div class="col-md-4"></div>
</div>
<hr>

<div class="row">
<div class="col-md-8">
<form action="index.php?r=parametros/sistema" method="POST" class="form-vertical">
    <div class="form-group">
        <label class="control-label">Nombre de la empresa</label>
        <input name="empresa" type="text" required="required" class="form-control" MAXLENGTH=200 value="<?= $empresa?>"/>
    </div>
    <button type="submit" class="btn btn-primary">
        Cambiar nombre
    </button>
</form> 
</div>
</div>

<hr>

<form class="form-horizontal" action="index.php/parametros/decimal" method="get">
            <label class="control-label">Separador decimal</label>
            <input type="hidden" name="r" value="parametros/decimal">
            <div class="signo">
            <div class="form-group" style="margin: 0px;padding: 0px;">
                <label class="col-sm-3 control-label required">
                    <?php echo CHtml::encode('Signo'); ?>:
                </label>
                <div class="col-sm-2">
                    <input type="radio" value="p" name="signos"  <?php print $decimal=='.' ? 'checked' : '' ;?>> Punto " <span style="font-size: 20px;">.</span> "   <br>
                    <input type="radio" value="c" name="signos" <?php print $decimal==',' ? 'checked' : '' ;?>> Coma  " <span style="font-size: 20px;">,</span> " 
                </div>
            </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar separador decimal','context' => 'primary','buttonType'=>'submit','htmlOptions'=>array('id'=>'LicorG')));?>
        </div>
</form>

<HR>
<script type="text/javascript">
    <?= $estado==1 ? "location.reload(true);" : ""?>
</script>

