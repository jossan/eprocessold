<?php 
$decimal = $this->decimal() 
?>
<h3 style="text-align: center">Parámetros humedad</h3>
<fieldset>
<legend>Valor Objetivo</legend>    
<?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
    <div class="panel-group" id="accordion">
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
    Anual
    </a>
    </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
    <div class="panel-body">
     <form class="form-horizontal" >
            <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa2',(float)$valora2, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala2')));?>
            </div>
            </div>
        
    </div>    
              <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa1',(float)$valora1, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala1')));?>
            </div>
            </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($actual); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioactual',(float)$valora, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anual')));?>
            </div>
        </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof1',(float)$valorf1, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf1')));?>
            </div>
            </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
                        <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof2',(float)$valorf2, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf2')));?>
            </div>
        </div>
                        
    </div>    
         <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
                        <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof3); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof3',(float)$valorf3, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf3')));?>
            </div>
        </div>
        
    </div>    
        </form>
    </div>
    </div>
    </div>
    
    </div>
    <?php $this->endWidget(); ?>
</fieldset>

<!-- Valor de base-->
<fieldset>
<legend>Período Base</legend>    
<?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
    <div class="panel-group" id="accordion">
        
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseone">
    Anual
    </a>
    </h4>
    </div>
    <div id="collapseone" class="panel-collapse collapse">
    <div class="panel-body">
     <form class="form-horizontal" >
            <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa2pbH',(float)$valora2pbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala2pbH')));?>
            </div>
            
            </div>
        
    </div>    
              <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa1pbH',(float)$valora1pbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala1pbH')));?>
            </div>
        </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($actual); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioactualpbH',(float)$valorapbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualpbH')));?>
            </div>
        </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof1pbH',(float)$valorf1pbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
                <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf1pbH')));?>
            </div>
        </div>
        
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
                        <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof2pbH',(float)$valorf2pbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf2pbH')));?>
            </div>
        </div>
        
    </div>    
             <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
                        <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof3); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof3pbH',(float)$valorf3pbH, array('maxlength'=>8, 'class'=>'form-control')); ?>
            </div>
            <div class="col-sm-2">
               <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf3pbH')));?>
            </div>
        </div>
        
    </div> 
        </form>
    </div>
    </div>
    </div>
    
    </div>
    <?php $this->endWidget(); ?>
</fieldset>
<script type="text/javascript">
    var DECIMAL = '<?= $decimal ?>';
$('#anual').click(function(){
    var anioactual = $('#anioactual').val();
    anioactual = anioactual.replace('.','');
    anioactual = anioactual.replace(',','.');
        $.ajax({
            url:'index.php?r=/Parametros/anualH',
            type:'GET',
            data: {'anioactual':anioactual},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            }
        });
});   
$('#anuala1').click(function(){
     var anioa1 = $('#anioa1').val();
    anioa1 = anioa1.replace('.','');
    anioa1 = anioa1.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anuala1H',
            type:'GET',
            data: {'anioa1':anioa1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anuala2').click(function(){
        var anioa2 = $('#anioa2').val();
        anioa2 = anioa2.replace('.','');
        anioa2 = anioa2.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anuala2H',
            type:'GET',
            data: {'anioa2':anioa2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf1').click(function(){
    
    var aniof1 = $('#aniof1').val();
    aniof1 = aniof1.replace('.','');
    aniof1 = aniof1.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anualf1H',
            type:'GET',
            data: {'aniof1':aniof1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf2').click(function(){
    
    var aniof2 = $('#aniof2').val();
    aniof2 = aniof2.replace('.','');
    aniof2 = aniof2.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anualf2H',
            type:'GET',
            data: {'aniof2':aniof2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf3').click(function(){
    
    var aniof3 = $('#aniof3').val();
    aniof3 = aniof3.replace('.','');
    aniof3 = aniof3.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anualf3H',
            type:'GET',
            data: {'aniof3':aniof3},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

//Periodo Base
$('#anualpbH').click(function(){
    var anioactual = $('#anioactualpbH').val();
    anioactual = anioactual.replace('.','');
    anioactual = anioactual.replace(',','.');
        $.ajax({
            url:'index.php?r=/Parametros/anualpbH',
            type:'GET',
            data: {'anioactual':anioactual},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});   
$('#anuala1pbH').click(function(){
     var anioa1 = $('#anioa1pbH').val();
    anioa1 = anioa1.replace('.','');
    anioa1 = anioa1.replace(',','.');
 
    $.ajax({
            url:'index.php?r=/Parametros/anuala1pbH',
            type:'GET',
            data: {'anioa1':anioa1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anuala2pbH').click(function(){
        var anioa2 = $('#anioa2pbH').val();
 anioa2 = anioa2.replace('.','');
        anioa2 = anioa2.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anuala2pbH',
            type:'GET',
            data: {'anioa2':anioa2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf1pbH').click(function(){
    
    var aniof1 = $('#aniof1pbH').val();
    aniof1 = aniof1.replace('.','');
    aniof1 = aniof1.replace(',','.');
    $.ajax({
            url:'index.php?r=/Parametros/anualf1pbH',
            type:'GET',
            data: {'aniof1':aniof1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf2pbH').click(function(){
    
    var aniof2 = $('#aniof2pbH').val();
    aniof2 = aniof2.replace('.','');
    aniof2 = aniof2.replace(',','.');
 
    $.ajax({
            url:'index.php?r=/Parametros/anualf2pbH',
            type:'GET',
            data: {'aniof2':aniof2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#anualf3pbH').click(function(){
    
    var aniof3 = $('#aniof3pbH').val();
    aniof3 = aniof3.replace('.','');
    aniof3 = aniof3.replace(',','.');
 
    $.ajax({
            url:'index.php?r=/Parametros/anualf3pbH',
            type:'GET',
            data: {'aniof3':aniof3},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

</script>
<script src="js/parametros.js" type="text/javascript"></script>