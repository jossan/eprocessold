<?php $decimal = $this->decimal() ?>
<script type="text/javascript">
    function justNumbers(e)
            {
          tecla = (document.all) ? e.keyCode : e.which;
   	if (tecla==46) return true; 
        if (tecla==44) return true;
//        if (tecla==110) return true; 
        if (tecla>=00 && tecla <31) return true;
         
   	 patron =/\d/;
   	 te = String.fromCharCode(tecla);
   	 return patron.test(te);
            }
</script>
<div id="mensaje_signo" style="color:green">
<?= isset($_GET['mensaje']) ? $_GET['mensaje'].'<br>' : '' ?><br>
</div>
<div class="view">
    <?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
    <div class="panel-group" id="accordion">
    
    <!--Aceite-->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#aceite" class="btn">
    ACEITE
    </a>
    </h4>
    </div>
    <div id="aceite" class="panel-collapse collapse">
    <div class="panel-body">
        <form class="form-horizontal" >
            <div class="aceiteD">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aceite1',$data->densidad_aceite, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
                         <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'aceiteDb')));?>
        </div>
         
        </form>
    </div>
    </div>
    </div>
    <!--Licor-->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#licor" class="btn">
    LICOR
    </a>
    </h4>
    </div>
    <div id="licor" class="panel-collapse collapse">
    <div class="panel-body">
        <form class="form-horizontal" >
            <div class="licorS">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor1',$data->LicorMinPorcentajeSolidos, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor2',$data->LicorMaxPorcentajeSolidos, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
                
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor3',$data->LicorPorcentajeSolidosAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorS')));?>
        </div>
<?php /*
            <div class="licorG">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor5',$data->LicorMinPorcentajeGrasas, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor6',$data->LicorMaxPorcentajeGrasas, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor7',$data->LicorPorcentajeGrasasAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
                          <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorG')));?>
        </div>
            */ ?>
        </form>
    </div>
    </div>
    </div>
    
    <!-- Harina -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#harina" class="btn">
    HARINA
    </a>
    </h4>
    </div>
        <div id="harina" class="panel-collapse collapse">
    <div class="panel-body">
        <form class="form-horizontal" >
            <div class="harinatemp">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har1',$data->HarinaMinTemperatura, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har2',$data->HarinaMaxTemperatura, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har3',$data->HarinaTemperaturaAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'HarinaTem')));?>
        </div>
        <div class="harinahum">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har4',$data->HarinaMinHumedad, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har5',$data->HarinaMaxHumedad, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har6',$data->HarinaHumedadAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'HarinaHum')));?>
        </div>
            <div class="harinaao">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har7',$data->HarinappmMin, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har8',$data->HarinappmMax, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har9',$data->HarinappmAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'HarinaAo')));?>
        </div>
            <div class="harinaprot">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har10',$data->HarinaprotMin, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har11',$data->HarinaprotMax, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har12',$data->HarinaprotAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'HarinaProt')));?>
        </div>
                                <div class="harinaTBVN">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har13',$data->HarinaTBVNmin, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har14',$data->HarinaTBVNmax, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('har15',$data->HarinaTBVNAlertaNaranja, array('class'=>"form-control",'maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'HarinaTBVN')));?>
        </div>
        </form>
    </div>
    </div>
        
    </div>
    <!-- cocinador -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=cocina/admin" class="btn btn-link">
            COCINADOR
        </a>
    </h4>
    </div>
    </div>
    <!-- prensa -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=prensa/admin" class="btn bn-link" >
            PRENSA
        </a>
    </h4>
    </div>
    </div>
    <!-- tricanter -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=tricanter/admin" class="btn bn-link" >
            DECANTER
        </a>
    </h4>
    </div>
    </div>
    <!-- Pulidora -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=pulidora/admin" class="btn bn-link" >
            PULIDORA
        </a>
    </h4>
    </div>
    </div>
    <!-- planta -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=planta/admin" class="btn btn-link">
            PLANTA
        </a>
    </h4>
    </div>
    </div>
    <!-- rotadisk -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=rotadisk/admin" class="btn btn-link">
            ROTATUBO
        </a>
    </h4>
    </div>
    </div>
    <!-- rotatubo -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        <a href="index.php?r=rotatubo/admin" class="btn btn-link">
            FUEGO DIRECTO
        </a>
    </h4>
    </div>
    </div>
    
    
    
    </div>
    <?php $this->endWidget(); ?>

</div>
<script type="text/javascript">
 
 var DECIMAL = '<?= $decimal ?>';
$('#LicorS').click(function(){
    var c1 = $('#licor1').val();
    var c2 = $('#licor2').val();
    var c3 = $('#licor3').val();
    c1 = c1.replace(DECIMAL,'.');
    c2 = c2.replace(DECIMAL,'.');
    c3 = c3.replace(DECIMAL,'.');
    $.ajax({
            url:'index.php?r=/Parametros/licorS',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
                
            },
        });
});
$('#aceiteDb').click(function(){
    var c1 = $('#aceite1').val();
    
    c1 = c1.replace(DECIMAL,'.');

    $.ajax({
            url:'index.php?r=/Parametros/aceiteD',
            type:'GET',
            data: {'c1':c1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
                
            },
        });
});

$('#LicorG').click(function(){
    var c1 = $('#licor5').val();
    var c2 = $('#licor6').val();
    var c3 = $('#licor7').val();
    c1 = c1.replace(DECIMAL,'.');
    c2 = c2.replace(DECIMAL,'.');
    c3 = c3.replace(DECIMAL,'.');
    $.ajax({
            url:'index.php?r=/Parametros/licorG',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#HarinaTem').click(function (){
    var h1 = $('#har1').val();
    var h2 = $('#har2').val();
    var h3 = $('#har3').val();
    peticionHarina(h1,h2,h3,1);
});
$('#HarinaHum').click(function (){
    var h1 = $('#har4').val();
    var h2 = $('#har5').val();
    var h3 = $('#har6').val();
    peticionHarina(h1,h2,h3,2);
});
$('#HarinaAo').click(function (){
    var h1 = $('#har7').val();
    var h2 = $('#har8').val();
    var h3 = $('#har9').val();
    peticionHarina(h1,h2,h3,3);
});
$('#HarinaProt').click(function (){
    var h1 = $('#har10').val();
    var h2 = $('#har11').val();
    var h3 = $('#har12').val();
    peticionHarina(h1,h2,h3,4);
});
$('#HarinaTBVN').click(function (){
    var h1 = $('#har13').val();
    var h2 = $('#har14').val();
    var h3 = $('#har15').val();
    peticionHarina(h1,h2,h3,5);
});
function peticionHarina(h1,h2,h3,item){
    h1 = h1.replace(DECIMAL,'.');
    h2 = h2.replace(DECIMAL,'.');
    h3 = h3.replace(DECIMAL,'.');
    $.ajax({
            url:'index.php?r=/Parametros/harina',
            type:'GET',
            data: {'h1':h1,'h2':h2,'h3':h3,'item':item},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
}

function verificar(valor){
    
    /* resultado 0 -> es falso entonces significa q ocurrio un error
         * resultado 1 -> es verdadero entonces significa que si guardo */
    if(valor==1){
        js:$.notify("Actualizado Correctamente", "success");//el valor es 1
    }else{
        js:$.notify("Error al Actualizar o sin cambios", "error")//el valor es 0
    }
}
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
$('form input[type=text]').each(function(){
    if(this.id!="aceite1"){
    this.value=formato_numero(this.value,1,DECIMAL,'');}else{
    this.value=formato_numero(this.value,3,DECIMAL,'');
    }
});
$('form input[type=text]').keyup(function(){
    var valor = $(this).val();
    valor = valor.replace(DECIMAL,'.');
    if(Number(valor))
        $(this).parent().parent().attr('class','form-group has-success');
    else
        $(this).parent().parent().attr('class','form-group has-error');

    });
$('form input[type=text]').blur(function(){
    var valor = $(this).val();
    valor = valor.replace(DECIMAL,'.');
    $(this).parent().parent().attr('class','form-group');
    if(this.id!="aceite1"){
    $(this).val(formato_numero(valor,1,DECIMAL,''));}else{
    $(this).val(formato_numero(valor,3,DECIMAL,''));
    }
});
</script>
