<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(
array('label'=>'Crear Clientes','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('clientes-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Clientes</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'clientes-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'Id',
		'Nombre',
                'Email',
                'Telefono',
		'Descripcion',
                array('name'=>'Exportacion',
                    'value'=>'$data->Exportacion?"Exportación":"Local"',
					'filter'=>array(0=>'Local',1=>'Exportación'),
                    ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{update} {delete}'
),
),
)); ?>
