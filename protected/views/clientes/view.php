<?php
$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	'Actualizar',
);

$this->menu=array(
array('label'=>'Nuevo Cliente','url'=>array('create')),
array('label'=>'Actualizar Cliente','url'=>array('update','id'=>$model->Id)),
array('label'=>'Borrar Cliente','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Esta seguro que desea borrarlo?')),
array('label'=>'Lista de Clientes','url'=>array('admin')),
);
?>

<h2>Detalles de cliente</h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'Nombre',
                'Email',
            'Telefono',
            array('name'=>'Exportacion',
                'value'=>$model->Exportacion ? 'Exportación':'Local',
                ),
            array('name'=>'Descripcion',),
	
),
)); ?>
