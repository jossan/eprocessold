<?php
$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Clientes','url'=>array('admin')),
);
?>

<h2>Crear Clientes</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>