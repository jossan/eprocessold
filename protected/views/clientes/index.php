<?php
$this->breadcrumbs=array(
	'Clientes',
);

$this->menu=array(
array('label'=>'Crear Clientes','url'=>array('create')),
array('label'=>'Administrar Clientes','url'=>array('admin')),
);
?>

<h2>Clientes</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
