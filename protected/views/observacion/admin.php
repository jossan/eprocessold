<?php
$this->breadcrumbs=array(
	'Observaciones'=>array('admin'),
	'Lista',
);

$this->menu=array(
array('label'=>'Crear Observación','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('observacion-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Observaciones</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'observacion-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		'Nombre',
		'Descripcion',
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{update} {delete}'
),
),
)); ?>
