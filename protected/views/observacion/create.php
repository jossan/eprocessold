<?php
$this->breadcrumbs=array(
	'Observaciones'=>array('admin'),
	'Nueva',
);

$this->menu=array(
array('label'=>'Lista de Observaciones','url'=>array('admin')),
);
?>

<h3>Nueva Observación</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>