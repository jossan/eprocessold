<?php
$this->breadcrumbs=array(
	'Observaciones'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Crear Observacion','url'=>array('create')),
	array('label'=>'Lista Observacion','url'=>array('admin')),
	);
	?>

	<h3>Actualizar Observación</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>