<?php
$this->breadcrumbs=array(
	'Observacions'=>array('index'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Crear Observación','url'=>array('create')),
array('label'=>'Actualizar Observación','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Observación','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),
array('label'=>'Lista Observaciones','url'=>array('admin')),
);
?>

<h3>Detalles</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'Nombre',
		'Descripcion',
),
)); ?>
