<?php
$this->breadcrumbs=array(
	'Observacions',
);

$this->menu=array(
array('label'=>'Crear Observacion','url'=>array('create')),
array('label'=>'Administrar Observacion','url'=>array('admin')),
);
?>

<h2>Observacions</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
