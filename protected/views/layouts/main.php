<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language;?>">
<meta charset="utf-8">
<head>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta charset="<?php echo Yii::app()->charset;?>">
<link rel="shortcut icon" type="image/x-icon" href="images/logoico.ico">
<link href="css/main.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){
        $('.glyphicon-asterisk').parent().parent().find('ul').attr('class','dropdown-menu pull-right nav')
    });
</script>
</head>
<body>
    
<header>
    <?php
    $derecha = []; $items=[];
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$consulta = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
$operador = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 2) ? true : false ;
$mantenimiento = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 5) ? true : false ;
$bascula = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 3) ? true : false ;
$calidad = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 6) ? true : false ;
if(!Yii::app()->user->isGuest){
    
$nombre = Usuarios::model()->find('ID='.Yii::app()->user->Id);
$valor = $nombre->NombreCompleto;
}else{
    $valor = 'Opciones';
}

if(Yii::app()->user->isGuest){
    array_push($derecha, array('label'=>'Ingresar', 'url'=>array('/site/login'),'icon' => 'log-in',));
}else{
    array_push($items, array('label'=>'Inicio', 'url'=>array('/site/index'), 'icon' => 'home',));
 if($admin || $jefeprod){
     array_push($items, 
             array(
    'label' => 'Producción',
    'url' => '#',
        'icon' => 'cog',
    
    'items' => array(
    array('label'=>'Control de producción diaria', 'url'=>array('/control')),
    array('label'=>'Control de producción semanal', 'url'=>array('/controlSemanal')),
    )
    ));
     
     
 }
 
 if($bascula || $operador){
     array_push($items, 
             array(
    'label' => 'Producción',
    'url' => '#',
        'icon' => 'cog',
    
    'items' => array(
    array('label'=>'Control de producción diaria', 'url'=>array('/control')),
    
    )
    ));
 }
 if($calidad){
     array_push($items, 
             array(
    'label' => 'Producción',
    'url' => '#',
        'icon' => 'cog',
    
    'items' => array(
    array('label'=>'Control de producción semanal', 'url'=>array('/controlSemanal')),
    
    )
    ));
 }
 
 if($admin || $jefeprod || $consulta){
     array_push($items, array(
    'label' => 'Informes',
    'url' => '#',
        'icon' => 'list-alt',
    
    'items' => array(
        array('label'=>'Proceso de producción', 'url'=>array('/control/produccionRango')),
    '---',    
        array('label'=>'Producción semanal', 'url'=>array('/controlSemanal/produccionSemana')),
        /*array('label'=>'Producción semanal por cliente', 'url'=>array('/controlSemanal/produccionSemanaCliente')),*/
        array('label'=>'Rendimiento semanal', 'url'=>array('/controlSemanal/grafico')),
    '---',
        array('label'=>'Eficiencia planta', 'url'=>array('/controlSemanal/eficienciaPlanta')),
        array('label'=>'Composición de la materia prima', 'url'=>array('/controlSemanal/compMateriaPrimaProcesada')),
        array('label'=>'Consumo de Bunker', 'url'=>array('/controlSemanal/ratioconsumo')),
        array('label'=>'Variación de la humedad', 'url'=>array('/controlSemanal/variacionHumedad')),
   '---',    
        array('label'=>'Volumen de producción', 'url'=>array('/controlSemanal/final')),
    '---',
               
                    array(
                        'itemOptions' =>   array('class' => 'dropdown-submenu'),
                        'url' => '#',
                        'label' => 'Informes unificados (L1, L2)',
                        'items' => 
                            array(
                                array('label' => 'Sacos producidos', 'url' =>array( '/controlSemanal/unificadosacos')),
                            )
)
    )
    ));
 }
 
 if($admin || $jefeprod){
     array_push($items, array(
    'label' => 'Parámetros',
    'url' => '#'
        ,'icon' => 'tasks',
    'items' => array(
    array('label'=>'Parámetros', 'url'=>array('/parametros/')),
        '---',
        array('label'=>'Parámetros de rendimiento', 'url'=>array('/parametros/admin')),
        array('label'=>'Parámetro de eficiencia planta', 'url'=>array('/parametros/objetivoTVC')),
        array('label'=>'Parámetros de galones bunker', 'url'=>array('/parametros/objetivoGal')),
        array('label'=>'Parámetros de humedad', 'url'=>array('/parametros/objetivoHum')),
        
        
    '---',
    array('label'=>'Tolva', 'url'=>array('/tolva/admin')),
    
        '---',
    array('label'=>'Proveedores', 'url'=>array('/proveedores/admin')),
    array('label'=>'Clientes', 'url'=>array('/clientes/admin')),
        '---',
    array('label'=>'Clasificación de harinas', 'url'=>array('/clasificacion/admin')),
    array('label'=>'Observaciones', 'url'=>array('/observacion/admin')),
        '---',
        array('label'=>'Tabla de asociación de especies', 'url'=>array('/parametros/equivalencia')),
        array('label'=>'Especies', 'url'=>array('/especie/index')),
        array('label'=>'Grupos de especies', 'url'=>array('/especiegrupo/index')),
        '---',
        array('label'=>'Configuración del sistema', 'url'=>array('/parametros/sistema'),'visible'=>$admin),
    )
    ),

            array(
    'label' => 'Equipos',
    'url' => '#',
                
                'icon' => 'wrench',
    'items' => array(
        array('label'=>'Cocinador', 'url'=>array('/cocina/admin')),
        array('label'=>'Prensa', 'url'=>array('/prensa/admin')),
        array('label'=>'Decanter', 'url'=>array('/tricanter/admin')),
        array('label'=>'Centrifugas', 'url'=>array('/pulidora/admin')),
        array('label'=>'Planta concentrado', 'url'=>array('/planta/admin')),
        array('label'=>'Rotatubo', 'url'=>array('/rotadisk/admin')),
        array('label'=>'Fuego directo', 'url'=>array('/rotatubo/admin')),
        ))
     );
     
     
 }
 
 if($mantenimiento){
     array_push($items,   array(
    'label' => 'Equipos',
    'url' => '#',
                
                'icon' => 'wrench',
    'items' => array(
        array('label'=>'Cocinador', 'url'=>array('/cocina/admin')),
        array('label'=>'Prensa', 'url'=>array('/prensa/admin')),
        array('label'=>'Decanter', 'url'=>array('/tricanter/admin')),
        array('label'=>'Centrifugado', 'url'=>array('/pulidora/admin')),
        array('label'=>'Planta evaporadora', 'url'=>array('/planta/admin')),
        array('label'=>'Rotatubo', 'url'=>array('/rotadisk/admin')),
        array('label'=>'Fuego directo', 'url'=>array('/rotatubo/admin')),
        )));
 }
 
 
 
 if($admin){
     array_push($derecha, array(
    'label' => 'Usuarios',
    'url' => '#'
        
        ,'visible'=>$admin ,'icon' => 'user',
    'items' => array(
    array('label' => 'Crear usuarios', 'url' =>array( '/usuarios/create')),
    array('label' => 'Lista usuarios', 'url' => array('/usuarios/admin')),
        '---',
    
        array('label' => 'Lista  de rol de usuarios', 'url' => array('/usuarioRol/admin')),
    )
    ) );

 }
 array_push($derecha, array(
    'label' =>$valor,
    'url' => '#',
    'icon' => 'asterisk',
    'items' => array(
        array('label'=>'Editar perfil', 'url'=>array('/usuarios/update&id='.Yii::app()->user->Id)),
        array('label'=>'Cambiar contraseña', 'url'=>array('/usuarios/Contrasenia&id='.Yii::app()->user->Id)),
        '---',
        array('label'=>'Cerrar sesión('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest,'icon' => 'off',),
        )));
}

$this->widget(
    'booster.widgets.TbNavbar',
    array(
    'type' => null, // null or 'inverse'
    'brand' => file_exists('images/brand.png') ?  CHtml::image('images/brand.png', "LOGO",['style'=>'height: 100%;']) : (file_exists('images/brand.jpg') ? CHtml::image('images/brand.jpg','LOGO',['style'=>'height: 100%']) : (file_exists('images/brand.jpeg') ? CHtml::image('images/brand.jpeg','LOGO',['style'=>'height: 100%']) : "")) ,
    'brandUrl' => '#',
    'brandOptions'=>array('style'=>'padding:1px;vertical-align: middle;'),
    'collapse' => true, // requires bootstrap-responsive.css
    'fixed' => false,
    'fluid' => true,
    'items' => array(
    array(
    'class' => 'booster.widgets.TbMenu',
    'submenuHtmlOptions' => array('class' => 'multi-level'),
    'type' => 'navbar',
    'items' => $items
        
        ),

    array(
    'class' => 'booster.widgets.TbMenu',
    'type' => 'navbar',
    'htmlOptions' => array('class' => 'pull-right'),
    'items' => $derecha
    ),
    ),

    )
    );

    
?>
    </header>

    

<div class="container" id="page" style='width: 94%;'>
    <?php $L = Yii::app()->params['linea'];?>
    <span  title="Linea de producción #<?= $L; ?>" class="badge" style="position: absolute;top: 55px; left: 2px;background-color: #428bca;">L<?=$L ?></span>
 <?php if(isset($this->breadcrumbs)):?>
 <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
 'links'=>$this->breadcrumbs,
 )); ?>
 <?php endif?>
 <?php echo $content; ?>
 <hr>

</div><!-- page -->
 <footer>
 Copyright &copy; <?php echo date('Y'); ?> | <?php echo 'ResulTec' ?> - Derechos reservados.<br/>
 </footer>
</body>
</html>
