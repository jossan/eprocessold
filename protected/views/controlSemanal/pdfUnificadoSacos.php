<?php
$tl1 = count($ml1);
$tl2 = count($ml2);
if($tl1!=$tl2){
    return;
}
?>
<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
    font-weight: bold;
    color:white;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 font-size: 9px;
 }
 .center{
     text-align: center;
 }
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="25%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 15px;">Sacos Producidos | Unificado </span></td>
<td width="25%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?><br> <b>Hasta: </b><?php echo $f2; ?> </td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
    
<table  class="items" width="100%" style="margin-top:0px;font-size: 8pt; border-collapse: collapse;" cellpadding="4">
    <thead>
        <tr style="background-color: #555;">
            <td width="100px;" >Fecha</td>
            <td width="100px;" >Día</td>
            <td width="100px;" >Semana</td>
            <td width="100px;" >Sacos Linea 1</td>
            <td width="100px;" >Sacos Linea 2</td>
            <td>Total Sacos (L1+L2)</td>
        </tr>
    </thead>
    <tbody>
        <?php for( $i = 0 ;  $i<$tl1 ; $i++ ): ?>
            <?php $l1 = $ml1[$i]; ?>
            <?php $l2 = $ml2[$i]; ?>
            <?php if($l1['dia']=='DO'): ?>
                <tr style="background-color: #DDD;">
                    <td align="center" ><?= $l1['fecha'];?> </td>
                    <td align="center" ><?= $l1['dia'];?></td>
                    <td align="center" ><?= $l1['semana'];?></td>
                    <?php $sql="select sum(sacosproducidos) from periodo where semana = ".$l1['semana']." and Anio = ".$l1['anio']; ?>
                    <td ><strong><?= $do_l1 = Yii::app()->db->createCommand($sql)->queryScalar(); ?></strong></td>
                    <td ><strong><?= $do_l2 = Yii::app()->db2->createCommand($sql)->queryScalar(); ?></strong></td>
                    <td ><strong><?= ((int)$do_l1)+((int)$do_l2) ?></strong></td>
                </tr>
            <?php else: ?>
                <tr>

                    <td align="center" ><?= $l1['fecha'];?> </td>
                    <td align="center" ><?= $l1['dia'];?></td>
                    <td align="center" ><?= $l1['semana'];?></td>
                    <td ><?= $l1['sacos'];?></td>
                    <td ><?= $l2['sacos'];?></td>
                    <td ><?= ((int)$l1['sacos']) + ((int) $l2['sacos']);?></td>
                </tr>
            <?php endif; ?>
        <?php endfor; ?>
                <tr style="background-color: #333;">
                    <td colspan="3" style="color:white;" >Total filtro:</td>
                    <?php $sql="select sum(sacosproducidos) from periodo where fecha between '$f1' and '$f2' "; ?>
                    <td style="color:white;" ><strong><?= $to_l1 = Yii::app()->db->createCommand($sql)->queryScalar(); ?></strong></td>
                    <td style="color:white;" ><strong><?= $to_l2 = Yii::app()->db2->createCommand($sql)->queryScalar(); ?></strong></td>
                    <td style="color:white;" ><strong><?= ((int)$to_l1)+((int)$to_l2) ?></strong></td>
                </tr>
    </tbody>
</table>
            
</body>
</html>