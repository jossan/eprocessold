<form action="index.php" method="get">
    <input type="hidden" name="r" value="controlSemanal/unificadosacos">
<fieldset>
    <legend>Sacos producidos | <span style="color:#000077">Unificados</span></legend>
<table style='width:100%'>
    <tr>
        <td style='width:20%'>
            Fecha Inicio<br>
            <?php

            $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'f1',
    'value'=>$f1,
    'options' => array(
    'language' => 'es',
     'format'=>'yyyy-mm-dd'
    )
    )
    );
?>
        </td>
        <td style='width:20%'>

            Fecha Fin<br>
     <?php
        $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'f2',
    'value'=>$f2,
    'options' => array(
    'language' => 'es',
        'format'=>'yyyy-mm-dd'
    )
    )
    );?>
        </td>
        <td>
            <br>
            <?php
            
            echo CHtml::button('Actualizar',array('type'=>'submit','class'=>'btn btn-info'));
            ?>
        </td><td style="text-align: right;">
            <?= CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/pdf_icon.png","Descargar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/unificadosacospdf', array('f1'=>$f1,'f2'=>$f2,'d'=>1))
        );?>&nbsp;&nbsp;
        <?= CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel_ico.png","Descarga reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/unificadosacosexcel', array('f1'=>$f1,'f2'=>$f2))
        );?>
        </td>
    </tr>
</table>

<iframe src="index.php?r=/controlSemanal/unificadosacospdf<?= '&f1='.$f1.'&f2='.$f2 ?>" width="100%" height="550px;">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
</form>