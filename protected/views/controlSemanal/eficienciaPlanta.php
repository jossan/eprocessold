<?php $decimal =$this->decimal(); ?>
<link rel="stylesheet" href="archivos/jquery-ui.css">
<script src="archivos/jquery-ui.js"></script>
<link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="archivos/rgbcolor.js"></script>
<script type="text/javascript" src="archivos/StackBlur.js"></script>
<script type="text/javascript" src="archivos/canvg.js"></script>
<!-- Hightchart Js -->
<script src="archivos/highcharts.js"></script>

<!-- GRIDVIEW -->
    <link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
    <script src="js/gridviewScroll.min.js" type="text/javascript"></script>

<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="archivos/highslide-full.min.js"></script>
<script type="text/javascript" src="archivos/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="archivos/highslide.css" />

<style type="text/css">
     #page{
        width:100% !important;
    }
        body {font-family: sans-serif;
 
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
    padding: 2px !important;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
   padding: 2px !important;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>

<?php
?>
<div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;
 $t = count($sql2)+1;
       $p = (100/$t)."%";
?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/eficienciaPlanta',array('id'=>'btnact','class'=>'btn btn-info btn-sm'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimgeficienciaPlanta',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank','style'=>'display:none'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button class="btn btn-info btn-sm" onclick="imapdf()">Generar PDF</button>
    
<h3 style="text-align:center">TVC: Eficiencia Planta</h3>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	
</div>

<!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>

<br>
<center>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1" colspan="<?php echo $t;?>">EFICIENCIA PLANTA</td></tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left " width="<?php echo $p; ?>">Semana</td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Año</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px; "width="<?php echo $p; ?>"><?php print $row['anio']?></td>
<?php endforeach; ?>
  </tr>
<tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Inicio</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px;" width="<?php echo $p; ?>">
<?php 
    print date('d-m', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>">Tiempo</td>
  <?php foreach($sqlT as $row):?>
<td style="font-size:11px;"width="<?php echo $p;?>" ><?php print $row['T'].'%';?></td>
<?php endforeach; ?>
  </tr>
    <tr >
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Velocidad</td>
  <?php foreach($sqlV as $row):?>
<td style="font-size:11px;"width="<?php echo $p;?>" ><?php print $row['V'].'%';?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Calidad</td>
  <?php foreach($sqlC as $row):?>
    <td style="font-size:11px;"width="<?php echo $p;?>" ><?php print $row['C'].'%';?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >TVC</td>
<?php  for ($i = 0; $i < count($sqlT1); $i++):?>
  <td style="font-size:11px;"width="<?php echo $p;?>" ><?php
  $Vcita =isset($sqlV1[$i]['V'])? (float)$sqlV1[$i]['V']:0;
  print round((((float)$sqlC1[$i]['C'])*$Vcita*((float)$sqlT1[$i]['T']))*100).'%';?></td>

<?php endfor; ?>
  </tr>
</table>
</center>

<script type="text/javascript">
    $(document).ready(function(){
       $("#btnact").click(function (){
            var anio =$('#anios').val();
            var anio1 =$('#anios1').val();
            var mes1 =$('#meses1').val();
            var mes2 =$('#meses2').val();
            var enlace=  'index.php?r=/controlSemanal/eficienciaPlanta';
            enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
            $(this).attr('href',enlace);
        } ); 
    });
window.chart = new Highcharts.Chart({

    chart: {
        renderTo: 'container',
        
          },
    
    title: {
        text:null
            
    },
    
    xAxis: {
        labels: {
                    formatter: function () {
                var semana=0;
                var cant = this.value;
                if((cant).length>5){
                semana=(cant).substring(0,2);}
                else if((cant).length>0){
                    semana=(cant).substring(0,1);
                }else{
                    semana = "x";
                }
                 
                 s=   'S'+semana;
             
                                  
                           return s;
            }
                
            },
            categories: <?php echo json_encode($arraySemana);?>
    },
    
    yAxis: {
        max: 100,
tickInterval:10,
min: 0,
       labels: {
                format: '{value} %',
                
            },
            title: {
                text: '% de cumplimiento'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
    },

   legend: {
            
            borderWidth: 1
        },
    tooltip: {
            valueSuffix: '%'  ,shared: true,
            formatter: function () {
                                var semana=0;
                var cant = this.x;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                var s = '<b>Semana ' + semana + '</b><br>';
<?php 
foreach ($sql2 as $row) {
    $fech= date('d-m-Y', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));
    print "if(".$row['Semana'].$row['anio']." ==  this.x){"
    . "s+='<b>Fecha Inicio:</b>'+'".$fech.
    "';}else{s+=''}";}
    ?>
                $.each(this.points, function () {
                    s += '<br/><span><b>' + this.series.name + ':</b> ' +
                        this.y + '%</span>';
                });

                return s;
            },
            crosshairs: true
        },

    series: [ {
            name: 'Tiempo',
            type: 'spline',
            data: <?php echo json_encode($T);?>,
            dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        }, {
            name: 'Velocidad',
            type: 'spline',
            data: <?php echo json_encode($V);?>,
            dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        }, 
        {
            name: 'Calidad',
            type: 'spline',
            data:  <?php echo json_encode($C);?>,
           dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        },{
            name: 'TVC',
            data: <?php echo json_encode($TVC);?>,
             
                dataLabels: {
                    enabled: true,
                     format: '{y} %'
                }},{
            name: 'Objetivo',
            data: <?php echo json_encode($arrayObjetivoPlanta);?>,
            color: '#00970A',          marker: {
                enabled: false
            }
        }
    ]
});
        function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
            var link = document.getElementById('btnpdf');
            $(link).attr('href','index.php?r=/controlSemanal/guardarimgeficienciaPlanta&filename='+data+'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2);
            document.getElementById('btnpdf').click();
          }
        });
    }

  function gridviewScroll() {
            gridView1 = $('#tabla').gridviewScroll({
                width:1000,
                height:450,
                freezesize: 1,
                arrowsize: 30,
                varrowtopimg: "images/arrowvt.png",
                varrowbottomimg: "images/arrowvb.png",
                harrowleftimg: "images/arrowhl.png",
                harrowrightimg: "images/arrowhr.png",
                headerrowcount: 1,
                railsize: 15,
                barsize: 8
            });
        }
</script>