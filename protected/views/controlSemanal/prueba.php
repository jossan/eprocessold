<html>
<head>
    <style type="text/css">
        body {font-family: sans-serif;
 
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
<?php $t = count($sql)+1;
       $p = (100/$t)."%";
?>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table widtd="100%"><tr>
 <td widtd="50%" style="color:#0000BB;"><img src="<?= $url_img?>" height="35px;" ></td>
<td widtd="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 16pt;">Rendimiento Semanal</span></td>
<td widtd="50%" style="text-align: right;"><b>Desde: </b><?php echo $fechaini; ?>, <b>Hasta: </b><?php echo $fechafin; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<setdtmlpageheader name="myheader" value="on" show-tdis-page="1" />
<setdtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<br>
<img src="<?php echo $filename ?>" />
<br>
<br>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1" colspan="<?php echo $t;?>">DATOS</td></tr>
    <tr>
    <td style="background: #E1E7E1" width="<?php echo $p; ?>">Semana</td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1" width="<?php echo $p;?>">Rendimiento</td>
  <?php foreach($sql as $row):?>
<td width="<?php echo $p;?>" ><?php print $row['Valor'];?></td>
<?php endforeach; ?>
  </tr>
  <tr style="background: #75E375">
  <td style="background: #E1E7E1" width="<?php echo $p;?>" >Objetivo</td>
  <?php foreach($sqlobjetivo as $row):?>
<td width="<?php echo $p;?>"><?php print $row['Objetivo'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1" width="<?php echo $p;?>" >Sacos Producidos</td>
  <?php foreach($sql4 as $row):?>
    <td width="<?php echo $p;?>" ><?php print $row['SumaSacos'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1" width="<?php echo $p;?>" >Materia Prima Procesada</td>
  <?php foreach($sql3 as $row):?>
<td width="<?php echo $p;?>" ><?php print $row['SumaProcesada'];?></td>
<?php endforeach; ?>
  </tr>
</table>

</body>
 </html>



