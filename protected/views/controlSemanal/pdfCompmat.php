<html>
<head>
<?php 
$decimal =$this->decimal();

       
$ArrayTable = Array();
foreach($grupos as $grupo){
    $sql ="select 
    Semana,
    Anio, 
    ROUND(IFNULL(
        (
        select IFNULL(sum(pesoneto),0) from recepcion join especie on especie.id = recepcion.EspecieID where yearweek(fechaproduccion,3) = concat(Anio, if(Semana<10, concat('0',Semana), Semana)) and GrupoID = $grupo->ID 
    ) / sum(periodo.mpprocesada), 0 )*100,1) as valor,
    sum(periodo.mpprocesada) total
    from periodo 
    where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin
    group by Anio, Semana
    order by Anio, Semana";
    $sqlFechaIni = "select min(Fecha) from periodo where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin group by Anio , Semana order by Anio, Semana";
    $ArrayFechaIni = Yii::app()->db->createCommand($sqlFechaIni)->queryColumn();
    $result = Yii::app()->db->createCommand($sql)->queryAll();
    $ArrayTable[$grupo->ID] = $result;
}
    ?>
<style type="text/css">
    body {font-family: sans-serif;
 
 }
        td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>

</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%" ><tr>
 <td width="20%" style="color:#0000BB;"><img src="<?= $url_img?>" height="35px;" ></td>
<td width="60%" style="text-align: center;"><span style="font-weight: bold; font-size: 14px;">Composición de la materia prima</span></td>
<td width="20%" style="text-align: right;"></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>-->

<img src="<?php echo $filename ?>"/>
<table id="tabla" class="items table table-bordered" style="border-collapse: collapse;">
        <tr>
            <td style="background: #E1E7E1;" >Semana</td>
            <?php foreach($semanas as $sem):?>
            <td style="background: #E1E7E1;" ><?= $sem['Semana'] ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <td style="background: #E1E7E1;" >Año</td>
            <?php foreach($semanas as $sem):?>
            <td  ><?= $sem['Y'] ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <td style="background: #E1E7E1;" >Inicio</td>
            <?php foreach($ArrayFechaIni as $fechaini):
                ?>
            
            <td  ><?= substr($fechaini, 5) ?></td>
            <?php endforeach;?>
        </tr>
        
    <?php 
    foreach($grupos as $grupo):
        $fila = $ArrayTable[$grupo->ID]
        ?>
        <tr>
            <td style="background: #E1E7E1;" ><?= $grupo->Nombre ?> </td>
        <?php foreach ($fila as $campo):?>
            <td><?= $campo['total'] ?> </td>
        <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
        <tr><td colspan="<?= $totalSemana+1 ?>" style="text-align: center;background: #E1E7E1;">PORCENTAJES DE MATERIA PRIMA PROCESADA</td></tr>
        <?php 
    foreach($grupos as $grupo):
        $fila = $ArrayTable[$grupo->ID]
        ?>
        <tr>
            <td style="background: #E1E7E1;" ><?= $grupo->Nombre ?> </td>
        <?php foreach ($fila as $campo):?>
            <td><?= $campo['valor'] ?>%</td>
        <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
    </table> 

</body>
 </html>