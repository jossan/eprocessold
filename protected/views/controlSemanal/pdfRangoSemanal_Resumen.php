<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    $countmpro = 0;
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="33%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="33%" style="text-align: center;"><span style="font-weight: bold; font-size: 15pt;">Control de producción semanal resumido</span></td>
<td width="33%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th>
<th style="background:Turquoise;">DÍA</th>
<th style="background:Turquoise;">SEMANA</th>';
<th style="background:Turquoise;">MP_PROCESADA</th>
<th style="background:NavajoWhite;">HORA REAL</th>
<th style="background:MediumSeaGreen;">VELOCIDAD PRODUCCIÓN</th>
<th style="background:burlywood;">CONCENTRADO DOSIFICACION M3</th>
<th style="background:burlywood;">% DOSIFICACION</th>
<th style="background:burlywood;">SALDO DE  CONCENTRADO ANTERIOR</th>
<th style="background:burlywood;">SALDO DE CONCENTRADO</th>
<th style="background:burlywood;">AGUA COLA PRODUCIDO</th>
<th style="background:burlywood;">SALDO AGUA COLA</th>
<td style="background:lightsalmon;">Sacos Producidos</td>
<td style="background:lightsalmon;">% HUMEDAD</td>
<td style="background:lightsalmon;">% PROTEINA</td>
<td style="background:lightsalmon;">% GRASA</td>
<td style="background:lightsalmon;">% CENIZA</td>
<td style="background:lightsalmon;">TOTAL</td>
<td style="background:lightsalmon;">TBVN</td>
<td style="background:Peru;">Sacos Humedos</td>
<td style="background:Peru;">Sacos Baja Calidad</td>
<td style="background:lightsalmon;"># DE LOTE</td>
<td style="background:lightsalmon;">CLASIFICACION</td>
<!--<td style="background:lightsalmon;">Calidad</td>-->
<td style="background:DarkSeaGreen;">ACEITE PRODUCIDO LITROS</td>
<td style="background:DarkSeaGreen;">ACEITE PRODUCIDO KILOS</td>
<td style="background:DarkSeaGreen;">ACEITE DESCONTADO BORRA</td>
<!--<td style="background:DarkSeaGreen;">Aceite 4</td>-->
<td style="background:lightsalmon;">Rendimiento diario</td>
<td style="background:lightsalmon;">Rendimiento Semanal</td>
<td style="background:LightSalmon;">Galones Bunker Caldero</td>
<td style="background:LightSalmon;">Galones Bunker Secado</td>
<td style="background:LightSalmon;">Galones Bunker Sumatoria</td>
<td style="background:LightSalmon;">GL/Tn Diario</td>
<td style="background:LightSalmon;">GL/Tn Semanal</td>
<td style="background-color:LightSalmon ;">%Humedad</td>
</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td><td><?php print $row->Dia; ?></td>
        <td></td> 
        <?php $c=0;
        $sql = "select "
                . "sum(TRtrabajo) as totaltr,"
                . " sum(MPprocesada) as totalmp, "
                ." sum(SacosProducidos) sp, "
                ." sum(SacosHumedos) sh, "
                ." sum(SacosBajaCalidad) sbc, "
                ." sum(GalonesBunker) gb "
                . "from periodo where Semana = $row->Semana and Anio= $row->Anio";
        
        $totales = Yii::app()->db->createCommand($sql)->queryRow();
        $sql = "select sum(mpprocesada) from periodo where Semana = $row->Semana and Anio=$anio";
                    $valorSemanal = Yii::app()->db->createCommand($sql)->queryScalar();?> 
        <td><?php print number_format($valorSemanal,3,$decimal,''); ?> </td>
        <td><?= number_format(  $totales['totaltr'], 2,$decimal,'') ;?></td>
        <?php
        $sqlvelocidadtotal = "SELECT IFNULL(round(sum( MPProcesada-piloto)/sum( TRTrabajo),2)  , 0) FROM periodo WHERE Anio = $row->Anio and semana = $row->Semana";
        $totalvelocidad = Yii::app()->db->createCommand($sqlvelocidadtotal)->queryScalar();
        ?>
        <td><?= number_format( $totalvelocidad, 2,$decimal,'') ;?></td>
         <td></td>
        <td></td>
        <td></td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td><?= number_format(  $totales['sp'], 0,$decimal,'') ;?></td>
            <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
        <td><?= number_format(  $totales['sh'], 0,$decimal,'') ;?></td>
        <td><?= number_format(  $totales['sbc'], 0,$decimal,'') ;?></td>
         <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <!--<td></td>-->
        <td><?= number_format($totales['gb'], 0,$decimal,'')?></td>
        <td></td>
        <td><?=''?></td>
        <!--<td></td>-->
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        <td><?php print number_format($row->MPProcesada,3,$decimal,''); ?></td>
        <?php  $c=0;?>
        <td><?php print number_format($row->TRTrabajo,2,$decimal,''); ?></td>
        <td><?php 
            $vp = $row->TRTrabajo<=0 ? 0 : ($row->MPProcesada-$row->piloto)/$row->TRTrabajo;
            /*$totalvelocidad += $vp;*/
        print number_format( $vp ,2,$decimal,''); 
        ?></td>
        <?php
        $conc = Yii::app()->db->createCommand("SELECT sum( concentrado )
FROM prensado
JOIN licor ON licor.codigofila = prensado.codigofila
WHERE fecha = '$row->Fecha'
GROUP BY PrensaID
LIMIT 1")->queryScalar()/1000;
                 $produccioncon = Yii::app()->db->createCommand("select sum(concentrado) from tratamiento where fecha='$row->Fecha'")->queryScalar();;
                if($row->MPProcesada<=0):
                    $dosificacion =0;
                    else:
                        $dosificacion = ($conc/$row->MPProcesada)*100;
                endif;
                $aguaprocesada = Yii::app()->db->createCommand("select sum(aguacola) from tratamiento where fecha='$row->Fecha'")->queryScalar();
                $saldoconcentrado = ($row->concentradoanterior+$produccioncon)-$conc;
                $saldoaguacola = $row->aguacolaproducida-$aguaprocesada;
        ?>
        
        <td><?php print number_format($conc,3,$decimal,'');?></td>
        <td><?php print number_format($dosificacion,3,$decimal,'');?></td>
        <td><?php print number_format($row->concentradoanterior,3,$decimal,'');?></td>
        <td><?php print number_format($saldoconcentrado,3,$decimal,'');?></td>
        <td><?php print number_format($row->aguacolaproducida,3,$decimal,'');?></td>
        <td><?php print number_format($saldoaguacola,3,$decimal,'');?></td>
        <td><?php print number_format($row->SacosProducidos,0,$decimal,'');?></td>
         <td><?php print number_format($row->porhumedad,2,$decimal,'');?></td>
        <td><?php print number_format($row->porproteina,2,$decimal,'');?></td>
        <td><?php print number_format($row->porgrasa,2,$decimal,'');?></td>
        <td><?php print number_format($row->porceniza,2,$decimal,'');?></td>
        <td><?php $porsuma = $row->porhumedad+$row->porproteina+$row->porgrasa+$row->porceniza; 
        print number_format($porsuma,2,$decimal,'');?></td>
        <td><?php print number_format($row->tbvn,2,$decimal,'');?></td>
        <td><?php print number_format($row->SacosHumedos,0,$decimal,''); ?></td>
        <td><?php print number_format($row->SacosBajaCalidad,0,$decimal,''); ?></td>
        <td><?php print $row->numlote;?></td>
        <td><?php print $row->ClasificacionID?$row->clasificacion->Nombre:'-';?></td>
        <td><?php print number_format($row->aceite1,0,$decimal,''); ?></td>
        <td><?php print number_format($row->aceite2,0,$decimal,''); ?></td>
        <td><?php print number_format($row->aceite3,0,$decimal,''); ?></td>
        <!--<td><?php// print number_format($row->aceite4,2,$decimal,''); ?></td>-->
        <td><?php print number_format($row->RendimientoDiario,2,$decimal,''); ?></td>
        <?php 
            $año = $row->Anio;
            $semana = $row->Semana;
        ?>
        <?php if($semana != $tsemana):?>
        <?php 
        $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
        $num = count($modelPeriodo);
        $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
        ?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->Valor,2,$decimal,''); ?></td>
        <?php
        $tsemana = $semana;
        endif ?>
        <td><?php print number_format($row->GalCaldero,2,$decimal,'');?></td>
        <td><?php print number_format($row->GalSecado,2,$decimal,'');?></td>
        <td><?php print number_format($row->GalonesBunker,2,$decimal,'');?></td>
        <td><?php print number_format($row->GalonesDiario,2,$decimal,'')?></td>
        <?php if($semana != $tsemana1):?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->GalonesSemana,2,$decimal,''); ?></td>
        <?php
        $tsemana1 = $semana;
        endif ?>
        <?php 
        
        $query = "select ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) from harina where fecha = '$row->Fecha' and harina.PorcentajeHumedad > 0";
        $r_harina = Yii::app()->db->createCommand($query)->queryScalar();
        ?>
        <td><?php print $r_harina == null ? '0.00' : $r_harina ;?></td>
        <?php endif;?>
    </tr>
<?php endforeach;?>
 </tbody>
 </table>
  


 </body>
 </html>
