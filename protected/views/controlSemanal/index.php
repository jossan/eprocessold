<script src="js/jquery-1.10.2.js"></script>
<?php $decimal = $this->decimal(); ?>
<?php $densidad = $this->densidadaceite(); ?>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="js/numeric-input-example.js"></script>
<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
<script src="js/gridviewScroll.min.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" href="css/jquery-ui.css">
<link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet">

<style type="text/css">
    th,td{
        font-size: 8.5px;
    }
     .hora{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
        outline: none;
    }
    .minutos{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
        outline: none;
    }
    tbody thead th{
        text-align: center;
    }
    tbody > tr > td{
        padding: 3px;
    }
    img:hover{
        opacity: 0.5;
    }
    .GridCellDiv{
        text-align: center;
    }
    input{
        text-transform: uppercase;
        border: 1.5px solid rgb(91, 157, 217);
    }
</style>

<script type="text/javascript">
        var DECIMAL = "<?= $decimal ?>";
        var DENSIDAD = "<?= $densidad ?>";
        var resolucion=((screen.width)*90)/100;
        $(document).ready(function(){
            /*gridviewScroll();*/
            $('.clearfix').remove()
            //'<input id="editable" keypress="alert(2345)">'
            $('#gridedi').editableTableWidget().numericInputExample();
            disenio_new();
        });
        var visiblemp = true;
        var visibletr =true;
        var visiblesp =true;
        function gridviewScroll() {
            
             gridView1 = $('#gridedi').gridviewScroll({
                width:resolucion,
                height:400,
                freezesize: 4,
                arrowsize: 30,
                varrowtopimg: "images/arrowvt.png",
                varrowbottomimg: "images/arrowvb.png",
                harrowleftimg: "images/arrowhl.png",
                harrowrightimg: "images/arrowhr.png",
                headerrowcount:2,
                railsize: 15,
                barsize: 8
            });
        }
        $(function($){
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(".hora").spinner( {min:0,max:23,step:1, });    
    $(".minutos").spinner( {min:0,max:59,step:1, });    
    $(".panel-default").attr("style","width:"+resolucion+"px;");
    });
</script>
<center>
    
<?php
$box = $this->beginWidget(
'booster.widgets.TbPanel',
array(
'title' => 'Control de producción semanal',
'headerIcon' => 'th-list',
'padContent' => false,
'headerHtmlOptions' => array('style'=>'text-align: left;'),
'headerButtons'=>array(   array(
     'class' => 'booster.widgets.TbButtonGroup',
     'context' => 'primary',

     'buttons' => array(
array('label' => 'Action', 'url' => '#'))
) ),
)
);
echo $this->tabladatos($lim_sem,$anio,-1);

$this->endWidget();
?>
</center>

<?php
$semanas= $this->getSemanas($mes,$anio);
$selectsemana = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="semanas" id="semanas" onchange="actualiza(false);" >';
$selectsemana .='<option value="-1" selected > Semana </option>';
foreach($semanas as $key =>$s):
        $selectsemana .='<option value="'.$s.'">Semana '.$s.' </option>';
endforeach;
$selectsemana.='</select>';

$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses" id="meses" onchange="actualiza(true);" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" onchange="actualiza(true);" >';
for($i = 2014 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
?>


<script type="text/javascript">
    /* comentado por Mpprocesada
     */
    load();
    $('.panel-heading .pull-right').html('<?php print $selectsemana.' '.$selectmes.' '.$selectanios; ?>');
    
    var checking = '<span id="ckecking" style="width:100px;margin-left:30px;">'+
    '<input type="checkbox" id="checkmp" style="margin-left:8px;" onclick="visible(\'.mp\',visiblemp);" /><label for="check1"> MP </label>'+
    '<input type="checkbox" id="checktr" style="margin-left:8px;" onclick="visible(\'.tr\',visibletr);" /><label for="check2"> HR </label>'+
    '<input type="checkbox" id="checksp" style="margin-left:8px;" onclick="visible(\'.sp\',visiblesp);" /><label for="check3"> SP </label></span>';
    $('.panel-heading').append(checking);
  function load(){  
      
    $(".hora").blur(function(){
        if(this.value>23){
            this.value=23;
        }  
        else if(this.value<0){
            this.value=0;
        }
        $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
    $(".minutos").blur(function(){
      if(this.value>59){
          this.value=59;
      }  
      else if(this.value<0){
          this.value=0;
      }
      $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
    $('.motivo').change(function(){
       $(this).parent().parent().css("background-color",'#CDF473'); 
    });
        $( ".fechafin" ).datepicker();
        $('table td').on('change', function(evt, newValue) {
            if( $(this).hasClass('texto')) {
                    $(this).html(newValue.toUpperCase());
                    return;
                }
          var campo = $(this);
          
          newValue = newValue.replace(DECIMAL,'.');
          $(this).parent().css( "background-color",'#CDF473');
          $('.RSemanal').css( "background-color",'#ffffff');
          var id =$(this).parent().attr('rel');
          var clase = $(this).attr('class');
          var t =0; var t2 = 0;
          var valor; var dos; var con;
          
          if(clase=='sp'){
              campo.html(formato_numero(newValue, 0, DECIMAL, ''));
              $(this).parent().find('.sp').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 0, DECIMAL, '');
              $('#SP'+id).html(t);
          }else if(clase=='mp'){
              campo.html(formato_numero(newValue, 3, DECIMAL, '')); 
              $(this).parent().find('.mp').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 3, DECIMAL, '');
              $('#MP'+id).html(t);
          }
          else if(clase=='gl'){
              newValue = Math.ceil(newValue);
              campo.html(formato_numero(newValue, 2, DECIMAL, '')); 
              valor = $(this).html();
              valor = valor.replace(DECIMAL,'.');
              t = Number(valor) / (Number($('#SP'+id).html())/20);
              t= isFinite(t) ? t : 0 ;
              $('#RDGL'+id).html(formato_numero(t, 2, DECIMAL, ''));
          }
          else if(clase=='sac'){
              campo.html(formato_numero(newValue, 0, DECIMAL, '')); 
          }
          else if(clase=='pp'){
              campo.html(formato_numero(newValue, 3, DECIMAL, '')); 
          }else if(clase=='ac'){
              campo.html(formato_numero(newValue, 0, DECIMAL, ''));
              
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  t=Number(valor)* DENSIDAD;
              t2 = t-((t*7)/100);
              t = formato_numero(t, 0, DECIMAL, '');
              t2 = formato_numero(t2, 0, DECIMAL, '');
              $('#aceite2'+id).html(t);
              $('#aceite3'+id).html(t2);
          }else if(clase=='glb'){
              campo.html(formato_numero(newValue, 2, DECIMAL, ''));
              $(this).parent().find('.glb').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 2, DECIMAL, '');
              $('#GL'+id).html(t);
          }else if(clase=='conant'){
              campo.html(formato_numero(newValue, 3, DECIMAL, ''));
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
              
              }else if(clase=='aguacpro'){
              campo.html(formato_numero(newValue, 3, DECIMAL, ''));
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  
              }else if(clase=='calpt'){
              campo.html(formato_numero(newValue, 2, DECIMAL, ''));
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  
              }
              else if(clase=='calpts'){
              campo.html(formato_numero(newValue, 2, DECIMAL, ''));
                  $(this).parent().find('.calpts').each(function(){ 
                  valor = $(this).html();
                  valor = valor.replace(DECIMAL,'.');
                  t+=Number(valor);
              });
              t = formato_numero(t, 2, DECIMAL, '');
              $('#porsuma'+id).html(t);
                  
              }
          rendimientoDiario(id);
        });
    }
    function disenio_new(){
        $('#gridediFreeze').attr('style','background-color:#FAFAFA');
        $('#gridediPanelItemContent').css('float','left');
        $('#gridediPanelHeaderContent').css('float','left');
    }
        function actualiza(mes){
            if(mes)
                $('#semanas').val(-1);
            var a = $('#anios').val();
            var m = $('#meses').val();
            var s = $('#semanas').val();
            var htmlBef = '<br/><img src="images/loader.gif"><br/>';
            $.ajax({
                type:"GET",
                url:"index.php?r=/controlSemanal/tabla&a="+a+"&m="+m+"&s="+s,
                 beforeSend: function( ){
                    $('#yw0').html(htmlBef);
                 },
                 success:function (resp){
                $('#yw0').html(resp);
                gridviewScroll();
                $('#gridedi').editableTableWidget().numericInputExample().find('td:first').focus();
                $(".hora").spinner( {min:0,max:23,step:1, });    
                $(".minutos").spinner( {min:0,max:59,step:1, });
                load();
                visible('.mp',visiblemp=true);
                visible('.tr',visibletr=true);
                visible('.sp',visiblesp=true);
                disenio_new();
            },
                error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
            });
            if(mes)
            $.getJSON('index.php?r=/controlSemanal/semanas&mes='+m+'&anio='+a,function(list){
                 $('#semanas').find('option').each(function(){ $(this).remove(); });
                 $('#semanas').append("<option value=-1> Ninguno </option>");
                 $.each(list, function(key, valor) {
                    $('#semanas').append("<option value='"+valor+"'>Semana "+valor+"</option>");
                });
            });
        }
        
function guardar(obj){
    var idper = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    var pnp= $('#idpnp'+idper).val();
    var pp = $('#pp'+idper).html() ? $('#pp'+idper).html() : '0';
    var h1= $('#inicioprodh'+idper).val() ? $('#inicioprodh'+idper).val() : '0';
    var m1 = $('#inicioprodm'+idper).val() ? $('#inicioprodm'+idper).val() : '0';
    var fechafin = $('#fechafin'+idper).val() ? $('#fechafin'+idper).val() : 'nulo';
    var inicioprod = h1+':'+m1;
    var h2= $('#finprodh'+idper).val() ? $('#finprodh'+idper).val() : '0';
    var m2 = $('#finprodm'+idper).val() ? $('#finprodm'+idper).val() : '0';
    var finprod = h2+':'+m2; 
    
    /**** Hora no programadas ****/
    var h3_1= $('#noprogoh'+idper).val() ? $('#noprogoh'+idper).val() : '0';
    var m3_1 = $('#noprogom'+idper).val() ? $('#noprogom'+idper).val() : '0';
    var noprogo = h3_1+':'+m3_1;    
    var h3_2= $('#noprogmh'+idper).val() ? $('#noprogmh'+idper).val() : '0';
    var m3_2 = $('#noprogmm'+idper).val() ? $('#noprogmm'+idper).val() : '0';
    var noprogm = h3_2+':'+m3_2;    
    var h3_3 = $('#noprogth'+idper).val() ? $('#noprogth'+idper).val() : '0';
    var m3_3 = $('#noprogtm'+idper).val() ? $('#noprogtm'+idper).val() : '0';
    var noprogt = h3_3+':'+m3_3;    
    /**** fin de horas no programadas ****/
    
    var h4= $('#progh'+idper).val() ? $('#progh'+idper).val() : '0';
    var m4 = $('#progm'+idper).val() ? $('#progm'+idper).val() : '0';
    var prog = h4+':'+m4;    
    var h5= $('#tercerh'+idper).val() ? $('#tercerh'+idper).val() : '0';
    var m5 = $('#tercerm'+idper).val() ? $('#tercerm'+idper).val() : '0';
    var tercer = h5+':'+m5;    
    var sachumed = $('#sachumed'+idper).html() ? $('#sachumed'+idper).html() : 'nulo';
    var sacbajacali = $('#sacbajacali'+idper).html() ? $('#sacbajacali'+idper).html() : 'nulo';
    var aceite1 = $('#aceite1'+idper).html() ? $('#aceite1'+idper).html() : 'nulo';
    var conant = $('#conant'+idper).html() ? $('#conant'+idper).html() : 'nulo';
    var aguacpro = $('#aguacpro'+idper).html() ? $('#aguacpro'+idper).html() : 'nulo';
    var gl = $('#GL'+idper).html() ? $('#GL'+idper).html(): 'nulo';
    var glc = $('#GLC'+idper).html() ? $('#GLC'+idper).html(): 'nulo';
    var gls = $('#GLS'+idper).html() ? $('#GLS'+idper).html(): 'nulo';
    
    var proteina = $('#proteina'+idper).html() ? $('#proteina'+idper).html() : 'nulo';
    var porhumedad = $('#porhumedad'+idper).html() ? $('#porhumedad'+idper).html() : 'nulo';
    var grasa = $('#grasa'+idper).html() ? $('#grasa'+idper).html() : 'nulo';
    var ceniza = $('#ceniza'+idper).html() ? $('#ceniza'+idper).html() : 'nulo';
    var tbvn = $('#tbvn'+idper).html() ? $('#tbvn'+idper).html() : 'nulo';
    var numlote = $('#numlote'+idper).html() ? $('#numlote'+idper).html() : 'nulo';
    var clasif = $('#hariClasif'+idper).val() ? $('#hariClasif'+idper).val() : 'nulo';
        
    var valores = Array();
    
    for(var i = 0; i<total;i++){
        valores.push($('#valorc'+i+idper).html() ? $('#valorc'+i+idper).html():'nulo');
    }
    $.ajax({
            url:'index.php?r=/controlSemanal/actualizartabla',
            type:'GET',
            dataType: "json",
            data:{
                'idper':idper,
                'total':total,
                'inicioprod':inicioprod,
                'fechafin':fechafin,
                'finprod':finprod,
                'noprogo':noprogo,
                'noprogm':noprogm,
                'noprogt':noprogt,
                'prog':prog,
                'grasa':grasa,
                'porhumedad':porhumedad,
                'proteina':proteina,
                'ceniza':ceniza,
                'tbvn':tbvn,
                'tercer':tercer,
                'sachumed':sachumed, 
                'sacbajacali':sacbajacali, 
                'aceite1':aceite1, 
                'aguacpro':aguacpro,
                'conant':conant,
                'valores':valores, 
                'numlote':numlote,
                'clasif':clasif,
                'gl':gl, 
                'glc':glc, 
                'gls':gls, 
                'pnp':pnp,
                'pp':pp 
            },
            success:function (resp){
                verificar(resp.status);
                var sem= $('#Semana'+idper).html();
                //resp.Mp = parseFloat(resp.Mp);
                $('#MP'+idper).html(resp.Mp);
                $('#TR'+idper).html(resp.Tr);
                $('#SP'+idper).html(resp.Sp);
                $('#CA'+idper).html(resp.Ca);
                $('#RD'+idper).html(resp.Rd);
                $('#aceite2'+idper).html(resp.Ac2);
                $('#aceite3'+idper).html(resp.Ac3);
                //$('#aceite4'+idper).html(resp.Ac4);
                $('#VP'+idper).html(resp.Vp);
                $('#RS'+sem).html(resp.Rs);
                $('#RDGL'+sem).html(resp.Gd);
                $('#GS'+sem).html(resp.Gs);
                $('#saldocola'+idper).html(resp.SAC);
                $('#salcon'+idper).html(resp.SALDOC);
                $(obj).parent().parent().css( "background-color",'');
                $('#MP'+idper).parent().css( "background-color",'');
                $('#fechafin'+idper).css( "background-color",'');
                $('#RDGL'+idper).html(resp.galond);
            },
            error:function(e){
                alert('Se produjo un error al intentar guardar los datos');
            }
        });
}

/* Funcion para verificar si actualiza corrrectamente (notificacion)*/
function verificar(resp){
    if(resp=='1')
        new PNotify({
        title: 'Actualizado',
        type: 'success'
        });
    else if(resp=='0')
        new PNotify({
        title: 'Sin cambios en actualización',
        });
    else if(resp=='2'){
        new PNotify({
        title: 'Sin permiso al guardar',
        type: 'error'
        });
    }
}
function visible(opc,validar){
    if (validar){
        $(opc).hide();
    }else{
        $(opc).show();
    }
    gridviewScroll();
    switch(opc){
        case '.mp':
            visiblemp = !visiblemp;
            document.getElementById("checkmp").checked = visiblemp;
            break;
        case '.tr':
            visibletr = !visibletr;
            document.getElementById("checktr").checked = visibletr;
            break;
        case '.sp':
            visiblesp = !visiblesp;
            document.getElementById("checksp").checked = visiblesp;
            break;
    }
}
visible('.mp',visiblemp);
visible('.tr',visibletr);
visible('.sp',visiblesp);
$( ".fechafin" ).change(function(){
    $(this).parent().parent().css("background-color",'#CDF473');
    $(this).css("background-color",'#CDF473');
});

function rendimientoDiario(ident){
    var SP = $('#SP'+ident).html();
    var MP = $('#MP'+ident).html();
    var RD = $('#RD'+ident);
    SP = SP.replace(DECIMAL,'.');
    MP = MP.replace(DECIMAL,'.');
    var rd = (Number(SP)/Number(MP));
    rd = isFinite(rd) ? rd : 0;
    RD.html(formato_numero(rd,2,DECIMAL,''));
}
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
</script>
