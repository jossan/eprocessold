<?php $decimal =$this->decimal(); ?>
<link rel="stylesheet" href="archivos/jquery-ui.css">
<script src="archivos/jquery-ui.js"></script>

<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="archivos/rgbcolor.js"></script>
<script type="text/javascript" src="archivos/StackBlur.js"></script>
<script type="text/javascript" src="archivos/canvg.js"></script>
<!-- Hightchart Js -->
<script src="archivos/highcharts.js"></script>
<style type="text/css">
    #page{
        width:100% !important;
    }
 body {font-family: sans-serif;
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
    padding: 2px !important;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
   padding: 2px !important;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
<?php


$rt = "";

$recep = array(); 
$countmpro = 0;

$colores = ['LimeGreen','Red','CornflowerBlue','Yellow','DarkOrchid','LightSlateGray','Sienna','DarkOrange','Khaki','Blue'];
//$index = [4,3,2,1,0];
$index = Array();
for ($x = $totalgrupo-1 ; $x >= 0 ; $x--):
    $index[] =$x; 
endfor;
//$indexl = [0,1,2,3,4];
$indexl = array();
for ($x = 0 ; $x < $totalgrupo ; $x++):
    $indexl[] =$x; 
endfor;
$label = "    dataLabels: {
                    align: 'right',
                    enabled: true,
                          formatter: function () {
                          var t;
                          if(this.y==null){
                          t =null;
                           }else{
                           t =this.y+'%';
                           }
              return t;
            },
                    
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                    style: {
                        textShadow: '0 0 3px gray'
                    }
                                        ,
                    rotation: 270,
                    x: 2,
                    y: -10
                }";

$labelarray = array();
$labelarray[] =$label;
for ($x = 1 ; $x < $totalgrupo ; $x++):
    $labelarray[] =""; 
endfor;

$cont=0;
$contador = $totalSemana;
$j=0;
$ArrayTable = Array();
foreach($grupos as $grupo){
    $valor = Array();
    $sql ="select 
    Semana,
    Anio, 
    ROUND(IFNULL(
        (
        select IFNULL(sum(pesoneto),0) from recepcion join especie on especie.id = recepcion.EspecieID where yearweek(fechaproduccion,3) = concat(Anio, if(Semana<10, concat('0',Semana), Semana)) and GrupoID = $grupo->ID 
    ) / sum(periodo.mpprocesada), 0 )*100,1) as valor,
    (
        select IFNULL(sum(pesoneto),0) from recepcion join especie on especie.id = recepcion.EspecieID where yearweek(fechaproduccion,3) = concat(Anio, if(Semana<10, concat('0',Semana), Semana)) and GrupoID = $grupo->ID
    ) as total
    from periodo 
    where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin
    group by Anio, Semana
    order by Anio, Semana";
    $sqlFechaIni = "select min(Fecha) from periodo where concat(Anio, if(Semana<10, concat('0',Semana), Semana)) between $anioIni$semIni and $anioFin$semFin group by Anio , Semana order by Anio, Semana";
    $ArrayFechaIni = Yii::app()->db->createCommand($sqlFechaIni)->queryColumn();
    
    $result = Yii::app()->db->createCommand($sql)->queryAll();
    $ArrayTable[$grupo->ID] = $result;
    foreach ($result as $fila){
        if((float) $fila['valor']==0){
            $valor[]=null;    
        }else{
            $valor[]=(float) $fila['valor'];}
    }

    $rt = $rt."{
            name: '".$grupo->Nombre."',
            index:".$index[$j].",
            legendIndex:".$indexl[$j].",
            data: ".json_encode($valor).",
            color: '".$colores[$j]."',
            ".$labelarray[$j]."    
        },";
    $j++;
}
$t = count($semanas)+1;
       $p = (100/$t)."%";
?>
<div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;

?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/compMateriaPrimaProcesada',array('id'=>'btnact','class'=>'btn btn-info btn-sm'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimgCompmat',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank','style'=>'display:none;'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info btn-sm">Generar PDF</button>
    

</div>
<h3 style="text-align:center">Composición de la materia prima</h3>
<!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    <table id="tabla" class="items table table-bordered">
        <tr>
            <td style="background: #E1E7E1;" >Semana</td>
            <?php foreach($semanas as $sem):?>
            <td style="background: #E1E7E1;" ><?= $sem['Semana'] ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <td style="background: #E1E7E1;" >Año</td>
            <?php foreach($semanas as $sem):?>
            <td  ><?= $sem['Y'] ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <td style="background: #E1E7E1;" >Inicio</td>
            <?php foreach($ArrayFechaIni as $fechaini):
                ?>
            
            <td  ><?= substr($fechaini, 5) ?></td>
            <?php endforeach;?>
        </tr>
        
    <?php 
    foreach($grupos as $grupo):
        $fila = $ArrayTable[$grupo->ID]
        ?>
        <tr>
            <td style="background: #E1E7E1;" ><?= $grupo->Nombre ?> </td>
        <?php foreach ($fila as $campo):?>
            <td><?= $campo['total'] ?> </td>
        <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
        <tr><td colspan="<?= $totalSemana+1 ?>" style="text-align: center;background: #E1E7E1;">PORCENTAJES DE MATERIA PRIMA PROCESADA</td></tr>
        <?php 
    foreach($grupos as $grupo):
        $fila = $ArrayTable[$grupo->ID]
        ?>
        <tr>
            <td style="background: #E1E7E1;" ><?= $grupo->Nombre ?> </td>
        <?php foreach ($fila as $campo):?>
            <td><?= $campo['valor'] ?>%</td>
        <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
    </table> 


<script type="text/javascript">
    var DECIMAL = '<?= $decimal; ?>';
        $(document).ready(function(){
           
            document.getElementById('btnpdf').style.display = 'none';
        });
$(function () {
    $('#container').highcharts({

        chart: {
            type: 'column',
          
                    },

        title: {
             text: '',
              x: -20 //center
        },

        xAxis: {
               labels: {
                    formatter: function () {
                var semana=0;
                var cant = this.value;
                if((cant).length>5){
                    semana=(cant).substring(0,2);
                }else if((cant).length>0){
                    semana=(cant).substring(0,1);
                }else{
                    semana = "S";
                }
                return 'S'+semana;
            }
                
            },
            categories: <?php echo json_encode($arraySemana);?>
        },

        yAxis: [{ // Primary yAxis
        tickInterval:10,
        max:100,
        
                min:0,
            labels: {
                format: '{value}%'
              
            },
            legend:{
                
            },
            title: {
                text: 'Materia Prima'

            }
        }, { // Secondary yAxis
            title: {
                
                text: 'Rendimiento Semanal',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            tickInterval:0.20,
                min:0,
            labels: {
                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],

        tooltip: {
            shared: true,
            formatter: function () {
                var semana=0;
                var cant = this.x;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                var s = '<b>Semana ' + semana + '</b><br>';
<?php 
foreach ($semanas as $row) {
        $fech= date('d-m-Y', strtotime($row['Y'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));
print "if(".$row['Semana'].$row['Y']." ==  this.x){"
. "s+='<b>Fecha Inicio:</b>'+'".$fech.
"';}else{s+=''}";}?>
                $.each(this.points, function () {
                    
                    if(this.series.name==='Rendimiento'){
                    s += '<br/><b>' + this.series.name + ':</b> ' +
                        this.y ;}else{
                    s += '<br/><b>' + this.series.name + ':</b> ' +
                        this.y+'%';    
                        }
                });

                return s;
            },
            crosshairs: true
        },

        plotOptions: {
                column: {
                stacking: 'normal',
           
            }
        },

        series: [
    <?php echo $rt;?>
                        {
            type: 'line',
            name: 'Rendimiento',
            color:'DarkBlue',
            legendIndex:<?= $totalgrupo;?>,
            data: <?php echo json_encode($rendimiento);?>,
                    yAxis: 1,
                
    dataLabels: {
                   enabled: true,
                    borderRadius: 2,
                    backgroundColor: 'rgba(0, 44, 44, 0.9)',
                    borderWidth: 1,
                    borderColor: '#AAA',
                    y: 40,x:-6
                    ,inside :true,
                    
                     formatter: function () {
                      var   s =formato_numero(this.y,2,DECIMAL,'');
                return s;
                }
                }
    }
                         
            ]
        
    });
});
$("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/compMateriaPrimaProcesada';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        $(this).attr('href',enlace);
    } );
          function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
              var link = document.getElementById('btnpdf');
              $(link).attr('href','index.php?r=/controlSemanal/guardarimgCompmat&filename='+data+'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2);
            document.getElementById('btnpdf').click();
          }
        });
    }
    function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
</script>
