<fieldset>
<legend>Control de producción semanal por cliente</legend>
<?php echo CHtml::beginForm([ 'id'=>'form_informe']); ?>
<table style='width:100%'>
    <tr>
        <td style='width:15%'>
            <label>Fecha Inicio</label>
            <?php
            //echo CHtml::dropDownList('anioini', $anioini, array('2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020'),array('class'=>'form-control'))
            

            $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'inicio',
    'value'=>$f1,
    'options' => array(
    'language' => 'es',
     'format'=>'yyyy-mm-dd'
    )
    )
    );
?>
        </td>
        <!--
        <td style='width:15%'>
            <label>Semana Inicio</label>
            <?php
            //echo CHtml::dropDownList('semini', $semini, CHtml::listData(Periodo::model()->findAll("Anio = $anioini"),'Semana','Semana'),array('class'=>'form-control'))
            ?>
        </td>
        -->
        <td style="width: 5%;"></td>
        <td style='width:15%'>
            <label>Fecha Fin</label> 
     <?php
     //echo CHtml::dropDownList('aniofin', $aniofin, array('2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020'),array('class'=>'form-control'))
     
        $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fin',
    'value'=>$f2,
    'options' => array(
    'language' => 'es',
        'format'=>'yyyy-mm-dd'
    )
    )
    );?>
        </td>
        
       <!-- <td style='width:15%'>
            <label>Semana Fin</label>
            <?php
            //echo CHtml::dropDownList('semfin', $semfin, CHtml::listData(Periodo::model()->findAll("Anio=$aniofin"),'Semana','Semana'),array('class'=>'form-control'))
            ?>
        </td>-->
        <td>
            <br>
            <?php
            
             $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'info',
			'label'=>'Actualizar',
		));
            
            
            ?>
        </td>
        <td style="text-align: right;">
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/pdf_icon.png","Descargar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                 Yii::app()->createUrl('/controlSemanal/verpdfSemanalCliente', array('f1'=>$f1,'f2'=>$f2,'d'=>1))
        );?>&nbsp;&nbsp;
        <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel_ico.png","Descarga reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/generarExcelprodSemanalCliente', array('f1'=>$f1,'f2'=>$f2,))
        );?>
        </td>
    </tr>
</table>
<?php echo CHtml::endForm(); ?>
<iframe src="index.php?r=/controlSemanal/verpdfSemanalCliente<?= '&f1='.$f1.'&f2='.$f2; ?>" width="100%" height="550px;">
<!--<iframe src="index.php?r=/controlSemanal/verpdfSemanalCliente<?php// '&anioini='.$anioini.'&aniofin='.$aniofin.'&semini='.$semini.'&semfin='.$semfin?>" width="100%" height="550px;">-->
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>