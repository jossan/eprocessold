<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="25%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 15px;">Producción de sacos de control de producción semanal</span></td>
<td width="25%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->

 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th>
<th style="background:Turquoise;">DÍA</th>
<th style="background:Turquoise;">SEMANA</th>';
<?php foreach($clientes as $row):
    if($row->Exportacion): ?>
    <th style="background:CornflowerBlue;"><?php print $row->Nombre; ?></th>
    <?php else:?>
    <td style="background:DarkGray;"><?php print $row->Nombre ?></td>
<?php 
endif;
endforeach;?>
<td style="background:lightsalmon;">Sacos Producidos</td>
<td style="background:Peru;">Sacos Humedos</td>
<td style="background:Peru;">Sacos Baja Calidad</td>
<!--<td style="background:lightsalmon;">Calidad</td>-->
<td style="background:lightsalmon;">Rendimiento diario</td>
<td style="background:lightsalmon;">Rendimiento Semanal</td>
</tr>
</thead>
<tbody>
<?php  
$tsac=0;
$tsach=0;
$tsacbj=0;
foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td><td><?php print $row->Dia; ?></td>
        <td></td> 
        <?php $c=0;
        $sql = "select "
                . " sum(MPprocesada) as totalmp, "
                ." sum(SacosProducidos) sp, "
                ." sum(SacosHumedos) sh, "
                ." sum(SacosBajaCalidad) sbc, "
                ." sum(GalonesBunker) gb "
                . "from periodo where Semana = $row->Semana and Anio= $row->Anio";
        
        $totales = Yii::app()->db->createCommand($sql)->queryRow();
        $totalmp = $totales['totalmp'];
        ?> 
        <?php 
        $sql = "select sum(Valor) from periodo join ptventa on periodo.ID = ptventa.PeriodoID join clientes on clientes.ID = ptventa.clienteID where clientes.Estado=1 and periodo.Semana = $row->Semana and Anio=$row->Anio group by clienteID order by Exportacion Desc, Nombre";
        $CliTotal = Yii::app()->db->createCommand($sql)->queryColumn();
        for($i = 0;$i<($totalClientes);$i++):
            $V = number_format($CliTotal[$i],0,$decimal,'');?>
            <td ><?= $V ?></td>
        <?php 
        $contadorPTVenta++;
        endfor;
        ?>
        <td><?= number_format(  $totales['sp'], 0,$decimal,'') ;?></td>
        <td><?= number_format(  $totales['sh'], 0,$decimal,'') ;?></td>
        <td><?= number_format(  $totales['sbc'], 0,$decimal,'') ;?></td>
        <td></td>
        <!--<td></td>-->
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        <?php  $c=0;?>
        <?php for($i = 0;$i<($totalClientes);$i++):?>
        <td><?php print number_format($ptventa[$contadorPTVenta++]->Valor,0,$decimal,''); ?></td>
        <?php endfor; ?>
        <td><?php print number_format($row->SacosProducidos,0,$decimal,''); $tsac+=$row->SacosProducidos;?></td>
        <td><?php print number_format($row->SacosHumedos,0,$decimal,''); $tsach+=$row->SacosHumedos;?></td>
        <td><?php print number_format($row->SacosBajaCalidad,0,$decimal,''); $tsacbj+=$row->SacosBajaCalidad;?></td>
        <td><?php print number_format($row->RendimientoDiario,2,$decimal,''); ?></td>
        <?php 
            $año = $row->Anio;
            $semana = $row->Semana;
        ?>
        <?php if($semana != $tsemana):?>
        <?php 
        $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
        $num = count($modelPeriodo);
        $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
        ?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->Valor,2,$decimal,''); ?></td>
        <?php
        $tsemana = $semana;
        endif ?>
        <?php endif;?>
    </tr>
<?php endforeach;?>
       <tr style='background-color: #333;'>
      <td colspan="3" style='text-align: right;color: white;'>TOTALES:</td>
    <?php
    
    $sql = "select sum(Valor) from periodo join ptventa on periodo.ID = ptventa.PeriodoID join clientes on clientes.ID = ptventa.clienteID where clientes.Estado=1 and periodo.fecha between '$f1' and '$f2' group by clienteID order by Exportacion Desc, Nombre";
                    $CliTotal = Yii::app()->db->createCommand($sql)->queryColumn();
        for($i = 0;$i<($totalClientes);$i++):
            $V = number_format($CliTotal[$i],0,$decimal,'');?>
            
            <td colspan="1" style='text-align: right;color: white;'><?=$V?></td>
        <?php 
        $contadorPTVenta++;
        endfor;
        ?>
       
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($tsac,0,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($tsach,0,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($tsacbj,0,$decimal,'');?></td>
      <td colspan="2" ></td>
  </tr>
 </tbody>
 </table>
  


 </body>
 </html>
