<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;
$cft=0;$agpt=0;
    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>

 <table width="100%"><tr>
 
<td width="33%" style="text-align: center;"><span style="font-weight: bold; font-size: 15pt;">Concentrado</span></td>
<td width="33%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>

<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
<br>
 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th>
<th style="background:Turquoise;">DÍA</th>
<th style="background:Turquoise;">SEMANA</th>';

<th style="background:burlywood;">CONCENTRADO DOSIFICACION M3</th>
<th style="background:burlywood;">% DOSIFICACION</th>
<th style="background:burlywood;">SALDO DE  CONCENTRADO ANTERIOR</th>
<th style="background:burlywood;">SALDO DE CONCENTRADO</th>
<th style="background:burlywood;">AGUA COLA PRODUCIDO</th>
<th style="background:burlywood;">SALDO AGUA COLA</th>


</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td>
         <td><?php print $row->Dia; ?></td>
        <td></td> 
      
        
        <td></td>
        
        <td></td>
        <td></td> 
      
        
        <td></td>
        
        <td></td>
        <!--<td></td>-->
        
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
      <?php
        $conc = Yii::app()->db->createCommand("SELECT sum( concentrado )
FROM prensado
JOIN licor ON licor.codigofila = prensado.codigofila
WHERE fecha = '$row->Fecha'
GROUP BY PrensaID
LIMIT 1")->queryScalar()/1000;
               $produccioncon = Yii::app()->db->createCommand("select sum(concentrado) from tratamiento where fecha='$row->Fecha'")->queryScalar();;
                if($row->MPProcesada<=0):
                    $dosificacion =0;
                    else:
                        $dosificacion = ($conc/$row->MPProcesada)*100;
                endif;
                $aguaprocesada = Yii::app()->db->createCommand("select sum(aguacola) from tratamiento where fecha='$row->Fecha'")->queryScalar();
                $saldoconcentrado = ($row->concentradoanterior+$produccioncon)-$conc;
                $saldoaguacola = $row->aguacolaproducida-$aguaprocesada;
        ?>
        
        <td><?php print number_format($conc,3,$decimal,''); $cft+=$conc;?></td>
        <td><?php print number_format($dosificacion,3,$decimal,'');?></td>
        <td><?php print number_format($row->concentradoanterior,3,$decimal,'');?></td>
        <td><?php print number_format($saldoconcentrado,3,$decimal,'');?></td>
        <td><?php print number_format($row->aguacolaproducida,3,$decimal,''); $agpt+=$row->aguacolaproducida;?></td>
        <td><?php print number_format($saldoaguacola,3,$decimal,'');?></td>

        <?php endif;?>
    </tr>
<?php endforeach;?>
    <tr style='background-color: #333;'>
      <td colspan="3" style='text-align: right;color: white;'>TOTALES:</td>
  
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($cft,3,$decimal,'');?></td>
      <td colspan="3"></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($agpt,3,$decimal,'');?></td>
      
      <td colspan="1"></td>
  </tr>
 </tbody>
 </table>
  


 </body>
 </html>
