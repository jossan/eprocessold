
<form action="index.php" method="get">
    <input type="hidden" name="r" value="controlSemanal/produccionSemana">
<fieldset>
<legend>Control de producción semanal</legend>
<table style='width:100%'>
    <tr>
        <td style='width:20%'>
            Fecha Inicio<br>
            <?php

            $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fechaini',
    'value'=>$f1,
    'options' => array(
    'language' => 'es',
     'format'=>'yyyy-mm-dd'
    )
    )
    );
?>
        </td>
        <td style='width:20%'>

            Fecha Fin<br>
     <?php
        $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fechafin',
    'value'=>$f2,
    'options' => array(
    'language' => 'es',
        'format'=>'yyyy-mm-dd'
    )
    )
    );?>
        </td>
        <td>
            <br>
            <?php
            
            echo CHtml::button('Actualizar',array('type'=>'submit','class'=>'btn btn-info'));
            ?>
        </td><td style="text-align: right;">
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/pdf_icon.png","Descargar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/verpdfSemanal', array('f1'=>$f1,'f2'=>$f2,'d'=>1,'area'=>$area))
        );?>&nbsp;&nbsp;
        <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel_ico.png","Descarga reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/generarExcelprodSemanal', array('f1'=>$f1,'f2'=>$f2,'area'=>$area))
        );?>
        </td>
    </tr>
</table>
<div style="font-size:10px;text-transform: uppercase;">
    <label>Filtros:</label><br>
    <label><input type="radio" value="1" <?= $area=='1' ? 'checked': ''?> name="area">Detallado</label>
    <label><input type="radio" value="2" <?= $area=='2' ? 'checked': ''?> name="area">Resumido</label>
    <label><input type="radio" value="3" <?= $area=='3' ? 'checked': ''?> name="area">Materia Prima</label>
    <label><input type="radio" value="4" <?= $area=='4' ? 'checked': ''?> name="area">Tiempos de producción</label>
    <label><input type="radio" value="5" <?= $area=='5' ? 'checked': ''?> name="area">Producción de sacos</label>
    <label><input type="radio" value="6" <?= $area=='6' ? 'checked': ''?> name="area">Calidad del producto terminado</label>
    <label><input type="radio" value="7" <?= $area=='7' ? 'checked': ''?> name="area">Aceite</label>
    <label><input type="radio" value="8" <?= $area=='8' ? 'checked': ''?> name="area">Concentrado</label>
    <label><input type="radio" value="9" <?= $area=='9' ? 'checked': ''?> name="area">Galones Bunker</label>
    
    
</div>
<iframe src="index.php?r=/controlSemanal/verpdfSemanal<?php print '&f1='.$f1.'&f2='.$f2.'&area='.$area?>" width="100%" height="550px;">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
</form>
<script type="text/javascript">
   /* $("#btnact").click(function (){

        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var enlace=  'index.php?r=/controlSemanal/produccionSemana';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin;
        $(this).attr('href',enlace);
    } );*/
    
//    $("#btnexcel").click(function (){
//        
//        var ini =$('#inicio').val();
//        var fin =$('#fin').val();
//        var enlace=  'index.php?r=/controlSemanal/produccionSemana';
//        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin;
//        $(this).attr('href',enlace);
//    } );
</script>