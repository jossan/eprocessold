<?php $decimal =$this->decimal(); ?>
<link rel="stylesheet" href="archivos/jquery-ui.css">
<script src="archivos/jquery-ui.js"></script>

<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="archivos/rgbcolor.js"></script>
<script type="text/javascript" src="archivos/StackBlur.js"></script>
<script type="text/javascript" src="archivos/canvg.js"></script>
<!-- Hightchart Js -->
<script src="archivos/highcharts.js"></script>
<style type="text/css">
    #page{
        width:100% !important;
    }
 body {font-family: sans-serif;
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
    padding: 2px !important;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
   padding: 2px !important;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
    <?php
?>
 <div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print '<span id="mf">Mes Final</span> '.$selectmes2;
print '<span id="af">Año</span> '.$selectanios;

?>
     <?php $t = count($sql2)+1;
       $p = (100/$t)."%";
       
?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo 'Diario&nbsp;&nbsp;';
    echo CHtml::checkBox('diario',false,array('style'=>'margin-right:20px;')); 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/variacionHumedad',array('id'=>'btnact','class'=>'btn btn-info btn-sm'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimgVariacionhumedad',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank','style'=>'display:none;'));
    
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info btn-sm">Generar PDF</button>
    

</div>
<h3 style="text-align:center">Variación de la humedad (P.Terminado)</h3>
<!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br>
<center>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1;" colspan="<?php echo $t;?>">VARIACIÓN DE LA HUMEDAD</td></tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left" width="<?php echo $p; ?>"><?php ECHO $diario?'Fecha':'Semana'?></td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1;" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Año</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px; "width="<?php echo $p; ?>"><?php print $row['Y']?></td>
<?php endforeach; ?>
  </tr>
<tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Inicio</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px;" width="<?php echo $p; ?>"><?php 
        print date('d-m', strtotime($row['Y'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Análisis</td>
  <?php foreach($sql3 as $key=>$value):?>
    <td style="font-size:11px;" width="<?php echo $p;?>" ><?php print number_format($value,0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Sacos</td>
  <?php foreach($sql2 as $row):?>
    <td style="font-size:11px;" width="<?php echo $p;?>" ><?php print number_format($row['SumaSacos'],0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  
</table>
    
</center>
<script type="text/javascript">
    var DECIMAL = '<?= $decimal ?>';
    var VISTADIARIO = <?=$diario?>;
    var DIARIO = 0;
      $(document).ready(function(){
           
            document.getElementById('btnpdf').style.display = 'none';
        });
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: null
        },
        legend: {
            
            borderWidth: 1
        },
        
       xAxis: {
                         labels: {
                             formatter: function () {
                                 
                var semana=0;
                var cant = this.value;
                if((cant).length===10){s=cant;}else{
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                 
                 s=   'S'+semana;
             
                                  
                           
                       }
                       return s;
            }
                
            },
            categories: <?php echo json_encode($arraySemana);?>
            
        },
        yAxis: {
            min: 3,
            max:10,
            tickInterval:1,
            title: {
                text: '% Humedad'
            }
        },
                tooltip: {
            shared: true,
            formatter: function () {
                var semana=0;
                var cant = this.x;
                if((cant).length===10){var s = '<b>Fecha ' + cant + '</b><br>';}else{
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                var s = '<b>Semana ' + semana + '</b><br>';}
<?php 
foreach ($sql2 as $row) {
    $fech= date('d-m-Y', strtotime($row['Y'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));
    print "if(".$row['Semana'].$row['Y']." ==  this.x){"
    . "s+='<b>Fecha Inicio:</b>'+'".$fech.
    "';}else{s+=''}";}
    ?>

                $.each(this.points, function () {
                    s += '<br/><b>' + this.series.name + ':</b> ' +
                        this.y ;
                });

                return s;
            },
            crosshairs: true
        },
        plotOptions: {
            line: {
                
                
            }
        },
        series: [
        {
            name: 'Período Base',
            type:'line',
            data: <?php echo json_encode($base);?>,
            color: 'red',          marker: {
                enabled: false
            }
        },{
            name: 'Humedad',
            data: <?php echo json_encode($arrayPorcentaje);?>,
            dataLabels: {
                    enabled: true,
            formatter: function () {
                      var   s =formato_numero(this.y,2,DECIMAL,'');
                return s;
                }   
                },
                marker: {
                symbol: 'square'
            },
            color:'#07164C',
        },{
            name: 'Objetivo',
            data: <?php echo json_encode($arrayObjetivo);?>,
            marker: {
                symbol: 'diamond'
            },
            color: '#38862A'
            
        }]
    });
});

$("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        
        var enlace=  'index.php?r=/controlSemanal/variacionHumedad';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2+'&diario='+DIARIO;
        $(this).attr('href',enlace);
    } );
    
            function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
              var link = document.getElementById('btnpdf');
              $(link).attr('href','index.php?r=/controlSemanal/guardarimgVariacionhumedad&filename='+data+'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2+'&diario='+DIARIO);
            document.getElementById('btnpdf').click();
          }
        });
    }
       function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}

$("#diario").click( function(){
   if( $(this).is(':checked') ){
       DIARIO = 1;
    $("#meses2").hide();
  $("#anios").hide();
  $("#mf").hide();
  $("#af").hide();}
    else{
        DIARIO = 0;
       
   $("#meses2").show();
  $("#anios").show();
  $("#mf").show();
  $("#af").show();
        
    }
});

if(VISTADIARIO===1){
    DIARIO = 1;
  
   $("#meses2").hide();
  $("#anios").hide();
  $("#mf").hide();
  $("#af").hide();
       $("#diario").prop('checked', true);
}else{
 $("#meses2").show();
  $("#anios").show();
  $("#mf").show();
  $("#af").show();
       DIARIO = 0;
       $("#diario").prop('checked', false);
}
</script>