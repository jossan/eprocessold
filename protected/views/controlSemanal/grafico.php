<?php $decimal =$this->decimal(); ?>
<link rel="stylesheet" href="archivos/jquery-ui.css">
<script src="archivos/jquery-ui.js"></script>
<link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="archivos/rgbcolor.js"></script>
<script type="text/javascript" src="archivos/StackBlur.js"></script>
<script type="text/javascript" src="archivos/canvg.js"></script>
<!-- Hightchart Js -->
<script src="archivos/highcharts.js"></script>

<!-- GRIDVIEW -->
    <link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
    <script src="js/gridviewScroll.min.js" type="text/javascript"></script>

<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="archivos/highslide-full.min.js"></script>
<script type="text/javascript" src="archivos/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="archivos/highslide.css" />


<style type="text/css">
     #page{
        width:100% !important;
    }
        body {font-family: sans-serif;
 
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
    padding: 2px !important;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
   padding: 2px !important;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
<div style="margin-right: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;

?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/grafico',array('id'=>'btnact','class'=>'btn btn-info'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimg',array('id'=>'btnpdf','class'=>'btn btn-primary','style'=>'display:none','target'=>'_blank'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info">Generar PDF</button>
    
<?php $t = count($sql)+1;
       $p = (100/$t)."%";
?>
</div>

<h3 style="text-align:center">Rendimiento Semanal</h3>
<!-- Highchart container -->
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
 
<!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>

<br>
<center>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1" colspan="<?php echo $t;?>">CONTROL DE PRODUCCIÓN</td></tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left " width="<?php echo $p; ?>">Semana</td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Año</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px; "width="<?php echo $p; ?>"><?php print $row['anio']?></td>
<?php endforeach; ?>
  </tr>
<tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Inicio</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px;" width="<?php echo $p; ?>"><?php if($row['Semana']==1){
            print date('d-m', strtotime('first day of January '.$row['anio'] ));
        }else{
        print date('d-m', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));}?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>">Rendimiento</td>
  <?php foreach($sql as $row):?>
<td style="font-size:11px;"width="<?php echo $p;?>" ><?php print number_format($row['Valor'],2,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
    <tr style="background: #75E375">
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Objetivo</td>
  <?php foreach($sqlobjetivo as $row):?>
    <td style="font-size:11px;"width="<?php echo $p;?>" ><?php print number_format($row['Objetivo'],2,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Sacos</td>
  <?php foreach($sql4 as $row):?>
    <td style="font-size:11px;" width="<?php echo $p;?>" ><?php print number_format($row['SumaSacos'],0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >M. Prima</td>
  <?php foreach($sql3 as $row):?>
<td style="font-size:11px;"width="<?php echo $p;?>" ><?php print number_format($row['SumaProcesada'],3,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
</table>
</center>
<script type="text/javascript">
    var DECIMAL = '<?= $decimal ?>';
    var objetivo=<?php echo json_encode($arrayObjetivo);?>;
    var chart;
    $(function () {
    chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container'
    },
        title: {
            text: null
            
        },
         xAxis: {
             labels: {
                 formatter: function () {
                var semana=0;
                var cant = this.value;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                 
                 s=   'S'+semana;
             
                                  
                           return s;
            }
                
            },
            categories: <?php echo json_encode($arraySemana);?>
            
        },
        yAxis: {
            tickInterval:0.25,
    min: 5,
    
            title: {
                text: 'Sacos/Tn MP Procesada'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
       tooltip: {
            valueSuffix: '%'  ,shared: true,
            formatter: function () {
                var semana=0;
                var cant = this.x;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                var s = '<b>Semana ' + semana + '</b><br>';
<?php 
foreach ($sql2 as $row) {
    if($row['Semana']==1){
            $fech= date('d-m-Y', strtotime('first day of January '.$row['anio'] ));
        }else{
        $fech= date('d-m-Y', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));}

print "if(".$row['Semana'].$row['anio']." ==  this.x){"
. "s+='<b>Fecha Inicio:</b>'+'".$fech.
"';}else{s+=''}";}?>

                $.each(this.points, function () {
                    s += '<br/><span><b>' + this.series.name + ':</b> ' +
                        formato_numero(this.y,2,DECIMAL,''); + '</span>';
                });

                return s;
            },
            crosshairs: true
        },
        legend: {
            
            borderWidth: 1
        },
//                plotOptions: {
//                series: {
//                    cursor: 'pointer',
//                    point: {
//                        events: {
//                            click: function (e) {
//                                hs.htmlExpand(null, {
//                                    pageOrigin: {
//                                        x: e.pageX || e.clientX,
//                                        y: e.pageY || e.clientY
//                                    },
//                                    headingText: this.series.name,
//                                    maincontentText:  'Semana' + this.x+ ':<br/> ' +
//                                        ' Rendimiento :'+this.y,
//                                    width: 200
//                                });
//                            }
//                        }
//                    },
//                    marker: {
//                        lineWidth: 1
//                    }
//                }
//            },
        series: [
        {
            name: 'Período Base',
            data: <?php echo json_encode($arraybase);?>,color:'red',
                      marker: {
                enabled: false
            }
        },
        {
            name: 'Rendimiento',
            data: <?php echo json_encode($arrayValor);?>,color:'#003897'
        },{
            name: 'Objetivo',
            data: objetivo,color: '#00970A',          marker: {
                enabled: false
            }
        } ]
    });
});
        function gridviewScroll() {
            gridView1 = $('#tabla').gridviewScroll({
                width:1000,
                height:450,
//                 railcolor: "#F0F0F0",
//                  barcolor: "#CDCDCD",
//                 barhovercolor: "#606060",
//                 bgcolor: "#F0F0F0",
                freezesize: 1,
                arrowsize: 30,
                varrowtopimg: "images/arrowvt.png",
                varrowbottomimg: "images/arrowvb.png",
                harrowrightimg: "images/arrowhl.png",
                harrowrightimg: "images/arrowhr.png",
                headerrowcount: 1,
                railsize: 15,
                barsize: 8
            });
        }
        
            $("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/grafico';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        $(this).attr('href',enlace);
    } );
        
   
        function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        //var url= 'index.php?r=/controlSemanal/guardarimg&bin_data='+img+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        //window.location= url;
        //var data = "bin_data=" + img;
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
              var link = document.getElementById('btnpdf');
              $(link).attr('href','index.php?r=/controlSemanal/guardarimg&filename='+data+'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2);
            document.getElementById('btnpdf').click();
          },
        });
    }
    
    $('.obj').change(function(){
        var mat = new Array();
        $('.obj').each(function( index ) {
            mat.push(Number($(this).val()));
        });
        chart.series[0].setData(mat);
    });
    function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    //numero=parseFloat(numero);
    
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}


</script>
