<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>

 <table width="100%"><tr>
 <td width="25%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 15px;">Tiempos de produccion de control de producción semanal</span></td>
<td width="25%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>

<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
 <table class="items" widtd="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th>
<th style="background:Turquoise;">DÍA</th>
<th style="background:Turquoise;">SEMANA</th>';
<th style="background:Turquoise;">MP_PROCESADA</th>
<th style="background:NavajoWhite;">INICIO_PROD<br >Materia prima</th>
<th style="background:NavajoWhite;" >FECHA_FIN PRODUCCIÓN</th>
<th style="background:NavajoWhite;">FIN_PROD<BR>Materia prima</th>
<th style="background:NavajoWhite;">PARADA NO PROGRAMADA</th>
<th style="background:NavajoWhite;">PARADA PROGRAMADA</th>
<th style="background:NavajoWhite;">PARADA TERCERO</th>
<th style="background:NavajoWhite;">HORA REAL</th>
<th style="background:MediumSeaGreen;">VELOCIDAD PRODUCCIÓN</th>
<td style="background:lightsalmon;">Rendimiento diario</td>
<td style="background:lightsalmon;">Rendimiento Semanal</td>
</tr>
</thead>
<tbody>
<?php  
$trt = 3;
foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td><td><?php print $row->Dia; ?></td>
        <td></td> 
        <?php $c=0;
        
        $sql = "select "
                . "sum(TRtrabajo) as totaltr,"
                . " sum(MPprocesada) as totalmp, "
                . " SEC_TO_TIME( SUM( TIME_TO_SEC(ParadanoProgramada))) as pnpo, "
                . " SEC_TO_TIME( SUM( TIME_TO_SEC(ParadanoProgramadaMant))) as pnpm, "
                . " SEC_TO_TIME( SUM( TIME_TO_SEC(ParadanoProgramadaTerc))) as pnpt, "
                . " SEC_TO_TIME( SUM( TIME_TO_SEC(ParadaProgramada))) as pp, "
                . " SEC_TO_TIME( SUM( TIME_TO_SEC(ParadaTerceros))) as pt, "
                ." sum(SacosProducidos) sp, "
                ." sum(SacosHumedos) sh, "
                ." sum(SacosBajaCalidad) sbc, "
                ." sum(GalonesBunker) gb "
                . "from periodo where Semana = $row->Semana and Anio= $row->Anio";
        
        $totales = Yii::app()->db->createCommand($sql)->queryRow();
        $totalmp = $totales['totalmp'];
        ?>
        <td><?php print $totalmp; ?> </td>
        <td></td>
        <td></td>
        <td></td>
        <td><?= $this->sumarHoras($totales['pnpo'],$totales['pnpm'], $totales['pnpt'] ) ?></td>
        <td><?= substr($totales['pp'],0,5) ?></td>
        <td><?= substr($totales['pt'],0,5) ?></td>
        <td><?= number_format(  $totales['totaltr'], 2,$decimal,'') ;?></td>
        <?php
        $sqlvelocidadtotal = "SELECT IFNULL(round(sum( MPProcesada-piloto)/sum( TRTrabajo),2)  , 0) FROM periodo WHERE Anio = $row->Anio and semana = $row->Semana";
        $totalvelocidad = Yii::app()->db->createCommand($sqlvelocidadtotal)->queryScalar();
        ?>
        <td><?= number_format( $totalvelocidad, 2,$decimal,'') ;?></td>
        <!--<td></td>-->
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        <?php  $c=0;?>
        <td><?php print number_format($row->MPProcesada,3,$decimal,''); ?></td>
        <td><?php print substr($row->InicioProd,0,5); ?></td>
        <td><?php print $row->FechaFinProd; ?></td>
        <td><?php print substr($row->FinProd,0,5); ?></td>
        <td><?php print  $this->sumarHoras($row->ParadanoProgramada, $row->ParadanoProgramadaMant,$row->ParadanoProgramadaTerc) ?></td>
        <td><?php print substr($row->ParadaProgramada,0,5) ?></td>
        <td><?php print substr($row->ParadaTerceros,0,5) ?></td>
        <td><?php print number_format($row->TRTrabajo,2,$decimal,''); $trt+=$row->TRTrabajo;?></td>
        <td><?php 
            $vp = $row->TRTrabajo<=0 ? 0 : ($row->MPProcesada-$row->piloto)/$row->TRTrabajo;
            /*$totalvelocidad += $vp;*/
        print number_format( $vp ,2,$decimal,''); 
        ?></td>
        <td><?php print number_format($row->RendimientoDiario,2,$decimal,''); ?></td>
        <?php 
            $año = $row->Anio;
            $semana = $row->Semana;
        ?>
        <?php if($semana != $tsemana):?>
        <?php 
        $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
        $num = count($modelPeriodo);
        $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
        ?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->Valor,2,$decimal,''); ?></td>
        <?php
        $tsemana = $semana;
        endif ?>
        <?php endif;?>
    </tr>
<?php endforeach;?>
        <tr style='background-color: #333;'>
      <td colspan="10" style='text-align: right;color: white;'>TOTAL:</td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($trt,2,$decimal,'');?></td>
      <td colspan="3" ></td>
  </tr>
 </tbody>
 </table>
  


 </body>
 </html>
