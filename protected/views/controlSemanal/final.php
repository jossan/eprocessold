<?php $decimal =$this->decimal(); ?>
<link rel="stylesheet" href="archivos/jquery-ui.css">
<script src="archivos/jquery-ui.js"></script>

<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="archivos/rgbcolor.js"></script>
<script type="text/javascript" src="archivos/StackBlur.js"></script>
<script type="text/javascript" src="archivos/canvg.js"></script>
<!-- Hightchart Js -->
<script src="archivos/highcharts.js"></script>
     
<style type="text/css">
    #page{
        width:100% !important;
    }
 body {font-family: sans-serif;
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
    padding: 2px !important;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
   padding: 2px !important;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>

<div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2015 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print '<span id="mi">Mes Inicial</span> '.$selectmes;
print 'Año '.$selectanios1;
print '<span id="mf">Mes Final</span> '.$selectmes2;
print '<span id="af">Año</span> '.$selectanios;

?>
    <?php $t = count($sql4)+1;
       $p = (100/$t)."%";
       $d = count($sql2);
       if($d >28){
           $d = $d-28;
           $ancho=(CEIL($d/4)*20)+100;
       }else{
           $ancho=100;
       }
?>
    &nbsp;&nbsp;&nbsp;
    <?php 
     echo 'Mensual&nbsp;&nbsp;';
    echo CHtml::checkBox('mensual',false,array('style'=>'margin-right:20px;')); 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/final',array('id'=>'btnact','class'=>'btn btn-info btn-sm'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimgFinal',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank', 'style'=>'display:none;'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info btn-sm">Generar PDF</button>
    

</div>

<h3 style="text-align:center">Volumen de producción</h3>
 <!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>

<div id="container" style="min-width: 310px; width:<?php echo $ancho; ?>%; height: 100%; margin: 0 auto;padding: 0px;"></div>
<br>
<center>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1;" colspan="<?php echo $t;?>">VOLUMEN DE PRODUCCIÓN</td></tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left" width="<?php echo $p; ?>"><?php ECHO $mensual?'Mes':'Semana'?></td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1;" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Año</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:10px; "width="<?php echo $p; ?>"><?php print $row['anio']?></td>
<?php endforeach; ?>
  </tr>
<tr>
    <td style="background: #E1E7E1; text-align: left; " width="<?php echo $p; ?>">Inicio</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:10px;" width="<?php echo $p; ?>">
<?php print date('d-m', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT))); ?></td>
<?php endforeach; ?>
  </tr>
  
  <tr>
  <td style="background: #E1E7E1; text-align: left" width="<?php echo $p;?>" >Sacos</td>
  <?php foreach($sql4 as $row):?>
    <td style="font-size:10px;" width="<?php echo $p;?>" ><?php print number_format($row['SumaSacos'],0,$decimal,'');?></td>
  <?php endforeach; ?>
  </tr>
  
  
</table>
</center>
<script type="text/javascript">
    var DECIMAL = '<?= $decimal; ?>';
    var VISTAMENSUAL = <?=$mensual?>;
    var MENSUAL = 0;
          $(document).ready(function(){
           
            document.getElementById('btnpdf').style.display = 'none';
        });
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column',alignTicks: false
            
        },
        title: {
            text: null
        },
        
         xAxis: {
             minTickInterval:1,
                labels: {
                  
                formatter: function () {
                    if(this.value>0 || this.value=== -1){
                   if(this.value=== -1){
                 s = 'Acumulado';
             }else{
                 var semana=0;
                var cant = this.value;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                 
                 s=   ''+semana;
             }}else{
             s=this.value;
             }
                                  
                           return s;
            }
            
            },
               
            categories: <?php echo json_encode($arraySemana);?>,
            crosshairs: true
        },
        
        yAxis: [{ // Primary yAxis
        
        tickAmount: 9,
        min:0,
        /*Intervalo para dimension del total de valores
         * 
         * ,tickInterval:20
         * 
         * */
            labels: {
                
                formatter: function () {
                 s=   formato_numero(this.value,3,DECIMAL,'');
                           return s;
            }
            
            },
            
            title: {
                text: 'Toneladas'
            }
        }, { // Secondary yAxis
            gridLineColor: null,
            
            tickAmount: 12,
            title: {
                
                text: 'Toneladas',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            min:0,tickInterval:5000,
            labels: {
                formatter: function () {
                 s=   formato_numero(this.value,3,DECIMAL,'');
                 
                           return s;
            }
            
            },
            opposite: true
        }],
        tooltip: {
            valueSuffix: '%'  ,shared: true,
            formatter: function () {
              if(this.value>0 || this.value=== -1){
                
                if(this.x===-1){
                    var s = '<b>Acumulado</b><br>';
                }else{
                      var semana=0;
                var cant = this.x;
                if((cant).length>5){
                semana=(cant).substring(0,2);}else{
                semana=(cant).substring(0,1);
                }
                var s = '<b>Semana ' + semana +'</b><br>';}}else{
            var s = this.x;
                }
       
<?php 

foreach ($sql2 as $row) {
        $fech = date('d-m-Y', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));
        $rendim = Yii::app()->db->createcommand('SELECT Valor FROM rendimientosemanal where Anio = '.$row['anio'].' and Semana = '.$row['Semana'])->queryScalar();
print "if(".$row['Semana'].$row['anio']." ==  this.x){"
. "s='<b>Fecha Inicio:</b>'+'".$fech."';".
        "var mpproc=0;var mpprod = 0;".
         "$.each(this.points, function () {if(this.series.name==='MP Procesada(Tn)'){mpproc = this.y ;s+=this.y;}if(this.series.name==='Tn Producidas'){mpprod = this.y ;s+='<br><b>total: </b>'+this.y;}});".
         "s+='<br><b>Ratio:</b>'+(mpproc/mpprod).toFixed(2);".
        "s+='<br><b>Redimiento: </b>'+".round($rendim,2).';'.
"}else{s+=''}";}

        
            ?>
               
                $.each(this.points, function () {
                    if(this.series.name==='MP Acumulada'&& this.y>0){
                     s += '<br/><b>MP Acumulada:</b> ' +
                        formato_numero(this.y,3,DECIMAL,'');   
                    }else if(this.series.name==='Tn Producidas Acumuladas'&& this.y>0){
                        s += '<br/><b>Tn Producidas Acumuladas:</b> ' +
                        formato_numero(this.y,3,DECIMAL,'');
                    }else{
                        if(this.series.name==='MP Acumulada' || this.series.name==='Tn Producidas Acumuladas'){}else{
                    s += '<br/><b>' + this.series.name + ':</b> ' +
                        formato_numero(this.y,3,DECIMAL,'');}}
                });

                return s;
            },
            crosshairs: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                
                dataLabels: {
                    formatter: function () {
                        if(this.y===0){
                            s=null;
                        }else{
                 s=   formato_numero(this.y,3,DECIMAL,'');}
                           return s;
            },
               
                    align: 'left',
                    enabled: true,
                    rotation: 270,
                    x: 2,
                    y: -10,
                     style: {
                        fontWeight: 'normal',fontSize:'9.5px'
                
                    }
                    }
            }
             
        },
        series: [{
            name: 'MP Procesada(Tn)',
            data: <?php echo  json_encode($arraympprocesada);?>,
            color: '#8ADCF1'

        }, {
            name: 'Tn Producidas',
            data: <?php echo json_encode($arraytnproducida);?>,
            color: '#F8B770'

        }, {
            name: 'MP Acumulada',
            data: <?php echo json_encode($acumulada);?>,
            borderWidth:1.5,
            borderColor:'#139701',
            color: '#139701',yAxis: 1

        }, {
            name: 'Tn Producidas Acumuladas',
            
            data: <?php echo json_encode($acumulada2);?>,
            borderWidth:1.5,
            borderColor:'#E58620',
            color: '#E58620',yAxis: 1

        }]
    });
});
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    //numero=parseFloat(numero);
    
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}



$("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/final';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2+'&mensual='+MENSUAL;
        $(this).attr('href',enlace);
    } );
    
       function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
              var link = document.getElementById('btnpdf');
              $(link).attr('href','index.php?r=/controlSemanal/guardarimgFinal&filename='+data+'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2+'&mensual='+MENSUAL);
            document.getElementById('btnpdf').click();
          },
        });
    }
    $("#mensual").click( function(){
   if( $(this).is(':checked') ){
       MENSUAL = 1;
    $("#meses2").hide();
  $("#anios").hide();
  $("#mf").hide();
  $("#mi").hide();
  $("#meses1").hide();
  $("#af").hide();}
    else{
        MENSUAL = 0;
       
   $("#meses2").show();
  $("#anios").show();
  $("#mf").show();
  $("#meses1").show();
  $("#af").show();
        $("#mi").show();
    }
});

if(VISTAMENSUAL===1){
    MENSUAL = 1;
  
   $("#meses2").hide();
   $("#meses1").hide();
  $("#anios").hide();
  $("#mf").hide();
  $("#af").hide();
  $("#mi").hide();
       $("#mensual").prop('checked', true);
}else{
 $("#meses2").show();
  $("#anios").show();
  $("#mf").show();
  $("#af").show();
  $("#meses1").show();
  $("#mi").show();
       MENSUAL = 0;
       $("#mensual").prop('checked', false);
}
</script>