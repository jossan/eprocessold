<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
    $ga1=0;$ga2=0;$ga3=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>

 <table width="100%"><tr>

<td width="33%" style="text-align: center;"><span style="font-weight: bold; font-size: 15pt;">Producción de galones bunker de control de producción semanal</span></td>
<td width="33%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>

<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
<br>
 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th width= "12%" style="background:Turquoise;">FECHA</th>
<th width= "11%" style="background:Turquoise;">DÍA</th>
<th width= "11%" style="background:Turquoise;">SEMANA</th>';

<th width= "11%" style="background:LightSalmon;">Galones Bunker Caldero</td>
<th width= "11%" style="background:LightSalmon;">Galones Bunker Secado</td>
<th width= "11%" style="background:LightSalmon;">Galones Bunker Sumatoria</td>
<th width= "11%" style="background:LightSalmon;">GL/Tn Diario</td>
<th width= "11%" style="background:LightSalmon;">GL/Tn Semanal</td>

</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td>
         <td><?php print $row->Dia; ?></td>
        <td></td> 
      <td></td>
        
        <td></td>
        
        <td></td>
        
        <td></td>
        <!--<td></td>-->
        
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        
        <td><?php print number_format($row->GalCaldero,2,$decimal,'');$ga1+=$row->GalCaldero;?></td>
        <td><?php print number_format($row->GalSecado,2,$decimal,'');$ga2+=$row->GalSecado;?></td>
        <td><?php print number_format($row->GalonesBunker,2,$decimal,'');$ga3+=$row->GalonesBunker;?></td>
        <td><?php print number_format($row->GalonesDiario,2,$decimal,'')?></td>
         <?php 
            $año = $row->Anio;
            $semana = $row->Semana;
            $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
            $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
        $num = count($modelPeriodo);
        ?>
        <?php if($semana != $tsemana1):?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->GalonesSemana,2,$decimal,''); ?></td>
        <?php
        $tsemana1 = $semana;
        endif; ?>

        <?php endif;?>
    </tr>
<?php endforeach;?>
    <tr style='background-color: #333;'>
      <td colspan="3" style='text-align: right;color: white;'>TOTALES:</td>
  
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($ga1,2,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($ga2,2,$decimal,'');?></td>
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($ga3,2,$decimal,'');?></td>
      <td colspan="2"></td>
  </tr>
 </tbody>
 </table>
  


 </body>
 </html>
