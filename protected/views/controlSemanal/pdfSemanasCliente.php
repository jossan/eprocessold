<?php $decimal =$this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha between '$f1' and '$f2' ");
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
    
    $totalesClientes = Array();
    $totalesGenerales = Array();
    $totalesGenerales['Galones'] = 0;
    $totalesGenerales['sacosProducidos'] = 0;
    $totalesGenerales['SacosHumedos'] = 0 ;
    $totalesGenerales['SacosBajaCalidad']= 0;
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><img src="<?= $url_img?>" height="35px;" ></td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 15pt;">Control de producción semanal</span></td>
<td width="50%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
    <table class="items" widtd="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th><th style="background:Turquoise;">DÍA</th><th style="background:Turquoise;">SEMANA</th>';
<?php foreach($clientes as $row):
    if(isset($clientesPermitidos[$row->Id])):
        if($row->Exportacion): ?>
        <th style="background:CornflowerBlue;"><?php print $row->Nombre; ?></th>
        <?php else:?>
        <td style="background:DarkGray;"><?php print $row->Nombre ?></td>
        <?php endif;?>
    <?php endif;?>
<?php endforeach;?>
<td style="background:lightsalmon;">Sacos Producidos</td>
<!--<td style="background:lightsalmon;">Calidad</td>-->
<td style="background:lightsalmon;">Rendimiento diario</td>
<td style="background:lightsalmon;">Rendimiento Semanal</td>
<td style="background:LightSalmon;">Galones Bunker</td>
<td style="background:LightSalmon;">GL Diario</td>
<td style="background:LightSalmon;">GL Semanal</td>
</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td><td><?php print $row->Dia; ?></td>
        <td></td> 
        <?php $c=0;
        $sql = "select "
                . "sum(TRtrabajo) as totaltr,"
                . " sum(MPprocesada) as totalmp, "
                ." sum(SacosProducidos) sp, "
                ." sum(SacosHumedos) sh "
                . "from periodo where Semana = $row->Semana and Anio= $row->Anio";
        $totales = Yii::app()->db->createCommand($sql)->queryRow();
        $sql = "select sum(Valor) from periodo join ptventa on periodo.ID = ptventa.PeriodoID join clientes on clientes.ID = ptventa.clienteID where clientes.Estado=1 and periodo.Semana = $row->Semana and Anio=$row->Anio group by clienteID order by Exportacion Desc, Nombre";
        $CliTotal = Yii::app()->db->createCommand($sql)->queryColumn();
        $i = 0;
        foreach($clientes as $client):
            if(isset($clientesPermitidos[$client->Id])):?>
                <td ><?= number_format($CliTotal[$i++],0,$decimal,''); ?></td>
            <?php  else:
                $i++;
            endif;?>
        <?php 
        $contadorPTVenta++;
        endforeach;
        ?>
        <td><?= number_format(  $totales['sp'], 0,$decimal,'') ;?></td>
        <td></td>
        <td><?= number_format($totales['gb'], 0,$decimal,'')?></td>
        <td></td>
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        <?php  $c=0; ?>
        <?php for($i = 0;$i<($totalClientes);$i++):?>
            <?php if (isset($clientesPermitidos[$ptventa[$contadorPTVenta]->ClienteID])):?>
                <td><?= number_format($ptventa[$contadorPTVenta]->Valor,0,$decimal,''); ?>
                <?php $totalesClientes[$i]+=$ptventa[$contadorPTVenta++]->Valor;?>
                </td>
            <?php else: ?>
                <?php $contadorPTVenta++; ?>
            <?php endif; ?>
        <?php endfor; ?>
        <td><?php $totalesGenerales['sacosProducidos'] += $row->SacosProducidos ; print number_format($row->SacosProducidos,0,$decimal,'');?></td>
        <td><?php print number_format($row->RendimientoDiario,2,$decimal,''); ?></td>
        <?php 
        $año = $row->Anio;
        $semana = $row->Semana;
         ?>
        <?php if($semana != $tsemana):?>
        <?php 
        $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
        $num = count($modelPeriodo);
        
        $modeloRendimiento = RendimientoSemanal::model()->find("Anio=$año and Semana=$semana");
        ?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->Valor,2,$decimal,''); ?></td>
        <?php
        $tsemana = $semana;
        endif ?>
        <td><?php $totalesGenerales['Galones'] += $row->GalonesBunker; print number_format($row->GalonesBunker,0,$decimal,'');?></td>
        <td><?php print number_format($row->GalonesDiario,2,$decimal,'')?></td>
        <?php if($semana != $tsemana1):?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->GalonesSemana,2,$decimal,''); ?></td>
        <?php
        $tsemana1 = $semana;
        endif ?>
        <?php 
        
        $query = "select ROUND((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad)),2) from harina where fecha = '$row->Fecha' and harina.PorcentajeHumedad > 0";
        $r_harina = Yii::app()->db->createCommand($query)->queryScalar();
        ?>
        <?php endif;?>
    </tr>
<?php endforeach;?>
    <tr style="background-color: DimGray;color:white;">
        <td style="color:white;" colspan="3">TOTALES:</td>
        <?php 
        foreach($totalesClientes as $key => $val): ?>
            <td style="color:white;" ><?= $val; ?></td>
        <?php endforeach; ?>
        <td style="color:white;" ><?= number_format($totalesGenerales['sacosProducidos'],0,$decimal,''); ?></td>
        <td style="color:white;" ></td>
        <td style="color:white;" ></td>
        <td style="color:white;" ><?= $totalesGenerales['Galones']; ?></td>
        <td style="color:white;" ></td>
        <td style="color:white;" ></td>
    </tr>
 </tbody>
 </table>
  


 </body>
 </html>
