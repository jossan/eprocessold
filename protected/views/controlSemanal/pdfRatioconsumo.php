<html>
<head>
<?php $decimal =$this->decimal(); ?>
<style type="text/css">
        body {font-family: sans-serif;
 
 }
 td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
<?php $t = count($sql2)+1;
       $p = (100/$t)."%";
?>

</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="33%" style="color:#0000BB;"><img src="<?= $url_img?>" height="35px;" ></td>
<td width="33%" style="text-align: center;"><span style="font-weight: bold; font-size: 16pt;">Ratio de consumo : Bunker (GL / Tn)</span></td>
<td width="33%" style="text-align: right;"><b>Desde: </b><?php echo $fechaini; ?>, <b>Hasta: </b><?php echo $fechafin; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>-->


<img src="<?php echo $filename ?>" />
<br>
<table id="tabla" class="items table table-bordered table-striped" style="border-collapse: collapse;">
    <tr><td style="background: #E1E7E1;" colspan="<?php echo $t;?>">CONSUMO BUNKER</td></tr>
    <tr>
    <td style="background: #E1E7E1; text-align: left" width="<?php echo $p; ?>"><?php ECHO $diario?'Fecha':'Semana'?></td>
  <?php foreach($sql2 as $row):?>
<td style="background: #E1E7E1;" width="<?php echo $p; ?>"><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
    <tr>
    <td style="background:#E1E7E1; text-align: left " width="<?php echo $p; ?>">Año</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px; "width="<?php echo $p; ?>"><?php print $row['anio']?></td>
<?php endforeach; ?>
  </tr>
<tr>
    <td style="background:#E1E7E1; text-align: left " width="<?php echo $p; ?>">Inicio</td>
  <?php foreach($sql2 as $row):?>
<td style="font-size:11px;" width="<?php echo $p; ?>"><?php
        print date('d-m', strtotime($row['anio'] . 'W' . str_pad($row['Semana'] , 2, '0', STR_PAD_LEFT)));?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <td style="background: #E1E7E1; text-align :left ">Galones</td>
  <?php foreach($sqlgal as $row):?>
<td style="font-size:11px;"><?php print number_format($row['galones'],0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
 <tr>
  <td style="background:#E1E7E1; text-align: left ">Tn M Producidas</td>
  <?php foreach($sqlgal as $row):?>
<td style="font-size:11px;"><?php print number_format($row['tn'],0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  
  <tr>
  <td style="background:#E1E7E1; text-align: left " width="<?php echo $p;?>" >Sacos</td>
  <?php foreach($sqlgal as $row):?>
    <td style="font-size:11px;" width="<?php echo $p;?>" ><?php print number_format($row['sacos'],0,$decimal,'');?></td>
<?php endforeach; ?>
  </tr>
  
</table>

</body>
 </html>