<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
    $conmpproc = 0;


?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>

 <table width="100%"><tr>
 <td width="25%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 15px;">Materia prima de control de producción semanal</span></td>
<td width="25%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>

<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="4">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th>
<th style="background:Turquoise;">DÍA</th>
<th style="background:Turquoise;">SEMANA</th>';
<?php foreach($grupos as $grupo):?>
<th style="background:Turquoise;"><?php print $grupo->Nombre;?></th>
<?php endforeach; ?>

<th style="background:Turquoise;">MP_PROCESADA</th>
<td style="background:lightsalmon;">Rendimiento diario</td>
<td style="background:lightsalmon;">Rendimiento Semanal</td>
</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td><td><?php print $row->Dia; ?></td>
        <td></td> 
        <?php $c=0;
        $sql = "select "
                . "sum(TRtrabajo) as totaltr,"
                . " sum(MPprocesada) as totalmp, "
                ." sum(SacosProducidos) sp, "
                ." sum(SacosHumedos) sh, "
                ." sum(SacosBajaCalidad) sbc, "
                ." sum(GalonesBunker) gb "
                . "from periodo where Semana = $row->Semana and Anio= $row->Anio";
        
        $totales = Yii::app()->db->createCommand($sql)->queryRow();
            $c=0;

                    $sql = "select sum(mpprocesada) from periodo where Semana = $row->Semana and Anio=$anio";
                    $valorSemanal = Yii::app()->db->createCommand($sql)->queryScalar();
                    $sql = "select grupoID as grupo, IFNULL(sum(Pesoneto),0) as peso from recepcion join especie on especie.ID = EspecieID join periodo on periodo.fecha = recepcion.fechaproduccion where Semana = $row->Semana and Anio=$anio group by grupoID;";
                    $tt = Yii::app()->db->createCommand($sql)->queryAll();
                    $valgrupo = array();
                    foreach($tt as $t):
                        $valgrupo[$t['grupo']] = $t['peso'] ;
                    endforeach;
                    foreach($grupos as $grupo):
                        $totalespecie = isset($valgrupo[$grupo->ID]) ? $valgrupo[$grupo->ID] : 0;
                        $porcentaje = (($valorSemanal==0 ) ? 0 : ($totalespecie/$valorSemanal)*100);?>
       
       <td>  <?php print number_format($porcentaje,1,$decimal,'').'%' ?></td>
        <?php endforeach; ?> 
        <td><?php print number_format($valorSemanal,3,$decimal,''); ?> </td>
        <!--<td></td>-->
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
          <?php  $c=0;?>
        <?php
         $sql = "select grupoID as grupo, IFNULL(sum(Pesoneto),0) as peso from recepcion join especie on especie.ID = EspecieID where fechaproduccion = '$row->Fecha' group by grupoID;";
                    $tt = Yii::app()->db->createCommand($sql)->queryAll();
                    $valgrupo = array();
                    foreach($tt as $t):
                        $valgrupo[$t['grupo']] = $t['peso'] ;
                    endforeach;
                    foreach($grupos as $grupo):
                        $totalespecie = isset($valgrupo[$grupo->ID]) ? $valgrupo[$grupo->ID] : 0;?>
        
        <td><?php print number_format($totalespecie,3,$decimal,''); ?></td>
        <?php endforeach; ?>  
        <td><?php print number_format($row->MPProcesada,3,$decimal,''); $conmpproc+=$row->MPProcesada;?></td>
        <td><?php print number_format($row->RendimientoDiario,2,$decimal,''); ?></td>
        <?php 
            $año = $row->Anio;
            $semana = $row->Semana;
        ?>
        <?php if($semana != $tsemana):?>
        <?php 
        $modelPeriodo = Periodo::model()->findAll("Anio=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
        $num = count($modelPeriodo);
        $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
        ?>
        <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print number_format($modeloRendimiento->Valor,2,$decimal,''); ?></td>
        <?php
        $tsemana = $semana;
        endif ?>
        <?php endif;?>
    </tr>
<?php endforeach;?>
      <tr style='background-color: #333;'>
      <td colspan="3" style='text-align: right;color: white;'>TOTALES:</td>
    <?php
    $sql = "select grupoID as grupo, IFNULL(sum(Pesoneto),0) as peso from recepcion join especie on especie.ID = EspecieID join periodo on periodo.fecha = recepcion.fechaproduccion where fechaproduccion between '$f1' and '$f2' group by grupoID;";
                    $tt = Yii::app()->db->createCommand($sql)->queryAll();
                    $valgrupo = array();
                    foreach($tt as $t):
                        $valgrupo[$t['grupo']] = $t['peso'] ;
                    endforeach;
                    foreach($grupos as $grupo):
                        $totalespecie = isset($valgrupo[$grupo->ID]) ? $valgrupo[$grupo->ID] : 0;
                        $porcentaje = (($valorSemanal==0 ) ? 0 : ($totalespecie/$valorSemanal)*100);?>
       
       <td colspan="1" style='text-align: right;color: white;'><?php print number_format($totalespecie,3,$decimal,'');?></td>
        <?php endforeach; ?> 
      
      <td colspan="1" style='text-align: right;color: white;'><?php print number_format($conmpproc,3,$decimal,'');?></td>
      <td colspan="2" ></td>
  </tr>
 </tbody>
 </table>
  


 </body>
 </html>
