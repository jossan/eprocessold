<?php $decimal = $this->decimal(); ?>
<?php
/*CARGAR DATOS*/
    $totalvelocidad = 0;
    $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
    $grupos = Especiegrupo::model()->findAll('1=1 order by ID');
    $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc,Nombre');
    $cliventa=new CDbCriteria();
    $cliventa->select="*";
    $cliventa->alias="t1";
    $cliventa->with = array ('periodo','cliente');
    $cliventa->compare('cliente.Estado',1);
    $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
    $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
    $ptventa = PTVenta::model()->findAll($cliventa);
    $anio = strftime("%Y", strtotime($f1));
    //$sql = "select count(*) as total from Periodo where Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
    //$modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
    $totalClientes = count($clientes);
    $contadorPTVenta=0;
    $contador=count($periodo);
    $tsemana = -1;
    $tsemana1 = -1;

    $peri=0;$peri2=0;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 table thead td { 
    text-align: center;
    border: 0.06mm solid gray;
 }
 .items tr {
 border: 0.06mm solid gray;
 }
 .items td {
 text-align: right;
 border: 0.01mm solid gray;
 }
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="33%" style="color:#0000BB;"><img src="<?=$url_img ?>" height="30px;"></td>
<td width="33%" style="text-align: center;"><span style="font-weight: bold; font-size: 15pt;">Calidad del producto terminado</span></td>
<td width="33%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>-->
<br>
<br>
 <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;table-layout: fixed;" cellpadding="4">
     <thead>
     <tr>
<th width= "20%" style="background:Turquoise;">FECHA</th>
<th width= "10%" style="background:Turquoise;">DÍA</th>
<th width= "10%" style="background:Turquoise;">SEMANA</th>';
<th width= "10%" style="background:lightsalmon;">% HUMEDAD</th>
<th width= "10%" style="background:lightsalmon;">% PROTEINA</th>
<th width= "10%" style="background:lightsalmon;">% GRASA</th>
<th width= "10%" style="background:lightsalmon;">% CENIZA</th>
<th width= "10%" style="background:lightsalmon;">TOTAL</th>
<th width= "10%" style="background:lightsalmon;">TBVN</th>
<th width= "10%" style="background:lightsalmon;"># DE LOTE</th>
<th width= "10%" style="background:lightsalmon;">CLASIFICACION</th>

</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):
    if($row->Dia=='DO'):?>
        <tr style="background-color: #EEEEEE;">
         <td><?php print $row->Fecha; ?></td>
         <td><?php print $row->Dia; ?></td>
        <td></td> 
      <td></td> 
        <td></td>
        <td></td>
        <td></td> 
        <td></td> 
        <td></td> 
        <td></td> 
        <td></td> 
        <!--<td></td>-->
        
    <?php else:?>   
        <tr>
        <td><?php print $row->Fecha; ?></td>
        <td><?php print $row->Dia; ?></td>
        <td><?php print $row->Semana; ?></td>
        <td><?php print number_format($row->porhumedad,2,$decimal,'');?></td>
        <td><?php print number_format($row->porproteina,2,$decimal,'');?></td>
        <td><?php print number_format($row->porgrasa,2,$decimal,'');?></td>
        <td><?php print number_format($row->porceniza,2,$decimal,'');?></td>
        <td><?php $porsuma = $row->porhumedad+$row->porproteina+$row->porgrasa+$row->porceniza; 
        print number_format($porsuma,2,$decimal,'');?></td>
        <td><?php print number_format($row->tbvn,2,$decimal,'');?></td>
        <td><?php print $row->numlote;?></td>
        <td><?php print $row->ClasificacionID?$row->clasificacion->Nombre:'-';?></td>

        <?php endif;?>
    </tr>
<?php endforeach;?>
 </tbody>
 </table>
  


 </body>
 </html>

 