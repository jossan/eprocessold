<?php
$this->breadcrumbs=array(
	'Rotatubos',
);

$this->menu=array(
array('label'=>'Crear Rotatubo','url'=>array('create')),
array('label'=>'Administrar Rotatubo','url'=>array('admin')),
);
?>

<h2>Rotatubos</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
