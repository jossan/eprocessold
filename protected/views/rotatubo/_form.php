<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'rotatubo-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model,'Por favor, verifique los siguientes errores de ingreso:'); ?>

	<?php echo $form->textFieldGroup($model,'Nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textAreaGroup($model,'Descripcion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>500,'autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

        
        <?php echo $form->switchGroup($model, 'EstadoRotatubo',
        array('widgetOptions' => array('events'=>array('switchChange'=>'js:function(event, state) {}',
        ),'options' => array(
         'size'=>'small',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => '&nbsp;&nbsp;Activo&nbsp;&nbsp;',
        'offText' => '&nbsp;Inactivo&nbsp;',
        'handleWith' => 100,
),
            ))); ?>
<?php 
      //1
      $model->RotatuboMinAmperaje = (float)$model->RotatuboMinAmperaje;
      $model->RotatuboMaxAmperaje = (float)$model->RotatuboMaxAmperaje;
      $model->RotatuboAmperajeAlertaNaranja = (float)$model->RotatuboAmperajeAlertaNaranja;
      //2
      $model->RotatuboMinPresionEje = (float)$model->RotatuboMinPresionEje;
      $model->RotatuboMaxPresionEje = (float)$model->RotatuboMaxPresionEje;
      $model->RotatuboPresionEjeAlertaNaranja = (float)$model->RotatuboPresionEjeAlertaNaranja;
      //3
      $model->RotatuboMinSRC = (float)$model->RotatuboMinSRC;
      $model->RotatuboMaxSRC = (float)$model->RotatuboMaxSRC;
      $model->RotatuboSRCAlertaNaranja = (float)$model->RotatuboSRCAlertaNaranja;
      //4
      $model->RotatuboMinHumedad = (float)$model->RotatuboMinHumedad;
      $model->RotatuboMaxHumedad = (float)$model->RotatuboMaxHumedad;
      $model->RotatuboHumedadAlertaNaranja = (float)$model->RotatuboHumedadAlertaNaranja;    

      ?>

<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
        Parámetros
  </h4>
    </div>
    <div class="panel-body">    
            <div class="secadoAmp"><div style="margin-left:230px;">
         <?php echo $form->textFieldGroup($model,'RotatuboMinAmperaje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboMaxAmperaje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboAmperajeAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	
</div></div>
        <div class="secadoPresPsi"><div style="margin-left:230px;">
            <?php echo $form->textFieldGroup($model,'RotatuboMinPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboMaxPresionEje',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboPresionEjeAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
        <div class="secadoSrc"><div style="margin-left:230px;">
             <?php echo $form->textFieldGroup($model,'RotatuboMinSRC',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboMaxSRC',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboSRCAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div></div>
                <div class="secadoHumedad"><div style="margin-left:230px;">
         <?php echo $form->textFieldGroup($model,'RotatuboMinHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboMaxHumedad',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
	<?php echo $form->textFieldGroup($model,'RotatuboHumedadAlertaNaranja',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8,'onkeypress' => 'return justNumbers(event);','autocomplete'=>'off')))); ?>
       </div> </div>    
    </div>    
    </div>
	
	
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
   
    
    function justNumbers(e)
            {
                var tecla = (e.which) ? e.which : e.keyCode;
                if (tecla==8) return true;
                if (tecla==9) return true;
                if (tecla==13) return true;
                if (tecla==46) return true;
                if (tecla==37) return true;
                if (tecla==38) return true;
                if (tecla==39) return true;
                if (tecla==40) return true;
                var letra = String.fromCharCode(tecla);
                if($.isNumeric(letra)){
                    return true;
                }else{
                    return false;
                }
                
            }
    </script>