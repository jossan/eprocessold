<?php
$this->breadcrumbs=array(
	'Fuego directos'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Fuego directos','url'=>array('admin')),
	array('label'=>'Crear Fuego directo','url'=>array('create')),
	array('label'=>'Ver Fuego directo','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Fuego directo</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
