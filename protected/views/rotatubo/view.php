<?php
$jefeprod = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 7) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
$this->breadcrumbs=array(
	'Fuego directos'=>array('admin'),
	$model->Nombre,
);

$this->menu=array(
array('label'=>'Lista de Fuego directos','url'=>array('admin')),
array('label'=>'Crear Fuego directo','url'=>array('create'),'visible'=>$admin || $jefeprod),
array('label'=>'Actualizar Fuego directo','url'=>array('update','id'=>$model->ID),'visible'=>$admin || $jefeprod),
array('label'=>'Borrar Fuego directo','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?'),'visible'=>$admin || $jefeprod),

);
?>

<h3>Detalles de Fuego directo</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
//		'ID',
		'Nombre',
		'Descripcion',
    array('name'=>'EstadoRotatubo',
        'value'=>$model->EstadoRotatubo?"Activo":"Inactivo",
        ),
		
//		'Estado',
),
)); ?>
<?php 
      //1
      $model->RotatuboMinAmperaje = (float)$model->RotatuboMinAmperaje;
      $model->RotatuboMaxAmperaje = (float)$model->RotatuboMaxAmperaje;
      $model->RotatuboAmperajeAlertaNaranja = (float)$model->RotatuboAmperajeAlertaNaranja;
      //2
      $model->RotatuboMinPresionEje = (float)$model->RotatuboMinPresionEje;
      $model->RotatuboMaxPresionEje = (float)$model->RotatuboMaxPresionEje;
      $model->RotatuboPresionEjeAlertaNaranja = (float)$model->RotatuboPresionEjeAlertaNaranja;
      //3
      $model->RotatuboMinSRC = (float)$model->RotatuboMinSRC;
      $model->RotatuboMaxSRC = (float)$model->RotatuboMaxSRC;
      $model->RotatuboSRCAlertaNaranja = (float)$model->RotatuboSRCAlertaNaranja;
      //4
      $model->RotatuboMinHumedad = (float)$model->RotatuboMinHumedad;
      $model->RotatuboMaxHumedad = (float)$model->RotatuboMaxHumedad;
      $model->RotatuboHumedadAlertaNaranja = (float)$model->RotatuboHumedadAlertaNaranja;    

      ?>

<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
    
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="secadoAmp">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMinAmperaje, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMaxAmperaje, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboAmperajeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="secadoPresPsi">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->RotatuboMinPresionEje, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMaxPresionEje, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboPresionEjeAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        <div class="secadoSrc">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMinSRC, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMaxSRC, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboSRCAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
             
        </div>
                <div class="secadoHumedad">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMinHumedad, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboMaxHumedad, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->RotatuboHumedadAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
     </form>
    </div>
    
    </div>
