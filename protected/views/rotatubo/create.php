<?php
$this->breadcrumbs=array(
	'Fuego directos'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Fuego directo','url'=>array('admin')),

);
?>

<h3>Nuevo Fuego directo</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
