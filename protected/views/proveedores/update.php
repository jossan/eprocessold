<?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista de proveedores', 'url'=>array('admin')),
	array('label'=>'Crear proveedor', 'url'=>array('create')),
);
?>

<h3>Actualizar proveedor</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>