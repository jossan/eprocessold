<?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */
$this->breadcrumbs=array(

	'Proveedores',
);

$this->menu=array(
	array('label'=>'Crear proveedor', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#proveedores-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Proveedores</h1>

<!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'observacion-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		'Nombre',
		'Descripcion',
                'telefono',
                'email',
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{update} {delete}'
),
),
)); ?>


