<?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */

$this->menu=array(
	array('label'=>'Lista de proveedores', 'url'=>array('admin')),
);
?>

<h2>Nuevo proveedor</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>