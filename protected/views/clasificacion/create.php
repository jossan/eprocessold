<?php
$this->breadcrumbs=array(
	'Clasificación de Harinas'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Clasificación de Harinas','url'=>array('admin')),

);
?>

<h2>Crear Clasificación de Harina</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
