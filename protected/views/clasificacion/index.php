<?php
$this->breadcrumbs=array(
	'Clasificacions',
);

$this->menu=array(
array('label'=>'Crear Clasificación','url'=>array('create')),
array('label'=>'Administrar Clasificaciones','url'=>array('admin')),
);
?>

<h2>Clasificación</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
