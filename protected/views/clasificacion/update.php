<?php
$this->breadcrumbs=array(
	'Clasificación de Harinas'=>array('admin'),
	
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Clasificaciones','url'=>array('admin')),
	array('label'=>'Crear Clasificación','url'=>array('create')),
	
	
	);
	?>

	<h3>Actualizar Clasificación de Harina</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
