<?php
$this->breadcrumbs=array(
	'Clasificación de Harinas'=>array('index'),
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Clasificación de Harinas','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('clasificacion-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Clasificación de Harinas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'clasificacion-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
	//	'ID',
		'Nombre',
		'Descripcion',
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{update} {delete}'
),
),
)); ?>
